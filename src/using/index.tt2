[% title = "始めの一歩" %]

<h2>Debian とは何でしょうか？</h2>

	<p><strong>Debian</strong> (デビアン) は、あなたのコンピュータのための、自由に利用・変更・配布可能な、日本国内でも多彩な形で利用されているオペレーティングシステム (OS) です。</p>
<ul>
<li>個人用途のデスクトップとして
<li>教育機関や公共機関のデスクトップとして
<li>スマートフォンのOSの置き換えとして
<li>NASや組み込み機器ベンダーがそのベースシステムとして
<li>大手ネット系企業がそのサーバーとして
<li>セキュリティ系企業がシステム検証用途として
<li>etc...
</ul>
<p>
(実は気づいていないだけで、何らかの形であなたは Debian を利用しているかもしれません)</p>
<p>
Debian は、世界規模のコミュニティである Debian プロジェクトによって開発され、無償で提供されています。<br>
興味を持った方は<a href="http://cdimage.debian.or.jp./"><img src="../image/media-optical.png" alt="[   ]" width="22" height="22">
インストールイメージ、あるいは Live DVD イメージファイル(iso) をダウンロード</a>して試してみてください。</p>

<h3>Debian の特徴</h3>
	<ul>
	<li><strong>多くのソフトウェアが利用できます</strong><br>
          オフィスソフト、ブラウザ、プログラミングツールなど 
          <strong>[% stable_packages %]個以上もの多様なソフトウェア</strong>をすぐに利用可能です</li>
	<li><strong>ソフトの導入も手軽です</strong><br>
          定評のあるパッケージングシステムとユーティリティ (dpkg, apt, aptitude) により、<strong>ソフトの導入や更新が簡単</strong>です</li>
        <li><strong>パソコンはもちろん、様々なコンピュータで動作します</strong><br>
          Windows が動作する一般的なパソコンのみならず、ハイエンド／Unix 系サーバ・Mac・組込み・NAS・ゲーム機・汎用機等でも動作し、
          様々な利用形態を可能にします</li>     
	<li><strong>利用／配布／改変は「無償であり自由」です</strong><br>
          後に「<a href="http://www.opensource.jp/osd/osd-japanese.html">オープンソースの定義</a>」の基になった「<a href="[% wwworg %]/social_contract#guidelines">Debian フリーソフトウェアガイドライン(DFSG) </a>」を満たしたフリーソフトウェアで構成されており、<strong>利用・変更・配布に関して一貫したDebian は<a href="[% wwworg %]/intro/free">「自由」</a>を提供</strong>します</li>
	<li><strong>皆で作り上げるシステムです</strong><br>
          1,000 人以上のフリーソフトウェア開発者の集合である <a href="[% wwworg %]/">Debian プロジェクト</a> に加え、
          さらに 1,000 人を越える貢献者らによって開発されています（そして、これを読んでいる「あなた」もその貢献者の一人になることができるのです！）</li>
	</ul>

<p>
最新のリリースは
<strong>
Debian [% stable_version %]
[%- IF stable_pointrelease == "0" -%]
[%- ELSE -%]
.[% stable_pointrelease %]
[%- END -%]
(コードネーム「[% stable_codename %]」)</strong>
です。<br>
[%- IF stable_pointrelease == "0" -%]
 [% stable_initialdate %]
にリリースされました。

<strong>このリリースの詳細については
<a href="[% stable_release_url %]">
こちらのアナウンス</a>
をご覧ください</strong>。
[%- ELSE -%]
 [% stable_initialdate %]
にリリースされ
<a href="[% stable_pointrelease_url %]"> [% stable_pointrelease_date %]
に更新 (ポイントリリース) </a>が行われました。
[%- END -%]
</p>

<h3>その他詳細について</h3>
<ul>
<li>上記以外でも、Debian はいろいろなやり方で<a href="[% wwworg %]/releases/stable/installmanual">インストール</a>できます（<a href="http://cdimage.debian.or.jp">配布サイト</a>から入手できる CD や DVD イメージを使ったインストールが最も手軽です）。
<li>Debian は、用途・更新頻度・サポート体制の異なるいくつかの<a href="release.html">リリース</a>に分けられています。<strong>通常の用途では、安定版 (stable) を使うこと</strong>を強くお勧めします（現在のバージョンは
[% stable_version %]
[%- IF stable_pointrelease == "0" -%]
[%- ELSE -%]
.[% stable_pointrelease %]
[%- END -%]
(コードネーム「[% stable_codename %]」)
です）。
<li>
一旦インストールした後で、必要なソフトは専用ツール (apt/aptitude 等)を使って簡単にインストールできます (国内にある多数の<a href="mirror.html">ミラー</a>サイトの好きなところからダウンロードするように指定することも可能です）。
<li>
<a href="../community/ml/">メーリングリスト</a>や <a href="../community/irc.html">IRC</a> で質問したりあるいはほかの困っている人を助けたりできます。
もっと Debian について知るために、多数の<a href="book.html">書籍や雑誌記事</a>があります。<a href="../community/events/">イベント</a>は同好の士に実際に会うチャンスです。
<li>Debian に慣れてきたら、<a href="../community/bugreport.html">バグ報告</a>や<a href="../community/devel/">開発</a>・<a href="../community/translate/">ドキュメントの執筆や翻訳</a>に挑戦するのもよいでしょう。私たちの会への<a href="../project/join.html">参加</a>や<a href="../project/donations.html">寄付</a>も歓迎いたします。
</ul>
