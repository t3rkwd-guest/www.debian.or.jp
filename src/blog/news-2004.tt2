[% title = "2004年のニュース" %]

<H2>2004年のニュース</H2>

[% INCLUDE newsdesc %]

<UL class="linkmenu">
<LI><A href="#20041026">Debian GNU/Linux 3.0 updated (r3)</A>
	<SPAN class="lastmodified">(2004年10月26日)</SPAN></LI>
<LI><A href="#20040726">packages.debian.or.jp の停止のお知らせ</A>
	<SPAN class="lastmodified">(2004年7月26日)</SPAN></LI>
</UL>

<HR>
<H2><A name="20041026">2004年10月26日: Debian GNU/Linux 3.0 updated (r3)</A></H2>

<p>Debian GNU/Linux 3.0r3 は、Debian GNU/Linux 3.0 (コードネーム「woody」)
の 3 度目のアップデートです。主に、安定版 (stable) にセキュリティアッ
プデートを追加するものですが、深刻な問題もいくつか修正されています。
security.debian.org から頻繁にアップデートしているなら、それほど多くの
パッケージをアップデートする必要はないでしょう。また、
security.debian.org に登録されたアップデートはほとんど、今回のアップデー
トに含まれています。</p>

<p>今回のアップデートは、Debian GNU/Linux 3.0 の新バージョンではなく、単
に一部のパッケージをアップデートしたものです。バージョン 3.0 の CD を
捨てる必要はありませんが、インストール後、最近の変更を組み入れるために
ftp.debian.org に対してアップデートする必要があります。</p>

<p>オンラインでこのリビジョンへアップデートするには、通常、Debian の
FTP/HTTP ミラーのどれか一つを参照するように「apt」パッケージツールを設
定します。 (sources.list(5) のマニュアルページを参照してください) ミラー
の一覧は、

 <a href="http://www.debian.org/distrib/ftplist">http://www.debian.org/distrib/ftplist</a>

で参照できます。</p>

<H3>セキュリティ上の修正</H3>

<p>本リビジョンでは、安定版リリースに対して、以下のセキュリティアップデー
トが追加されています。セキュリティチームはすでに、以下の個々のアップデー
トに対してセキュリティ勧告を発表済みです。</p>

<PRE>
Debian セキュリティ勧告番号                       パッケージ
       
       DSA 209                wget
       DSA 210                lynx
       DSA 212                mysql
       DSA 223                geneweb
       DSA 228                libmcrypt
       DSA 234                kdeadmin
       DSA 238                kdepim
       DSA 240                kdegames
       DSA 241                kdeutils
       DSA 243                kdemultimedia
       DSA 244                noffle
       DSA 247                courier-ssl
       DSA 264                lxr
       DSA 265                bonsai
       DSA 267                lpr
       DSA 269                heimdal
       DSA 273                krb4
       DSA 275                lpr-ppd
       DSA 289                rinetd
       DSA 291                ircii
       DSA 294                gkrellm-newsticker
       DSA 301                libgtop
       DSA 303                mysql
       DSA 316                nethack
       DSA 329                osh
       DSA 336                kernel-source-2.2.20
       DSA 337                gtksee
       DSA 338                proftpd
       DSA 346                phpsysinfo
       DSA 354                xconq
       DSA 359                atari800
       DSA 365                phpgroupware
       DSA 368                xpcd
       DSA 381                mysql
       DSA 385                hztty
       DSA 393                openssl
       DSA 398                conquest
       DSA 404                rsync
       DSA 405                xsok
       DSA 406                lftp
       DSA 407                ethereal
       DSA 408                screen
       DSA 409                bind
       DSA 410                libnids
       DSA 411                mpg321
       DSA 412                nd
       DSA 415                zebra
       DSA 416                fsp
       DSA 417                kernel-patch-2.4.18-powerpc
       DSA 418                vbox3
       DSA 420                jitterbug
       DSA 421                mod-auth-shadow
       DSA 422                cvs
       DSA 423                kernel-image-2.4.17-ia64
       DSA 424                mc
       DSA 425                tcpdump
       DSA 427                kernel-source-2.4.17
       DSA 429                gnupg
       DSA 430                trr19
       DSA 431                perl
       DSA 432                crawl
       DSA 433                kernel-patch-2.4.17-mips
       DSA 434                gaim
       DSA 435                mpg123
       DSA 436                mailman
       DSA 437                cgiemail
       DSA 438                kernel-source-2.4.18, kernel-patch-2.4.18-powerpc
       DSA 439                kernel-source-2.4.16, kernel-patch-2.4.16-arm,
                              kernel-image-2.4.16-arm
       DSA 440                kernel-source-2.4.17, kernel-patch-2.4.17-apus
       DSA 441                kernel-source-2.4.17, kernel-patch-2.4.17-mips
       DSA 442                kernel-source-2.4.17, kernel-patch-2.4.17-s390,
                              kernel-image-2.4.17-s390
       DSA 443                xfree86
       DSA 444                kernel-image-2.4.17-ia64
       DSA 445                lbreakout2
       DSA 446                synaesthesia
       DSA 449                metamail
       DSA 450                kernel-patch-2.4.19-mips
       DSA 450                kernel-source-2.4.19
       DSA 451                xboing
       DSA 452                libapache-mod-python
       DSA 453                kernel-image-2.2.20-amiga, kernel-image-2.2.20-atari,
                              kernel-image-2.2.20-mac
       DSA 453                kernel-image-2.2.20-bvme6000, kernel-image-2.2.20-mvme147,
                              kernel-image-2.2.20-mvme16x
       DSA 453                kernel-image-2.2.20-i386, kernel-patch-2.2.20-powerpc
       DSA 453                kernel-patch-2.2.20-powerpc
       DSA 453                kernel-source-2.2.19
       DSA 454                kernel-source-2.2.22, kernel-image-2.2.22-alpha
       DSA 455                libxml
       DSA 455                libxml2
       DSA 456                kernel-image-2.2.19-netwinder, kernel-image-2.2.19-riscpc,
                              kernel-patch-2.2.19-arm
       DSA 457                wu-ftpd
       DSA 458                python2.2
       DSA 459                kdelibs
       DSA 460                sysstat
       DSA 461                calife
       DSA 462                xitalk
       DSA 463                samba
       DSA 464                gdk-pixbuf
       DSA 465                openssl
       DSA 466                kernel-source-2.2.10
       DSA 467                ecartis
       DSA 468                emil
       DSA 470                kernel-image-2.4.17-hppa
       DSA 471                interchange
       DSA 474                squid
       DSA 475                kernel-image-2.4.18-hppa
       DSA 477                xine-ui
       DSA 478                tcpdump
       DSA 479                kernel-source-2.4.18
       DSA 480                kernel-image-2.4.17-hppa, kernel-image-2.4.18-hppa
       DSA 481                kernel-image-2.4.17-ia64
       DSA 482                kernel-source-2.4.17, kernel-image-2.4.17-s390
       DSA 482                kernel-source-2.4.17, kernel-patch-2.4.17-apus
       DSA 483                mysql
       DSA 484                xonix
       DSA 485                ssmtp
       DSA 486                cvs
       DSA 487                neon
       DSA 488                logcheck
       DSA 489                kernel-source-2.4.17, kernel-patch-2.4.17-mips
       DSA 490                zope
       DSA 491                kernel-source-2.4.19, kernel-patch-2.4.19-mips
       DSA 492                iproute
       DSA 493                xchat
       DSA 494                ident2
       DSA 495                kernel-source-2.4.16, kernel-patch-2.4.16-arm,
                              kernel-image-2.4.16-arm
       DSA 496                eterm
       DSA 497                mc
       DSA 499                rsync
       DSA 500                flim
       DSA 501                exim
       DSA 503                mah-jong
       DSA 505                cvs
       DSA 506                neon
       DSA 509                gatos
       DSA 510                jftpgw
       DSA 511                ethereal
       DSA 512                gallery
       DSA 513                log2mail
       DSA 514                kernel-image-sparc-2.2
       DSA 516                postgresql
       DSA 517                cvs
       DSA 518                kdelibs
       DSA 519                cvs
       DSA 520                krb5
       DSA 521                sup
       DSA 521                super
       DSA 525                apache
       DSA 526                webmin
       DSA 528                ethereal
       DSA 529                netkit-telnet-ssl
       DSA 531                php4
       DSA 533                courier
       DSA 534                mailreader
       DSA 535                squirrelmail
       DSA 536                libpng, libpng3
       DSA 537                ruby
       DSA 539                kdelibs
       DSA 539                rsync
       DSA 540                mysql
       DSA 541                icecast-server
       DSA 542                qt-copy
       DSA 543                krb5
       DSA 544                webmin
       DSA 545                cupsys
       DSA 546                gdk-pixbuf
       DSA 548                imlib
       DSA 549                gtk+2.0
       DSA 550                wv
       DSA 551                lukemftpd
       DSA 552                imlib2
       DSA 553                getmail
       DSA 554                sendmail
       DSA 555                freenet6
       DSA 556                netkit-telnet
       DSA 557                rp-pppoe
       DSA 558                libapache-mod-dav
       DSA 559                net-acct
       DSA 560                lesstif1-1
       DSA 561                xfree86
       DSA 562                mysql
       DSA 563                cyrus-sasl
       DSA 564                mpg123
       DSA 565                sox
       DSA 566                cupsys
       DSA 568                cyrus-sasl-mit
       DSA 569                netkit-telnet-ssl
       DSA 570                libpng
       DSA 571                libpng3
       DSA 572                ecartis
       DSA 573                cupsys
</PRE>

<H3>その他のバグ修正</H3>

<p>本リビジョンでは、以下のパッケージに対して重要な修正が追加されています。
その多くはシステムのセキュリティには関係しませんが、データが損なわれる
可能性があります。</p>

<PRE>
  パッケージ名                     理由

aptitude         woody から sarge へのスムーズな移行のため
aspell           破損の修正
bind9            S/390 でパーサを修正
cfs              S/390 で利用できるよう再構築
debootstrap      インストール時に依存性が不足
imagemagick      依存性の修正
ipmasq           iptables のパスを修正
iptables         初期化スクリプトの修正
junior-puzzle    rocks-n-diamonds の削除
kdebase          powerpc で konqueror を修正
libxslt          libxslt1-dev の修正
lvm10            データの損失を回避
mm               PHP4 でのクラッシュを修正
nbd              パッケージを修正
ncompress        潜在的なデータの汚染を修正
scsh             non-free へ移行
spamassassin     パッケージを修正
teg              メニューアイテムの移動
ttf-kochi        ライセンス問題の修正
ttf-kochi-naga10 ライセンス問題の修正
</PRE>

<H3>削除されたパッケージ</H3>

<P>以下のパッケージは、ディストリビューションから削除しなければなりません
でした。</P>

<PRE>
        パッケージ                理由

gnomesword                  壊れたパッケージ
hdate                       ライセンス上の問題
heyu                        ライセンス上の問題
kernel-patch-2.4.0-ia64     使用できないパッケージ
kernel-patch-2.4.0-reiserfs 使用できないパッケージ
kernel-patch-2.4.1-ia64     使用できないパッケージ
pcmcia-modules-2.2.22       インストールできないパッケージ
spellcast                   ライセンス上の問題
ttf-xtt                     ライセンス上の問題
ttf-xwatanabe-mincho        ライセンス上の問題
watanabe-vfont              ライセンス上の問題
xroach                      ライセンス上の問題
</PRE>

<P>受理または拒否されたパッケージの完全なリストは、その理由とともに、この
リビジョンのための準備のページにあります。

 <a
 href="http://people.debian.org/~joey/3.0r3/">http://people.debian.org/~joey/3.0r3/</a>
</p>

<H3>URL</H3>

<dl>
<dt>本リビジョンで変更されたパッケージの一覧:</dt>
<dd><p>
  <a href="http://http.us.debian.org/debian/dists/woody/ChangeLog">http://http.us.debian.org/debian/dists/woody/ChangeLog</a>
  <a href="http://non-us.debian.org/debian-non-US/dists/woody/non-US/ChangeLog">http://non-us.debian.org/debian-non-US/dists/woody/non-US/ChangeLog</a>
 </p></dd>

<dt>現在の安定版 (stable) ディストリビューション:</dt>
<dd><p>
  <a href="http://ftp.debian.org/debian/dists/stable/">http://ftp.debian.org/debian/dists/stable/</a>
  <a
  href="http://non-us.debian.org/debian-non-US/dists/stable/">http://non-us.debian.org/debian-non-US/dists/stable/</a>
 </p></dd>

<dt>安定版ディストリビューションに対する更新の候補:</dt>
<dd><p>
 <a href="http://ftp.debian.org/debian/dists/proposed-updates/">http://ftp.debian.org/debian/dists/proposed-updates/</a>
 <a href="http://non-us.debian.org/debian-non-US/dists/proposed-updates/">http://non-us.debian.org/debian-non-US/dists/proposed-updates/</a>
 </p></dd>

<dt>安定版ディストリビューションの情報 (リリースノート、訂正など):</dt>
<dd><p>
 <a href="http://www.debian.org/releases/stable/">http://www.debian.org/releases/stable/</a>
 </p></dd>

<dt>セキュリティに関する告知と情報:</dt>
<dd><p>
 <a href="http://security.debian.org/">http://security.debian.org/</a>
 </p>
</dd>
</dl>

<HR>
<H2><A name="20040726">2004年7月26日: packages.debian.or.jp の停止のお知らせ</A></H2>

<P>Debian JP project 会長の荒木です。</P>

<P>非常に遺感な出来事をご報告いたします。
Debian JP Project の保有する packages.debian.or.jp への DDoS 攻撃が
日本時間 7/22 朝頃から継続しております。</P>

<P>packages.debian.or.jp を運用するホストは他に ftp.debian.or.jp として運用しており、
ftp.debian.or.jp/packages.debian.or.jp ホストが HTTP ポートに対する DDoS を受け、
つながりにくい状態になっていました。</P>

<P>DDoS の原因は、W32/Bagle worm による攻撃リストに、
http://packages.debian.or.jp/1.jpg がリストされ、攻撃が開始されたことによるものです。
(worm についての詳細は、
<A HREF="http://us.mcafee.com/virusInfo/default.asp?id=description&amp;virus_k=127029">http://us.mcafee.com/virusInfo/default.asp?id=description&amp;virus_k=127029</A>
等を御覧ください)</P>

<P>そこで、対応策として、packages.debian.or.jp の運用を停止しました。</P>

<P>packages.debian.or.jp は、現在ほとんど使われていないこともあり、
Debian JP Project は packages.debian.or.jp の運用を停止します。</P>

<P>最後に、今回の事件対応に尽力された、関係者各位の協力に感謝します。</P>
