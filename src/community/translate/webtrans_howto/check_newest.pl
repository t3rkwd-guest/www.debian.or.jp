#!/usr/bin/perl
# -*- mode: perl -*-
# Title: 翻訳ファイルバージョン監視スクリプト
# Copyright: Copyright (C) 1998-2000, Mitsuru Oka <oka@debian.or.jp>
# License: This code is redistributed under the terms of the GPL license.
# Depends: perl-5.005 libdigest-md5-perl
#

use File::Find;
use MD5;

print <<'EOF';
状態=(J)日本語版が最新/(E)英語版が最新/(N)まだ着手してない<Not yet>
|検査方法=(R)CVSリビジョン/(M)MD5/(T)更新時間
|/ ファイル名
+--============================================================================
EOF

$cwd = Cwd::cwd();
find(\&check_for_each_file, 'webwml/english/');

sub check_for_each_file {
    if (/^.*\.wml$/) {
        my $en_name = $File::Find::name;
        my $ja_name = $en_name;
        $ja_name =~ s/english/japanese/ || die "it must contain 'english'";
        my $en_fullname = "$cwd/$en_name";
        my $ja_fullname = "$cwd/$ja_name";

        if (-f $ja_fullname) {
            my $id_val_at_ja;
            if ($id_val_at_ja = &get_revision_id_from_ja ($ja_fullname)) {
                my $id_val = &get_revision_id_from_entries ($en_fullname);
                if($id_val eq $id_val_at_ja) {
                    print "JR $ja_name\n";
                } else {
                    print "ER $ja_name\n";
                }
            } elsif($id_val_at_ja = &get_md5sum_id_from_ja($ja_fullname)) {
                my $md5 = new MD5;
                open(E,$en_fullname) || die "can't open $en_name";
                $md5->addfile(E);
                if($id_val_at_ja eq $md5->hexdigest) {
                    print "JM $ja_name\n";
                } else {
                    print "EM $ja_name\n";
                }
                close(E);
            } else {
                if(&is_newest_time($ja_fullname, $en_fullname)) {
                    print "JT $ja_name\n";
                } else {
                    print "ET $ja_name\n";
                }
            }
        } else {
            print "N  $ja_name\n";
        }
    }
}

sub get_revision_id_from_entries {
    $_ = shift;
    return 0 unless m|^(.+)/(.+)$|;
    my $entries = "$1/CVS/Entries";
    my $file = $2;
    open(F,$entries) || die "can't open $entries";
    while(<F>){
        if(m|^/$file/(\d+\.\d+)/|){
            close(F);
            return $1;
        }
    }
    close(F);
    0;
}

sub get_revision_id_from_ja {
    my $file = shift;

    open(F,$file) || die "can't open $file";
    while(<F>){
        # <!-- original_revision: revison :-->
        if(m/original_revision\s*:\s*(\d+\.\d+)\s*:/){
            close(F);
            return $1;
        }
        # #use wml::debian::translation-check translation="revision"
        if(/translation(\s+|=")([.0-9]*)("|\s*-->)/oi){
            close(F);
            return $2;
        }
    }
    close(F);
    0;
}

sub get_md5sum_id_from_ja {
    my $file = shift;

    open(F,$file) || die "can't open $file";
    while(<F>){
        if(m/md5sum\s*:\s*([\da-f]+)\s+([\w\d\/.]+)\s*:/){
            close(F);
            return $1;
        }
    }
    close(F);
    0;
}

sub is_newest_time {
    my($first,$second) = @_;

    my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev,
        $size, $atime, $en_mtime, $ctime, $blksize, $blocks)
        = lstat($second);
    my $ja_mtime;
    ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev,
     $size, $atime, $ja_mtime, $ctime, $blksize, $blocks)
        = lstat($first);

    $ja_mtime>$en_mtime;
}
