What is Yasdi?
Why Yasdi?
How does it work?


What is Yasdi?

Yet Another Simple Debian Installation
Automatic installation method
The target of installation is pre determined
    hardware configuration and list of packages
    "tumkey" solution
Collaboration Free Project
      Pick Systems + Alcove + Infodata


Why Yasdi?
    Simplicity reliability fast installation of an
 information system based on Linux

Deploy Rapidly, Massively & Maintaine Easily
       Reduce installation time
       1 stable release every 5 months 

??

How does it work?
    /etc/yasdi/

    Pragmatic Patch for Debian boot-floppies package
    Benefit
	inheritance of features from standard Debian installation

    Profile
	selection-cdrom => packages available on CD-ROM
	selections => Packages to install on disk
	partitions.sfdisk ==> disk partitioning
	installation.params => installation parameters (keyboard ...)
	modules => modules to use
	configuration-script.expect => answers to questions while
				    configurating packages
		* waiting more better solution in Debian
	final-script.sh => last set-up of system

   Production
	definition of profile
	./make
	./cdrecord

   Applications
	Infodata
		ERP Auto installation
	Pick Systems
		New Distribution
	Alcove
		Customized Installations
	ServBox
		Thin Server Software
