[% title = "Debian MIME サポートサブポリシィ - MIME サポートの機構" %]

<p><a name="ch2"></a></p>
<hr>

<p>
[ <a href="ch1.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch1.html">1</a> ]
[ 2 ]
[ <a href="index.html">next</a> ]
</p>

<hr>

<h2>
Debian MIME サポートサブポリシィ
<br>Chapter 2 - MIME サポートの機構
</h2>

<hr>

<p>
このサブポリシィの実装について助けがほしい場合には、debian-devel
メーリングリストに一報ください。このサブポリシィの変更や追加の提
案がある場合には、debian-policy メーリングリストに提議してくださ い。
</p>

<hr>

<h2 id="s2.1">2.1 このポリシィの背景について</h3>

<p>
MIME (Multipurpose Internet Mail Extensions, RFC 1521) はファ
イルやデータストリームの符号化の方法を規定すると共に、それら
に対するメタ情報の与え方を規定したものです。このメタ情報とは、 ファイルの種類
(例 音や画像など) やフォーマット (例 PNG、HTML や MP3 など)
などを示すものです。
</p>

<p>
MIME のファイル種別を登録することによって、メール受信プログラム (MUA)
やウェブブラウザからそれらのプログラムで直接扱えない MIME
タイプを見たり編集したり表示したりするためのハンドラを起動するこ
とができるようになります。
</p>

<hr>

<h2 id="s2.2">2.2 MIME サポートの実装</h3>

<p>
<code>mime-support</code> は MIME タイプを表示、作成、編 集、印刷するための
<code>update-mime</code> プログラムを収録し ています。
</p>

<p>
新たな MIME タイプを追加するようなパッケージは <code>update-mime(8)</code>
記載に従って <code>update-mime</code>
を使って登録を行わなければいけません。このようなパッケージは
<code>mime-support</code> パッケージに depend していたり、 recommend
していたり、suggest したりしていてはいけません。かわ りに postinst や postrm
スクリプト中で以下のようにしてください。
</p>

<pre>
       if [ -x /usr/sbin/update-mime ]; then
           update-mime
       fi
</pre>

<hr>

<p>
[ <a href="ch1.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch1.html">1</a> ]
[ 2 ]
[ <a href="index.html">next</a> ]
</p>

<hr>

<p>
Debian MIME サポートサブポリシィ
</p>

<address>
version 3.8.3.0, 2009-08-16<br>
<br>
J.H.M. Dassen (Ray) <code><a href="mailto:jdassen&#64;debian.org">mailto:jdassen&#64;debian.org</a></code><br>
The Debian Policy mailing List <code><a href="mailto:debian-policy&#64;lists.debian.org">mailto:debian-policy&#64;lists.debian.org</a></code><br>
<br>
</address>
<hr>

