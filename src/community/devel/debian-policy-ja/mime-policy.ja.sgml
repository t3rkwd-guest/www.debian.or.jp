<!doctype debiandoc system [
<!-- include version information so we don't have to hard code it
     within the document -->
<!entity % versiondata SYSTEM "version.ent"> %versiondata;
]>
<debiandoc>

  <!--
  Debian GNU/Linux Menu Sub-Policy Manual.
  Copyright (C)1999 ;

  released under the terms of the GNU General Public License, version
  2 or (at your option) any later.
  GNU General Public License 第二版 (もしくはお好みでそれ以降の版) に従
  って配布されます。

  The debian-policy mailing list has taken responsibility for the
  contents of this document, with the package maintainers responsible
  for packaging adminstrivia only.
  debian-policy メーリングリストがこの文書の内容についての責任を負います。
  また、このパッケージのメンテナはパッケージ関係の管理面のみを行います。
  -->


  <book>
    <titlepag>
      <!-- <title>The Debian MIME support sub-policy</title>-->
      <title>Debian MIME サポートサブポリシィ</title>
      <author>
	<name>J.H.M. Dassen (Ray)</name>
	<email>jdassen@debian.org</email>
      </author>
      <author>
	<name>The Debian Policy mailing List</name>
	<email>debian-policy@lists.debian.org</email>
      </author>
      <version>version &version;, &date;</version>

      <abstract><!--
	This manual describes the policy requirements for the MIME support
	system used in the Debian GNU/Linux distribution. This
	document is part of the policy package for Debian. The policy
	package itself is maintained by a group of maintainers that
	have no editorial powers. At the moment, the list of
	maintainers is:-->
	このマニュアルは Debian GNU/Linux ディストリビューションで使用す
	る MIME サポートシステムについてのポリシィ上の要求事項を記載した
	ものです。この文書は Debian のポリシィパッケージの一部です。この
	ポリシィパッケージは内容を編集する権限のないメンテナのグループで
	保守されています。現時点でのメンテナは以下の人たちです。
	<enumlist>
	  <item>
	    <p>Michael Alan Dorman <email>mdorman@debian.org</email></p>
	  </item>
	  <item>
	    <p>Richard Braakman <email>dark@xs4all.nl</email></p>
	  </item>
	  <item>
	    <p>Philip Hands <email>phil@hands.com</email></p>
	  </item>
	  <item>
	    <p>Manoj Srivastava <email>srivasta@debian.org</email></p>
	  </item>
	</enumlist>
      </abstract>


      <copyright>
	<copyrightsummary>
	  Copyright &copy;1999 .
	</copyrightsummary>
	<p><!--
	  This manual is free software; you may redistribute it and/or
	  modify it under the terms of the GNU General Public License
	  as published by the Free Software Foundation; either version
	  2, or (at your option) any later version.-->
	  このマニュアルはフリーソフトウェアです。Free Software Foundation
	  発行の GNU General Public License 第二版 (もしくはお好みでそれ
	  以降の版) に従って配布可能です。
	</p>

	<p><!--
	  This is distributed in the hope that it will be useful, but
	  <em>without any warranty</em>; without even the implied
	  warranty of merchantability or fitness for a particular
	  purpose.  See the GNU General Public License for more
	  details.-->
	  これは有用であることを期待して配布されるものではありますが、
	  <em>無保証</em>です。商品としての価値や、特定の目的に対する整
	  合性などを、明文あるいは暗黙に保証するものではありません。
	  詳細については GNU Public License を参照ください。
	  </p>
	<p><!--
	  A copy of the GNU General Public License is available as
	  <tt>/usr/doc/copyright/GPL</tt> in the Debian GNU/Linux
	  distribution or on the World Wide Web at
	  <url id="http://www.gnu.org/copyleft/gpl.html"
	  name="&urlname">. You can also obtain it by writing to the
	  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
	  Boston, MA 02111-1307, USA.-->
	  GNU General Public License の写しが Debian GNU/Linux のディス
	  トリビューションでは <tt>/usr/doc/copyright/GPL</tt> に収録さ
	  れており、また World Wide Web では
	  <url id="http://www.gnu.org/copyleft/gpl.html" name="&urlname">
	  より得られます。また、Free Software Foundation, Inc., 59 Temple
	  Place - Suite 330, Boston, MA 02111-1307, USA. に手紙を書いて入
	  手することもできます。
	</p>
      </copyright>
    </titlepag>

    <toc detail="sect">
    <chapt>
      <!-- <heading>About this document</heading> -->
      <heading>この文書について</heading>
      <p><!--
	The latest copy of this document can be found on
	<ftpsite>ftp.debian.org</ftpsite> at
	<ftppath>/debian/doc/package-developer/mime_policy.txt</ftppath> -->
	この文書の最新版は <ftpsite>ftp.debian.org</ftpsite> の 以下の場
	所にあります。
	<ftppath>/debian/doc/package-developer/mime-policy.txt</ftppath>
      </p>
    </chapt>
    <chapt>
      <!-- <heading>MIME support mechanism</heading>-->
      <heading>MIME サポートの機構</heading>
      <p><!--
	If you need assistance implementing this sub-policy, please
	please ask for it on the debian-devel mailing list.  If you
	have proposals for changes or additions to this sub-policy,
	please bring it up on debian-policy.-->
	このサブポリシィの実装について助けがほしい場合には、debian-devel
	メーリングリストに一報ください。このサブポリシィの変更や追加の提
	案がある場合には、debian-policy メーリングリストに提議してくださ
	い。
      </p>
      <sect>
        <!-- <heading>Background</heading>-->
        <heading>このポリシィの背景について</heading>
        <p><!--
          MIME (Multipurpose Internet Mail Extensions, RFC 1521) is
          a mechanism for encoding files and datastreams and providing
          meta-information about them, in particular their type (e.g. audio
          or video) and format (e.g. PNG, HTML, MP3).-->
          MIME (Multipurpose Internet Mail Extensions, RFC 1521) はファ
          イルやデータストリームの符号化の方法を規定すると共に、それら
          に対するメタ情報の与え方を規定したものです。このメタ情報とは、
          ファイルの種類 (例 音や画像など) やフォーマット (例 PNG、HTML
          や MP3 など) などを示すものです。
        </p>

        <p><!--
          Registration of MIME type handlers allows programs like mail
          user agents and web browsers to to invoke these handlers to
          view, edit or display MIME types they don't support directly.-->
          MIME のファイル種別を登録することによって、メール受信プログラム
          (MUA) やウェブブラウザからそれらのプログラムで直接扱えない MIME
          タイプを見たり編集したり表示したりするためのハンドラを起動するこ
          とができるようになります。
        </p>

      </sect>

      <sect>
	<!-- <heading>MIME support implementation</heading>-->
	<heading>MIME サポートの実装</heading>
	<p><!--
          The <package>mime-support</package> package provides the
          <prgn>update-mime</prgn> program which allows packages to
          register programs that can show, compose, edit or print
          MIME types.-->
          <package>mime-support</package> は MIME タイプを表示、作成、編
          集、印刷するための <prgn>update-mime</prgn> プログラムを収録し
          ています。
	</p>

	<p>
          <!-- Packages containing such programs must register them
          with <prgn>update-mime</prgn> as documented in <manref
          name="update-mime" section="8">. They should <em>not</em> depend
          on, recommend, or suggest <prgn>mime-support</prgn>. Instead,
          they should just put something like the following in the
          postinst and postrm scripts:-->
          新たな MIME タイプを追加するようなパッケージは <manref name=
          "update-mime" section="8"> 記載に従って <prgn>update-mime</prgn>
          を使って登録を行わなければいけません。このようなパッケージは
          <prgn>mime-support</prgn> パッケージに depend していたり、
          recommend していたり、suggest したりしていてはいけません。かわ
          りに postinst や postrm スクリプト中で以下のようにしてください。

          <example>
  if [ -x /usr/sbin/update-mime ]; then
      update-mime
  fi
          </example>
	</p>
      </sect>
    </chapt>
  </book>
</debiandoc>
