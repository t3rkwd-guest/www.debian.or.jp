[% title = "Debian ポリシーマニュアル - コントロールファイルとそのフィールド (旧 Packaging Manual より)" %]

<p><a name="ap-pkg-controlfields"></a></p>
<hr>

<p>
[ <a href="ap-pkg-sourcepkg.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ D ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-conffiles.html">next</a> ]
</p>

<hr>

<h2>
Debian ポリシーマニュアル
<br>Appendix D - コントロールファイルとそのフィールド (旧 Packaging Manual より)</h2>

<hr>

<p>
<code>dpkg</code>
プログラム群のうちの多くは、コントロールファイルとして知られる共通のファイル形式を扱います。
バイナリとソースパッケージは、データを <code>.changes</code>
ファイルに従って制御します。この、<code>.changes</code>
ファイルは、アップロードされたファイルのインストールを制御し、
<code>dpkg</code> の内部データベースも類似したフォーマットになっています。
</p>

<hr>

<h2 id="sD.1">D.1 コントロールファイルの書式</h3>

<p>
<a href="ch-controlfields.html#s-controlsyntax">コントロールファイルの書式,
Section 5.1</a> を参照ください。
</p>

<p>
<code>dpkg</code>
とその関連ツールに関する範囲では、いくつかのフィールドがオプションとして扱われることに注意が必要です。
これらのフィールドは、すべての Debian
パッケージに必要なものか、省略した場合に問題を引き起こすようなものである場合があります。
</p>

<hr>

<h2 id="sD.2">D.2 フィールドのリスト</h3>

<p>
<a href="ch-controlfields.html#s-controlfieldslist">フィールドのリスト, Section
5.6</a> を参照ください。
</p>

<p>
このセクションでは、ポリシーマニュアルに収録されていないフィールドについてのみ説明しています。
</p>

<hr>

<h3 id="s-pkg-f-Filename">D.2.1 <samp>Filename</samp> and <samp>MSDOS-Filename</samp></h4>

<p>
<code>Packages</code> ファイルにあるこれらのフィールドは、
ディストリビューションディレクトリのパッケージ、またはパッケージの一部のファイル名を、
Debian 階層構造のトップからの相対パスで示します。
そのパッケージがいくつかの部分に分かれているときは、順番に空白で区切られて並べられます。
</p>

<hr>

<h3 id="s-pkg-f-Size">D.2.2 <samp>Size</samp> and <samp>MD5sum</samp></h4>

<p>
<code>Packages</code> ファイルにあるこれらのフィールドは、 サイズ
(単位は十進法で表現されたバイトです)
とディストリビューションのバイナリパッケージを構成するファイルの MD5
チェックサムです。
パッケージがいくつかの部分に分かれているときは、順番に空白で区切られて並べられます。
</p>

<hr>

<h3 id="s-pkg-f-Status">D.2.3 <samp>Status</samp></h4>

<p>
このフィールドは <code>dpkg</code> のステータスファイルに含まれます。
そして、ユーザがパッケージをインストールしたか、また削除されたか、そのまま残しておいたのか、あるいはそのパッケージが壊れている
(再インストールが必要) かどうか等、現在のシステムの状態を示します。
それぞれの情報は一単語で示されます。
</p>

<hr>

<h3 id="s-pkg-f-Config-Version">D.2.4 <samp>Config-Version</samp></h4>

<p>
パッケージがインストールまたは設定されていなかった場合、 <code>dpkg</code>
のステータスファイル中のこのフィールドは、ユーザによってきちんと設定されてインストールされたときの最後のバージョンを記録しています。
</p>

<hr>

<h3 id="s-pkg-f-Conffiles">D.2.5 <samp>Conffiles</samp></h4>

<p>
<code>dpkg</code>
のステータスファイル中のこのフィールドは、自動処理されるパッケージ中の設定ファイルについての情報を保持します。
</p>

<hr>

<h3 id="sD.2.6">D.2.6 廃止されたフィールド</h4>

<p>
これらは、<code>dpkg</code> によって認識されますが、使ってはいけません。
これらは互換性のためだけに残されています。
</p>
<dl>
<dt><samp>Revision</samp></dt>
<dt><samp>Package-Revision</samp></dt>
<dt><samp>Package_Revision</samp></dt>
<dd>
<p>
パッケージバージョンの Debian
バージョンの部分は、以前は独立のコントロールフィールドでした。
このフィールドは、このようにいくつかの名前で実装されていました。
</p>
</dd>
<dt><samp>Recommended</samp></dt>
<dd>
<p>
<samp>Recommends</samp> の古い名前。
</p>
</dd>
<dt><samp>Optional</samp></dt>
<dd>
<p>
<samp>Suggests</samp> の古い名前
</p>
</dd>
<dt><samp>Class</samp></dt>
<dd>
<p>
<samp>Priority</samp> の古い名前
</p>
</dd>
</dl>

<hr>

<p>
[ <a href="ap-pkg-sourcepkg.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ D ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-conffiles.html">next</a> ]
</p>

<hr>

<p>
Debian ポリシーマニュアル
</p>

<address>
バージョン 3.9.5.0, 2014-07-03<br>
<br>
<a href="ch-scope.html#s-authors">The Debian Policy Mailing List</a><br>
<br>
</address>
<hr>

