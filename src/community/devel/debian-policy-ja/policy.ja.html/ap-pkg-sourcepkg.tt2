[% title = "Debian ポリシーマニュアル - ソースパッケージ (旧 Packaging Manual より)" %]

<p><a name="ap-pkg-sourcepkg"></a></p>
<hr>

<p>
[ <a href="ap-pkg-binarypkg.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ C ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-controlfields.html">next</a> ]
</p>

<hr>

<h2>
Debian ポリシーマニュアル
<br>Appendix C - ソースパッケージ (旧 Packaging Manual より)</h2>

<hr>

<p>
Debian バイナリパッケージは Debian ソースから生成されます。 Debian
ソースはバイナリパッケージを簡単に、かつ自動的に構築しやすいように特殊な形式になっています。
</p>

<hr>

<h2 id="s-pkg-sourcetools">C.1 ソースパッケージを処理するためのツール</h3>

<p>
ソースパッケージを扱うために様々なツールが提供されています。
これらはソースをパックやアンパックしたり、バイナリパッケージの構築や新しいバージョンのディストリビューションを扱うのを手助けしたりします。
</p>

<p>
ここではこれらのツールの紹介と典型的な用途を説明します。
引数や動作についての完全な文書は <code>dpkg-source(1)</code> を見て下さい。
</p>

<p>
Debian ソースパッケージをどうやって作るかの例と、 Debian
ソースパッケージからどの様にこれらのユーティリティを使うかについては、例題パッケージである
<code>hello</code> を見て下さい。
</p>

<hr>

<h3 id="s-pkg-dpkg-source">C.1.1 <code>dpkg-source</code> - Debian ソースパッケージの パックとアンパック</h4>

<p>
このプログラムは手動でよく使われます。また、 <code>dpkg-buildpackage</code>
の様なパッケージに依存しない自動構築スクリプトからも呼び出されます。
</p>

<p>
パッケージをアンパックするには次のようにコマンドを実行します。
</p>

<pre>
       dpkg-source -x <var>.../path/to/filename</var>.dsc
</pre>

<p>
この時、<code><var>filename</var>.tar.gz</code> と、もし存在するなら
<code><var>filename</var>.diff.gz</code>
は同じディレクトリに置いておきます。これにより
<code><var>package</var>-<var>version</var></code>
ディレクトリにソースをアンパックし、必要に応じて
<code><var>package</var>-<var>version</var>.orig</code>
をカレントディレクトリにアンパックします。
</p>

<p>
パックされたソースアーカイブを作るには、次のコマンドを実行します。
</p>

<pre>
       dpkg-source -b <var>package</var>-<var>version</var>
</pre>

<p>
これにより、<code>.dsc</code>、<code>.tar.gz</code> と、もし必要なら
<code>.diff.gz</code> がカレントディレクトリに作られます。
<code>dpkg-source</code> は最初にソースツリーに clean
を行ないません。必要な場合は別にやっておく必要があります。
</p>

<p>
<a href="#s-pkg-sourcearchives">Source packages as archives, Section C.3</a>
も見て下さい。
</p>

<hr>

<h3 id="s-pkg-dpkg-buildpackage">C.1.2 <code>dpkg-buildpackage</code> - 全体的なパッケージ構築の制御スクリプト</h4>

<p>
<code>dpkg-buildpackage(1)</code> マニュアルページを参照ください。
</p>

<hr>

<h3 id="s-pkg-dpkg-gencontrol">C.1.3 <code>dpkg-gencontrol</code> - バイナリパッケージコントロールファイルの生成</h4>

<p>
このプログラムは通常ソースツリーのトップレベルで <code>debian/rules</code> (<a
href="#s-pkg-sourcetree">Debian パッケージソースツリー, Section
C.2</a>を見て下さい) から呼び出されます。
</p>

<p>
これは通常、パッケージが構築されている一時的なディレクトリツリー中のファイルやディレクトリの許可属性や所有権を設定したあと、パッケージが
<code>dpkg-deb</code> を用いて構築される直前に [<a href="footnotes.html#f124"
name="fr124">124</a>] 行なわれます
</p>

<p>
<code>dpkg-gencontrol</code>
は、パッケージに入るファイルが一時的な構築ディレクトリの中に全て置かれた後で呼ばれなければなりません。
パッケージがインストールされた時のサイズの計算を正確にするためです。
</p>

<p>
また、<code>dpkg-gencontrol</code> は <code>dpkg-shlibdeps</code>
の後で実行する必要があります。<code>debian/substvars</code> 中で
<code>dpkg-shlibdeps</code> が行った 変数置換 (variable substitutions)
を反映できるようにするためです。
</p>

<p>
ソースパッケージのトップから相対パスで <code>debian/tmp</code>
にあるファイルからバイナリパッケージを一つだけ作成する場合は、通常
<code>dpkg-gencontrol</code> を呼び出せば十分です。
</p>

<p>
複数のバイナリを構築するソースでは、一般に次のようにする必要があります。
</p>

<pre>
       dpkg-gencontrol -Pdebian/tmp-<var>pkg</var> -p<var>package</var>
</pre>

<p>
<samp>-P</samp> は <code>dpkg-gencontrol</code>
にパッケージをデフォルト以外のどのディレクトリで構築するかを伝え、
<samp>-p</samp>
はどのパッケージのコントロールファイルを生成するべきかを伝えます。
</p>

<p>
<code>dpkg-gencontrol</code> は (例えば) <code>dpkg-genchanges</code>
を将来呼び出すときのために <code>debian/files</code>
中のファイルのリストに情報を加えることも行います。
</p>

<hr>

<h3 id="s-pkg-dpkg-shlibdeps">C.1.4 <code>dpkg-shlibdeps</code> - 共有ライブラリの依存関係の算定</h4>

<p>
<code>dpkg-shlibdeps(1)</code> を参照ください。
</p>

<hr>

<h3 id="s-pkg-dpkg-distaddfile">C.1.5 <code>dpkg-distaddfile</code> - <code>debian/files</code> へのファイルの追加</h4>

<p>
幾つかのパッケージのアップロードではソースパッケージやバイナリパッケージ以外のファイルを含める必要があります。
</p>

<p>
<code>dpkg-distaddfile</code> は <code>debian/files</code>
ファイルにファイル記述を加えます。 <code>dpkg-genchanges</code>
が実行されたときに <code>.changes</code>
にそのファイルが含まれるようにする為です。
</p>

<p>
これは通常、<code>debian/rules</code> の <samp>binary</samp>
ターゲットで呼び出されます:
</p>

<pre>
       dpkg-distaddfile <var>filename</var> <var>section</var> <var>priority</var>
</pre>

<p>
<var>filename</var> は <code>dpkg-genchanges</code>
がそのファイルを見つけると思われるようなディレクトリ -
これは通常ソースツリーのトップレベルの上のディレクトリ -
に対する相対ファイル名です。<code>debian/rules</code> のターゲットは
<code>dpkg-distaddfile</code>
が呼ばれる直前か直後にそのファイルをその場所に置かねばなりません。
</p>

<p>
<var>section</var> と <var>priority</var> は、生成される <code>.changes</code>
ファイルに変更されずに渡されます。
</p>

<hr>

<h3 id="s-pkg-dpkg-genchanges">C.1.6 <code>dpkg-genchanges</code> - アップロードコントロールファイル <code>.changes</code> の生成</h4>

<p>
<code>dpkg-genchanges(1)</code> のマニュアルページを参照ください。
</p>

<hr>

<h3 id="s-pkg-dpkg-parsechangelog">C.1.7 <code>dpkg-parsechangelog</code> - changelog の解析結果の生成</h4>

<p>
<code>dpkg-parsechangelog(1)</code> のマニュアルページを参照ください。
</p>

<hr>

<h3 id="s-pkg-dpkg-architecture">C.1.8 <code>dpkg-architecture</code> - パッケージを構築するシステム、あるいはホストシステムについての情報</h4>

<p>
<code>dpkg-architecture(1)</code> のマニュアルページを参照ください。
</p>

<hr>

<h2 id="s-pkg-sourcetree">C.2 Debian パッケージソースツリー</h3>

<p>
以降で述べるソースアーカイブの構成は、関連した制御情報をもつ Debian
パッケージソースツリーが容易に再現され、容易に持ち運べるようにすることを意図したものになっています。
Debian
パッケージソースツリーは、オリジナルのプログラムにパッケージ化の工程の為のファイルを付け、
残りのソースコードとインストールスクリプトに必要な変更を加えたものです。
</p>

<p>
Debian のために作られた特別なファイルは、 Debian
パッケージソースツリーのトップレベルの <code>debian</code>
ディレクトリに置かれます。
</p>

<hr>

<h3 id="s-pkg-debianrules">C.2.1 <code>debian/rules</code> - メイン構築スクリプト</h4>

<p>
<a href="ch-source.html#s-debianrules"><code>debian/rules</code> -
メイン構築スクリプト, Section 4.9</a> を参照ください。
</p>

<hr>

<h3 id="sC.2.2">C.2.2 <code>debian/control</code></h4>

<p>
<a
href="ch-controlfields.html#s-sourcecontrolfiles">ソースパッケージコントロールファイル
-- <code>debian/control</code>, Section 5.2</a> を参照ください。
</p>

<hr>

<h3 id="s-pkg-srcsubstvars">C.2.3 <code>debian/substvars</code> と変数の置換</h4>

<p>
<a href="ch-source.html#s-substvars">変数置換: <code>debian/substvars</code>,
Section 4.10</a> を参照ください。
</p>

<hr>

<h3 id="sC.2.4">C.2.4 <code>debian/files</code></h4>

<p>
<a href="ch-source.html#s-debianfiles"><code>debian/files</code>, Section
4.12</a> を参照ください。
</p>

<hr>

<h3 id="sC.2.5">C.2.5 <code>debian/tmp</code></h4>

<p>
<samp>binary</samp>
ターゲットによってバイナリパッケージを構築する際に標準的に使用される一時的ディレクトリです。
パッケージ構築の際は、<code>tmp</code>
ディレクトリがファイルシステムツリーのルートになります
(例えば、パッケージに付属する makefile の install
ターゲットを使用するときや、出力をリダイレクトする場合です)。
また、<samp>DEBIAN</samp> サブディレクトリを含みます。 <a
href="ap-pkg-binarypkg.html#s-pkg-bincreating">パッケージファイルの作成 -
<code>dpkg-deb</code>, Section B.1</a> をご覧ください。
</p>

<p>
同じソースツリーから複数のバイナリパッケージが生成されるときは、通常複数の
<code>debian/tmp<var>something</var></code> ディレクトリを使用します。
例えば、<code>tmp-a</code> や <code>tmp-doc</code> といった具合です。
</p>

<p>
<samp>binary</samp> によって、どんな <code>tmp</code>
ディレクトリが作成されたとしても、もちろん、<samp>clean</samp>
ターゲットによって削除されなければいけません。
</p>

<hr>

<h2 id="s-pkg-sourcearchives">C.3 Source packages as archives</h3>

<p>
FTP サイトにおいてある様に、Debian ソースパッケージは 3
つの関連したファイルから成ります。 これら 3
つのファイルは、正しいバージョンのものを入手しないと利用することが出来ません。
</p>
<dl>
<dt>Debian ソースコントロールファイル - <samp>.dsc</samp></dt>
<dd>
<p>
このファイルは一連のフィールドを含んでいて、各フィールドはバイナリパッケージのコントロールファイルと同様に識別され分離されています。
<a href="ch-controlfields.html#s-debiansourcecontrolfiles">Debian
ソースコントロールファイル -- <samp>.dsc</samp>, Section 5.4</a>
を参照ください。
</p>
</dd>
</dl>
<dl>
<dt>もとのソースアーカイブ <code><var>package</var>_<var>upstream-version</var>.orig.tar.gz</code></dt>
<dd>
<p>
このファイルは、プログラムの上流の作者からのソースコードを含む <code>tar</code>
(<samp>gzip -9</samp> されている) です。
</p>
</dd>
</dl>
<dl>
<dt>Debian パッケージの diff ファイル <code><var>package</var>_<var>upstream_version-revision</var>.diff.gz</code></dt>
<dd>
<p>
このファイルは、オリジナルソースを Debian
ソースに変換するのに必要な変更を行なうための unified context diff (<samp>diff
-u</samp>) です。
プレインファイルの編集や作成といった変更のみを含むことが出来ます。
ファイルのパーミッション、シンボリックリンク先、特殊ファイルやパイプの特性の変更は出来ません。
またファイルの移動や名前変更も出来ません。
</p>

<p>
diff に含まれるディレクトリは、ソースツリーのトップにある <code>debian</code>
ディレクトリ以外は前もって存在していないといけません。 <code>debian</code>
ディレクトリは、アンパック時に必要な場合は <code>dpkg-source</code>
によって作られます。
</p>

<p>
<code>dpkg-source</code> は <code>debian/rules</code>
という実行ファイルを自動的に作ります (下を参照)。
</p>
</dd>
</dl>

<p>
「オリジナル」のソースコード (に相当する物) がない場合 - 例えば、パッケージが
Debian のために特別に用意されたものだったり、Debian maintainer が上流の
maintainer でもある場合 - は、 構成が少し違います。 diff ファイルは無く、tar
ファイルは <code><var>package</var>_<var>version</var>.tar.gz</code>
という名前で <code><var>package</var>-<var>version</var></code>
というディレクトリを含むものになります。
</p>

<hr>

<h2 id="sC.4">C.4 <code>dpkg-source</code> を使わない Debian ソースパッケージのアンパック</h3>

<p>
Debian ソースパッケージのアンパックには <samp>dpkg-source -x</samp>
がお勧めです。
しかし、それが出来ない場合には次のような方法でアンパック出来ます。
</p>
<!-- ol type="1" start="1"  -->
<li>
<p>
tar ファイルを展開し、<code>.orig</code> ディレクトリを作ります。
</p>
</li>
<li>
<p>
<code>.orig</code> ディレクトリの名前を
<code><var>package</var>-<var>version</var></code> に変えます。
</p>
</li>
<li>
<p>
<code>debian</code> ディレクトリをソースツリーのトップに作ります。
</p>
</li>
<li>
<p>
diff を <samp>patch -p0</samp> として適用します。
</p>
</li>
<li>
<p>
Debian 化されたバージョンと一緒にオリジナルのソースコードも欲しい場合は、 tar
ファイルをもう一度展開します。
</p>
</li>
</ol>

<p>
<code>dpkg-source</code> を使わずに正当な Debian
ソースアーカイブを作ることは出来ません。特に、 <code>.diff.gz</code>
ファイルを作るのに <code>diff</code> 直接使おうとしてもうまくいかないでしょう。
</p>

<hr>

<h3 id="sC.4.1">C.4.1 ソースパッケージに含まれるものに対する制限</h4>

<p>
ソースパッケージには、ハードリンク [<a href="footnotes.html#f125"
name="fr125">125</a>] [<a href="footnotes.html#f126" name="fr126">126</a>]
デバイスファイル、ソケットファイル、及び setuid や setgid されたファイル [<a
href="footnotes.html#f127" name="fr127">127</a>] が含まれていてはいけません。
</p>

<p>
ソースパッケージングツールは <code>diff</code> と <code>patch</code>
を用いてオリジナルのソースと Debian
パッケージソースの間の変更を処理します。<code>.orig.tar.gz</code>
に含まれたオリジナルのソースツリーを Debian
パッケージのソースにするために、これらのツールで処理出来ないような変更を伴ってはいけません。
ソースパッケージを構築する時に <code>dpkg-source</code>
エラーで停止してしまうような問題のある変更は次の通りです。
</p>
<ul>
<li>
<p>
シンボリックリンク、ソケット、パイプの追加や削除。
</p>
</li>
<li>
<p>
シンボリックリンク先の変更。
</p>
</li>
<li>
<p>
<code>debian</code> ディレクトリ以外のディレクトリの作成。
</p>
</li>
<li>
<p>
バイナリファイルの内容に対する変更。
</p>
</li>
</ul>

<p>
ソースパッケージを構築する時に、<code>dpkg-source</code>
警告が表示されるものも、処理は継続されるような問題のある変更は次の通りです。
</p>
<ul>
<li>
<p>
ファイル、ディレクトリ、シンボリックリンクの削除 [<a href="footnotes.html#f128"
name="fr128">128</a>] 。
</p>
</li>
<li>
<p>
通常の最後の改行が (オリジナル及び修正版のどちらのソースツリーにも)
ない変更されたテキストファイル。
</p>
</li>
</ul>

<p>
変更が指摘されず、<code>dpkg-source</code>
によって検出もされない変更は次の通りです。
</p>
<ul>
<li>
<p>
(<code>debian/rules</code> 以外の)
ファイルやディレクトリのパーミッションの変更。
</p>
</li>
</ul>

<p>
<code>debian</code> ディレクトリと <code>debian/rules</code> は
<code>dpkg-source</code> によって別々に処理されます - 変更を行なう前に
<code>debian</code> ディレクトリを作成し、 その後 <code>debian/rules</code>
を誰もが実行できるようにします。
</p>

<hr>

<p>
[ <a href="ap-pkg-binarypkg.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ C ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-controlfields.html">next</a> ]
</p>

<hr>

<p>
Debian ポリシーマニュアル
</p>

<address>
バージョン 3.9.5.0, 2014-07-03<br>
<br>
<a href="ch-scope.html#s-authors">The Debian Policy Mailing List</a><br>
<br>
</address>
<hr>

