[% title = "Debian ポリシーマニュアル - 設定ファイルの取り扱い (旧 Packaging Manual より)" %]

<p><a name="ap-pkg-conffiles"></a></p>
<hr>

<p>
[ <a href="ap-pkg-controlfields.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ E ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-alternatives.html">next</a> ]
</p>

<hr>

<h2>
Debian ポリシーマニュアル
<br>Appendix E - 設定ファイルの取り扱い (旧 Packaging Manual より)</h2>

<hr>

<p>
<code>dpkg</code> はパッケージ設定ファイルをある程度自動的に操作できます。
</p>

<p>
そのメカニズムが妥当かどうかは、多数の要因によります。
けれども、基本的にはある特定の設定ファイルに対して二つのアプローチがあります。
</p>

<p>
簡単な方法は、パッケージ中にできうる限り最良のパッケージ設定ファイルを含んだ形でリリースし、以降の更新には
<code>dpkg</code> の conffile メカニズムを使用することです。
設定ファイルをユーザが編集することがなさそうでも、仮に編集していた場合にはその編集が失われないようにしたい、またはそのパッケージの新しいバージョンはそんなに頻繁にはリリースされない、そんな場合にはこれは適したアプローチです。
</p>

<p>
より複雑な方法として、設定ファイルを <code>postinst</code>
スクリプト中でスクラッチから構築する方法があります。
そして、パッケージの初期のバージョンからのバグをその中で自動的にフィックスしていくようにします。
これは、構成ファイルがそれぞれのシステムによって違う場合に使用される方法です。
</p>

<hr>

<h2 id="sE.1">E.1 <code>dpkg</code> による設定ファイルの自動操作</h3>

<p>
パッケージに は、<code>conffiles</code>
と呼ばれる制御情報ファイルを含めることができます。
このファイルは、自動操作を必要とする設定ファイル名一覧です。
設定ファイル名は、一行につきひとつのファイルを絶対パスで書かれていなければいけません。
また、参照されるファイルはパッケージ中に含まれていなければいけません。
</p>

<p>
パッケージが更新されるとき、<code>dpkg</code>
は、設定の段階において、設定ファイルを処理します。 設定の段階とは、パッケージの
<code>postinst</code> スクリプトを実行する直前です。
</p>

<p>
<code>dpkg</code>
は、それぞれの設定ファイルについて、パッケージに含まれているものが現在のバージョン
(つまり、今アップグレードしようとしている)
のパッケージに最初に含まれていたものと同一のものであるかをチェックします。
同時に、現在のバージョンのパッケージで最初に提供されていたものと、現在システムにインストールされているものとの比較も行ないます。
</p>

<p>
ユーザとパッケージ管理者のどちらがもが設定ファイルを変更していない場合は、設定ファイルはそのままインストールされます。
どちらかが以前のバージョンの設定ファイルを編集していた場合は、その編集されたバージョンが優先されます。
つまり、ユーザが設定ファイルを編集しており、パッケージ管理者が違ったバージョンを出していなかったなら、
何の報告も無しに、ユーザの変更がそのまま残されます。
けれども、パッケージ管理者が新しいバージョンを出していて、
ユーザが設定ファイルに手を加えていなかった場合は、新しいバージョンがインストールされます
(このとき、そのことを知らせるメッセージも表示されます)。
ユーザもパッケージ管理者も両方が設定ファイルを変更していた場合は、
ユーザが自分自身でこの問題を解決するように、入力を促すプロンプトを出力します。
</p>

<p>
この比較は、ファイル中の MD5
メッセージダイジェストを計算することによって行なれます。
そして、そのインストールされた最新バージョンのファイル中の MD5
を同様に保存します。
</p>

<p>
そのパッケージのインストールが初めてだったときは、 <code>dpkg</code>
は、そのファイルが、現存システムのファイルを上書きするものでないかぎり、そのままインストールします。
</p>

<p>
けれども、<code>dpkg</code>
は、ユーザまたはスクリプトによって削除された設定ファイル
(<samp>conffile</samp>) を置き換えることは <em>ありません</em>。
いくつかのプログラムにおいては、ある設定ファイルが存在しないことによって、他の方法では代替できないような効果をねらっているものがあります。
したがって、もしユーザがそうしたのであれば、削除されたファイルはそのままにしておく必要があります。
</p>

<p>
パッケージは、パッケージ管理スクリプトの中では、<code>dpkg</code>
によって操作される conffile を変更することは <em>できません</em>。
もし、これを行った場合、パッケージのアップデートのとき、 <code>dpkg</code>
は、ユーザを混乱させる、また潜在的に危険なオプションを与えることになります。
</p>

<hr>

<h2 id="sE.2">E.2 管理スクリプトによる設定の取り扱い</h3>

<p>
ホスト名やネットワークの詳細など、サイト依存の情報を含むファイルは，パッケージの
<code>postinst</code> スクリプトによって、作成するようにするのがよいでしょう。
</p>

<p>
たいてい、値や他の情報を決定するのに、システムの他の部分の状態が必要となります。
そして、その情報が得られないときは、プロンプトを出力して、ユーザに情報を入力してもらうことになります。
</p>

<p>
この方法を使用するとき、重要なことをいくつか考慮しなければいけません。
</p>

<p>
設定ファイルを生成するプログラムにバグを発見した場合や、そのファイルのフォーマットが以前のバージョンから変更されたときには、
postinst スクリプトを編集しなければならなくなるでしょう。
ふつう、この場合は、インストールされた設定ファイルから、問題をとりのぞくことになるか、そのファイルの文法を変更することになります。
これは、気をつけて行わなくてはなりません。
すでにユーザはその問題に対処するように設定ファイルを更新しているかもしれません。
スクリプトはすでに対処済みである場合を検出して、正しくそれを扱わなければなりません。
</p>

<p>
この方針を押し進めるならば、設定ファイルの生成を <code>/usr/sbin</code>
に置かれる別のプログラムにまかせるというのは、よい考えです。
このプログラム名は慣習として、<code><var>package</var>config</code>
を使うことになっており、そうすることが適切な場合、パッケージのインストール後に
postinst スクリプトから実行されます。この <code><var>package</var>config</code>
プログラムは、ユーザに問い合わせることなしに既存の設定を上書きしてはいけません。
もし、そのプログラムが (後に再設定行う場合ではなく)
初めてセットアップする場合には、そのプログラムは設定ファイルが存在するかどうかをチェックしなければいけません。
そして上書きするためには、dpkg に <samp>--force</samp>
オプションが与えられていなければいけません。
</p>

<hr>

<p>
[ <a href="ap-pkg-controlfields.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ E ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ap-pkg-alternatives.html">next</a> ]
</p>

<hr>

<p>
Debian ポリシーマニュアル
</p>

<address>
バージョン 3.9.5.0, 2014-07-03<br>
<br>
<a href="ch-scope.html#s-authors">The Debian Policy Mailing List</a><br>
<br>
</address>
<hr>

