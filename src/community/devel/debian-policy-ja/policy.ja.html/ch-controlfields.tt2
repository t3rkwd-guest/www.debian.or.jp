[% title = "Debian ポリシーマニュアル - コントロールファイル (Control) とそのフィールド" %]

<p><a name="ch-controlfields"></a></p>
<hr>

<p>
[ <a href="ch-source.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ 5 ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ch-maintainerscripts.html">next</a> ]
</p>

<hr>

<h2>
Debian ポリシーマニュアル
<br>Chapter 5 - コントロールファイル (Control) とそのフィールド
</h2>

<p>
パッケージ管理システムは共通のフォーマットで記述されたデータを扱います。
このフォーマットを <em>control data</em> と呼び、<em>コントロールファイル</em>
中に記載されています。
コントロールファイルはソースパッケージ、バイナリパッケージ、および
<code>.changes</code> で使われており、
インストールとファイルのアップロードを制御します[<a href="footnotes.html#f32"
name="fr32">32</a>] 。
</p>

<hr>

<h2 id="s-controlsyntax">5.1 コントロールファイルの書式</h3>

<p>
コントロールファイルは、一つまたはそれ以上のフィールドの段落から成ります [<a
href="footnotes.html#f33" name="fr33">33</a>] 。段落は、空白行で区切られます。
パーザは空白文字とタブだけからなる行も段落の区切りとして処理しても構いませんが、
コントロールファイルでは空行を使うべきです。
いくつかのコントロールファイルは一つの段落しか持てませんが、複数の段落を持つことができるコントロールファイルもあります。
その場合には、通常、各段落は、それぞれ違うパッケージに対する記述になっています
(例えば、ソースパッケージでは第一段落はソースパッケージを指し、以降のパッケージはそこから作成されたバイナリパッケージ群を指します)。
コントロールファイル中の段落の順序は意味を持ちます。
</p>

<p>
一つ一つの段落は、フィールドと値の連続したものです。
個々のフィールドは、名前とそれに続くコロンとデータ/値によって構成されます。
フィールド名は制御文字、空白とコロンを除く ASCII 文字から構成されます
(つまり、33-57 または 59-126 までの間でなければいけません)。
フィールド名は、コメント文字 <samp>#</samp> やハイフン文字 <samp>-</samp>
で始まってはいけません。
</p>

<p>
そして、改行または最後の継続行の終わりがフィールドの終わりです。
水平方向に連続する空白 (スペースとタブ)
は値の前後に入れることもできますが、その場合は無視されます;
慣習として、コロンの後に一つの空白を入れます。
例えば、フィールドは以下のようなものです。
</p>
<pre>
     Package: libc6
</pre>

<p>
ここで、フィールド名は <samp>Package</samp> で、フィールドの値は
<samp>libc6</samp> です。
</p>

<p>
一つの段落に、特定のフィールド名が二回以上現れることは許されません。
</p>

<p>
フィールドには三種類あります。
</p>
<dl>
<dt>simple (単一行)</dt>
<dd>
<p>
このフィールドは値を含めて一行でなければなりません。
フィールドを折り返すことは許されません。
これは、フィールドの定義で他のフィールド種別を許していない場合、標準のフィールドの種類になります。
</p>
</dd>
</dl>
<dl>
<dt>folded (折り返しを許す)</dt>
<dd>
<p>
折り返しを許す (folded) フィールドの値は、複数行に展開可能な論理的な行です。
最初の行に続く行は継続行と呼ばれ、空白またはタブで始まらなければいけません。
改行を含む空白文字は、折り返しを許すフィールドの値の場合、意味を持ちません [<a
href="footnotes.html#f34" name="fr34">34</a>]
</p>
</dd>
</dl>
<dl>
<dt>multiline (複数行)</dt>
<dd>
<p>
複数行 (multiline) フィールドの値は、複数の継続行から構成されます。
最初の行の値は (フィールド名と同じ行にある値の部分)、
しばしば特別の意味を持ち、空にしなければならない場合もあります。
残りの行は「折り返しを許すフィールド」の継続行と同じ書式で付け加えられます。
空白や改行は、複数行フィールドでは意味を持ちます。
</p>
</dd>
</dl>

<p>
空白は、 名前 (パッケージやアーキテクチャ、ファイル、その他)
やバージョン番号、複数のキャラクタのバージョン関係の中には、絶対にあってはいけません。
</p>

<p>
各フィールドの存在と用途、および値の書式はコントロールファイル毎に異なります。
</p>

<p>
フィールドの名前には、大文字か小文字かは関係ありません。
しかし、以下に示すように、大文字小文字を混在させる場合には、最初の一文字だけを大文字にするのが普通です。
フィールドの値は、フィールドの説明で特段の記載がない限り、大文字と小文字は区別されます。
</p>

<p>
段落の区切り (空行)
や、空白行や空白とタブでだけ構成されている行が、フィールドの値や、フィールドとフィールドの間にあってはいけません。
フィールドの値としての空行は、通常は 空白とドット
という値にエスケープされます。
</p>

<p>
行が前に空白なしで #
で始まっている場合、それはソースパッケージコントロールファイル
(<code>debian/control</code>) 中でのみ許されるコメント行です。
このコメント行は、継続行の途中にある場合も含めて、無視されます。
また、フィールドの論理行の終りを意味することもありません。
</p>

<p>
コントロールファイルは UTF-8 でエンコードしなければいけません。
</p>

<hr>

<h2 id="s-sourcecontrolfiles">5.2 ソースパッケージコントロールファイル -- <code>debian/control</code></h3>

<p>
<code>debian/control</code>
ファイルは、ソースパッケージとそれから生成されるバイナリパッケージについての最も重要な
(バージョン非依存の) 詳細を含みます。
</p>

<p>
コントロールファイルの最初の段落には、ソースパッケージ全般に関する情報が収録されています。
それに引き続く部分にはソースツリーからビルドされたバイナリパッケージの記載が来ます。
</p>

<p>
全般部分の段落 (ソースパッケージ用の最初の部分) のフィールドは以下のものです。
</p>
<ul>
<li>
<p>
<a href="#s-f-Source"><samp>Source</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Maintainer"><samp>Maintainer</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Uploaders"><samp>Uploaders</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Section"><samp>Section</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Priority"><samp>Priority</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-sourcebinarydeps"><samp>Build-Depends</samp>
など</a>
</p>
</li>
<li>
<p>
<a href="#s-f-Standards-Version"><samp>Standards-Version</samp></a>(推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Homepage"><samp>Homepage</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-VCS-fields"><samp>Vcs-Browser</samp>, <samp>Vcs-Git</samp>
ほか</a>
</p>
</li>
</ul>

<p>
バイナリパッケージ用の段落のフィールドは以下のものです。
</p>
<ul>
<li>
<p>
<a href="#s-f-Package"><samp>Package</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Architecture"><samp>Architecture</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Section"><samp>Section</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Priority"><samp>Priority</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Essential"><samp>Essential</samp></a>
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-binarydeps"><samp>Depends</samp> et al</a>
</p>
</li>
<li>
<p>
<a href="#s-f-Description"><samp>Description</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Homepage"><samp>Homepage</samp></a>
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-built-using"><samp>Built-Using</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Package-Type"><samp>Package-Type</samp></a>
</p>
</li>
</ul>

<p>
フィールドの書式と意味は以下で記載されています。
</p>

<p>
これらのフィールドは、<code>dpkg-gencontrol</code>
がバイナリパッケージ用のコントロールファイルを 生成するとき (以下参照) や
<code>dpkg-genchanges</code> がアップロードに付随する <code>.changes</code>
ファイルを生成するとき、または <code>dpkg-source</code>
がソースアーカイブの一部として <code>.dsc</code>
ソースコントロールファイルを生成するときに使用されます。
一部のフィールドは、<code>debian/control</code>
中での折り返しを許す行の使用が許されていますが、
他のコントロールファイル中ではこれは認められていません。
上で挙げたツールは、<code>debian/control</code>
中のそのようなフィールドに対して、他のコントロールファイルを作成するために使用する際に、改行を削除する責任を負っています。
</p>

<p>
また、これらのフィールドは、変数参照を含むこともあります。
それらの値は、出力コントロールファイルを生成するときに
<code>dpkg-gencontrol</code>、<code>dpkg-genchanges</code> 及び
<code>dpkg-source</code> によって使用されます。 詳細は、<a
href="ch-source.html#s-substvars">変数置換: <code>debian/substvars</code>,
Section 4.10</a> をご覧ください。
</p>

<hr>

<h2 id="s-binarycontrolfiles">5.3 バイナリパッケージコントロールファイル -- <code>DEBIAN/control</code></h3>

<p>
<code>DEBIAN/control</code> ファイルには、バイナリパッケージに関する最も重要な
(バージョン依存の) 情報が収録されています。これは一つの段落からなります。
</p>

<p>
このファイルのフィールドは以下の通りです。
</p>
<ul>
<li>
<p>
<a href="#s-f-Package"><samp>Package</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Source"><samp>Source</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Version"><samp>Version</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Section"><samp>Section</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Priority"><samp>Priority</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Architecture"><samp>Architecture</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Essential"><samp>Essential</samp></a>
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-binarydeps"><samp>Depends</samp> et al</a>
</p>
</li>
<li>
<p>
<a href="#s-f-Installed-Size"><samp>Installed-Size</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Maintainer"><samp>Maintainer</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Description"><samp>Description</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Homepage"><samp>Homepage</samp></a>
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-built-using"><samp>Built-Using</samp></a>
</p>
</li>
</ul>

<hr>

<h2 id="s-debiansourcecontrolfiles">5.4 Debian ソースコントロールファイル -- <samp>.dsc</samp></h3>

<p>
このファイルは、一つの段落からなり、恐らく PGP 署名で囲われています。
含まれるフィールドを以下に示します。またフィールドの書式は前に <a
href="#s-controlsyntax">コントロールファイルの書式, Section 5.1</a>
で説明されています。
</p>
<ul>
<li>
<p>
<a href="#s-f-Format"><samp>Format</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Source"><samp>Source</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Binary"><samp>Binary</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Architecture"><samp>Architecture</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Version"><samp>Version</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Maintainer"><samp>Maintainer</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Uploaders"><samp>Uploaders</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Homepage"><samp>Homepage</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-VCS-fields"><samp>Vcs-Browser</samp>, <samp>Vcs-Git</samp>
ほか</a>
</p>
</li>
<li>
<p>
<a href="#s-f-Dgit"><samp>Dgit</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Standards-Version"><samp>Standards-Version</samp></a>(推奨)
</p>
</li>
<li>
<p>
<a href="ch-relationships.html#s-sourcebinarydeps"><samp>Build-Depends</samp>
ほか</a>
</p>
</li>
<li>
<p>
<a href="#s-f-Package-List"><samp>Package-List</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Checksums"><samp>Checksums-Sha1</samp> および
<samp>Checksums-Sha256</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Files"><samp>Files</samp></a> (必須)
</p>
</li>
</ul>

<p>
Debian ソースコントロールファイルはソースアーカイブビルド時に
<code>dpkg-source</code>
によりソースパッケージ中の、上記の説明している他のファイルから作成されます。
パッケージを展開する時、ソースパッケージ中の他の部分のファイルとディレクトリとの整合性がチェックされます。
</p>

<hr>

<h2 id="s-debianchangesfiles">5.5 Debian changes ファイル -- <code>.changes</code></h3>

<p>
<code>.changes</code> ファイルは、パッケージ更新を処理する際に Debian
アーカイブ管理ソフトウェアによって使用されます。
このファイルは一つの段落からなり、<samp>debian/control</samp> と、
<samp>debian/changelog</samp> や <samp>debian/rules</samp>
などから抽出したソースパッケージの情報が含まれています。
</p>

<p>
<code>.changes</code>
ファイルは、文書化されたフィールドまたはその意味が変更されるたびに +1
されるフォーマットバージョン番号を持っています。 この文書では、1.8
フォーマットを記載します。
</p>

<p>
このファイルのフィールドは以下のものです。
</p>
<ul>
<li>
<p>
<a href="#s-f-Format"><samp>Format</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Date"><samp>Date</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Source"><samp>Source</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Binary"><samp>Binary</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Architecture"><samp>Architecture</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Version"><samp>Version</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Distribution"><samp>Distribution</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Urgency"><samp>Urgency</samp></a> (推奨)
</p>
</li>
<li>
<p>
<a href="#s-f-Maintainer"><samp>Maintainer</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Changed-By"><samp>Changed-By</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Description"><samp>Description</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Closes"><samp>Closes</samp></a>
</p>
</li>
<li>
<p>
<a href="#s-f-Changes"><samp>Changes</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Checksums"><samp>Checksums-Sha1</samp> および
<samp>Checksums-Sha256</samp></a> (必須)
</p>
</li>
<li>
<p>
<a href="#s-f-Files"><samp>Files</samp></a> (必須)
</p>
</li>
</ul>

<hr>

<h2 id="s-controlfieldslist">5.6 フィールドのリスト</h3>

<hr>

<h3 id="s-f-Source">5.6.1 <samp>Source</samp></h4>

<p>
このフィールドは、ソースパッケージの名前です。
</p>

<p>
<code>debian/control</code> ファイルや <code>.dsc</code>
ファイル中のフィールドでは、
ソースパッケージの名前のみを含まなければいけません。
</p>

<p>
バイナリパッケージ中のコントロールファイルの中、または <code>.changes</code>
ファイル中では、ソースパッケージ名の後に、
かっこに入れたバージョン番号を続けて記載することができます [<a
href="footnotes.html#f35" name="fr35">35</a>]。
このバージョン番号は、該当のバイナリパッケージの <samp>Version</samp>
フィールドと同一の値を持っていた場合、<code>dpkg-gencontrol</code>
によって削除されます。
また、ソースパッケージがバイナリパッケージと同じ名前とバージョンを持っていた場合は、
このフィールド自体がバイナリパッケージコントロールファイルから削除されます。
</p>

<p>
パッケージ名 (ソースパッケージとバイナリパッケージの両方とも。 <a
href="#s-f-Package"><samp>Package</samp>, Section 5.6.7</a> 参照) 小文字英字
(<samp>a-z</samp>), 数字 (<samp>0-9</samp>), プラス記号 (<samp>+</samp>)
マイナス記号 (<samp>-</samp>) とピリオド (<samp>.</samp>)
のみを含むようにしなければいけません。
また、少なくとも二文字で、英数字から始まらなければいけません。
</p>

<hr>

<h3 id="s-f-Maintainer">5.6.2 <samp>Maintainer</samp></h4>

<p>
パッケージメンテナの名前と email アドレスです。
最初に名前がこなくてはいけません。 その後に email アドレスを
<samp>&lt;&gt;</samp> の中に (RFC822 フォーマットで) 入れます。
</p>

<p>
パッケージメンテナの名前にピリオドが含まれていると、RFC822
に規定されている文法のミスによって、全てのフィールドが email
アドレスとして適用されなくなります;
このフィールドをアドレスとして使用するプログラムは、これをチェックして、必要であれば修正しなければいけません
(例えば、名前をかっこのなかに入れて最後尾に移動し、 email
アドレスを前の方に持ってきます)。
</p>

<p>
パッケージメンテナに関するこれ以外の要求事項や説明は、 <a
href="ch-binary.html#s-maintainer">パッケージのメンテナ, Section 3.3</a>
を参照ください。
</p>

<hr>

<h3 id="s-f-Uploaders">5.6.3 <samp>Uploaders</samp></h4>

<p>
パッケージの共同メンテナがいる場合、その人 (達) の名前と email
アドレスの一覧です。もし、パッケージに <a href="#s-f-Maintainer">Maintainer
フィールド</a> に記載した以外のメンテナがいる場合、その人たちの名前と email
アドレスをここに記載します。
書式はメンテナフィールドのものと同じであり、複数のエントリはコンマ
&quot;,&quot; で区切ります。
</p>

<p>
これは通常はオプションのフィールドですが、<samp>Maintainer</samp>
にメールアドレスを共有するグループが指定されている場合には
<samp>Uploaders</samp>
フィールドの指定が必須で、ひとりの個人とその人の電子メールアドレスとが記載されていなければいけません。
</p>

<p>
<code>debian/control</code> ファイルの Uploaders
フィールドでは、折り返しが許されています。
</p>

<hr>

<h3 id="s-f-Changed-By">5.6.4 <samp>Changed-By</samp></h4>

<p>
この版のパッケージを用意した人、通常はメンテナ、の名前と電子メールアドレスです。
<a href="#s-f-Maintainer">Maintainer field</a>と同じ書式が適用されます。
</p>

<hr>

<h3 id="s-f-Section">5.6.5 <samp>Section</samp></h4>

<p>
このフィールドには、パッケージを分類したアプリケーション分野を指定します。 <a
href="ch-archive.html#s-subsections">セクション, Section 2.4</a>
を参照ください。
</p>

<p>
<code>debian/control</code> ファイルに現れている場合、 <code>.changes</code>
ファイルの <samp>Files</samp>
フィールドの同名のサブフィールドの値に代入されます。
また、バイナリパッケージの同名のフィールドの初期値にも代入されます。
</p>

<hr>

<h3 id="s-f-Priority">5.6.6 <samp>Priority</samp></h4>

<p>
このフィールドには、このパッケージをインストールすることの重要性が示されています。<a
href="ch-archive.html#s-priorities">プライオリティ, Section 2.5</a>
参照のこと。
</p>

<p>
<code>debian/control</code> ファイルに現れている場合、 <code>.changes</code>
ファイルの <samp>Files</samp>
フィールドの同名のサブフィールドの値に代入されます。
また、バイナリパッケージの同名のフィールドの初期値にも代入されます。
</p>

<hr>

<h3 id="s-f-Package">5.6.7 <samp>Package</samp></h4>

<p>
バイナリパッケージの名前です。
</p>

<p>
バイナリパッケージの名前は、ソースパッケージの名前と同じ書式および制限を持ちます。
詳細は <a href="#s-f-Source"><samp>Source</samp>, Section 5.6.1</a>
を参照ください。
</p>

<hr>

<h3 id="s-f-Architecture">5.6.8 <samp>Architecture</samp></h4>

<p>
これまでのコンテキストと、コントロールファイルの内容に依存しますが、
<samp>Architecture</samp> は以下の値を取ることができます。
</p>
<ul>
<li>
<p>
Debian マシンアーキテクチャを示す、一意の一語。 <a
href="ch-customized-programs.html#s-arch-spec">アーキテクチャ指定のための文字列,
Section 11.1</a> で規定されています。
</p>
</li>
</ul>
<ul>
<li>
<p>
アーキテクチャワイルドカードは Debian マシンアーキテクチャの集合を指定します。
<a
href="ch-customized-programs.html#s-arch-wildcard-spec">アーキテクチャワイルドカード,
Section 11.1.1</a> を参照ください。 <samp>any</samp> は全ての Debian
マシンアーキテクチャにマッチし、もっとも良く使われます。
</p>
</li>
</ul>
<ul>
<li>
<p>
<samp>all</samp>
これはアーキテクチャに依存しないパッケージであることを示します。
</p>
</li>
</ul>
<ul>
<li>
<p>
<samp>source</samp> 、つまりソースパッケージであることを示します。
</p>
</li>
</ul>

<p>
ソースパッケージの中の、メインの <code>debian/control</code>
ファイル中では、このフィールドには特別な値 <samp>all</samp> または
特別のアーキテクチャワイルドカード <samp>any</samp>
か、スペースで区切られた複数のアーキテクチャまたはアーキテクチャワイルドカードのリストが許されます。
<samp>any</samp> または <samp>all</samp>
が記載されている場合、これ以外の値を書くことは許されません。
殆どのパッケージでは、<samp>any</samp> または <samp>all</samp>
のいずれかを使うことになります。
</p>

<p>
特定のアーキテクチャのリストを使う場合、そのソースからはフィールドに含まれているアーキテクチャのみに依存したパッケージがビルドされることを意味します。
特定のアーキテクチャのワイルドカードリストを使う場合、そのソースがフィールドにマッチするアーキテクチャのみに依存したパッケージをビルドすることを意味します。
アーキテクチャのリストや、<samp>any</samp>
以外のアーキテクチャワイルドカードを使う場合は例外的で、プログラムに可搬性がないか、一部のアーキテクチャでは役に立たない場合です。
一般的に言って、可能な限りプログラムは可搬性を持つように作成すべきです。
</p>

<p>
Debian ソースコントロールファイル <code>.dsc</code>
の中には、スペースで区切られたアーキテクチャまたはアーキテクチャワイルドカードのリストを書くことができます。
リストにアーキテクチャワイルドカード <samp>any</samp> が含まれる場合は、他には
<samp>all</samp> のみをリストに記載することが許されます。
</p>

<p>
リストには、特別な値 <samp>all</samp> (あるいはそれのみ)
を記載することが許されます。言い方を変えれば、<code>.dsc</code> は
<code>debian/control</code> とは異なり、<samp>all</samp>
は他の特定のアーキテクチャとの組み合わせとして記載することが許されています。
Debian ソースコントロールファイル <code>.dsc</code> 中の
<samp>Architecture</samp> フィールドは、通常はソースパッケージの
<code>debian/control</code> の <samp>Architecture</samp>
フィールドから作成されます。
</p>

<p>
<samp>any</samp>
のみを指定した場合、ソースパッケージは特定のアーキテクチャへの依存性はなく、どのアーキテクチャでもコンパイルできるはずであるということを意味します。
作成されたバイナリパッケージ (群)
は、現在のビルドアーキテクチャ依存になります。
</p>

<p>
単に <samp>all</samp>
は、ソースパッケージは特定のアーキテクチャに依存しないパッケージのみをビルドすることを示しています。
</p>

<p>
<samp>any all</samp>
が指定されている場合は、ソースパッケージは特定のいずれのアーキテクチャにも依存しないことを示しています。
作成されるバイナリパッケージには、少なくともひとつのアーキテクチャ依存のパッケージと、ひとつのアーキテクチャ非依存のパッケージが含まれます。
</p>

<p>
アーキテクチャあるいはアーキテクチャワイルドカードが列挙されている場合、
ソースはアーキテクチャ依存としてビルドされることを示しており、列挙されているか、あるいはワイルドカードにマッチしたアーキテクチャでのみ動作します。
ソースパッケージから少なくとも一種類のアーキテクチャ非依存のパッケージがビルドされる場合、リストに
<samp>all</samp> を含めてください。
</p>

<p>
<code>.changes</code> ファイルの中の、<samp>Architecture</samp>
フィールドは、そのパッケージが現在対応しているアーキテクチャを示します。
これはリストです。もし、特別なエントリ <samp>source</samp>
があれば、そのパッケージのソースもアップロードされます。
アーキテクチャ非依存のパッケージがアップロードされる場合、<samp>all</samp>
を含めます。<code>.changes</code> ファイルの <samp>Architecture</samp>
フィールドに <samp>any</samp>
などのアーキテクチャワイルドカードが現れてはいけません。
</p>

<p>
パッケージ構築の過程でアーキテクチャをどのように得るかについての情報は、 <a
href="ap-pkg-sourcepkg.html#s-pkg-debianrules"><code>debian/rules</code> -
メイン構築スクリプト, Section C.2.1</a> を見てください。
</p>

<hr>

<h3 id="s-f-Essential">5.6.9 <samp>Essential</samp></h4>

<p>
これは、バイナリパッケージのコントロールファイル中、またはソース制御データファイル中のパッケージごとの段落に記述される
yes/no の値です。
</p>

<p>
<samp>yes</samp> とセットしてあった場合、<code>dpkg</code> と
<code>dselect</code> は、このパッケージの削除を行ないません
(更新と置きかえは可能です)。 <samp>no</samp>
の場合は、このフィールドを持っていない場合と同じです。
</p>

<hr>

<h3 id="s5.6.10">5.6.10 パッケージ間の関連性を示すフィールド: <samp>Depends</samp>, <samp>Pre-Depends</samp>, <samp>Recommends</samp>, <samp>Suggests</samp>, <samp>Breaks</samp>, <samp>Conflicts</samp>, <samp>Provides</samp>, <samp>Replaces</samp>, <samp>Enhances</samp></h4>

<p>
これらのフィールドは、パッケージ間の関係を記述します。 <a
href="ch-relationships.html">パッケージ間の関連性の宣言, Chapter 7</a>
を参照してください。
</p>

<hr>

<h3 id="s-f-Standards-Version">5.6.11 <samp>Standards-Version</samp></h4>

<p>
パッケージが準拠した最新の標準 (Debian
ポリシーマニュアルおよびそれに関連するテキスト) のバージョンです。
</p>

<p>
バージョンナンバーはメジャー及びマイナーバージョン番号、さらにメジャー及びマイナーパッチレベルの
4 つの数字からなります。
全てのパッケージに渡って変更がなされる場合には、メジャーバージョンを変更します。
多くのパッケージに修正作業が必要な重要な変更の場合には、マイナーバージョンを変更することによってそれを示します。
パッチレベルについては、小さい変更であっても、それが基準の変更を伴う場合にはメジャーパッチレベルの変更となります。
マイナーパッチレベルの変更となるのは、表面上の変更、文書上の修正など意味合いの変化の無い場合、
またパッケージの内容に関して影響を与えない場合になります。
</p>

<p>
従ってマニュアルバージョンのうち最初の 3
つの数字が一般的なバージョンとしての意味を持ちます。 この数字を 3 つにするか 4
つ全てを使ってバージョンを規定するかは、オプション [<a
href="footnotes.html#f36" name="fr36">36</a>] です。
</p>

<hr>

<h3 id="s-f-Version">5.6.12 <samp>Version</samp></h4>

<p>
パッケージのバージョン番号です。書式は、
[<var>epoch</var><samp>:</samp>]<var>upstream_version</var>[<samp>-</samp><var>debian_revision</var>]
です。
</p>

<p>
バージョンを構成する 3 つの要素は
</p>
<dl>
<dt><var>epoch</var></dt>
<dd>
<p>
これは一桁の符号なし整数です。普通は小さい数になるはずです。
ゼロと仮定して良い場合は省略できます。省略した時には、
<var>upstream_version</var> にコロンを含めてはいけません。
</p>

<p>
これはパッケージの古いバージョンのバージョン番号の誤りを許したり、
パッケージの以前のバージョン番号体系をそのままに残しておくためにあります。
</p>
</dd>
</dl>
<dl>
<dt><var>upstream_version</var></dt>
<dd>
<p>
これがバージョン番号の主要部分です。通常支障ない場合は <code>.deb</code>
ファイルが作られたオリジナルの (&quot;上流の&quot;)
パッケージのバージョン番号になります。
普通は上流の作者によって定められたものと同じ形式になりますが、
パッケージ管理システムと比較手法に沿って修正を加えなければならないかもしれません。
</p>

<p>
<var>upstream_version</var>
に関するパッケージ管理システムの比較の挙動については次節で述べます。
バージョン番号中で、この <var>upstream_version</var> の部分は必須です。
</p>

<p>
<var>upstream_version</var> は英数字 [<a href="footnotes.html#f37"
name="fr37">37</a>] と文字 <samp>.</samp> <samp>+</samp> <samp>-</samp>
<samp>:</samp> <samp>~</samp> (ピリオド、プラス、ハイフン、コロン、チルド)
だけから構成されており、数字で始まるようにすべきです。
ただし、<var>debian_revision</var> がない場合、ハイフンは許されません。
また、<var>epoch</var> がない場合、コロンは許されません。
</p>
</dd>
</dl>
<dl>
<dt><var>debian_revision</var></dt>
<dd>
<p>
バージョンのこの部分は、そのパッケージを Debian
バイナリパッケージにするためにほどこした修正のバージョン番号を表わしています。
これは英数字と <samp>+</samp> <samp>.</samp> <samp>~</samp> (プラス、ピリオド
およびチルド) の三記号のみからなり、<var>upstream_version</var>
と同じやり方で比較されます。
</p>

<p>
この部分はオプションです。 <var>debian-revision</var> を持たない場合には、
<var>upstream-version</var> はハイフンを含んでいてはいけません。 この
<var>debian-revision</var> を持たない形式のものは Debian
パッケージとして特別に書かれたソフトウェアであることを示しています。
その場合、Debian
パッケージソースは元のソースと常に同一の筈ですから、レビジョンの追加は必要ありません。
</p>

<p>
<var>upstream_version</var> が増加するたびに、 <var>debian_revision</var> を
<samp>1</samp> に戻すのが慣習となっています。
</p>

<p>
パッケージ管理システムは文字列中の最後のハイフン (あれば)
のところでバージョン番号を <var>upstream_version</var> と
<var>debian_revision</var> とに分割しようとします。 <var>debian_revision</var>
がないものは、<var>debian_revision</var> が <samp>0</samp> と等価です。
</p>
</dd>
</dl>

<p>
二つのバージョン番号を比較する場合には、まず <var>epoch</var>
値が比較され、次に <var>epoch</var> が同じである場合には
<var>upstream_version</var> が比較され、さらに <var>upstream_version</var>
も同じである場合には <var>debian_revision</var> が比較されます。
<var>epoch</var> は数字として比較されます。 <var>upstream_version</var> と
<var>debian_revision</var>
の部分はパッケージ管理システムによって、以下記載のアルゴリズムを用いて比較されます。
</p>

<p>
文字列は左から右へと比較されます。
</p>

<p>
最初に、比較対象となる2つの文字列の中で、全て非数字で構成される最初の部分を決定します。
両方の文字列に対する、この数字でない部分
(そのうちの一つは空であってもかまいません) を辞書順で比較します。
もし違いが見つかれば、それを返します。
ここでの辞書順とは、すべての文字が非文字より先に来る様に修正し、
更にチルドがもっとも前に来る修正 (行末の空文字列より更に前) を加えた ASCII
順です。 例えば、以下の各部分は早いほうから遅いほうへの順でソートされます。
<samp>~~</samp>, <samp>~~a</samp>, <samp>~</samp>, 空部分, <samp>a</samp>[<a
href="footnotes.html#f38" name="fr38">38</a>]。
</p>

<p>
次に、二つの文字列の残りの部分から、全て数字で構成される最初の部分を決定します。
この二つの数値を比較し、比較結果として見つかった違いを返します。
このとき、空文字列
(比較している一方もしくは双方のバージョン文字列の末尾においてのみ生じる) は 0
として数えます。
</p>

<p>
違いが見つかるか、双方の文字列を調べ尽くすかするまで、この二つのステップを
(先頭から、最初の非数字の文字列と最初の数字の文字列を比較し、切り離しながら)
繰り返します。
</p>

<p>
epoch
の目的はバージョン番号付けのミスをそのままにできるようにするため、またバージョン番号の付け方を変更した場合に、
それをうまく扱えるようにするためだということに注意してください。
パッケージ管理システムが解釈することのできない文字 (<samp>ALPHA</samp> や
<samp>pre-</samp> など)
から成る文字列を含むバージョン番号や、思慮の浅い順序付け[<a
href="footnotes.html#f39" name="fr39">39</a>] をうまく処理するためでは
<em>ありません</em>。
</p>

<hr>

<h3 id="s-f-Description">5.6.13 <samp>Description</samp></h4>

<p>
ソースおよびバイナリパッケージのコントロールファイル中で、
<samp>Description</samp>
フィールドにはバイナリパッケージの説明が含まれています。
この説明は、概要または短い説明と、長い説明の二つの部分からなります。
このフィールドは複数行を許す行であり、書式を以下に示します。
</p>

<pre>
     	Description: &lt;一行概要&gt;
     	 &lt;複数行にわたる長い説明&gt;
</pre>

<p>
長い説明の部分のフォーマットは以下の通りです。
</p>
<ul>
<li>
<p>
空白一つから始まる行は、パラグラフ (節) の一部です。
この行に引き続く行は、表示の際にはワードラップが行われます。 最初の空白は通常
(表示の際は) 削除されます。
この行には少なくとも一文字の空白以外の文字が含まれていなければいけません。
</p>
</li>
</ul>
<ul>
<li>
<p>
空白二つ以上から始まる行は、そのまま表示されます。
表示部を水平方向に広げられない場合は、表示プログラムは内容を強制改行
(単語境界を考慮せずに改行を挿入する) します。
可能な場合は必要に応じて表示が右側にはみ出します。表示時に、0 個 から 2
個までの空白が削除されますが、この際に削除される空白の個数は各行で一緒です
(従って、インデントされた内容は正しく表示されます)。
この行には少なくとも一文字の空白以外の文字が含まれていなければいけません。
</p>
</li>
</ul>
<ul>
<li>
<p>
空白とドット ('.') のみからなる行は、空行として表示されます。
これは空行を表示させる<em>唯一の</em>手段です[<a href="footnotes.html#f40"
name="fr40">40</a>]。
</p>
</li>
</ul>
<ul>
<li>
<p>
空白、ドットといくつかの文字が続く行形式は将来の拡張用で、使わないでください。
</p>
</li>
</ul>

<p>
タブは使用しないでください。どういう動作をするか分かりません。
</p>

<p>
更に詳しい説明は、<a href="ch-binary.html#s-descriptions">パッケージの説明,
Section 3.4</a> の項を参照ください。
</p>

<p>
<code>.changes</code> ファイル中では、<samp>Description</samp>
フィールドにアップロードされるパッケージの説明文の要約を含んでいます。
この場合、フィールド値の最初の行 (<samp>Description:</samp> などと同じ行の部分)
は常に空行です。
これは複数行を許すフィールドで、パッケージ毎に論理行一行になります。
それぞれの行は、一つの空白でインデントされ、バイナリパッケージ名、空白、
ハイフン
(<samp>-</samp>)、空白、パッケージから持ってきた短い説明という構成になっています。
</p>

<hr>

<h3 id="s-f-Distribution">5.6.14 <samp>Distribution</samp></h4>

<p>
<code>.changes</code> ファイルか changelog の解析出力には、
このパッケージがインストールされていた、またはこれからインストールされるディストリビューションの名前が空白で区切られて含まれます。
有効なディストリビューション名はアーカイブメンテナが決定します。 [<a
href="footnotes.html#f41" name="fr41">41</a>] Debian
アーカイブソフトウェアは、単一のディストリビューションの記載しかサポートしていません。
パッケージを他のディストリビューションに移動する処理はアップロードプロセスの外で行われます。
</p>

<hr>

<h3 id="s-f-Date">5.6.15 <samp>Date</samp></h4>

<p>
このフィールドにはパッケージがビルドされた、または修正された日付を記載します。
これは、<code>debian/changelog</code> エントリの <var>date</var>
と同じフォーマットでなければいけません。
</p>

<p>
このフィールドの値は通常 <code>debian/changelog</code>
ファイルから抽出します。<a href="ch-source.html#s-dpkgchangelog">Debian
changelog: <code>debian/changelog</code>, Section 4.4</a> を参照ください。
</p>

<hr>

<h3 id="s-f-Format">5.6.16 <samp>Format</samp></h4>

<p>
<a href="#s-debianchangesfiles"><code>.changes</code></a>
ファイルでは、このフィールドはこのファイルのフォーマットバージョンを宣言します。
このフィールドの値の書式は <a
href="#s-f-Version">パッケージバージョン番号</a>と同じですが、 epoch や Debian
revision は付けることができません。 書式は、本文書では <samp>1.8</samp>
に記載されています。
</p>

<p>
<a href="#s-debiansourcecontrolfiles">Debian ソースコントロールファイル
<code>.dsc</code></a>
では、このフィールドはソースパッケージのフォーマットを宣言します。
このフィールドの値は、ソースパッケージ内のファイルのリストを解釈し、アンパック方法を判断する必要があるような、パッケージを操作するためのプログラムで用いられます。
このフィールドの値の書式は、数字のメジャーレビジョン、ピリオド、数字のマイナーレビジョンに、その後空白を挟んでオプションのサブタイプが続くものです。
サブタイプがある場合、それはかっこで囲まれた英数字の語です。
サブタイプは書式上は省略可能ですが、特定のソースフォーマットレビジョンでは必須になります
[<a href="footnotes.html#f42" name="fr42">42</a>]。
</p>

<hr>

<h3 id="s-f-Urgency">5.6.17 <samp>Urgency</samp></h4>

<p>
以前のバージョンからのアップグレードがどの程度重要なのかを示します。
以下のいずれかのキーワードのひとつを指定します
<samp>low</samp>、<samp>medium</samp>、 <samp>high</samp>
<samp>emergency</samp> および <samp>critical</samp>[<a
href="footnotes.html#f43" name="fr43">43</a>]
(大文字と小文字は区別されません)。 空白で区切られたコメント
(たいていかっこにはいっています) がオプションとしてつくこともあります。例えば：
</p>

<pre>
       Urgency: low (HIGH for users of diversions)
</pre>

<p>
このフィールドの値は普通は <code>debian/changelog</code> から取ってきます。<a
href="ch-source.html#s-dpkgchangelog">Debian changelog:
<code>debian/changelog</code>, Section 4.4</a> を参照ください。
</p>

<hr>

<h3 id="s-f-Changes">5.6.18 <samp>Changes</samp></h4>

<p>
このフィールドは複数行を許す書式で、人が読むための変更点のデータを示します。
一つ前のバージョンと現在のバージョンとの相違を説明します。
</p>

<p>
フィールドの最初の値 (<samp>Changes:</samp> と同じ行の部分) は必ず空です。
フィールドの値は継続行に記載され、最低限一つの空白によってインデントされます。
空行は、空白とピリオド (<samp>.</samp>) だけからなる行で作成されます。
</p>

<p>
このフィールドの値は普通は <code>debian/changelog</code> から取ってきます。<a
href="ch-source.html#s-dpkgchangelog">Debian changelog:
<code>debian/changelog</code>, Section 4.4</a> を参照ください。
</p>

<p>
それぞれのバージョンの変更情報は、「title」行の後に続きます。
この「title」行には、すくなくとも、バージョン、ディストリビューション、緊急度のフィールドが人が読める形式で含まれています。
</p>

<p>
もし、いくつかのバージョンのバージョン情報が返されるような場合、
最新のバージョンに対するものを最初に返すようにすべきで、
同時に空行を入れてエントリ行を分割してください
(「title」行の後もまた空行のあとで 続けてください)。
</p>

<hr>

<h3 id="s-f-Binary">5.6.19 <samp>Binary</samp></h4>

<p>
このフィールドは折り返しを許す書式で、バイナリパッケージのリストです。
書式と意味は、このフィールドが現れたコントロールパッケージが何であるかに依存します。
</p>

<p>
<code>.dsc</code>
ファイルに記載されている場合は、それはそのソースパッケージが生成できるバイナリパッケージのカンマで区切られたリストです
[<a href="footnotes.html#f44" name="fr44">44</a>]。
ソースパッケージでは、ここに記載された全バイナリパッケージがすべてのアーキテクチャで生成できる必要はありません。
個々のバイナリパッケージがどのアーキテクチャに対応しているかの詳細は、ソースコントロールファイルには含まれません。
</p>

<p>
<code>.changes</code> ファイル中では、このフィールドの値は、
実際にアップロードされているバイナリパッケージの名前の、空白 (カンマではなく)
で区切られたリストです。
</p>

<hr>

<h3 id="s-f-Installed-Size">5.6.20 <samp>Installed-Size</samp></h4>

<p>
このフィールドは、バイナリパッケージのコントロールファイルと
<code>Packages</code>ファイルに現われます。
名前で指定されたパッケージをインストールするのに必要なディスク容量の概算値を示します。
実際に消費されるインストールサイズはブロックサイズ、ファイルシステムプロパティやパッケージメンテナスクリプトでの処理に依存します。
</p>

<p>
ディスク容量は、バイト値のインストールサイズを 1,024
で割って切り上げた整数値です。
</p>

<hr>

<h3 id="s-f-Files">5.6.21 <samp>Files</samp></h4>

<p>
このフィールドは、ファイルとそれについての情報を逐一記したリストから構成されます。
厳密な情報と書式は使われる状況によります。
</p>

<p>
どの場合においても、Files は複数行からなるフィールドです。 フィールドの最初の行
(即ち <samp>Files:</samp> のある行) は空です。 継続行に 1
つのファイルにつき一行の内容が記されます。 このとき各行は 1
つの空白文字でインデントされ、下記の通り、空白で区切られた各サブフィールドが続きます。
</p>

<p>
<code>.dsc</code> ファイルの場合には、このフィールドには tar
ファイルと、ソースパッケージの残りの部分である diff ファイル (存在する場合)
の各々について、MD5 チェックサム、サイズ、ファイル名が記されます。 この diff
ファイルは、ソースパッケージの変更分 [<a href="footnotes.html#f45"
name="fr45">45</a>] です。例を挙げます。
</p>

<pre>
     Files:
      c6f698f19f2a2aa07dbb9bbda90a2754 571925 example_1.2.orig.tar.gz
      938512f08422f3509ff36f125f5873ba 6220 example_1.2-1.diff.gz
</pre>

<p>
ファイル名などの項目についての正確な書式は、 <a
href="ap-pkg-sourcepkg.html#s-pkg-sourcearchives">Source packages as archives,
Section C.3</a> をごらんください。
</p>

<p>
<code>.changes</code> ファイルの場合には、このフィールドには 1
つのアップロードされるファイルごとに 1 行ずつ、 MD5
チェックサムとサイズ、セクション、プライオリティ、ファイル名が記述されます。
以下に例を示します。
</p>

<pre>
     Files:
      4c31ab7bfc40d3cf49d7811987390357 1428 text extra example_1.2-1.dsc
      c6f698f19f2a2aa07dbb9bbda90a2754 571925 text extra example_1.2.orig.tar.gz
      938512f08422f3509ff36f125f5873ba 6220 text extra example_1.2-1.diff.gz
      7c98fe853b3bbb47a00e5cd129b6cb56 703542 text extra example_1.2-1_i386.deb
</pre>

<p>
<a href="#s-f-Section">セクション (section)</a> と<a
href="#s-f-Priority">プライオリティ (priority)</a>
フィールドはメインのソースコントロールファイルの対応する値となります。
もし、セクションとプライオリティが決定されていなかったら、 <samp>-</samp>
を使うべきです。
しかしながら、セクションとプライオリティの値は新しいパッケージを適切にインストールするため、本来指定しておかなければならないものです。
</p>

<p>
<code>.changes</code> ファイルのセクションが <samp>byhand</samp>
という特別の値であれば、該当するファイルは通常のパッケージファイルとして扱えないため、
ディストリビューション管理者が手動でインストールしなければなりません。
セクションが <samp>byhand</samp> 値であった場合には、プライオリティは
<samp>-</samp> にすべきです。
</p>

<p>
新しい Debian
パッケージをリリースする時に、新しい上流のソースアーカイブの配布を伴わない場合、
<code>.dsc</code> ファイルの <samp>Files</samp>
フィールドには、オリジナルのソースアーカイブ
<code><var>package</var>_<var>upstream-version</var>.orig.tar.gz</code>
に対するエントリを含んでいなければなりません。 一方、<code>.changes</code>
ファイル中の <samp>Files</samp>
フィールドにはこのエントリがあってはいけません。
この場合、配布サイトのオリジナルソースアーカイブは、アップロードしようとしている
<code>.dsc</code> ファイルと、diff
ファイルを生成するのに使用されたソースアーカイブとバイト単位で正確に一致していなければなりません。
</p>

<hr>

<h3 id="s-f-Closes">5.6.22 <samp>Closes</samp></h4>

<p>
.changes
ファイルでの変更/修正により、アップロードで閉じられるバグレポート番号を、空白で区切って列挙します。
</p>

<hr>

<h3 id="s-f-Homepage">5.6.23 <samp>Homepage</samp></h4>

<p>
このパッケージのウェブサイトの URL です。
可能な場合、望ましいのは元のソースが入手でき、追加の上流の文書や情報が入手できるようなサイトです。
このフィールドの内容には、<samp>&lt;&gt;</samp> などで囲まない単純な URL
を記載します。
</p>

<hr>

<h3 id="s-f-Checksums">5.6.24 <samp>Checksums-Sha1</samp> および <samp>Checksums-Sha256</samp></h4>

<p>
これらのフィールドは複数行を許す書式で、各ファイルに対しチェックサムとサイズを付けたリストが格納されています。
<samp>Checksums-Sha1</samp> と <samp>Checksums-Sha256</samp>
は同じ書式で、使ったチェックサムアルゴリズムのみ異なります。
<samp>Checksums-Sha1</samp> では SHA-1 が用いられ、
<samp>Checksums-Sha256</samp> では SHA-256 が使われます。
</p>

<p>
<samp>Checksums-Sha1</samp> と <samp>Checksums-Sha256</samp>
は、複数行を許すフィールドです。フィールドの最初の行の値
(つまり、<samp>Checksums-Sha1</samp> や <samp>Checksums-Sha256</samp>
と同じ行にある値) は、常に空白です。
実際のフィールドの内容は継続行に記載され、ファイル一つにつき一行です。
各行は、チェックサム、空白、ファイルサイズ、空白、ファイル名となります。 例を
(<code>.changes</code> ファイルから) 以下に示します。
</p>

<pre>
     Checksums-Sha1:
      1f418afaa01464e63cc1ee8a66a05f0848bd155c 1276 example_1.0-1.dsc
      a0ed1456fad61116f868b1855530dbe948e20f06 171602 example_1.0.orig.tar.gz
      5e86ecf0671e113b63388dac81dd8d00e00ef298 6137 example_1.0-1.debian.tar.gz
      71a0ff7da0faaf608481195f9cf30974b142c183 548402 example_1.0-1_i386.deb
     Checksums-Sha256:
      ac9d57254f7e835bed299926fd51bf6f534597cc3fcc52db01c4bffedae81272 1276 example_1.0-1.dsc
      0d123be7f51e61c4bf15e5c492b484054be7e90f3081608a5517007bfb1fd128 171602 example_1.0.orig.tar.gz
      f54ae966a5f580571ae7d9ef5e1df0bd42d63e27cb505b27957351a495bc6288 6137 example_1.0-1.debian.tar.gz
      3bec05c03974fdecd11d020fc2e8250de8404867a8a2ce865160c250eb723664 548402 example_1.0-1_i386.deb
</pre>

<p>
<code>.dsc</code>
ファイルには、これらのフィールドにはソースパッケージを構成する全てのファイルを列挙します。
<code>.changes</code>
ファイルには、これらのフィールドにはアップロードしようとする全てのファイルを列挙します。
これらのフィールド中のファイルのリストは、<samp>Files</samp>
フィールドのファイルのリストと一致している必要があります。
</p>

<hr>

<h3 id="s5.6.25">5.6.25 <samp>DM-Upload-Allowed</samp></h4>

<p>
廃止になりました。<a href="#s-f-DM-Upload-Allowed">以下を</a>参照ください。
</p>

<hr>

<h3 id="s-f-VCS-fields">5.6.26 バージョンコントロールシステム(VCS) フィールド</h4>

<p>
VCS を使って開発される Debian ソースパッケージが増えてきています。
以下のフィールドは、Debian
ソースパッケージの開発が行われている一般からアクセス可能なリポジトリを示すために用いられます。
</p>
<dl>
<dt><samp>Vcs-Browser</samp></dt>
<dd>
<p>
リボジトリのブラウズのためのウェブインターフェースの URL です。
</p>
</dd>
</dl>
<dl>
<dt><samp>Vcs-Arch</samp>, <samp>Vcs-Bzr</samp> (Bazaar), <samp>Vcs-Cvs</samp>, <samp>Vcs-Darcs</samp>, <samp>Vcs-Git</samp>, <samp>Vcs-Hg</samp> (Mercurial), <samp>Vcs-Mtn</samp> (Monotone), <samp>Vcs-Svn</samp> (Subversion)</dt>
<dd>
<p>
フィールド名で VCS の種別を識別します。
フィールドの値は各バージョンコントロールシステムで一般的なリポジトリの位置を示す書式で、パッケージングのためのリポジトリの位置を決めるのに十分な値にすべきです。
理想的には、この値は Debian
パッケージの新版の開発で用いているブランチの位置も示しているのが望ましいです。
</p>

<p>
Git の場合、この値は URL からなり、オプションとして <samp>-b</samp>
という語と、示されたレポジトリのブランチ名が続きます。これは <samp>git
clone</samp> コマンドの書式に沿ったものです。
ブランチが指定されない場合、パッケージングは既定のブランチで行われているべきです。
</p>

<p>
ひとつのパッケージで、ひとつ以上の VCS を指定しても構いません。
</p>
</dd>
</dl>

<hr>

<h3 id="s-f-Package-List">5.6.27 <samp>Package-List</samp></h4>

<p>
このフィールドは複数行を許す書式で、当該ソースパッケージから作成されるすべてのパッケージを、すべてのアーキテクチャを考慮して列挙します。
このフィールドの最初の行の値は空です。
次の行からは順に各行に一つのバイナリパッケージが、名前、タイプ、セクション、プライオリティの順に空白区切りで列挙されます。
5
番目、およびそれ以降の空白区切りの要素を記載可能で、このフィールドのパーザはそれを許すようにしなければいけません。
パッケージタイプについては <a href="#s-f-Package-Type">Package-Type</a>
フィールドを参照してください。
</p>

<hr>

<h3 id="s-f-Package-Type">5.6.28 <samp>Package-Type</samp></h4>

<p>
1 語からなる単純なフィールドで、パッケージのタイプを記載します。
バイナリパッケージに対しては <samp>deb</samp>
を、マイクロバイナリパッケージには <samp>udeb</samp> を指定します。
ここで定義されていない他のタイプも記載可能です。
ソースパッケージコントロールファイルでは、<samp>Package-Type</samp> には
<samp>deb</samp> を指定せず、省略すべきです。
これは、このフィールドを省略した場合、<samp>deb</samp> とみなされるためです。
</p>

<hr>

<h3 id="s-f-Dgit">5.6.29 <samp>Dgit</samp></h4>

<p>
折り返しされるフィールドで、git のコミットハッシュ (full 形式) 1
つに、将来の拡張時に定義される予定の空白区切りのデータが続きます。
</p>

<p>
ソースパッケージが、<em>dgit-repos</em> という名称の基準となる場所で提供される
Git
リポジトリにて、ここで記載されたコミットと正確に一致しているということを宣言します。
上記の場所は、Debian アーカイブと Git との間の双方向ゲートウェイで、
<code>dgit</code> で利用されます。 当該コミットは、名称が
<samp>refs/dgit/*</samp> に一致する一参照から (少なくとも) 到達可能です。詳細は
<code>dgit</code> のマニュアルを参照ください。
</p>

<hr>

<h2 id="s5.7">5.7 ユーザ定義フィールド</h3>

<p>
Debian
ソースコントロールファイルにはユーザ定義フィールドを追加することができます。
これらは無視され、(例えば)
バイナリやソースパッケージコントロールファイル、アップロードコントロールファイルにはコピーされません。
</p>

<p>
出力ファイルにサポートされていないフィールドを付加したいときは、ここに述べる方法を使用してください。
</p>

<p>
メインのソースコントロールファイルにおいて、<samp>X</samp>
から始まり、<samp>BCS</samp> とハイフン <samp>-</samp>
が続くフィールドは、出力ファイルにコピーされます。
そして出力ファイルには、フィールド名のハイフンの後に続く文字部のみが使用されます。
ここで、<samp>B</samp>
はバイナリパッケージコントロールファイルに使用されるとき、 <samp>S</samp>
Debian ソースコントロールファイルに使用されるとき、 <samp>C</samp>
は、アップロードコントロールファイル (<samp>.changes</samp>)
に使用されるときに使われます。
</p>

<p>
メインのソース情報コントロールファイルが以下のフィールドを含んでいるときは、
</p>

<pre>
       XBS-Comment: I stand between the candle and the star.
</pre>

<p>
バイナリと Debian ソースコントロールファイルは、次のフィールドを含みます。
</p>

<pre>
       Comment: I stand between the candle and the star.
</pre>

<hr>

<h2 id="s-obsolete-control-data-fields">5.8 廃止になったフィールド</h3>

<p>
以下のフィールドは廃止になっており、ポリシィの以前の版に対応したパッケージで指定されているかもしれません。
</p>

<hr>

<h3 id="s-f-DM-Upload-Allowed">5.8.1 <samp>DM-Upload-Allowed</samp></h4>

<p>
Debian メンテナに、対象パッケージを Debian
アーカイブにアップロードして良いことを指示します。 このフィールドの有効な値は
<samp>yes</samp> のみです。 このフィールドは Debian
メンテナの行うアップロードを調整するために使われていました。
この件の詳細は、一般決議 <code><a
href="http://www.debian.org/vote/2007/vote_003">Endorse the concept of Debian
Maintainers (Debian メンテナの考え方を裏書する)</a></code> を参照ください。
</p>

<hr>

<p>
[ <a href="ch-source.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ 5 ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ <a href="ch-files.html">10</a> ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ch-maintainerscripts.html">next</a> ]
</p>

<hr>

<p>
Debian ポリシーマニュアル
</p>

<address>
バージョン 3.9.5.0, 2014-07-03<br>
<br>
<a href="ch-scope.html#s-authors">The Debian Policy Mailing List</a><br>
<br>
</address>
<hr>

