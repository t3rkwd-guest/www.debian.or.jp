[% title = "Debian JP パッケージについて" %]
	<h2>Debian パッケージと Debian JP パッケージ</h2>

	<p>
	<a href="[% wwworg %]/intro/about#what">Debian システム</a>では、
	全てのソフトウェアは<em>パッケージ</em>の形で配布されます。</p>

	<p>
	Debian JP Project では、活動の一環として、
	<em>Debian JP パッケージ</em>の作成・配布を行なっています。
	これには、Debian のより良い国際化のために将来
	Debian に提供する予定であるパッケージ、
	日本語対応のテスト中のパッケージ、
	Debian JP Project で使うパッケージ (bug-ja など)、
	Debian 開発者をめざす人が練習で作ったパッケージなどが含まれています。
	</p>
	<div class="past">
	<p>(なお、Slink-JP 以前とは異なり、
	<a href="/obsolute/jp-release.html">
	Debian JP パッケージは「正式」にはリリースしません</a>)。</p>
	</div>
	</div>

	<div class="section">
	<h2>Debian パッケージの入手について</h2>

	<p>
	Debian パッケージの一覧、検索、入手については、
	<a href="[% wwworg %]/distrib/packages">Debian
	パッケージのウェブページ</a>をご覧ください。</p>
	</div>

	<div class="section">
	<h2>Debian JP パッケージの入手について</h2>
	<p>
	Debian JP パッケージは以下を apt line に加えることで手軽に導入可能です。</p>
	<p>
	<kbd>deb http://http.debian.or.jp/debian-jp/ [dist] main contrib non-free</kbd></p>
	<p>
	([dist] の部分には stable/testing/unstable のいずれかが入ります)</p>
	<p>
	また、上記以外にミラーしているサーバがありますので、
	近くの <a href="/using/mirror.html">ミラーサイト</a> を探して導入してください。</p>
	<p>
	これらのパッケージは正しく動作するように作成されていますが基本的に無保証です。
	実際にインストールして利用する場合は、各自の責任 (at your own risk)
	において行なってください。</p>

	<p>
	またこれらのパッケージについて、
	もし「バグかな ? 」と思われる不具合を発見した場合は、
	<a href="/Bugs/index.html">バグ情報</a>
	のページで既に同様な問題についての報告があるかどうかを調べた上で、
	それが未報告の問題であれば
	<a href="../bugreport.html">バグレポート</a> を
	<a href="mailto:submit&#64;bugs.debian.or.jp">submit&#64;bugs.debian.or.jp</a>
	へお送り頂けると、パッケージ開発の参考になります。よろしくお願いします。</p>

<!--
	</div>
	<div class="section">
	<H2>Debian JP パッケージリスト</H2>

	<DL>
	<DT><A href="http://packages.debian.or.jp/stable-jp/">安定版の 
    main, contrib, non-free ディストリビューションの JP パッケージ</A></dt>
	<DD>Debian の最新の安定版向けパッケージです。
		<DL>

		<DT>Main</dt>
		<DD><A HREF="[% wwworg %]/social_contract#guidelines">Debian
		フリーソフトウェアガイドライン</A>に適合するライセンスを持つ
		ソフトウェアが収録されています。全てのパッケージは
		<A HREF="[% wwworg %]/intro/free">自由</A>
		に使用することが可能です。
		また、これらのパッケージはソースコードと一緒に自由に再配布することが
		許されています。</dd>

		<DT>Contrib</dt>
		<DD>それ自体はフリーですが、他の Non-Free に属するパッケージに
		依存関係のあるパッケージです。</dd>

		<DT>Non-Free</dt>
		<DD>何らかの理由で
		<A href="[% wwworg %]/social_contract#guidelines">Debian
		フリーソフトウェアガイドライン</A>に合わないライセンスのパッケージです。</dd>

		</dl>
	</dd>

	<DT><A href="http://packages.debian.or.jp/unstable-jp/">開発版の 
	main, contrib, non-free ディストリビューションの JP パッケージ</A></dt>
	<DD>ここには現在開発中のパッケージが含まれています。
    これらはまだ完全にテストされていないもの
    (そのためあなたのシステムを壊す危険性があります)
    です。これらのパッケージのうち、多くの部分は
    <A href="[% wwworg %]/">Debian Project</A>への提供と
    <A href="[% wwworg %]/intro/about#what">Debian システム</A>
    の公式なパッケージとして登録するための準備が進行中です。</dd>
	</DL>
-->
