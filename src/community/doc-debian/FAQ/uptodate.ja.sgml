<!--
<chapt id="uptodate">Keeping Your Debian System Up To Date
-->
<chapt id="uptodate">Debian システムを最新に保つには

<!--
<p>One of Debian goals is to provide a consistant upgrade path and a secure
upgrade process, and we always do our best to make the new release smoothly
upgradable from the previous ones. In case there's some important note to
add to the upgrade process, the packages will alert the user, and often
provide a solution to a possible problem.
-->
<p>Debian の目標の一つは首尾一貫したアップグレード方針と安全なアップグレー
ド手順を提供することであり、前のリリースから新リリースにスムーズにアップ
グレードできるよう我々は常に最善を尽しています。アップグレード手順に加え
るべき何か重要な注記事項がある場合には、パッケージはユーザに注意を促し、
ありうる問題への解決法を提供します。

<!--
<p>You should also read the Release Notes, document that describes the
details of specific upgrades, shipped on all Debian CDs, and available on
the WWW at <url id="http://www.debian.org/releases/stable/i386/release-notes/">.
-->
<p>アップグレードの詳細を具体的に解説した『リリースノート』も読んでおく
べきです。これはすべての Debian CD と一緒に出荷されており、WWW でも
<url id="http://www.debian.org/releases/stable/i386/release-notes/"> で
利用可能です。

<!--
<sect id="libc5to6upgrade">How can I upgrade my Debian 1.3.1 (or earlier)
  distribution, based on libc5, to 2.0 (or later), based on libc6?
-->
<sect id="libc5to6upgrade">libc5 に基づく私の Debian 1.3.1 (あるいはそれ
以前) を libc6 に基づく2.0 (あるいはそれ以降) にアップグレードするにはど
うすればいいのですか?

<!--
<p>There are several ways to upgrade:
<list>
  <item>Using a simple shell script called <tt>autoup.sh</tt> which upgrades
    the most important packages. After <tt>autoup.sh</tt> has done his job,
    you may use dselect to install the remaining packages <em>en masse</em>.
    This is probably the recommended method, but not the only one.
    <p>Currently, the latest release of <tt>autoup.sh</tt> may be found on the
    following locations:
    <list>
      <item><url id="http://www.debian.org/releases/2.0/autoup/">
      <item><url id="http://www.taz.net.au/autoup/">
      <item><url id="http://csanders.vicnet.net.au/autoup/">
    </list>
  <item>Following closely the <url name="libc5-libc6-Mini-HOWTO"
    id="ftp://ftp.debian.org/pub/debian/doc/libc5-libc6-Mini-HOWTO.txt"> and
    upgrade the most important packages by hand. <tt>autoup.sh</tt> is based
    on this Mini-HOWTO, so this method should work more or less like using
    <tt>autoup.sh</tt>.
  <item>Using a libc5-based <tt>apt</tt>. APT stands for A Package Tool, and
    it might replace dselect some day. Currently, it works just as a
    command-line interface, or as a dselect access method. You will find a
    libc5 version in the <tt>dists/slink/main/upgrade-older-i386</tt>
    directory at the Debian archives.
  <item>Using just dselect, without upgrading any package by hand
    first. It is highly recommended that you do NOT use this method
    if you can avoid it, because dselect alone currently does not install
    packages in the optimal order. APT works much better and it is safer.
    <!&#45;&#45; This should probably work if dpkg's max-error-before-stop internal
    variable is increased. Question: Will it be increased some day? &#45;&#45;>
</list>
-->
<p>アップグレードするにはいくつかの方法があります。
<list>
  <item>重要なパッケージの大部分をアップグレードする
    <tt>autoup.sh</tt> という簡単なシェルスクリプトを使う。
    <tt>autoup.sh</tt> がその仕事を終えたあと、dselect を使って残りのパッ
    ケージを<em>まとめて</em>インストールできます。おそらくこれが推奨さ
    れる方法ですが、唯一の方法ではありません。
    <p>現在 <tt>autoup.sh</tt> の最新リリースは以下にあります。
    <list>
<!--
      <item><url id="http://www.debian.org/releases/2.0/autoup/">
      11 月 10 日現在、このディレクトリは存在しませんでした。
-->
      <item><url id="http://www.taz.net.au/autoup/">
      <item><url id="http://csanders.vicnet.net.au/autoup/">
    </list>
  <item><url name="libc5-libc6-Mini-HOWTO"
    id="ftp://ftp.debian.or.jp/debian-jp/doc-jp/libc5-libc6-Mini-HOWTO-ja.txt">
    に厳密に従って、ほとんどの重要なパッケージを手動でアップグレードする。
    <tt>autoup.sh</tt> はこの mini-HOWTO を元にしているので、この方法は
    およそ <tt>autoup.sh</tt> を使うのと同じように動作するはずです。
  <item>libc5 ベースの <tt>apt</tt> を使う。APT は A Package Tool の略で、
    いつか dselect に取って代わるかもしれないものです。現在はコマンドラ
    インインタフェースか dselect のアクセスメソッドとしてしか動きません。
    libc5 バージョンが Debian アーカイブの
    <tt>dists/slink/main/upgrade-older-i386</tt> ディレクトリにあります。
  <item>最初に手動ではどんなパッケージのアップグレードせず、単に
    dselect を使う。可能な限りこの方法を使わ<em>ない</em>ことを強くお勧
    めします。今のところ dselect だけではパッケージは最適な順序でインス
    トールされません。APT の方がよりよいですし安全です。
    <!-- This should probably work if dpkg's max-error-before-stop internal
    variable is increased. Question: Will it be increased some day? -->
</list>

<!--
This paragraph is obsolete, but I will keep it here as a reminder in
case libc6-based dpkg happen to be some better than the one in Debian
1.3.1: Note that the version of <tt>dpkg</tt> in this directory has the
a.out binary format.  The versions of <tt>dpkg</tt> in the development and
stable trees have the ELF format.
-->

<!--
<sect id="howtocurrent">How can I keep my Debian system current?
-->
<sect id="howtocurrent">Debian システムを最新に保つにはどうすればいいの
ですか?

<!--
<p>One could simply execute an anonymous ftp call to a Debian archive, then
peruse the directories until he finds the desired file, and then fetch it,
and finally install it using <tt>dpkg</tt>.  Note that <tt>dpkg</tt>
will install upgrade files in place, even on a running system.
Sometimes, a revised package will require the installation of a newly revised
version of another package, in which case the installation will fail
until/unless the other package is installed.
-->
<p>単純に Debian アーカイブに匿名 ftp をかけ、次にディレクトリをよく調べ
て、欲しいファイルを見つけ、それを取ってきます。最後に <tt>dpkg</tt> を
使ってそのファイルをインストールします。たとえシステムが稼動していても、
<tt>dpkg</tt> はアップグレードする必要のあるファイルをきちんとインストー
ルしてくれます。あるパッケージをアップグレードしようとした時、他のパッケー
ジもアップグレードするように要求される場合があります。この場合、指示され
たアップグレードを行うまで、あるいは行わなければ、希望するパッケージのアッ
プグレードは失敗してしまいます。

<!--
<p>Many people find this approach much too time-consuming, since Debian
evolves so quickly &#45;&#45; typically, a dozen or more new packages are uploaded
every week.  This number is larger just before a new major release.
To deal with this avalanche, many people prefer to use an automated
programs.  Several different packages are available for this purpose:
-->
<p>この方法はあまりにも時間がかかり過ぎると多くの人が感じるでしょう。と
いうのも Debian は急速に進化しているからです。通常、一週間に 1 ダース以
上もの新しいパッケージがアップロードされています。新しいメジャーリリース
の直前にはもっと増えます。このような大変なアップグレード作業を自動化して
くれるプログラムを使いたいという人が多くいます。このためにいくつかのパッ
ケージが利用できます。

<sect1 id="apt">APT

<!--
<p>APT is a management system for software packages, namely Debian binary
and source packages. apt-get is the command-line tool for handling packages,
and APT dselect method is an interface to APT through <prgn/dselect/, both
of which provide a simpler, safer way to install and upgrade packages.
-->
<p>APT はソフトウェアパッケージ、言い換えると Debian のバイナリとソース
のパッケージのための管理システムです。パッケージを扱うコマンドラインツー
ルとして apt-get があり、<prgn/dselect/ 経由の APT インタフェースとして
APT dselect メソッドがあります。

<!--
<p>APT features complete installation ordering, multiple source capability
and several other unique features, see the User's Guide in
<tt>/usr/share/doc/apt/guide.html/index.html</tt>.
-->
<p>APT の特徴はインストールの順序が完璧であること、複数のソースを利用で
きることです。他にもいくつかありますので、
<tt>/usr/share/doc/apt/guide.html/index.html</tt> のユーザーズガイドを見
てください。

<!-- more details are missing... -->

<sect1 id="dpkg-ftp">dpkg-ftp

<!--
<p>This is an access method for <prgn/dselect/. It can be invoked from
within <prgn/dselect/, thereby allowing a user the ability to download
files and install them directly in one step.  To do this, bring up the
<prgn/dselect/ program, choose option "0" ("Choose the access method
to use"), highlight the option "ftp" then specify the remote hostname and
directory. <prgn/dpkg-ftp/ will then automatically download the files
that are selected (either in this session of <prgn/dselect/ or earlier
ones).
-->
<p>この方法は <prgn/dselect/ のアクセスメソッドの一つになっています。
<prgn/dselect/ の中から実行できるので、ファイルをダウンロードしてインス
トールするという作業を一度に直接行うことができます。やり方は、まず
<prgn/dselect/ プログラムを起動してオプション「0」("Choose the access
method to use") を選択します。そしてオプション「ftp」を反転表示させてリ
モートホスト名とディレクトリを指定します。<prgn/dpkg-ftp/ は今回新たに選
択したファイルや以前に選択したファイルを自動的にダウンロードしてくれます。

<!--
<p>Note that, unlike the <prgn/mirror/ program, <prgn/dpkg-ftp/ does
not grab everything at a mirror site. Rather, it downloads only those
files which you have selected (when first starting up <prgn/dpkg-ftp/),
and which need to be updated.
-->
<p><prgn/mirror/ プログラムと違って <prgn/dpkg-ftp/ はミラーサイトの全て
のファイルを取ってくるわけではありません。このプログラムは
(<prgn/dpkg-ftp/ を立ち上げた時に) 選択したパッケージのうちアップグレー
ドする必要のあるファイルだけをダウンロードします。

<sect1 id="mirror">mirror

<!--
<p>This Perl script, and its (optional) manager program called
<prgn/mirror-master/, can be used to fetch user-specified parts of
a directory tree from a specified host <em>via</em> anonymous FTP.
-->
<p>この Perl スクリプトと <prgn/mirror-master/ という (付属の) 管理プロ
グラムは、ユーザが指定したホストから匿名 ftp を<em>使って</em>指定したディ
レクトリツリーを取ってくるのに使います。

<!--
<p><prgn/mirror/ is particularly useful for downloading large volumes of
software.  After the first time files have been downloaded from a site, a
file called <tt>.mirrorinfo</tt> is stored on the local host.  Changes to
the remote filesystem are tracked automatically by <prgn/mirror/, which
compares this file to a similar file on the remote system and downloads only
changed files.
-->
<p><prgn/mirror/ は大量のソフトウェアをダウンロードする場合に特に役立ち
ます。あるサイトから初めてファイルをダウンロードすると、
<tt>.mirrorinfo</tt> と呼ばれるファイルがローカルホストに作成されます。
<prgn/mirror/ がこのファイルとリモートシステムにある同様のファイルを比較
することによって、リモートファイルシステムの変更が自動的に認識され、変更
されたファイルだけがダウンロードされます。

<!--
<p>The <prgn/mirror/ program is generally useful for updating local copies
of remote directory trees.  The files fetched need not be Debian files.
(Since <prgn/mirror/ is a Perl script, it can also run on non-Unix systems.)
Though the <prgn/mirror/ program provides mechanisms for excluding files
names of which match user-specified strings, this program is most useful
when the objective is to download whole directory trees, rather than
selected packages.
-->
<p><prgn/mirror/ プログラムは一般にリモートディレクトリツリーのローカル
なコピーを更新するのに役立ちます。取ってくるファイルは Debian ファイルで
ある必要はありません。(<prgn/mirror/ は Perl スクリプトなので非 Unix シ
ステム上でも動かせます。) <prgn/mirror/ プログラムにには指定した文字列に
マッチする名前のファイルを除外する機能もありますが、このプログラムはパッ
ケージを選択してダウンロードするよりもディレクトリツリー全体をダウンロー
ドするのに大変役立ちます。

<!-- Should we recommend GNU wget here, too? -->

<sect1 id="dftp">dftp

<!--
<p>This Perl script can be used to fetch user-specified Debian packages from
a specified host.  It begins by downloading the Packages.gz files for the
directories specified by the user (e.g. stable, contrib, non-free) and
presents him with a list of packages.  These are placed in various
sections: new upgrades, downgrades, new packages, ignored upgrades, and
ignored packages.  The user then selects the packages he wants and dftp
downloads and installs them.  This makes it very easy to have your Debian
system 100% current all the time without downloading packages that you are
not going to install.
-->
<p>この Perl スクリプトは特定のホストからユーザ指定の Debian パッケージ
を取ってくるのに使えます。このプログラムは、ユーザが指定したディレクトリ
(たとえば、stable、contrib、non-free) の Packages.gz ファイルをダウンロー
ドして、そのディレクトリに含まれるパッケージの一覧を表示します。ファイル
は new upgrades、downgrades、new packages、ignored upgrades、ignored
packages などの様々なセクションに置かれます。それから欲しいパッケージを
選択すると dftp はそれをダウンロードしてインストールします。これによって
インストールするつもりのないパッケージをダウンロードすることなく、非常に
簡単に Debian システムを常に最新にしておくことができます。

<!--
<p><prgn/dftp/ can be used to call <prgn/dselect/ (see <ref id="dselect">),
thereby providing an integrated way to fetch and update the Debian packages
on one's system.  When the installation is finished, another <prgn/dftp/
command can be used to remove the package archive (".deb") files.
Changes to the remote filesystem are tracked automatically by <prgn/dftp/,
which compares the local Packages.gz file to the files on the remote system.
-->
<p><prgn/dftp/ は <prgn/dselect/ (<ref id="dselect"> 参照) を呼び出すこ
とができるので、Debian パッケージのシステムへの取得と更新を統合して行え
ます。インストールが終了すると別の <prgn/dftp/ コマンドでパッケージアー
カイブ (「.deb」) ファイルを消去することができます。リモートファイルシス
テムの変更は <prgn/dftp/ がローカルとリモートシステムの Packages.gz ファ
イルを比較することによって、自動的に認識されます。

<sect1 id="dpkg-mountable">dpkg-mountable

<!--
<p>dpkg-mountable adds an access method called `mountable' to dselect's
list, which allows you to install of any filesystem specified in /etc/fstab
(e.g. a normal hard disk partition, or an NFS server), which it will
automatically mount and umount for you if necessary.
-->
<p>dpkg-mountable を使うと dselect のリストに「mountable」というアクセス
メソッドが加わります。こうすると /etc/fstab に指定されている任意のファイ
ルシステム (例えば通常のハードディスクパーティションや NFS サーバなど)
からインストールできるようになります。これらのファイルシステムは必要に応
じて自動的にマウントされアンマウントされます。

<!--
<p>It also has some extra features not found in the standard dselect
methods, such as provision for a local file tree (either parallel to the
main distribution or totally separate), and only getting packages which are
required, rather than the time-consuming recursive directory scan, as well
as logging of all dpkg actions in the install method.
-->
<p>dselect の標準的なメソッドにない特別な機能もいくつかあります。ローカ
ルのファイルツリー (メインのディストリビューションと並行か全く別か) を指
定できたり、時間をかけてディレクトリを再帰的に見渡すのではなく必要なパッ
ケージだけを取得できたりします。インストールメソッドで dpkg アクションの
すべてを記録することもできます。

<!--
<sect id="upgradesingle">Must I go into single user mode in order to
  upgrade a package?
-->
<sect id="upgradesingle">パッケージをアップグレードするにはシングルユー
ザモードに入る必要がありますか?

<!--
<p>No.  Packages can be upgraded in place, even in running systems.
Debian has a <tt>start-stop-daemon</tt> program that is invoked to stop,
then restart running process if necessary during a package upgrade.
-->
<p>
いいえ、その必要はありません。システム稼動中であってもパッケージをきちん
とアップグレードすることができます。Debian には
<tt>start-stop-daemon</tt> プログラムがあり、パッケージをアップグレード
する時にもし必要なら、動いているプロセスをストップしてその後再びスタート
するという作業を行ってくれます。

<!--
<sect id="savedebs">Do I have to keep all those .deb archive files on
  my disk?
-->
<sect id="savedebs">自分のディスクに全ての .deb アーカイブファイルを保存
しておかなければならないのですか?

<!--
<p>No.  If you have downloaded the files to your disk (which is not
absolutely necessary, see above for dpkg-ftp or dftp descriptions),
then after you have installed the packages, you can remove them from
your system.
-->
<p>いいえ、その必要はありません。もし自分のディスクにファイルをダウンロー
ドしているなら (これは必ずしも必要なことではありません。上記の
dpkg-ftp や dftp を見てください)、そのパッケージをインストールした後、そ
のファイルをシステムから消去しても構いません。

<!--
<sect id="keepingalog">How can I keep a log of the packages I added to
  the system?
-->
<sect id="keepingalog">システムに追加したパッケージのログを保存するには
どうしたらいいのですか?

<!--
<p><prgn/dpkg/ keeps a record of the packages that have been unpacked,
configured, removed, and/or purged, but does not (currently) keep a log
of terminal activity that occured while a package was being so manipulated.
Some users overcome this simply by using <prgn/tee/, like this:
<example>
dpkg -iGOEB -R main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</example>
-->
<p><prgn/dpkg/ は、展開したり設定したり削除したり完全削除したりしたパッ
ケージの記録をとっていますが、パッケージが処理される間の端末表示のログは
(現在のところは) 保存しません。あるユーザはこの問題を以下のように
<prgn/tee/ コマンドを使うことで簡単に克服しています。
<example>
dpkg -iGOEB -R main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</example>

<!--
The same command written using long options:
<example>
dpkg &#45;&#45;install &#45;&#45;refuse-downgrade &#45;&#45;selected-only \
  &#45;&#45;skip-same-version &#45;&#45;auto-deconfigure \
  &#45;&#45;recursive main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</example>
-->
長いオプションを用いて書くと次のようになります。
<example>
dpkg --install --refuse-downgrade --selected-only \
  --skip-same-version --auto-deconfigure \
  --recursive main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</example>
