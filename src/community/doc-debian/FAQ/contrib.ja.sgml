<!--
<chapt id="contributing">Contributing to the Debian project
-->
<chapt id="contributing">Debian プロジェクトに貢献する

<!--
<p>Donations of time (to develop new packages, maintain existing packages,
or provide user support), resources (to mirror the FTP and WWW archives),
and money (to pay for new testbeds as well as hardware for the archives)
can help the project.
-->
<p>時間の寄付 (新たなパッケージを開発する、既存のパッケージの保守を行な
う、あるいはユーザサポートを提供する)、資源の寄付 (FTP や WWW のアーカイ
ブのミラーをする)、そして金銭の寄付 (アーカイブのためのハードウェアの他、
新たなテストベッドにお金を出す) はプロジェクトの助けになります。

<!--
<sect id="contrib">How can I become a Debian software developer?
-->
<sect id="contrib">Debian のソフトウェア開発者になるにはどうすればよいの
ですか?

<!--
<p>The development of Debian is open to all, and new users with the right
skills and/or the willingness to learn are needed to maintain existing
packages which have been "orphaned" by their previous maintainers, to
develop new packages, and to provide user support.
-->
<p>Debian の開発はすべての人々に公開されています。前の保守担当者によって
「みなしご化」された既存パッケージの保守を行なったり、新たなパッケージを
開発したり、ユーザサポートを提供したりするだけの確かな腕を持っている、あ
るいは、それらを進んで習得しようとする新規ユーザが必要とされています。

<!--
<p>All the details about becoming a Debian developer may now be found in the
<package/developers-reference/ package. You should therefore install this
package and read it carefully.
-->
<p>Debian の開発者になるための詳細はすべて
<package/developers-reference/ パッケージに書かれているので、それをイン
ストールして熟読してください。

<!--
<p>An on-line version of developers-reference is available
<url id="http://www.debian.org/doc/packaging-manuals/developers-reference/"
name="at our web site">.
-->
<p>developers-reference のオンライン版は <url
id="http://www.debian.org/doc/packaging-manuals/developers-reference/"
name="我々の Web サイト"> にあります。

<sect id="contribresources">
<!--
How can I contribute resources to the Debian project?
-->
どうすれば Debian プロジェクトに資源を寄贈することができますか?

<!--
<p>Since the project aims to make a substantial body of software rapidly
and easily accessible throughout the globe, mirrors are urgently needed.
It is desirable but not absolutely necessary to mirror all of the archive.
Please visit the <url id="http://www.debian.org/mirror/size" name="Debian
mirror size"> page for information on the disk space requirements.
-->
<p>本プロジェクトは価値あるソフトウェア群を世界中の至るところから迅速か
つ容易にアクセスできるようにすることを目標としているため、差し迫ってミラー
が必要とされています。全アーカイブをミラーすることが望ましいですが、絶対
に必要というわけではありません。ディスクスペース要求の情報は <url
id="http://www.debian.org/mirror/size" name="Debian ミラーサイズ"> のペー
ジを見てください。

<!--
<p>Most of the mirroring is accomplished entirely automatically by scripts,
without any human intervention.  However, the occasional glitch or system
change occurs which requires human intervention.
-->
<p>ミラーリングの大部分は人が介在することなくスクリプトによって完全に自
動的に行なえます。しかしながら、人の介在を必要とする偶発的な不調やシステ
ムの変更が起こることはあります。

<!--
<p>If you have a high-speed connection to the Internet, the resources to
mirror all or part of the distribution, and are willing to take the time
(or find someone) who can provide regular maintenance of the system,
then please contact <email/debian-admin@lists.debian.org/.
-->
<p>もしあなたがインターネットへ高速に接続でき、ディストリビューションの
すべてあるいはその一部をミラーするだけの資源を持っていて、進んで時間を割
いてくれるなら (あるいは、定期的なシステムの保守を行える人を見つけてくれ
るなら)、<email/debian-admin@lists.debian.org/ まで連絡下さい。

<!--
<sect id="contribresources">How can I contribute resources to the Debian
  project?
-->
<sect id="supportingorganizations">金銭的に Debian プロジェクトに貢献す
るにはどうすればいいのですか?

<!--
<p>One can make individual donations to one of two organizations that
are critical to the development of the Debian project.
-->
<p>Debian プロジェクトの開発にとって重要な二つの組織に対して別々に寄付を
することができます。

<sect1 id="SPI">Software in the Public Interest

<!--
<p>Software in the Public Interest (SPI) is an IRS 501(c)(3) non-profit
organization, formed when FSF withdrew their sponsorship of Debian.
The purpose of the organization is to develop and distribute free software.
-->
<p>Software in the Public Interest (SPI) は、FSF が Debian のスポンサー
から撤退したときに結成された IRS 501(c)(3) の非営利組織です。

<!--
<p>Our goals are very much like those of FSF, and we encourage programmers
to use the GNU General Public License on their programs. However, we have
a slightly different focus in that we are building and distributing
a Linux system that diverges in many technical details from the GNU system
planned by FSF. We still communicate with FSF, and we cooperate in sending
them changes to GNU software and in asking our users to donate to FSF and
the GNU project.
-->
<p>我々のゴールは FSF のゴールと非常に似ており、プログラマ達に自分のプロ
グラムに「GNU 一般公有使用許諾書」を適用することを奨励しています。しかし
ながら、我々は FSF によって計画された GNU システムの数多くの技術から分岐
した Linux システムを構築し配布するという、少し異なった点に興味の対象を
向けています。我々は今でも FSF と通じ合っていますし、GNU ソフトウェアへ
の変更点を彼らに伝えたり Debian のユーザに FSF と GNU プロジェクトへの寄
付をお願いするなどの協力を行なっています。

<!--
<p>SPI can be reached at: <url id="http://www.spi-inc.org/">.
-->
<p>SPI には <url id="http://www.spi-inc.org/"> で連絡を取ることができま
す。

<!--
<sect1 id="FSF">Free Software Foundation
-->
<sect1 id="FSF">フリーソフトウェア財団 (Free Software Foundation, FSF)

<!--
<p>At this time there is no formal connection between Debian and the Free
Software Foundation. However, the Free Software Foundation is
responsible for some of the most important software components in
Debian, including the GNU C compiler, GNU Emacs, and much of the C
run-time library that is used by all programs on the system. FSF pioneered
much of what free software is today: they wrote the General Public
License that is used on much of the Debian software, and they invented
the "GNU" project to create an entirely free Unix system. Debian should
be considered a descendent of the GNU system.
-->
<p>現在は Debian とフリーソフトウェア財団との間に正式な関係はありません。
しかしながら、GNU C コンパイラや GNU Emacs、システム上のすべてのプログラ
ムによって利用される多くの C のランタイムライブラリなど、Debian の最も重
要なソフトウェアのいくつかは FSF によるものです。FSF は今日のフリーソフ
トウェアのあり方の多くを開拓してきました。彼らは Debian のソフトウェアの
多くで用いられている「GNU 一般公有使用許諾書」を書き上げ、完全に自由な
Unix システムを作り上げるために「GNU」プロジェクトを創設しました。
Debian は GNU システムの子孫と考えられるべきです。

<!--
<p>FSF can be reached at: <url id="http://www.fsf.org/">.
-->
<p>FSF には <url id="http://www.fsf.org/"> で連絡を取ることができます。
