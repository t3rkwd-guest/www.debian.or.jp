<!--
<chapt id="compat">Compatibility issues
-->
<chapt id="compat">互換性に関する問題

<!--
<sect id="arches">On what hardware architectures/systems does &debian; run?
-->
<sect id="arches">&debian; はどんなハードウェアのアーキテクチャ/システム
で動きますか?

<!--
<p>&debian; includes complete source-code for all of the included programs,
so it should work on all systems which are supported by the Linux kernel;
see the <url name="Linux FAQ" id="http://www.linuxdoc.org/FAQ/Linux-FAQ/">
for details.
-->
<p>&debian; にはすべてのプログラムの完全なソースコードが入っています。で
すから Linux カーネルでサポートされるあらゆるシステムで動きます。詳しく
は <url name="Linux FAQ"
id="http://www.linuxdoc.org/FAQ/Linux-FAQ/"> を見てください。

<!-- XXX update for new distros -->
<!--
<p>The current &debian; release, &release;, contains a complete, binary
distribution for the following architectures:
-->
<p>現在の &debian; リリース &release; には以下のアーキテクチャ用の完全な
バイナリ配布物が含まれています。

<!--
<p><em/i386/: this covers PCs based on Intel and compatible processors,
including Intel's 386, 486, Pentium, Pentium Pro, Pentium II (both Klamath
and Celeron), and Pentium III, and most compatible processors by AMD, Cyrix
and others.
-->
<p><em/i386/: Intel とその互換プロセッサを使用した PC をカバーします。
Intel の 386 や 486、Pentium、Pentium Pro、Pentium II (Klamath と
Celeron の両方)、Pentium III、ほとんどの AMD や Cyrix の互換プロセッサな
どです。

<!--
<p><em/m68k/: this covers Amigas and ATARIs having a Motorola 680x0
processor for x>=2; with MMU.
-->
<p><em/m68k/: MMU が附属している Motorola 680x0 プロセッサ (ただし x >=2) を
持っている Amiga や ATARI をカバーします。

<!--
<p><em/alpha/: Compaq/Digital's Alpha systems.
-->
<p><em/alpha/: Compaq/Digital の Alpha システムをカバーします。

<!--
<p><em/sparc/: this covers Sun's SPARC and most UltraSPARC systems.
-->
<p><em/sparc/: Sun の SPARC と ほとんどの UltraSPARC システムをカバーし
ます。

<!--
<p><em/powerpc/: this covers some IBM/Motorola PowerPC machines, including
CHRP, PowerMac and PReP machines.
-->
<p><em/powerpc/: IBM/Motorola の PowerPC マシンのいくつかをカバーします。
CHRP、PowerMac、PReP マシンなどです。

<!--
<p><em/arm/: ARM and StrongARM machines.
-->
<p><em/arm/: ARM と StrongARM マシン。

<!--
<p>The development of binary distributions of Debian for Sparc64
(UltraSPARC native) and MIPS architectures is currently underway.
-->
<p>Sparc64 (UltraSPARC ネイティブ) や MIPS アーキテクチャ用の Debian バ
イナリ配布物は現在開発が進行中です。

<!--
<p>For further information on booting, partitioning your drive, enabling
PCMCIA (PC Card) devices and similar issues please follow the instructions
given in the Installation Manual, which is available from our WWW site at
<url id="http://www.debian.org/releases/stable/i386/install">.
-->
<p>起動の方法やドライブのパーティショニング、PCMCIA (PC カード) デバイス
を有効にするやり方などについてはインストールマニュアルの指示に従ってくだ
さい。インストールマニュアルは我々の WWW サイト <url
id="http://www.debian.org/releases/stable/i386/install"> にあります。

<!--
<sect id="otherdistribs">How compatible is Debian with other distributions
                         of Linux?
-->
<sect id="otherdistribs">Debian は他の Linux ディストリビューションとど
れくらい互換性がありますか?

<!--
<p>Debian developers communicate with other Linux distribution creators
in an effort to maintain binary compatibility across Linux distributions.
Most commercial Linux products run as well under Debian as they do on the
system upon which they were built.
-->
<p>Debian の開発者は、Linux ディストリビューション同士でバイナリ互換性が
維持できるよう、他の Linux ディストリビューション作成者と連絡を取りあっ
ています。商用の Linux 製品はたいていその製品が構築されたシステムと同じ
くらいうまく Debian の上でも動きます。

<!--
<p>&debian; adheres to the <url name="Linux Filesystem Hierarchy
Standard" id="http://www.pathname.com/fhs/">. However, there is room for
interpretation in some of the rules within this standard, so there may be
slight differences between a Debian system and other Linux systems.
-->
<p>&debian; は <url name="Linux ファイルシステム階層標準"
id="http://www.pathname.com/fhs/"> に厳密に従っています。とはいえ、この
標準には解釈の余地の残っている規則がいくつか存在するので、Debian システ
ムと他の Linux システムとで異なる点があるかもしれません。

<!--
<sect id="otherunices">How source code compatible is Debian with other
  Unix systems?
-->
<sect id="otherunices">Debian は他の Unix システムとどれぐらいソースレベ
ルの互換性があるのでしょうか?

<!--
<p>For most applications Linux source code is compatible with other Unix
systems. It supports almost everything that is available in System V Unix
systems and the free and commercial BSD-derived systems. However in the Unix
business such claim has nearly no value because there is no way to prove it.
In the software development area complete compatibility is required instead
of compatibility in "about most" cases. So years ago the need for standards
arose, and nowadays POSIX.1 (IEEE Standard 1003.1-1990) is one of the major
standards for source code compatibility in Unix-like operating systems.
-->
<p>Linux のソースコードはたいていのアプリケーションで他の Unix システム
と互換性があります。System V Unix システムや BSD から派生したフリーま
たは商用のシステムが提供するものを Linux はほとんどすべてサポートします。
しかし、Unix 業界ではこのように主張しても検証するすべがないのでほとんど
意味を持ちません。ソフトウェア開発においては、「たいていの」場合に互換性
が保たれていることではなく、完全に互換性が保たれていることが要求されます。
数年前に標準の必要性が高まり、今日では POSIX.1 (IEEE bStandard
1003.1-1990) が Unix ライクなオペレーティングシステム間でソースコードの
互換性をとるための主要な標準の一つになっています。

<!--
<p>Linux is intended to adhere to POSIX.1, but the POSIX standards cost real
money and the POSIX.1 (and FIPS 151-2) certification is quite expensive;
this made it more difficult for the Linux developers to work on complete
POSIX conformance. The certification costs make it unlikely that Debian will
get an official conformance certification even if it completely passed the
validation suite. (The validation suite is now freely available, so it is
expected that more people will work on POSIX.1 issues.)
-->
<p>Linux も POSIX.1 に従おうとしていますが、POSIX 標準は現金を要し、
POSIX.1 (と FIPS 151-2) 検定はかなり高額です。このため完全な POSIX 準拠
をめざして働くのは Linux 開発者にとって簡単ではありません。たとえ
Debian が確認テストセットに全てパスしていても、検定コストが高いので
Debian が公式に適合保証を得る見込みはほとんどありません (確認テストセッ
トは今では自由に利用でき、より多くの人が POSIX.1 問題のために働くことが
期待されています)。

<!-- <p><strong>(The authors would very much like to give you a pointer to
an on-line document that described that standard, but the IEEE is another
one of those organizations that gets away with declaring standards and then
requiring that people PAY to find out what they are.  This makes about as
much sense as having to find out the significance of various colored lights
on traffic signals.)</strong> -->

<!--
<p>Unifix GmbH (Braunschweig, Germany) developed a Linux system that has
been certified to conform to FIPS 151-2 (a superset of POSIX.1). This
technology was available in Unifix' own distribution called Unifix
Linux 2.0 and in Lasermoon's Linux-FT.
-->
<p>Unifix GmbH (ブラウンシュワイク、ドイツ) は FIPS 151-2 (POSIX.1 のスー
パーセット) に準拠していると認められた Linux システムを開発しました。この
技術は Unifix Linux 2.0 という Unifix 自身のディストリビューションと
Lasermoon の Linux-FT として利用できます。
<!-- I had to comment this out for obvious reasons... SVD
 <url name="Linux-FT" url="http://www.lasermoon.co.uk/linux-ft/linux-ft.html">.
Currently Unifix merges its patches into the Linux kernel, gcc and other
tools; so it is expected that their fixes towards POSIX.1 conformance will
be available in Debian (and other distributions).
-->
<!--
現在、Unifix は Linux カーネルや gcc や他のツールに加
えた変更点を、もとのソースへ反映しています。ですから、POSIX.1 に適合する
ように Unifix が加えた修正は、Debian(とほかのディストリビューション)でも
利用できるようになると思われます。
-->

<!--
<sect id="otherpackages">Can I use Debian packages (".deb" files) on my
  RedHat/Slackware/... Linux system? Can I use RedHat packages (".rpm"
  files) on my &debian; system?
-->
<sect id="otherpackages">Debian パッケージ (「.deb」ファイル) は
RedHat/Slackware/... の Linux システムで使えますか? RedHat パッケージ
(「.rpm」ファイル) を &debian; システムで使えますか?

<!--
<p>Different Linux distributions use different package formats and different
package management programs.
-->
<p>Linux ディストリビューションが異なればパッケージフォーマットが異なり、
パッケージ管理プログラムも異なります。

<!--
<taglist>
<tag><strong/You probably can:/
  <item>A program to unpack a Debian package onto a Linux host that is been
  built from a `foreign' distribution is available, and will generally work,
  in the sense that files will be unpacked.  The converse is probably also
  true, that is, a program to unpack a RedHat or Slackware package on a host
  that is based on &debian; will probably succeed in unpacking the package
  and placing most files in their intended directories.  This is largely a
  consequence of the existence (and broad adherence to) the Linux Filesystem
  Hierarchy Standard.
-->
<taglist>
<tag><strong/おそらくできること:/
  <item>「よその」ディストリビューションが動いている Linux ホストに
  Debian パッケージをアンパックするプログラムがあります。ファイルがアン
  パックされるという意味においてはたいてい動作するでしょう。逆もまたおそ
  らく真です。つまり RedHat や Slackware のパッケージをアンパックするプ
  ログラムは、Debian Linux が動いているホストでもパッケージを滞りなくア
  ンパックし、たいていのファイルを意図されたディレクトリに置くでしょう。
  これは Linux ファイルシステム標準が存在する (また広くそれに従っている)
  からです。

<!--
<tag><strong/You probably do not want to:/
  <item>Most package managers write administrative files when they
  are used to unpack an archive.  These administrative files are generally
  not standardized.  Therefore, the effect of unpacking a Debian package
  on a `foreign' host will have unpredictable (certainly not useful) effects
  on the package manager on that system.  Likewise, utilities from other
  distributions might succeed in unpacking their archives on Debian systems,
  but will probably cause the Debian package management system to fail when the
  time comes to upgrade or remove some packages, or even simply to report
  exactly what packages are present on a system.
-->
<tag><strong/できる望みがあまりないもの:/
  <item>たいていのパッケージ管理プログラムは、アーカイブをアンパックする
  ときに管理ファイルを書き出しています。このような管理ファイルは一般に標
  準化されていないので、Debian パッケージを「よその」ホストでアンパック
  すると、そのシステムのパッケージ管理プログラムに予測できない (まあ、無
  用な) 効果を与えます。同様に他のディストリビューションのユーティリティ
  は Debian システムにそのシステム用のアーカイブをアンパックできるでしょ
  うが、おそらく Debian パッケージ管理システムがパッケージをアップグレー
  ドしようとしたり、削除しようとしたとき、あるいは単に現在どんなパッケー
  ジがシステムに入っているか調べようとしただけのときでも失敗するかもしれ
  ません。

<!--
<tag><strong/A better way:/
  <item>The Linux File System Standard (and therefore &debian;) requires that
  subdirectories under <tt>/usr/local/</tt> be entirely under the user's
  discretion.  Therefore, users can unpack `foreign' packages into this
  directory, and then manage their configuration, upgrade and removal
  individually.
</taglist>
-->
<tag><strong/よりよい方法:/
  <item>Linux ファイルシステム標準では、(ということはつまり &debian;) で
  は、サブディレクトリ <tt>/usr/local/</tt> 以下はユーザの裁量にすべて任
  せることになっています。そのため、「よその」パッケージをこのディレクト
  リにアンパックでき、パッケージの構成やアップグレード・削除も個別に管理
  できます。
</taglist>

<!-- It would be useful to document all the ways in which Debian and RedHat
systems differ.  I believe such a document would do a lot to dispell
fears of using a Unix system. SGK -->

<!--
<sect id="a.out">Is Debian able to run my very old "a.out" programs?
-->
<sect id="a.out">私の非常に古い「a.out」プログラムをDebian で実行できま
すか?

<!--
<p>Do you actually still have such a program? :-)
-->
<p>実際にそんなプログラムをまだ持っているんですか? :-)

<!--
<p>To <em>execute</em> a program whose binary is in <tt>a.out</tt>
(i.e., QMAGIC or ZMAGIC) format,
-->
<p><tt>a.out</tt> (つまり QMAGIC や ZMAGIC) 形式のプログラムを<em>実行す
る</em>には以下のようにして下さい。

<!--
<list>
  <item>Make sure your kernel has <tt>a.out</tt> support built into it,
  either directly (CONFIG_BINFMT_AOUT=y) or as a module (CONFIG_BINFMT_AOUT=m).
  (Debian's kernel-image package contains the module <tt>binfmt_aout</tt>.)
-->
<list>
  <item>カーネルに <tt>a.out</tt> サポートが組み込まれていることを確かめ
  てください。直接組み込まれていても (CONFIG_BINFMT_AOUT=y) モジュールに
  なっていても (CONFIG_BINFMT_AOUT=m) どちらでもかまいません (Debian の
  kernel-image パッケージには <tt>binfmt_aout</tt> モジュールが含まれて
  います)。

<!--
  <p>If your kernel supports <tt>a.out</tt> binaries by a module, then be
  sure that the <tt>binfmt_aout</tt> module is loaded.  You can do this
  at boot time by entering the line <tt>binfmt_aout</tt> into the file
  <tt>/etc/modules</tt>.  You can do it from the command line by
  executing the command <tt>insmod DIRNAME/binfmt_aout.o</tt> where
  <tt>DIRNAME</tt> is the name of the directory where the modules that
  have been built for the version of the kernel now running are stored.
  On a system with the 2.2.17 version of the kernel,
  <tt>DIRNAME</tt> is likely to be <tt>/lib/modules/2.2.17/fs/</tt>.
-->
  <p>モジュールで <tt>a.out</tt> をサポートしているカーネルでは、
  <tt>binfmt_aout</tt> モジュールが必ずロードされるようにして下さい。
  <tt>/etc/modules</tt> ファイルに <tt>binfmt_aout</tt> という行を追加し
  てブート時に読み込まれるようにもできます。コマンドラインからコマンド
  <tt>insmod DIRNAME/binfmt_aout.o</tt> を実行してもロードできます。ここ
  で<tt>DIRNAME</tt> は現在実行されているカーネルのバージョンに対応した
  モジュールが置かれているディレクトリ名です。カーネルのバージョンが
  2.2.17 のシステムだったら、<tt>DIRNAME</tt> は
  <tt>/lib/modules/2.2.17/fs/</tt> のようになります。

<!--
  <item>install the package <package/libc4/, found in one of the releases
  prior to release 2.0 (because at that time we removed the package).
  You might want to look for an old Debian CD-ROM (Debian 1.3.1 still had
  this package), or see <url id="ftp://archive.debian.org/debian-archive/">
  on the Internet.
-->
  <item><package/libc4/ パッケージをインストールしてください。2.0 より前
  のリリースにあります (このパッケージはその時に削除してしまったのです
  )。古い Debian CD-ROM (Debian 1.3.1 にはまだこのパッケージがありました
  ) を探したり、インターネットで <url
  id="ftp://archive.debian.org/debian-archive/"> を見ることもできるでしょ
  う。

<!--
  <item>If the program you want to execute is an <tt>a.out</tt> X client,
  then install the <package/xcompat/ package (see above for availability).
</list>
-->
  <item>実行したいプログラムが <tt>a.out</tt> 形式の X クライアントなら
  <package/xcompat/ パッケージをインストールして下さい (入手については上
  記参照)。
</list>

<!--
<p>If you have a commercial application in <tt>a.out</tt> format, now would
be a good time to ask them to send you an <tt>ELF</tt> upgrade.
-->
<p><tt>a.out</tt> 形式の商用アプリケーションを持っているのなら、
<tt>ELF</tt> 形式へのアップグレードを催促する絶好の機会です。

<!--
<sect id="libc5">Is Debian able to run my old libc5 programs?
-->
<sect id="libc5">私の古い libc5 プログラムを Debian で実行できますか?

<!--
<p>Yes. Just install the required <package/libc5/ libraries, from the
<tt>oldlibs</tt> section (containing old packages included for compatibility
with older applications).
-->
<p>できます。必要となる <package/libc5/ ライブラリを <tt>oldlibs</tt> セ
クションからインストールするだけです (このセクションには互換性のため古い
アプリケーションに含まれる古いパッケージがあります)。

<!--
<sect id="libc5-compile">Can Debian be used to compile libc5 programs?
-->
<sect id="libc5-compile">libc5 プログラムをコンパイルするのに Debian を
使えますか?

<!--
<p>Yes.  Install <package/libc5-altdev/ and <package/altgcc/ packages (from
the <tt>oldlibs</tt> section). You can find the appropriate libc5-compiled
<prgn/gcc/ and <prgn/g++/ in directory <tt>/usr/i486-linuxlibc1/bin</tt>.
Put them in your $PATH variable to get <prgn/make/ and other programs to
execute these first.
-->
<p>使えます。<package/libc5-altdev/ と <package/altgcc/ パッケージを
(<tt>oldlibs</tt> セクションから) インストールしてください。libc5 コンパ
イルに適した <prgn/gcc/ と <prgn/g++/ が
<tt>/usr/i486-linuxlibc1/bin</tt> ディレクトリに置かれます。
<prgn/make/ などのプログラムがそれらを最初に実行するように変数 $PATH に
配置してください。

<!--
<p>If you need to compile libc5 X clients, install <package/xlib6/ and
<package/xlib6-altdev/ packages.
-->
<p>libc5 な X クライアントをコンパイルする必要があるならば
<package/xlib6/ と <package/xlib6-altdev/ パッケージをインストールしてく
ださい。

<!--
<p>Be aware that libc5 environment isn't fully supported by our other
packages anymore.
-->
<p>libc5 環境はもはや他のパッケージでは完全にはサポートされないことに注
意してください。

<!--
<sect id="non-debian-programs">How should I install a non-Debian program?
-->
<sect id="non-debian-programs">非 Debian プログラムをインストールするに
はどうしたらよいですか?


<!--
<p>Files under the directory <tt>/usr/local/</tt> are not under the control
of the Debian package management system.  Therefore, it is good practice
to place the source code for your program in /usr/local/src/.  For example,
you might extract the files for a package named "foo.tar" into the directory
<tt>/usr/local/src/foo</tt>.  After you compile them, place the binaries
in <tt>/usr/local/bin/</tt>, the libraries in <tt>/usr/local/lib/</tt>,
and the configuration files in <tt>/usr/local/etc/</tt>.
-->
<p>ディレクトリ <tt>/usr/local/</tt> 以下のファイルは Debian パッケージ
管理システムの制御下にありません。ですから、自分のプログラムのソースコー
ドは <tt>/usr/local/src/</tt> に置くようお勧めします。例えば、
「foo.tar」という名前のパッケージのファイルは、ディレクトリ
<tt>/usr/local/src/foo/</tt> に展開できます。コンパイルした後は
<tt>/usr/local/bin/</tt> にバイナリを置いて下さい。ライブラリは
<tt>/usr/local/lib/</tt> に、構成ファイルは <tt>/usr/local/etc/</tt> に
置いて下さい。

<!--
<p>If your programs and/or files really must be placed in some other
directory, you could still store them in <tt>/usr/local/</tt>,
and build the appropriate symbolic links from the required location
to its location in <tt>/usr/local/</tt>, e.g., you could make the link
<example>ln -s /usr/local/bin/foo /usr/bin/foo</example>
-->
<p>プログラムやファイルをどうしても他のディレクトリに置かなければならな
い場合は、<tt>/usr/local/</tt> に置いたままにしておき、必要な場所から
<tt>/usr/local/</tt> へ適切なシンボリックリンクを張って下さい。例えば以
下のようにします。
<example>ln -s /usr/local/bin/foo /usr/bin/foo</example>

<!--
<p>In any case, if you obtain a package whose copyright allows
redistribution, you should consider making a Debian package of it, and
uploading it for the Debian system.  Guidelines for becoming a package
developer are included in the Debian Policy manual (see <ref id="debiandocs">).
-->
<p>いずれにせよ、手に入れたパッケージが著作権で再配布を許可していたら、
Debian パッケージにして Debian システム向けにアップロードすることを考え
てみてください。パッケージ開発者になるためのガイドラインは、Debian ポリ
シーマニュアルに書かれています (<ref id="debiandocs"> 参照)。

<!--
<sect id="xlib6">Why do I get "Can't find libX11.so.6" errors when I try
  to run <tt>foo</tt>?
-->
<sect id="xlib6"><tt>foo</tt> を実行しようとすると「Can't find
libX11.so.6」というエラーが起きるのはなぜですか?

<!--
<p>This error message could mean that the program is linked against the
<tt>libc5</tt> version of the X11 libraries. In this case you need to
install the <package/xlib6/ package, from the <tt>oldlibs</tt> section.
-->
<p>このエラーメッセージはおそらくプログラムが <tt>libc5</tt> バージョン
の X11 ライブラリでリンクされていることを示しています。この場合
<tt>oldlibs</tt> セクションから <prgn/xlib6/ パッケージをインストールす
る必要があります。

<!--
<p>You may get similar error messages referring to libXpm.so.4 file, in
which case you need to install the libc5 version of the XPM library,
from the <package/xpm4.7/ package, also in the <tt>oldlibs</tt> section.
-->
<p>libXpm.so.4 ファイルについて同じようなエラーメッセージが表示される場
合は、やはり <tt>oldlibs</tt> セクションの <prgn/xpm4.7/ パッケージから
libc5 バージョンの XPM ライブラリをインストールする必要があります。

<!--
<sect id="termcap">Why can't I compile programs that require libtermcap?
-->
<sect id="termcap">どうして libtermcap を使用するプログラムをコンパイル
できないのですか?

<!--
<p>Debian uses the <tt>terminfo</tt> database and the <tt>ncurses</tt>
library of terminal interface routes, rather than the <tt>termcap</tt>
database and the <tt>termcap</tt> library.  Users who are compiling programs
that require some knowledge of the terminal interface should replace
references to <tt>libtermcap</tt> with references to <tt>libncurses</tt>.
-->
<p>Debian は端末インターフェースの手段として <tt>terminfo</tt> データベー
スと <tt>ncurces</tt> ライブラリを使用しています。<tt>termcap</tt> デー
タベースと <tt>termcap</tt> ライブラリは使用しません。ターミナルインター
フェースの何らかの情報が必要なプログラムをコンパイルするなら
<tt>libtermcap</tt> への参照を <tt>libncurses</tt> への参照に置き換えて
ください。

<!--
<p>To support binaries that have already been linked with the <tt>termcap</tt>
library, and for which you do not have the source, Debian provides a package
called <package/termcap-compat/.  This provides both <tt>libtermcap.so.2</tt>
and <tt>/etc/termcap</tt>.  Install this package if the program fails to run
with the error message "can't load library 'libtermcap.so.2'", or complains
about a missing <tt>/etc/termcap</tt> file.
-->
<p>すでに <tt>termcap</tt> ライブラリとリンクされているがソースコードが
手元にないというバイナリをサポートするために、Debian では
<package/termcap-compat/ パッケージを用意しています。このパッケージには
<tt>libtermcap.so.2</tt> と <tt>/etc/termcap</tt> が入っています。プログ
ラムが実行できずに「Can't load library 'libtermcap.so.2'
(「libtermcap.so.2」ライブラリをロードできません)」というエラーメッセー
ジが出力されたり、<tt>/etc/termcap</tt> ファイルが見つからないと表示され
たりしたら、このパッケージをインストールしてください。

<!--
<sect id="accelx">Why can't I install AccelX?
-->
<sect id="accelx">どうして AccelX がインストールできないのですか?
	
<!--
<p>AccelX uses the <tt>termcap</tt> library for installation. See
<ref id="termcap"> above.
-->
<p>AccelX は インストール時に <tt>termcap</tt> ライブラリを使用します。
上記 <ref id="termcap"> を見て下さい。

<!--
<sect id="motifnls">Why do my old XFree 2.1 Motif applications crash?
-->
<sect id="motifnls">古い XFree 2.1 Motif のアプリケーションがクラッシュ
するのはなぜですか?

<!--
<p>You need to install the <package/motifnls/ package, which provides the
XFree-2.1 configuration files needed to allow Motif applications compiled
under XFree-2.1 to run under XFree-3.1.
-->
<p><package/motifnls/ パッケージをインストールする必要があります。これは、
XFree-2.1 でコンパイルされた Motif アプリケーションを XFree-3.1 で動かす
のに必要な XFree-2.1 の設定ファイルを提供します。

<!--
<p>Without these files, some Motif applications compiled on other machines
(such as Netscape) may crash when attempting to copy or paste from or to
a text field, and may also exhibit other problems.
-->
<p>これらのファイルがなかったら、他のマシンでコンパイルされた Motif アプ
リケーション (Netscape など) のいくつかは、テキストフィールドの間でコピー
や貼り付けをするとクラッシュしたり他の問題が表示されたりするかもしれませ
ん。
