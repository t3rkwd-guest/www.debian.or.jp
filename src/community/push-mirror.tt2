[% title = "CDN 対応ミラーの設定" %]

<h2>CDN 対応ミラーの設定</h2>

<p>
Debian JP Project では、1 つのサーバがダウンしただけで APT によるパッケージのダウンロードができなくなるといった障害に対処するため、複数の Debian ミラーサーバを仮想的に集約して耐障害性を持たせる CDN (Contents Delivery Network) 化を進めています。CDN ミラーには、次の APT リポジトリでアクセスできます (<a href="http://www.debian.or.jp/blog/cdn_debian_or_jp-start.html">詳細</a>)。</p>

<pre>
deb http://ftp.jp.debian.org/debian/ [% stable_codename %] main contrib non-free non-free-firmware
deb http://ftp.jp.debian.org/debian/ [% stable_codename %]-updates main contrib non-free non-free-firmware
deb http://ftp.jp.debian.org/debian/ [% stable_codename %]-backports main contrib non-free non-free-firmware
#deb-src http://ftp.jp.debian.org/debian/ [% stable_codename %] main contrib non-free non-free-firmware
#deb-src http://ftp.jp.debian.org/debian/ [% stable_codename %]-updates main contrib non-free non-free-firmware
#deb-src http://ftp.jp.debian.org/debian/ [% stable_codename %]-backports main contrib non-free non-free-firmware
</pre>

<p>(※[% stable_codename %] 以外にも、Debian 公式ミラーで提供されている stable、testing、unstable、[% oldstable_codename %]、[% testing_codename %]、sid、experimental といった名前も利用できます)</p>

<p>ミラー参加を検討しているサイトのために、本ドキュメントでは、Debian ミラーの構築方法と、CDN への追加方法を説明します。</p>

<h3>前提条件</h3>
<p>
安定したミラーを構築するにあたっては、いくつかの条件があります。これらが満たされていないと、ユーザに十分なサービスを提供することができません。CDN への加盟にあたっては、次の条件があります。
</p>

<ul>
<li>Debian のフルミラーであること。 つまり、ftp.debian.org で公開されているものと同様に、すべてのアーキテクチャ、膨大なすべてのファイルを格納する必要があります。</li>
<li>push ミラーであること。詳細は後述しますが、上流ミラーが更新を完了したタイミングでシグナルを送り、それを受けてミラーを行うようにします。こうすることで、上流がまだ更新中なのにミラーを行って APT の整合性を壊した状態にしてしまったり、上流のハードウェアクラッシュを受けて一緒にミラーを失ってしまったりといった事態を回避しやすくなります。</li>
<li>Debian のミラーは HTTP (Web サーバ) で提供されており、Web ブラウザから見て /debian ディレクトリの下にあること。エイリアスや rewrite モジュールなどを使ってもかまいません。ただし、HTTP リダイレクトヘッダは APT ツールが対応していないので利用できません。</li>
<li>cdn.debian.net への加盟には不要ですが、ftp.jp.debian.org に加盟するには、FTP サービス (/debian として見えること。シンボリックリンクの使用も可能) および rsync サービス (::debian として見えること) を提供する必要があります。</li>
</ul>

<p>
また、ミラーにあたっては、次の条件を満たしていることが要求されます。
</p>

<ul>
<li><strong>大容量ディスク:</strong> ミラーをするために何よりも必要なのは、大きなディスクです。現時点 (2018年5月) で<a href="http://www.debian.org/mirror/size">フルミラーには 2,787GB 程度を消費しており</a>、将来見込まれる増加を前提とすると最低でも 4TB 以上のクラスのディスク容量が必要になります。ミラーするアーテキクチャを絞ればディスクの消費は抑えられますが、本ドキュメントでは CDN の一員とするための条件であるフルミラーを行うものとします。RAID は必須ではありませんが、ディスク障害によってミラーが消失すると、復旧に時間がかかることになります。</li>
<li><strong>安定した広帯域ネットワークと堅固なハードウェア:</strong> ミラーとなったサーバには、(CDN では分散されるとはいえ) 多数のディスクアクセスおよびネットワークアクセスが発生することになります。特に rsync で上流ミラーに追従する最中は、短時間のうちに多量のアクセスが起こり、高負荷になります。たとえば安価な NAS のようなものではこの負荷に対処しきれず、頻繁なクラッシュが起きる可能性もあります。<a href="../project/organization.html#admin">管理チーム</a>では、x86 アーキテクチャのように十分に枯れたものを推奨しています。</li>
<li><strong>Unix OS:</strong> 基本的には、rsync と Web サーバ (ftp.jp.debian.org に加盟するには FTP サーバも) が動作すれば OS は特に問いません (実際、Debian GNU/Linux を使っていない Debian ミラーもいくつかあります)。でも、Debian ならきっと簡単ですよ :)</li>
<li><strong>十分な実メモリ:</strong> rsync および Web サーバ (通常は Apache) は、実メモリを多量に消費します。<a href="../project/organization.html#admin">管理チーム</a>の経験では、最低でも 512MB、できれば 1GB 以上のメモリを塔載しているのが望ましいと思われます。</li>
<li><strong>外部からの SSH アクセス:</strong> このドキュメントの手順で推奨する push ミラーを実現するためには、外部からの SSH アクセスを許可するよう設定しなければなりません。基本的には上流ミラーからミラーを開始するよう送られる「シグナル」を何らかの形で受け取れさえすればよいので、上流ミラーだけにアクセスを許可して、またシェルログインを与える必要もありません。SSH 以外のシグナル (たとえば指定の Web ページにアクセスしたり自動処理メールを送ったりするなど) を使うことも理論上はできますが、現時点では要求する声もないので本ドキュメントでは扱いません。</li>
<li><strong>信頼できる連絡・管理責任者:</strong> ミラーのほぼすべての作業は自動で行われますが、まれに障害・事故で手動の作業が要求されることがあります。気付かないうちにディスクがいっぱいになっているということもあるでしょう。そのようなとき、<a href="../project/organization.html#board">理事会</a>または<a href="../project/organization.html#admin">管理チーム</a>から連絡が必要になりますが、誰に聞いたらよいのかわからないというのは困ります。また、Debian ミラーの整合性は APT の機構によってひとまず守られているとは言えますが、ファイルを汚染されたりすることのないよう、ミラーサーバ上のユーザ管理は厳密にお願いいたします。</li>
</ul>

<h3>ミラーの設定</h3>

<p>Debian ミラーを構成するには、まず ftpsync スクリプトで rsync によるミラーを行います。</p>

<ol>
<li>サーバの適当なディレクトリに git リポジトリ https://ftp-master.debian.org/git/archvsync.git のクローンまたは<a href="http://ftp-master.debian.org/ftpsync.tar.gz">スナップショット</a>を展開します。たとえば /home/ftpadm/archvsync とします。ミラーを行うユーザは専用の (あまり権限のない) ものを作ったほうがよいでしょう。</li>
<li>~/bin と ~/etc のシンボリックリンクを作成します。そして、PATH に ~/bin を追加します。
<pre>
$ cd
$ ln -s ~/archvsync/bin bin
$ ln -s ~/archvsync/etc etc
</pre>
  </li>
<li>Debian ミラーのために、etc/ftpsync.conf.sample を etc/ftpsync.conf としてコピーし、必要な箇所のコメント記号#を外して編集します。
<ul>
 <li>TO: Debian ミラーを配置するディレクトリ。たとえば /var/www/debian。事前に空ディレクトリを用意し、ミラーを実行するユーザが書き込める必要があります</li>
 <li>RSYNC_PATH: 上流ミラーで公開されている Debian ミラーの名前。通常は「debian」</li>
 <li>RSYNC_HOST: Debian 上流ミラー。たとえば ftp.jaist.ac.jp。push ミラーサービスを開始するにあたってどの上流を選ぶべきかは<a href="../project/organization.html#board">理事会</a>または<a href="../project/organization.html#admin">管理チーム</a>に問い合わせてください。最初の時点では手近なミラーでかまいません</li>
 <li>LOGDIR: ミラーログを置くディレクトリ。たとえば /home/andy/logs。ミラーを実行するユーザが書き込める必要があります</li>
 <li>MAILTO: ログを送付するアドレス。長大ですし、通常はコメントアウトされたままでよいでしょう</li>
 <li>EXCLUDE: 除外するファイル。フルミラーのためには空のままにしておきます</li>
 <li>ARCH_EXCLUDE: 除外するアーキテクチャ。フルミラーのためには空のままにしておきます</li>
</ul>
</li>
<li>rsync、debianutils をインストールしておきます。debianutils はミラーの動作には影響しませんが、ログファイルのローテーションのために savelog コマンドを使っています。</li>
<li>Debian の CD や DVD があるなら、ミラーディレクトリにコピーしておくと、ダウンロード時間を一部軽減できます。</li>
<li>ftpsync を実行します (~/bin/ftpsync)。LOGDIR で指定したディレクトリにログが書き込まれるので、tail -f /home/andy/logs/rsync-ftpsync.log のようにしてうまくダウンロードできているか監視するのがよいでしょう。</li>
<li>ftpsync での rsync ミラーは 2 回行われます。最初にパッケージプールの pool/ ディレクトリへの追加ミラーが行われ、そのあとにパッケージ情報ファイルを格納する dists/ ディレクトリの更新と、上流で削除されたファイルの削除が行われます。こうすることで、パッケージ情報にはあるのにプールにファイルがないという事態を避けられます。</li>
<li>上流からミラーがうまくできたことを確認したら一段落です。Web サーバ経由で http://サーバ名/debian/ でミラーを参照できる (つまり、http://サーバ名/debian/dists/stable のように参照できる) ようにしておきましょう。</li>
</ol>


<h3>push ミラー化する</h3>

<p>前述の方法で上流からミラーを引っ張ってくる (pull) ことはできるようになりましたが、いつ上流の更新のミラーを行うかという問題があります。だいたいのアタリを付けて cron で定期的に実行するという方法もありますが、上流で何か不具合があったときにはその影響を被ってしまいます。push ミラーは、上流が自身のミラーの完了後に下流に向けてシグナルを送り (push)、これを受けて初めて下流がミラーを開始するというものです (下図を参照)。
</p>

<div class="img"><img src="pushmirror.png" width="568" height="365" alt="pushミラーの仕組み"></div>

<p>
シグナルには、SSH アクセスが一般的に用いられています。上流のミラーユーザの Debian ミラーのための SSH 公開鍵 (<a href="../project/organization.html#board">理事会</a>または<a href="../project/organization.html#admin">管理チーム</a>に尋ねてください) をサイト上のミラーユーザの認証済み鍵リング (~/.ssh/authorized_keys) に加え、この鍵でのアクセスを受けたときに ftpsync を起動する、というように設定します。
</p>
<pre>
no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty,command="/home/ftpadm/bin/ftpsync &amp;" ssh-rsa AB12....2vIg== ftpadm&#64;ftp.debian.or.jp
</pre>
<p>上流からシグナルの送信テストを行い、ミラースクリプトが起動することを確認します。あとは、マスターサーバでの更新が発生するごとに順にシグナルとミラーの波及が行われるようになります。</p>

<h3>push ミラー元の設定</h3>

<p>ちなみにシグナルを送る側は、etc/runmirrors.conf.sample を etc/runmirrors.conf としてコピーし、これを編集します。</p>

<ul>
<li>KEYFILE: 相手に SSH ログインするときに使う秘密鍵ファイル。複数のミラーを行う場合は異なる鍵ファイルを指定する必要がある</li>
<li>LOGDIR: ftpsync.conf と合わせる</li>
</ul>

<p>同様に、etc/runmirrors.mirror.sample を etc/runmirrors.mirror としてコピーし、ミラーシグナルを送るホストの情報を末尾に指定します。一般的な書式は次のとおりです。</p>

<pre>
all ホスト名またはニックネーム ホスト名 相手ユーザ名
</pre>

<p>たとえば ftp.example.jp の ftpadm ユーザにシグナルを送る (ログイン試行する) には、次のようになります。</p>

<pre>
all ftp.example.jp ftp.example.jp ftpadm
</pre>

<p>etc/ftpsync.conf の HUB 行のコメントを取り、「HUB=true」とします。これで、ミラー完了後にシグナルを送るようになりました。手動でシグナル送出を試すには、~/bin/runmirrors を実行します。</p>

<h3>CDN に加盟する</h3>

<p>ここまでできたら、あとは CDN に加盟するだけです。CDN は、DNS サーバを使ったラウンドロビンの一種ですが、重み付けを設定し、加盟サイトの生存確認も行うことで、耐障害性が高く負荷も分散されたダウンロードサービスを提供します (下図を参照)。</p>

<div class="img"><img src="cdn.png" width="487" height="410" alt="通常のDNSラウンドロビンとCDNによるラウンドロビンの違い"></div>

<p>push ミラー構築後、<a href="../project/organization.html#board">理事会</a>に連絡してください。重み付けの設定のため、あなたの提供できる帯域についての情報も付加していただけると助かります。連絡を受けた<a href="../project/organization.html#admin">管理チーム</a>は、あなたのミラーを CDN のネットワークに追加します。「host cdn.debian.net」で DNS 問い合わせを実行し、あなたのサーバが加わっていることを確認してください。</p>

<h3>その他</h3>

<p>ロボットによる絨毯爆撃的なアクセスなどへの対処は、ミラー担当者のローカルアクセスポリシーに基いて随時設定を行ってかまいません。広範囲な拒否設定を行う場合には、<a href="../project/organization.html#board">理事会</a>および<a href="../project/organization.html#admin">管理チーム</a>に一報していただければ幸いです。</p>

<h3>参考文献</h3>
<ul>
<li><a href="[% wwworg %]/mirror/ftpmirror">Debian アーカイブミラーの構築</a></li>
<li><a href="http://www.debian.or.jp/blog/cdn_debian_or_jp-start.html">cdn.debian.or.jp試験運用開始</a></li>
</ul>
