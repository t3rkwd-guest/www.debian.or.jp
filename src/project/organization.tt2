[%
  title = "組織構成"
%]
    <h2>Debian JP Project の組織構成</h2>
    <P>
    Debian JP Project の特定の事柄について誰かに連絡を取りたいときには、
    以下の組織構成表 (2023年度) が役立つでしょう。
    </P>

    <h3><a id="board" name="board" href="#board">理事会</a> &lt;board&#64;debian.or.jp&gt;</h3>

  <ul>
    <li>会長: &lt;leader&#64;debian.or.jp&gt;
      <ul>
        <li>佐々木 洋平 <small>(前職: 同左)</small></li>
      </ul>
    </li>
    <li>監事
      <ul>
        <li>河田鉄太郎 <small>(前職: 同左)</small></li>
      </ul>
    </li>
    <li>副会長
      <ul>
        <li>吉野 与志仁 <small>(前職: 同左)</small></li>
        <li>杉本 典充 <small>(前職: 同左)</small></li>
      </ul>
    </li>
    <li>事務局長
      <ul>
        <li>倉澤 望 <small>(前職: 同左)</small></li>
      </ul>
    </li>
  </ul>
    <p class="small">※プレス対応、新規会員・寄付の受け付けも理事会にて承ります。</p>

    <h3><a id="keyring" name="keyring" href="#keyring">アカウント管理受付／鍵署名</a></h3>

    <ul>
      <li>アカウント管理受付／公開鍵管理
    <ul>
          <li>理事会: &lt;board&#64;debian.or.jp&gt;</li>
          <li>アカウント作業担当: システム管理チーム
      <li>公開鍵作業担当: 武藤 健志</li>
      <li>公開鍵作業担当: 前田 耕平</li>
    </ul>
      </li>
    </ul>

    <h3><a id="admin" name="admin" href="#admin">システム管理</a></h3>

    <ul>
      <li>システム管理チーム: &lt;debian-admin&#64;debian.or.jp&gt;
    <ul>
      <li>リーダ: 鵜飼 文敏</li>
      <li>構成員: 荒木 靖宏</li>
      <li>構成員: 佐野 武俊</li>
      <li>構成員: 野首 貴嗣</li>
      <li>構成員: 能城 茂雄</li>
      <li>構成員: 八田 修三</li>
      <li>構成員: 武藤 健志</li>
      <li>構成員: 森本 淳</li>
      <li>構成員: 安井 卓</li>
      <li>構成員: 杉浦 達樹</li>
      <li>構成員: 前田 耕平</li>
    </ul>
      </li>
    </ul>

    <h3><a id="vote" name="vote" href="#vote">投票</a></h3>

    <ul>
      <li>投票管理: &lt;voteadm&#64;debian.or.jp&gt;
            <ul>
        <li>担当: 事務局長</li>
        </ul>
      </li>
    </ul>


    <h3><a id="document" name="document" href="#document">ドキュメント</a></h3>

    <ul>
      <li>Debian.org Web コミッタ: &lt;debian-www&#64;debian.or.jp&gt;
        <ul>
        <li>構成員: 今井 伸広</li>
        <li>構成員: 武藤 健志</li>
        <li>構成員: 前原 恵太</li>
                </ul>
      </li>
      <li>Debian-Installer コミッタ: &lt;debian-doc&#64;debian.or.jp&gt;
        <ul>
        <li>構成員: 武藤 健志</li>
        <li>構成員: 倉澤 望 (鍋太郎)</li>
        <li>構成員: やまねひでき</li>
        </ul>
      </li>
      <li>リリースノート コミッタ: &lt;debian-doc&#64;debian.or.jp&gt;
        <ul>
        <li>構成員: 武藤 健志</li>
        <li>構成員: やまねひでき</li>
        </ul>
      </li>
<!--
      <li>Debian ウィークリーニュース翻訳チーム: &lt;debian-www&#64;debian.or.jp&gt;
        <ul>
        <li>リーダ: 今井 伸広</li>
        <li>構成員: 田村 一平</li>
        </ul>
      </li>
      <li>Debian Security 翻訳アナウンス: &lt;debian-www&#64;debian.or.jp&gt;
        <ul>
        <li>構成員: かねこ せいじ</li>
        <li>構成員: 中野 武雄</li>
        </ul>
      </li>
-->
    </ul>

    <h3><a id="list" name="list" href="#list">メーリングリスト</a></h3>

    <ul>
      <li>アナウンス: &lt;debian-announce&#64;debian.or.jp&gt;</li>
      <li>ユーザ向け: &lt;debian-users&#64;debian.or.jp&gt;</li>
      <li>開発者向け: &lt;debian-devel&#64;debian.or.jp&gt;</li>
      <li>ドキュメント: &lt;debian-doc&#64;debian.or.jp&gt;</li>
      <li>Web: &lt;debian-www&#64;debian.or.jp&gt;</li>
    </ul>
