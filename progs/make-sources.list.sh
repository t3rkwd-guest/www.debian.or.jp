#!/bin/sh
# MirrorsJP.list から sources.list のサンプルを生成
LANG=ja_JP.EUC-JP
export LANG
MIRRORLIST=MirrorsJP.list
if [ $# = 1 ]; then
 OUTDIR=$1
else
 OUTDIR=www.debian.or.jp/using/apt
fi
[ ! -d $OUTDIR ] && mkdir -p $OUTDIR

make_sources_list()
{
    echo "# See sources.list(5) for more information, especially"  >  $OUTDIR/sources.list.$METHOD.$NAME
    echo "# Remember that you can only use http, ftp or file URIs" >> $OUTDIR/sources.list.$METHOD.$NAME
    echo "# CDROMs are managed through the apt-cdrom tool."        >> $OUTDIR/sources.list.$METHOD.$NAME

# DDTPはunstable APTがまだ対応してない (kmuto)
#    if test $DDTP = "yes"
#    then
#        echo "" >> $OUTDIR/sources.list.$METHOD.$NAME
#        echo "# もし Debian パッケージの description (説明文) を日本語で読みたいならば" >> $OUTDIR/sources.list.$METHOD.$NAME
#        echo "# 次の 2 行の先頭の # を外し、さらにその次の 1 行を # でコメントアウトしてください。" >> $OUTDIR/sources.list.$METHOD.$NAME

#	echo "#deb ${SITE}debian-ddtp ja/${VERSION} main"     >> $OUTDIR/sources.list.$METHOD.$NAME
#	echo "#deb ${SITE}debian ${VERSION} contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

    if test $DEBIAN = "yes"
    then
	echo "deb ${SITE}debian ${VERSION} main contrib non-free non-free-firmware" >> $OUTDIR/sources.list.$METHOD.$NAME
    fi

#    if test $DEBIANNONUS = "yes"
#    then
#	echo "deb ${SITE}debian-non-US ${VERSION}/non-US main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

#    if test $DEBIANJP = "yes"
#    then
#	echo "deb ${SITE}debian-jp ${VERSION}-jp main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

#    echo "" >> $OUTDIR/sources.list.$METHOD.$NAME

#    if test $DEBIAN = "yes"
#    then
#	echo "#deb ${SITE}debian proposed-updates main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

#    if test $DEBIANJP = "yes"
#    then
#	echo "#deb ${SITE}debian-jp proposed-updates-jp main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

    echo "" >> $OUTDIR/sources.list.$METHOD.$NAME

    echo "deb http://security.debian.org/debian-security ${VERSION}-security main contrib non-free non-free-firmware" >> $OUTDIR/sources.list.$METHOD.$NAME

    echo "" >> $OUTDIR/sources.list.$METHOD.$NAME

    if test $DEBIAN = "yes"
    then
        echo "# Uncomment if you want to use Debian stable-updates" >> $OUTDIR/sources.list.$METHOD.$NAME
	echo "#deb ${SITE}debian ${VERSION}-updates main contrib non-free non-free-firmware" >> $OUTDIR/sources.list.$METHOD.$NAME
    fi

    echo "# Uncomment if you want the apt-get source function to work" >> $OUTDIR/sources.list.$METHOD.$NAME

    if test $DEBIAN = "yes"
    then
	echo "#deb-src ${SITE}debian ${VERSION} main contrib non-free non-free-firmware" >> $OUTDIR/sources.list.$METHOD.$NAME
    fi

#    if test $DEBIANNONUS = "yes"
#    then
#	echo "#deb-src ${SITE}debian-non-US ${VERSION}/non-US main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

#    if test $DEBIANJP = "yes"
#    then
#	echo "#deb-src ${SITE}debian-jp ${VERSION}-jp main contrib non-free" >> $OUTDIR/sources.list.$METHOD.$NAME
#    fi

    if test $DEBIAN = "yes"
    then
	echo "#deb-src ${SITE}debian ${VERSION}-updates main contrib non-free non-free-firmware" >> $OUTDIR/sources.list.$METHOD.$NAME
    fi
}

VERSION=$(grep " stable_codename" include/config | cut -d\' -f2)

while read LINE
do
    PARAMETER=$(echo $LINE | sed -e "s/:.*//")
    VALUE=$(echo $LINE | sed -e "s/.*: //")

    if test "$(echo $LINE | sed -e 's/\(.\).*/\1/')" = "#"
    then
	continue
    fi

    if test -z $PARAMETER
    then
	continue
    fi

    if test $PARAMETER = "Site"
    then
	SITE=$VALUE
	METHOD=$(echo $SITE | sed -e "s/:.*//")
	NAME=$(echo $SITE | sed -e "s|.*://||" | sed -e "s|/.*||")
    elif test $PARAMETER = "Debian"
    then
	DEBIAN=$VALUE
    elif test $PARAMETER = "DebianJP"
    then
        DEBIANJP=$VALUE
    elif test $PARAMETER = "DebianNonUS"
    then
        DEBIANNONUS=$VALUE
    elif test $PARAMETER = "DDTP"
    then
        DDTP=$VALUE
    elif test $PARAMETER = "Volatile"
    then
        VOLATILE=$VALUE
    else
	continue
    fi

    if test -z $SITE || test -z $DEBIAN || test -z $DEBIANJP || test -z $DEBIANNONUS || test -z $DDTP || test -z $VOLATILE
    then
	continue
    fi

    make_sources_list

    SITE=""
    DEBIAN=""
    DEBIANJP=""
    DEBIANNONUS=""
    DDTP=""
    VOLATILE=""
done < $MIRRORLIST
