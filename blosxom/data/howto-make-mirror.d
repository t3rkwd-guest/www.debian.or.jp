CDN ミラーの構築方法の紹介

<p><a href="http://www.debian.or.jp/blog//cdn_debian_or_jp-start.html">以前にご紹介した CDN ミラーサーバ</a>の強化にあたり、ディスクとネットワークに余裕のあるサイトで CDN に対応した Debian のミラーを構築するための<a href="http://www.debian.or.jp/community/push-mirror.html">ガイド (http://www.debian.or.jp/community/push-mirror.html)</a>を作成いたしました。</p>
<p><a href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>では、CDN 対応ミラーサーバを随時募集しています。また、既存の pull 型ミラーサイトを push ミラー化することも推奨しております。</p>
