Debian 4.0 "Etch" のアップデートリリース (Debian GNU/Linux 4.0r5)

<p>
Debian GNU/Linux バージョン 4.0 (stable/安定版)、コードネーム "Etch" の 5 回目のマイナーアップデートとなる 4.0r5 が、2008 年 10 月 23 日に Debian Project からリリースされています。</p>
<p>
本アップデートは、Debian GNU/Linux 4.0 の機能向上ではなく、既存パッケージのセキュリティ修正および重要な問題の更新 (apache2 パッケージのセキュリティ修正、linux2.6 での Xen や xfs での問題の修正、postgresql-8.1 での開発元での修正の導入など) を目的としたものです。</p>
<p>
更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。CD および DVD イメージは、<strong>今現在(10/25)準備中</strong>であり、後ほど<a href="http://www.debian.org/CD/netinst/">「最小の CD を使って、ネットワークインストールする」のページ</a>などから取得可能です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2008/20081023">4.0r5 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/etch/ChangeLog">Changelog</a>の内容を参照してください。
</p>
