Software Design 2015 年 05 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2015/201505"><img src="http://www.debian.or.jp/image/book/sd201505.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2015/201505">Software Design 2015 年 05 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は "reproducible build" について・cdn.debian.net の運用停止・最新chrome/chromiumでのDebianでの利用状況などを取り上げています。</p>
<p>
ぜひ一度記事をご覧になって、雰囲気を掴んでみてください。

<br>

また、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJPこうほうチーム (@debianjp)</a> などへもどうぞ)。</p>
