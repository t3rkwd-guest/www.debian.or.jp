「Debian GNU/Linux クイックインストール解説」の Etch 編を公開

<p>
Debian のインストールから基本デスクトップ環境の構成までの手順を簡潔にまとめた、
「<a href="http://www.debian.or.jp/using/quick-etch/">Debian GNU/Linux クイックインストール解説</a>」を、最新の Debian GNU/Linux 4.0 Etch に合わせて改訂いたしました。
</p><p>
Etch ではインストーラや各パッケージの改良により、デスクトップ環境の構築はますます簡単になっています。本ガイドを見ながら、あなたのマシンに Debian をインストールしてみませんか。
</p>
