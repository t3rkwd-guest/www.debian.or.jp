Ubuntu Weekly Recipe で Debian 7.0 「Wheezy」 の紹介

<p>
Debian JP Project 会員の執筆記事が掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/">gihyo.jp</a> で連載中の <a href="http://gihyo.jp/admin/serial/01/ubuntu-recipe">Ubuntu Weekly Recipe</a> にて、
<a href="http://gihyo.jp/admin/serial/01/ubuntu-recipe/0277">第277回 Debian 7.0 「Wheezy」 の紹介</a>が掲載されています。
記事内容は、リリースされた Debian 7.0 (Wheezy) と使い始めの TIPS を紹介しています。<br>
どうぞご覧になってください (感想等を twitter で <a href="https://twitter.com/gihyojp">gihyo.jp (@gihyojp)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。
</p>
