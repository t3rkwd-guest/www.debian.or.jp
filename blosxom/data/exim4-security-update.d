Exim への攻撃観測とセキュリティ更新についての注意喚起

<p>
Debian のデフォルトメールサーバパッケージである Exim に、細工を施した SMTP 通信にてバッファオーバーフローを引き起こし <strong>root 権限を外部から取得される脆弱性</strong>が発見されています。外部から侵入を受ける脆弱性は<a href="http://www.debian.org/security/2010/dsa-2131">既に Debian では修正済</a>ではありますが、<strong>すでに攻撃プログラムが公開されて容易に入手できる</strong>ので、多くのサイトにてこの更新を適用してないために被害に遭うケースがみられます。</p>
<p>
Debian による公開サーバを運用されている方は、以下の点をチェックしてください。</p>
<ul>

<li>メールサーバを Exim4 で運用していないかどうか。Exim4 は Debian において、MTA が必要になる場合に依存関係上、自動的にインストールされることがあります。なお、postfix など他のメールサーバを利用している場合は、今回の場合は問題がありません。</li>
<li>Exim4 をインストールしている場合、外部からのメールを受け取る設定になっているかどうか (ローカル配送専用のメールサーバとして Exim4 が導入されている場合は、外部からは攻撃を受けない可能性が高くなりますが、依然としてローカルユーザからは攻撃が可能な点にはご注意ください)。</li>
<li><strong>12/10 以降</strong>にサーバの更新を実施しているかどうか。</li>
<li>以下のようにして、Exim4 パッケージがセキュリティ更新されたものであるかを確認してください。<a href="http://packages.debian.org/lenny/exim4">こちら</a>で現在の安定版でのバージョンの確認が可能になっています (現在のところ 4.69-9+lenny1 以上であれば問題ありません)。<br>
<pre>
$ dpkg -s exim4 | grep Version
Version: 4.69-9+lenny1</pre></li>
<li>既に被害にあっていないかどうか。
<ul>
<li>現在確認されているところでは、<strong>/var/spool/exim4 以下</strong>にツールが配置されているケースがあります。以下のようなファイルが配置されている場合は、残念ながら既に侵入されています (<a href="http://www.reddit.com/r/netsec/comments/en650/details_of_the_root_kit_that_got_installed_on_my/">参考URL</a>) 。
<pre>
-rw------- 1 Debian-exim Debian-exim     117 Dec 15 16:41 a.conf
-rw------- 1 Debian-exim Debian-exim     119 Dec 15 16:41 e.conf 
drwxr-xr-x 3 root        root             75 Dec 16 18:00 rk
-rw------- 1 root        root        4421289 Dec 15 20:13 rk.tgz
-rw-r--r-- 1 root        root              0 Dec 16 13:26 s
-rw-r--r-- 1 root        root              0 Dec 16 13:26 s.c
-rwsr-xr-x 1 root        root           6764 Dec 15 23:29 setuid
-rw------- 1 Debian-exim Debian-exim    3120 Dec 15 16:41 setuid.1
-rw------- 1 Debian-exim Debian-exim     130 Dec 15 16:41 setuid.c
</pre></li>
<li>別の被害ケースでは、上記のような状態にはなっていなかったが、netstat コマンドの実行 (netstat -antp) で dropbear という SSH サーバを別途インストールされていることを発見し、さらに確認したところ、外部から接続可能なように root ユーザの SSH の設定に鍵が追加されていたとのことです。</li>
</ul>
<li>その他、メールログやシステムログに異常がみられないかの確認。</li>
</ul>
<p>
対策:
<ul>
<li><strong>すぐに Exim4 のアップデートを実施</strong>してください (aptitude update; aptitude safe-upgrade などしてパッケージを最新の状態に保ってください)。</li>
<li>最新のセキュリティ更新情報が送付される <a href="http://lists.debian.org/debian-security-announce/">debian-security-announce メーリングリストの購読</a>や <a href="http://www.debian.org/security/dsa">セキュリティ更新情報の RSS の取得</a>を検討ください (日本語訳は <a href="http://www.debian.or.jp/community/ml/openml.html#usersML">debian-users メーリングリスト</a>で配布されています)。</li>
<li><a href="http://packages.debian.org/lenny/apticron">apticron パッケージ</a>を使った自動更新の確認などを検討ください。</li>
</ul>
<p>
今回の問題は、容易に侵入を許し root 権限を奪われる非常に危険なケースです。十分に注意・確認をお願いします。なお、今回の更新に加えて権限上昇に対する Exim4 のアップデートが配布される予定がありますので、その際も更新をお願いします。</p>

