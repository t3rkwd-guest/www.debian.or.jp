Software Design 2014 年 10 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201410"><img src="http://www.debian.or.jp/image/book/sd201410.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201410">Software Design 2014 年 10 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は、2014 年 9 月号に引き続きRed Hat Enterprise Linuxとの対比で「yum/aptの機能比較とその操作」を中心に説明を行っています。いつもはyum派、という方もaptでの操作方法を知ることができるいい機会です。

<br>

どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
