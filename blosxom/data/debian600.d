Debian 6.0 "Squeeze" リリース!!!

<p>以前のリリースから約２年近くを経て、<strong>Debian 6.0 コードネーム “Squeeze” が 2011/02/06、ついにリリースされました</strong> (リアルタイムで <a href="http://identi.ca/debian">identi.ca の Debian アカウント</a>が実況していたのでご覧になった方も多かったかと思います)。リリースに携わった多数の開発者、バグ報告者、利用者の皆様に感謝致します。パッケージの大幅な追加／削除／更新を含んでいる今回の更新ですが、アップグレード作業の注意点など、詳細については<a href="http://www.debian.org/releases/squeeze/releasenotes">Debian GNU/Linux 6.0 リリースノート</a>を参照してください。</p>
<p>
リリースノートには、Debian 6.0 で追加・変更されたさまざまな機能説明を各アーキテクチャごとに掲載しているほか、前リリース (Debian GNU/Linux 5.0 コードネーム“Lenny”) からのアップグレードを行うユーザーのための手順と注意も記述されています。いくつかの重要なパッケージに大幅な変更があるため、前リリースからのアップグレードを行う場合には、必ずこのリリースノートに目を通してください (例: <a href="http://www.debian.org/releases/squeeze/i386/release-notes/ch-upgrading.ja.html">i386</a>、<a href="http://www.debian.org/releases/squeeze/amd64/release-notes/ch-upgrading.ja.html">amd64</a>)。</p>
<p>
リリースノートの翻訳にあたっても、Debian-JP Doc メーリングリスト上で多数の翻訳者・査読者が力を合わせました。この場を借りて感謝いたします。</p>
<p>
なお、このリリースにより旧安定版(oldstable) Debian GNU/Linux 5.0 "Lenny" のサポートは残り約１年ほどとなります。Lenny の利用者の方は直前で慌てないよう、移行計画を立てて速やかに更新ください。</p>
