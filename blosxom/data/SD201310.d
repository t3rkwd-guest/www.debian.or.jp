Software Design 2013 年 10 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201310"><img src="../image/book/sd201310.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201310">Software Design 2013 年 10 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
前回に引き続いて Debian での Ruby 環境、特にパッケージングについて取り上げています。
どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
