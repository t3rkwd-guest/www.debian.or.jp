Debian 6.0.6 リリース

<p>Debian GNU/Linux 6.0 コードネーム “Squeeze” のポイントリリースが 9 月 29 日に行われ、バージョンが 6.0.6 となりました。</p>
<p>
本アップデートには、既存パッケージのセキュリティ修正および重要な問題への対応（特に今回の場合は<strong>Linux カーネルの閏秒対応の修正</strong>）が行われています。更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2012/20120929">6.0.6 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/squeeze/ChangeLog">Changelog</a>の内容を参照してください。
</p>

