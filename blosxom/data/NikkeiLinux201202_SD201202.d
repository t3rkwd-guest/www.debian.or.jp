日経Linux / Software Design 2012 年 2 月号記事掲載

<p>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://itpro.nikkeibp.co.jp/article/MAG/20120106/377917/">日経Linux 2012 年 2 月号「[玄人好み 最新「Fedora」「Debian」「KNOPPIX」「Vine Linux」 Linuxディストリビューションの意外な魅力」</a>にて、岩松信洋とやまねひできの２名が記事を担当しました。<br>
また、<a href="http://gihyo.jp/magazine/SD/archive/2012/201202">Software Design 2012 年 2 月号の第二特集「あなたをもっと自由にするOS　Debianを使い倒せ！」</a>として佐々木 洋平・のがたじゅん・倉敷 悟・山下 尊也・かわだ てつたろうの５名が記事を担当しています。興味のある方は現在発売中ですので、これを機会にぜひ書店でお手に取ってみてください。</p>
