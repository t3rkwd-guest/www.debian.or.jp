Software Design 2014 年 9 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201409"><img src="http://www.debian.or.jp/image/book/sd201409.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201409">Software Design 2014 年 9 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は「設定ファイルの読み方・書き方でわかるLinuxのしくみ（Debian編）」として、Red Hat Enterprise Linux 6 を前提に記述された Software Design 2014 年 6 月号の記事「設定ファイルの読み方・書き方でわかるLinuxのしくみ」をDebianの流儀で見た場合をおさらいしています。Red Hat系ディストリビューションに慣れていてDebian/Ubuntuを触る必要が出てきた方は、その差異に戸惑われることもあるかと思いますが、その辺を押さえた内容となっています。<br>

どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
