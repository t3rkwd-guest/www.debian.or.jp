Software Design 2014 年 11 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201411"><img src="http://www.debian.or.jp/image/book/sd201411.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201411">Software Design 2014 年 11 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は、8月末にアメリカ・オレゴン州で開催された例年のカンファレンス「DebConf14」のレポートを開催地のポートランドの観光ガイドなども交えてお送りしています。

<br>

どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
