<strong>Debian GNU/Linux 5.0 "lenny" のリリースノート翻訳者募集中</strong>

<dl>
<dt>Debian 5.0 "lenny" リリースノート作業者募集</dt>
<dd>
 <ul>
  <li>対象：翻訳者</li>
  <li>必要人数：5、6名程度</li>
  <li>作業：<a href="http://lists.debian.or.jp/debian-doc/200810/msg00011.html">こちらを参照ください</a></li>
 </ul>
</dd>
<dd>
 <ul>
  <li>対象：査読者</li>
  <li>必要人数：多ければ多いほど :-)</li>
  <li>作業：<a href="http://www.debian.or.jp/community/ml/openml.html#docML">debian-doc メーリングリスト</a>で流れる翻訳を読んで間違いの発見やより良い日本語の提案を行う</li>
 </ul>
</dd>
</dl>
<p>
リリースが待望されている Debian 5.0 コードネーム “Lenny” での更新点を記す「Debian 5.0 リリースノート」ですが、大まかに内容が固まり、<a href="http://lists.debian.org/debian-i18n/2008/10/">翻訳作業の開始が宣言</a>されました。そこで、現在<strong><a href="http://lists.debian.or.jp/debian-doc/200810/msg00011.html">日本語への翻訳作業／査読の協力者を募集</a></strong>しています。翻訳者は5、6名程度、査読者 (訳が日本語文として意味合いがとれるか、原文とあっているかどうか確認) については何人でも結構です。</p>
<p>
本リリースノートには、Debian 4.0 から 5.0 で追加・変更されたさまざまな機能説明を各アーキテクチャごとに掲載しているほか、前リリース (Debian GNU/Linux 4.0 コードネーム“Etch”) からのアップグレードを行うユーザーのための手順と注意が記述されています。前リリースからのアップグレードを行う場合には、必ずこのリリースノートに目を通す必要がある重要なものです。 (例: Debian GNU/Linux 4.0 "Etch" <a href="http://www.debian.org/releases/etch/i386/release-notes/ch-upgrading.ja.html">i386 アーキテクチャのリリースノート</a>、<a href="http://www.debian.org/releases/etch/amd64/release-notes/ch-upgrading.ja.html">amd64 アーキテクチャのリリースノート</a>)。
</p>
<p>
これから作業期間として断続的に 2 〜 3 週間程度 (つまり11月中旬あたりまで) を見込んでいます。<br>
Debian 次期公式リリースに<strong>「あなた」</strong>の足跡を残しませんか？ :-)
</p>
<p>
興味のある方は、<a href="http://lists.debian.or.jp/debian-doc/200810/msg00011.html">こちら</a>をご覧の上、<a href="http://www.debian.or.jp/community/ml/openml.html#docML">debian-doc メーリングリスト</a>へメール頂ければ幸いです。</p>

