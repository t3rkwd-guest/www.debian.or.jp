Software Design 2013 年 3 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201303"><img src="../image/book/sd201303.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201303">Software Design 2013 年 3 月号より
<strong>「Debian Hot Topics」</strong></a>というタイトルにて Debian 関連記事の連載が始まっています。初回は やまねひでき が担当しました。</p>
<p>
3頁ほどの短かなエッセー記事ですのでお気軽にご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>

