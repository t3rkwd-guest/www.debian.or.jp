Software Design 2014 年 2 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201402"><img src="../image/book/sd201402.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201402">Software Design 2014 年 2 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は64bit ARMへの対応状況の進捗、テスト版でのRCバグ状況などを紹介しています。<br>
また、第二特集「2014年IT業界はどうなるのか？」にも寄稿をしております。</p>
<p>
どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>

