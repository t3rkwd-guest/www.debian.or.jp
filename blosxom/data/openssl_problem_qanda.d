OpenSSL パッケージの脆弱性と脆弱なパッケージを含まない他の環境への影響について（Q&amp;A)

<p>
既に幾つかのメディアで取り上げていただいていたり、ユーザ間でも話題になっているこの問題ですが、誤解を招かないよう注意が必要な点が幾つかあります。以下の問答を参考に誤解をしていないかどうか確認をお願い致します。</p>

<dl>
<dt><strong>Q: 影響は Debian だけ？</strong>〜「Debian とその派生ディストリビューションが影響を受けるだけなんでしょ？私は（*BSD / 他のディストリビューション）を使ってるから関係ないですよね。」</dt>
<dd>
<p>
<strong>A: いいえ、残念ながらそうではありません。</strong></p>
<p>
今回の問題は欠陥があるバージョンの OpenSSL パッケージを使って<strong>作られた鍵／証明書が脆弱である</strong>というところにあります。例えば運用しているサーバが Red Hat Enterprise Linux だったとして、ユーザの ~/.ssh/authorized_keys に該当の欠陥がある状態で作られた鍵が登録されていたら、その Red Hat Enterprise Linux に不正なアクセスが容易な状態にあることになります（例に上げた Red Hat Enterprise Linux は CentOS でも Fedora でも FreeBSD でも他のものでも置き換えて考えてください）。</p></dd>

<dt>Q: 自分のところは Sarge を運用しているので全く平気ですよね！</dt>
<dd>
<p>
<strong>A: いいえ、残念ながらそうではありません。</strong></p>
<p>
例えば、脆弱性のあるバージョンの OpenSSL を含んだ環境でユーザが SSH 鍵を作成していて、それを authorized_keys に登録した場合を想像してください…その Sarge を運用しているホストへの侵入は容易な状態となっています。<strong>Etch であれば更新された openssh-server と既知の脆弱な鍵リストである openssh-blacklist によってその様な鍵を使った場合に接続が拒否されますが、Sarge はセキュリティサポートが終了しているためにこの様な危険な状態からは保護されません</strong>。</p></dd>

<dt>Q: では、どうやって私のサーバが影響があるのかを確認すれば良いのですか？</dt>
<dd>
<p>
<strong>A: Debian Project より提供されている <a href="http://security.debian.org/project/extra/dowkd/dowkd.pl.gz">Debian OpenSSL Weak Key Detector (dowkd)</a> を利用してください。</strong>
</p>
<p>
これは perl で書かれたスクリプトで、perl が導入されている環境であれば OS / ディストリビューションを問わずに実行できます (Debian では 5.8.8 及び 5.10 にて動作が確認されています)。実行前のファイルの正当性の確認には <a href="http://security.debian.org/project/extra/dowkd/dowkd.pl.gz.asc">OpenPGP 署名</a>を確認してください。なお、このスクリプトは随時更新されていますので、適宜チェックしてください。</p>
</dd>
<dd>
<p>ユーザが自分の authrized_keys に脆弱な鍵が含まれていないかをチェックした例</p>
<pre>
user@localhost:~$ perl dowkd.pl file ~/.ssh/authorized_keys 
/home/user/.ssh/authorized_keys:1: weak key
summary: keys found: 1, weak keys: 1</pre>

<p>SSH サーバが脆弱なホスト鍵を利用していないかをリモートからチェックした例</p>
<pre>
user@localhost:~$ perl dowkd.pl host 192.168.100.100
# 192.168.100.100 SSH-2.0-OpenSSH_4.3p2 Debian-9
# 192.168.100.100 SSH-2.0-OpenSSH_4.3p2 Debian-9
192.168.100.100: weak key
192.168.100.100: weak key</pre></dd>


<dt>Q: ダウンロードした Debian OpenSSL Weak Key Detector (dowkd) が正当なものであるかを確認したいのですが。</dt>
<dd>
<p>
以下の様にして、正当性の確認が可能です。環境として PGP あるいは GPG が利用可能であることが必要となります (参考として、以下の作業は Debian sid 環境で確認を行っています)。</p>
<ol>
<li>セキュリティ勧告のメールの PGP 署名を確認
<li>PGP 署名の ID が誰のものであるかを確認
<li>PGP 鍵サーバから確認した ID の公開鍵を取得
<li>dowkd.pl と署名ファイル dowkd.pl.gz.asc をダウンロードして同一ディレクトリに配置
<li>署名を確認</ol></dd>

<dd>
<p>(1) まず、<a href="http://lists.debian.org/debian-security-announce/2008/msg00152.html">今回のセキュリティ勧告のメール (DSA-1571)</a>にサインされている署名を確認します。</p>
<p>セキュリティ勧告が流れる debian-security-announce@lists.debian.org は PGP 署名をされた特定のアドレスからのみ投稿可能なメーリングリストですので、Florian Weimer さんのメッセージはまず真正なものであろう、と判断できます。より確実にこれが Florian Weimer さんが書いたものである、という確認するために<a href="http://lists.debian.org/debian-security-announce/2008/msg00152.html">該当のメール</a>をファイルとして保存して以下の確認をします。</p>
<pre>
$ gpg --verify \[SECURITY\]_\[DSA_1571-1\]_New_openssl_packages_fix_predictable_random_number_generator.txt 
gpg: Signature made 2008年05月13日 21時03分24秒 JST using RSA key ID 02D524BE
gpg: Can't check signature: public key not found</pre></dd>

<dd>
<p>
(2) セキュリティ勧告が 02D524BE という ID の鍵で署名されている事が確認できましたので、その鍵を所持しているのが誰なのかを公開鍵サーバから検索します。</p>
<pre>
$ gpg --keyserver pgp.nic.ad.jp --search-keys 02D524BE
gpg: searching for "02D524BE" from hkp server pgp.nic.ad.jp
(1)	Florian Weimer (HIGH SECURITY KEY) &#60;fw@deneb.enyo.de&#62;
	Florian Weimer (HIGH SECURITY KEY) &#60;Weimer@CERT.Uni-Stuttgart.DE&#62;
	Florian Weimer (HIGH SECURITY KEY) &#60;Florian.Weimer@RUS.Uni-Stuttgart.D
	Florian Weimer (HIGH SECURITY KEY) &#60;fw@deneb.enyo.de&#62;
	Florian Weimer (HIGH SECURITY KEY) &#60;Weimer@CERT.Uni-Stuttgart.DE&#62;
	Florian Weimer (HIGH SECURITY KEY) &#60;Florian.Weimer@RUS.Uni-Stuttgart.D
	  2048 bit RSA key 02D524BE, created: 2002-03-19
Enter number(s), N)ext, or Q)uit > Q</pre>
<p>
上記の様に Florian Weimer <fw@deneb.enyo.de> さんの鍵であることが確認できました。<a href="http://lists.debian.org/debian-security-announce/2008/msg00152.html">DSA-1571</a>は Florian Weimer &#60;fw@deneb.enyo.de&#62; さん (02D524BE) が少なくとも署名したものです。</p></dd>

<dd>
<p>
(3) 署名した鍵の確認ができたので、Florian Weimer さんの公開鍵 (02D524BE) を公開鍵サーバから取得します。</p>
<pre>
$ gpg --keyserver pgp.nic.ad.jp --recv-keys 02D524BE
gpg: requesting key 02D524BE from hkp server pgp.nic.ad.jp
gpg: key 02D524BE: duplicated user ID detected - merged
gpg: key 02D524BE: public key "Florian Weimer (HIGH SECURITY KEY) <fw@deneb.enyo.de>" imported
gpg: 3 marginal(s) needed, 1 complete(s) needed, classic trust model
gpg: depth: 0  valid:   1  signed:  11  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: depth: 1  valid:  11  signed:  12  trust: 2-, 0q, 0n, 5m, 4f, 0u
gpg: depth: 2  valid:  10  signed:   5  trust: 10-, 0q, 0n, 0m, 0f, 0u
gpg: Total number processed: 1
gpg:               imported: 1  (RSA: 1)
</pre></dd>


<dd>
<p>(4) dowkd.pl と dowkd.plについて署名されたファイルを取得します。</p>
<pre>
$ wget http://security.debian.org/project/extra/dowkd/dowkd.pl.gz
$ wget http://security.debian.org/project/extra/dowkd/dowkd.pl.gz.asc</pre></dd>

<dd>
<p>(5) 先ほどファイルを取得したディレクトリで dowkd.pl についての署名を確認します。</p>
<pre>
$ gpg --verify dowkd.pl.gz.asc
gpg: Signature made 2008年05月23日 05時39分49秒 JST using RSA key ID 02D524BE
gpg: Good signature from "Florian Weimer (HIGH SECURITY KEY) <fw@deneb.enyo.de>"
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: C8D3 D9CF FA9E 7056 3F32  FA54 BF7B FF04 02D5 24BE</pre>
<p>
Good signature from "Florian Weimer (HIGH SECURITY KEY) &#60;fw@deneb.enyo.de&#62;" とあるので、OpenSSL のセキュリティ勧告メールを流した Florian Weimer さんが、dowkd.pl についても署名をしたことが確認できました。</p>
<p>
なお、この場合　WARNING が出ているのは、自身が所有しているデータの中では「信用の輪(web of trust)」の中に含まれていない鍵であることを意味しています。この鍵が真正に Florian Weimer さんのものであることを信用するかについては、鍵交換などを実施していくか、<a href="http://pgp.nic.ad.jp/pks/lookup?op=vindex&search=0x02D524BE">その鍵に署名している他の人のリスト</a>などから別途確認ください。</p></dd>


<dt>Q:SSL の証明書も問題になるのですか？確認の仕方が知りたいのですが</dt>
<dd>
<p>
<strong>A: これも、<a href="http://security.debian.org/project/extra/dowkd/dowkd.pl.gz">Debian OpenSSL Weak Key Detector (dowkd)</a> を利用してください</strong>。PEM ファイルのチェックが行えます。</p>

<dt>Q:SSL の証明書の再発行は費用が発生するので行いたくないのですが…</dt>
<dd>
<p>
<strong>A: 残念ながら OpenSSL パッケージに該当の脆弱性があった期間に作られたものは、再度申請などして作り直しが必要になります。</strong></p>
<p>
ベンダによっては今回の場合について SSLサーバ証明書の残存期間の無償再発行や値引きでの対応を行うなどとしているところがありますので、詳しくは各ベンダに対応について問い合わせてください。（もし、情報がありましたら追記したいと思いますので、<a href="http://www.debian.or.jp/community/ml/openml.html#wwwML">お教えください</a>。</p>
<dl>
<dt>SSL 証明書取扱いベンダの対応</dt>
<dd>
<ul>
<li><a href="http://www.verisign.co.jp/">日本ベリサイン株式会社</a><br>
<a href="http://www.verisign.co.jp/support/maintenance/announce20080520.html">「Debian GNU/Linux に含まれる OpenSSL/OpenSSH の脆弱性」について</a>(<strong>通常価格より値引き</strong>での対応)</li>

<li><a href="http://jp.globalsign.com/">グローバルサイン株式会社</a><br>
<strong>通常でも期限内であれば<a href="http://jp.globalsign.com/support/index.php?action=artikel&cat=2&id=42&artlang=ja">無償再発行を行っている</a></strong>ため、特別な対応はなし。</li>

<li><a href="http://www.cybertrust.ne.jp/">サイバートラスト株式会社</a><br>
<a href="http://www.cybertrust.ne.jp/info/2008/080519.html">Debian GNU/Linuxに含まれるOpenSSL/OpenSSHの脆弱性への対応に関するお知らせ〜 該当するSSLサーバ証明書の残存期間の無償再発行を開始 〜</a>(<strong>無償再発行</strong>)</li>

<li><a href="http://www.secomtrust.net/">セコムトラストシステムズ株式会社</a><br>
<a href="http://www.secomtrust.net/service/ninsyo/pdf/GNULinux_20080522.pdf">「Debian GNU/Linux に含まれる OpenSSL/OpenSSH の脆弱性に関する注意喚起」について (PDF)</a>（<strong>証明書発行から30日以内のものについて、無償で対応</strong>）</li>

<li><a href="http://jp.comodo.com/index.html">株式会社コモドジャパン</a><br>
<a href="http://www.comodo.com/news/press_releases/21_05_08.html">COMODO OFFERS FREE REPLACEMENT CERTIFICATE TO ANY INDIVIDUALS AFFECTED BY DEBIAN VULNERABILITY FLAW</a> (<strong><a href="http://www.instantssl.com/ssl-certificate-support/debian/ssl-certificate-contact.html">無償再発行</a></strong>)</li>

<li><a href="http://www.thawte.com">Thawte</a>  (Verisign に買収されており、ブランドとして存続)<br>
<strong>通常でも期限内であれば<a href="http://www.thawte.com/reissue/?click=buyssl-buttonsleft">無償再発行を行っている</a></strong>ため、特別な対応はなし。</li>

<li><a href="http://www.geotrust.com/">GeoTrust</a> (Verisign に買収されており、ブランドとして存続)<br>
GeoTrust ブランドの場合 (QuickSSL等)、<strong>通常でも期限内であれば<a href="http://www.geotrust.com/products/ssl_certificates/quick_ssl.asp">無償再発行を行っている</a></strong>。<a href="http://www.geotrust.com/resources/cert_reissuance/index.asp">再発行手続きはこちら</a></li>

<li><a href="http://www.rapidssl.com/index_ssl.htm">RapidSSL</a> (Verisign に買収されており、ブランドとして存続)<br>
再発行については通常は有料対応。<strong><a href="http://www.rapidssl.com/ssl-certificate-products/rapidssl/usd/ssl-certificate-rapidssl-platinum.htm">RapidSSL + Platinum Support の場合</a>は無償再発行が可能</strong>。<a href="https://products.geotrust.com/geocenter/reissuance/reissue.do">再発行手続きはこちら</a></li>

</ul>
</dd></dl>
</dd></dl>



