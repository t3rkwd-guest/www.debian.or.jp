Debian GNU/Linux 4.0r4a リリースとサポート機器数増加について

<p>
一ヶ月ほど前の話題になりますので既にご存知の方も多数いらっしゃるかとは思いますが、2008 年 7 月 26 日に Debian GNU/Linux 4.0r4 (Etch の更新版) 、さらにその後修正版である 4.0r4a がリリースされました (「a」が付加されているのはリリースの手違いで本来 r4 にて同梱すべきパッケージが同梱されていなかったのを修正したためです)。なお、<strong>今回は今までの更新版と違う点</strong>が１点だけあります。</p>
<p>
今回の違いは、単にパッケージの更新／追加／削除に止まらず、<strong>新しいカーネル (2.6.24) と X ドライバなど「も」含むもの</strong>となっています(これは etch-and-a-half と呼ばれています。なお、旧来のカーネル (2.6.18) と X ドライバも利用可能です）。</p>
<p>
旧来の Debian 4.0 etch でインストールが無理な場合は、公式インストールイメージ (etch-and-a-half) の利用について検討してください。これにより、利用が可能、あるいは機能が向上する機器が増加しております。</p>
<p>
今回の更新に伴う新しいカーネルのバージョンや X ドライバについてなど、詳細は <a href="http://www.debian.org/releases/etch/etchnhalf">Debian GNU/Linux 4.0 -- Etch-And-A-Half リリースノートのページ</a>をご覧ください。</p>


<h5>インストール用 CD イメージについての注意</h5>
<p>
インストール用 CD イメージについて注意が1点あります。</p>
<p>
Debian は、多数のパッケージが存在するので CD イメージだけでのインストールを行おうとすると膨大な枚数が必要となるため、一番インストールが楽な方法として netinst イメージなどを利用してのインストールが多数となっています。</p>
<p>
ただし、netinst/名刺サイズ CD については、従来のカーネルバージョン (2.6.18) と新しいカーネル (2.6.24) を使ったものでは<strong class="attention">「メディアファイルが別になっている」(ファイル名が違う)</strong> のでご注意ください。</p>
<dl>
<dt>例：一般的な PC 用 netinst イメージ (i386)</dt>
<dd>・旧カーネルバージョン (2.6.18): debian-40r4a-i386-netinst.iso</dd>
<dd>・新カーネルバージョン (2.6.24): debian-40r4a-etchnhalf-i386-netinst.iso</dd>
</dl>
<p>
インストールは PC を有線 LAN にてインターネット接続可能な状態で、上記 netinst イメージ (debian-40r4a-etchnhalf-i386-netinst.iso) を使って実施するのがお勧めです。</p>


<h5>インストール用 CD イメージの入手</h5>
<p>
従来どおり、公式サイトあるいは信頼できるミラーサイトから入手ください。</p>
<dl>
<dt>公式サイト</dt>
<dd><a href="http://cdimage.debian.org/debian-cd/4.0_r4a/">http://cdimage.debian.org/debian-cd/4.0_r4a/</a></dd>
<dt>一般的な PC 用の etch-and-a-half の netinst イメージ例</dt>
<dd>
<a href="http://cdimage.debian.org/debian-cd/4.0_r4a/i386/iso-cd/debian-40r4a-etchnhalf-i386-netinst.iso">http://cdimage.debian.org/debian-cd/4.0_r4a/i386/iso-cd/debian-40r4a-etchnhalf-i386-netinst.iso</a></dd>
</dl>

<h6>付記</h6>
<p>
これまで武藤健志氏が個人的に新しいカーネルを backport したカスタムインストーラを配布していましたが、これはその成果を取り入れたものとなっております。武藤氏のこれまでの尽力に感謝申し上げます。</p>　

