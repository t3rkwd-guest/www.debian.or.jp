Debian 7.7 リリース (10/18)

<p>Debian GNU/Linux 7 コードネーム “wheezy” のポイントリリースが 10 月 18 日に行われ、バージョンが 7.7 となっています。</p>
<p>
本アップデートには、既存パッケージのセキュリティ修正および重要な問題への対応が行われています。更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。</p>
<p>
更新された内容の詳細については、<a href="https://www.debian.org/News/2014/20141018">7.7 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/wheezy/ChangeLog">Changelog</a>の内容を参照してください。
</p>
