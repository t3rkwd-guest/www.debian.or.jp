Debian 3.1 "Sarge" がアーカイブサイト (archive.debian.org) へ移動

<p>
Debian 3.1 "Sarge" はその役目を終え、セキュリティアップデートが終了していることは<a
href="http://www.debian.or.jp/blog//debian31r8.html">周知のとおり</a>ですが、
先般ミラーサーバの容量肥大化を防ぐための処置としてリポジトリからも削除され、
通常ではインストールおよびパッケージの導入ができない様になっています。</p>
<p>
<strong>既にセキュリティサポートが終了しており、現在発売されている機器では導入も困難な Sarge</strong>
ではありますが、万が一 Sarge 時点でのパッケージを必要とする場合については、
<a href="http://archive.debian.org/">archive.debian.org</a>
という既に役目を終えたリリースの公式アーカイブサイトが存在しているので、
<a href="http://archive.debian.org/debian/dists/Debian-3.1/">そちらを確認</a>してください。</p>

