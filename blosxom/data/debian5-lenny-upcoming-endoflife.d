<strong>Debian GNU/Linux 5.0 "Lenny" のサポート終了まで2ヶ月を切りました</strong>

<p>Debian GNU/Linux 5.0 コードネーム “Lenny”は、当初の予定どおり 2012 年 2 月 6 日を以て、セキュリティサポートの提供が終了します。</p>
<p>現在 5.0 "Lenny" を運用されている方は、早急に現行の安定版 Debian 6.0 "Squeeze" にアップグレードを検討してください。なお、<a href="http://www.debian.org/releases/stable/i386/release-notes/ch-upgrading.ja.html">5.0 から 6.0 へのアップグレードには、<strong>作業手順と注意点がリリースノートにて提供されています</strong></a>ので、こちらも参照してください。</p>

