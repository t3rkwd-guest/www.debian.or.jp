開発ニュース寄せ集め (第 14 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>debhelper の外部コマンドのオプション解析方法が変更になります</h5><p>debhelper の不安定版 (unstable) への次のアップロードがまもなく予定され
ていますが、特定のオプションを使っている一部の debhelper 外部コマンドは、
うまく動かなくなるかもしれません。そのようなコマンドでオプションを有効のまま
にするには、少々変更が必要です。これらのコマンドのメンテナは、詳細について、
<a href="http://lists.debian.org/debian-devel/2009/02/msg00387.html">アナウンスのメール</a>を見てください。</p><p>-- Joey Hess</p><h5>initramfs-tools の Lenny での新機能</h5><p>initramfs-tools はより小さな initramfs に /sys/ を押し込められる様にな
りました。また、ある種の環境ではさらに小さい initramfs を生成できます。
これに関する全ての情報については <a href="http://www.itp.tuwien.ac.at/~mattems/blog/2009/02/05#initramfs-tools-lenny-features">「innitramfs-tools new feature list」</a>
のブログ記事を参照して下さい。</p><p>-- Maximilian Attems</p><h5>bts-link がさらに多くのバグ追跡システムに対応しました</h5><p><a href="http://bts-link.alioth.debian.org/">bts-link</a> が GForge/Fusionforge のバグ追跡システムと Sourceforge の
V2 トラッカーを新たにサポート対象に加えました。それらの開発支援サイトにホスト
されている開発元のプロジェクトにおけるバグを追跡するために <code>forwarded-to</code> 属性
を追加し、それによりバグの状態の変更を通知することが可能になります。</p><p>-- Olivier Berger</p><h5>Debian Data Export</h5><p>Debian の情報を出力するのを容易にするシステム、<a href="http://wiki.debian.org/DDE">Debian Data Export</a>
についての<a href="http://lists.debian.org/debian-devel/2009/02/msg00247.html">アナウンスがありました</a>。</p><p>現在 DDE にあるデータについて、検索したり、ダウンロードをしたい場合は
<a href="http://dde.debian.net/dde">http://dde.debian.net/dde</a> へどうぞ。</p><p>Debian に関するデータをメンテナンスするのであれば、DDE について目を通
すことをお勧めします。シンプルなデータセットによって<a href="http://wiki.debian.org/DDE/StaticData">データの公開はとても
簡単</a>で、容易にデータの検索・アクセスをできるようにします。</p><p>-- Enrico Zini</p><h5>移行を妨げているバグのリスト</h5><p>ライブラリ移行の時期になりました。多くのパッケージをビルドしなおさなければ
なりません。これは、いくつかが FTBFS (ソースからのビルドが失敗する状態)
になる、あるいは移行の進捗を妨げる他の問題が現れるということを意味しています。</p><p>移行を妨げているバグの一覧は、現在 usertag
(user:debian-release@lists.debian.org, tag:transition-blocker)
を通して BTS に保存されています:</p><p><a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org;tag=transition-blocker">http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org;tag=transition-blocker</a></p><p>あるいは、以下の URL が良いでしょう:</p><p><a href="http://snipr.com/transition-blockers">http://snipr.com/transition-blockers</a></p><p>このバグ群に対する NMU のルールが緩められました:</p><p>バグが 3 日以上経つ場合は、パッケージは 0-day アップロード (delay キューに
ではなく、即日のアップロード) が可能となります。 どうか、礼儀正しく、役立つよう
にふるまいましょう。メンテナの希望を尊重しましょう。</p><p>-- Adeodato Simó</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/03/msg00000.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さんが行いました。
また、やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
