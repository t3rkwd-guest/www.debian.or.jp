開発ニュース寄せ集め (第 22 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>LWN サブスクリプション</h5><p>現在、DD に加え、<a href="http://wiki.debian.org/DebianMaintainer">DM</a> も HP がスポンサーしている LWN サブスクリプションを受ける
資格があると Bdale Garbee さんは<a href="http://www.gag.com/bdale/blog/posts/Debian_and_LWN.html">アナウンスしました</a>。
現時点で約571人の DD と DM が HP がスポンサーしている <a href="http://www.debian.org/doc/developers-reference/resources.html#lwn">LWN サブスクリプション</a>
の恩恵を受けています。</p><p>-- Paul Wise</p><h5>ubuntudiff.debian.net</h5><p><a href="http://ubuntudiff.debian.net/">UbuntuDiff</a> と呼ばれる新しいサービスがセットアップされました。
それは、grep-dctrl クエリを指定したり、またはショートカット(例えば<a href="http://ubuntudiff.debian.net/q/package/linux-ntfs">package</a>、
<a href="http://ubuntudiff.debian.net/q/uploaders/mehdi@debian.org">uploaders</a>)を使用することにより
Ubuntu の変化が、 Debianパッケージになされるのを即座に見ることを可能にします。
あなたのフィードバックとバグレポートを歓迎します。</p><p>-- Mehdi Dogguy</p><h5>debian.net の TXT レコード</h5><p>Debian のシステム管理者は、各 debian.net ドメインに対する TXT レコード
を有効にしました。これは、そのドメインが DD によるスポンサードなのか、
あるいは、非 DD によって管理されているのかを示すのに使うとよいとされて
います。CNAME レコードは、その他のレコードとは組み合わせられないので、
debian.net ドメインに CNAME を追加するのではなく、正規の名前に対して
TXT レコードを追加すべきことに注意してください。</p><p>-- Paul Wise</p><h5>Debian システム管理チームは支援を求めています</h5><p>先日、Setephen Gran は移植開発者およびプロジェクトのメーリングリストに
 対して移植用計算機上での chroot のメンテナンスについての<a href="http://lists.debian.org/debian-project/2010/03/msg00062.html">支援要請</a>
 を送りました。数人の人々が応募してくれて、実際に手助けをしています(感謝します!)。
 ですが、まだまだ手が足りていません。 興味のある方はこのメッセージを確
 認し、DSA に連絡を取って下さい。また Peter Palfrader は、移植用の
 <a href="http://lists.debian.org/debian-devel-announce/2010/05/msg00002.html">chroot のアップデートに関する最近のメール</a>において、
 DSA が管理しやすいメール題名のフォーマットについて述べています。</p><p>-- Martin Zobel-Helas</p><h5>PTS 経由で Ubuntu のバグをメール受信</h5><p>気になるパッケージについての、Ubuntu のバグメールを Launchpad を使うことなく
(そこでパッケージ毎の登録をしなくても) 購読することができるようになりました。これは
新しいオプトインの PTS キーワード：derivatives-bugs として実装されています。</p><p>自分のパッケージ全てを購読するなら、(<a href="http://www.debian.org/doc/developers-reference/resources.html#pts-commands">デベロッパーズリファレンス</a>にある通り)
'keyword [email] + derivatives-bugs' を使います。
'derivatives' (Ubuntu diff など。これもオプトインです) をあわせて購読しても
よいでしょう。</p><p>言うまでもないことですが、他の派生ディストリビューションでこのようなデータ提供に
関心があるという場合は、遠慮なく Debian QA チームに連絡してください。</p><p>また、自分がメンテナンスしているパッケージを購読しているか定かでないなら、
<a href="http://udd.debian.org/cgi-bin/pts-check.cgi">UDD スクリプト</a>を使って購読の漏れを確認することができます。</p><p>-- Lucas Nussbaum</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2010/05/msg00006.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん、倉敷悟が行いました。
ありがとうございます。</p>
