開発ニュース寄せ集め (第 7 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>debhelper v7</h5><p>debhelper v7 は、大胆な開発者が利用する準備ができています。このバージョンでは、cdbs
からいくつかのものを学ぼうとしました。その結果できたのが、多くのパッケージでたった 3
行の debian/rules ファイルを作成するのに使える、新しい「dh」コマンドです。
これが、cdbs よりも強力で、より低い学習曲線を持つことを期待しています。
どうぞ、お試しあれ。</p><p>-- Joey Hess</p><h5>DPL を助けましょう</h5><p>我らの愛すべき DPL、Steve McIntyre さんが CD
イメージ作成用のインフラを取り扱うのに<a href="http://lists.debian.org/debian-cd/2008/04/msg00041.html">助力</a>を求めています。週次／日次 ISO
イメージを処理する継続的な作業が必要であり、さらにリリース時に最終 ISO
イメージを生成してテストするための作業も必要です。</p><p>-- Raphaël Hertzog</p><h5>新しい debian-ports.org マシン</h5><p>先週末から、<a href="http://www.debian-ports.org">debian-ports.org</a>
は、より速く多くのディスクスペースを持つ新しいマシンに移行しました。</p><p>このマシンは、非公式のアーキテクチャがメインの Debian
アーカイブに統合されるまで、それらのためのインフラを供給することを目的としています。
これにより、移植者はインフラに苦労せずにパッケージを移植するために時間を使うことができます。</p><p>以下を供給します。</p><ul>
<li>パッケージとインストール CD をホストするリポジトリ</li>
<li>wanna-build データベース</li>
<li>ビルドログ用のウェブインターフェース</li>
<li>DNS ホスティングサービス</li>
<li>NAT の中にあるビルドデーモン (buildd) 用の POP サーバ</li>
</ul><p>このマシンは新しい移植版を受け入れることができ、現在のところ、次は sh4
を受け入れることになるでしょう。新しい移植版を受け入れる条件として、我々は .deb
パッケージの最小限の一式が移植されていることを要求していて、例えば buildd
が実行できる必要があります。より詳しいことは遠慮せずに問い合わせてください。</p><p>このマシンをホストしている Skyrock/Telefun、購入してくれた Debian、
可能にしてくれた前 DPL である Sam Hocevar さんに感謝します。</p><p>-- Aurélien Jarno</p><h5>debconf 翻訳の更新</h5><p>Debian 開発者やメンテナが、各自のパッケージの、lenny
用のバージョンを準備する時期がやってきました。素晴らしい。</p><p>これらのバージョンが含んでいるのは、今までは無視していたような、lintian
がレポートするささいな問題に対するちょっとした変更ばかりです (lintian には、Etch
以降かなりたくさんの新たなチェックが入っています。知ってました？)。これもよいです。</p><p>lintian がレポートするささいな問題には、debconf
テンプレートの書き方についての様々な警告もあります (一人称の使用、
選択/複数選択についての疑問形、注記のタイトルに文を使わない、などなど)。
メンテナはこれらを直した方がよいという気になります。ここまではよいでしょう。</p><p>そして、修正したパッケージをアップロードしますが……これがよくない :-)。</p><p>debconf のテンプレートを変更するとき、po-debconf
パッケージに入っている、その名も "podebconf-report-po"
という素敵なツールの使用を検討してください。これは、
訳が不完全な部分を担当している翻訳者にメールで「翻訳の要請」を送信し、<em>さらに</em>、
新規翻訳の依頼も送信してくれます。</p><p>おすすめの使い方は以下の通りです。</p><pre><code>cd debian/
debconf-updatepo
podebconf-report-po --call --languageteam --with-translators --deadline=`date -d '+10days'`
</code></pre><p>('man podebconf-report-po' にはもっとたくさんのオプションが書かれています……。
上の例では、既存の翻訳の更新要請、翻訳チームへの CC、debian-i18n
への新規翻訳依頼を、翻訳更新期間を最低限必要な 10 日間に設定して送信します。)</p><p>-- Christian Perrier</p><h5>メール (ML) 経由で Planet Debian を</h5><p>Joey Schulze さんは <a href="http://planet.debian.org/">Planet Debian</a>
に集約された記事を配布するメーリングリストがある、とアナウンスしました。
これによって、フィードリーダーを使ったり、RSS
からメールへの変換ユーティリティを個人的に準備したりすることなく、
関心のある人がウェブログの記事を読むことができます。彼の<a href="http://www.infodrom.org/~joey/log/?200804201121">アナウンスの全文</a>はこちら：</p><blockquote>
  <p>折にふれ、自分でウェブサイトを見て回るつもりがなくて、Planet Debian
上で記事を読む必要があるらしいことに不満を漏らす人がいます。RSS
フィードはありますが、これには RSS フィードリーダーを使う必要があります。</p>

<p>こういった人のほとんどは、Debian
プロジェクトでの開発状況を追いかけるためにいくつかのメーリングリストを購読しています。
そういう人たち向けに、Planet Debian
からの記事をメールで配信するメーリングリストを数年前に作成しました。
このやり方だと、Planet Debian は単にスレッドのないメーリングリストのようなもので、
より多くの人々が対応できます。</p>

<p>興味のある方は、次のコマンドを実行してください：</p>

<pre><code>echo subscribe ticker-planetdebian | mail majordomo@lists.infodrom.org
</code></pre>
</blockquote><p>-- Stefano Zacchiroli</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/04/msg00013.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さんが行いました。
また、石井一夫さん・倉敷悟さんから多数のコメントをいただきました。
ありがとうございます。</p>
