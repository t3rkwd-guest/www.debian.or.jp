開発ニュース寄せ集め (第 9 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>quilt の使い方と新しいソース形式の互換性に関するアドバイス</h5><p>将来、"3.0 (quilt)" ソースパッケージ形式に切り替える準備として、全アー
カイブを変換した後、パッケージをリビルドしてみました。この結果、
dpkg-source が既存の多くのビルドセットアップをサポート出来るようコード
を適応させ、多くの改善が得られましたが、dpkg-source 側だけではすべての
問題を解決できません。追加で変更が必要なパッケージに対しては、<a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=0;tag=3.0-quilt-by-default;repeatmerged=1;users=hertzog@debian.org">多くのバ
グ</a>を報告しました。新しいソース形式との互換性を確保するには、以下の
ガイドラインに従ってみてください。</p><p>既に quilt を使っているパッケージ:</p><ul>
<li>すべてのパッチを (-p0 ではなく) -p1 オプションで当てるようにしてくだ
さい。これは quilt のデフォルトですが、手動で取り込んだパッチの中に
は -p0 でしか当てられないものがあります (シリーズファイルではこのオ
プションが強制されます)。これは、quilt refresh で解決できます (詳し
くは (*) を見てください)。</li>
<li>quilt のパッチが、debian ディレクトリ内のファイルを変更したり、ファ
イルを作成または削除しないようにしてください。</li>
<li>.diff.gz ファイルには、自動で生成もしくは更新された (config.guess/sub
などの) ファイルが含まれないようにしてください (これを回避するには
<a href="http://bugs.debian.org/482716">#482716</a> を見てください)。</li>
<li>パッチを直接編集してはいけません。どうしても編集したいなら、パッチ
内の各オフセットが正しくなるよう "quilt refresh" してください。</li>
<li>パッチは debian/patches/ ディレクトリに、シリーズファイルは
debian/patches/series として保存してください。</li>
<li>".pc" ディレクトリの位置を上書きしてはいけません。</li>
<li>パッチを適用/非適用するルールを再発明せず、debian/rules ファイルから
/usr/share/quilt/quilt.make をインクルードしてください。</li>
<li>もし CDBS を使っているなら、/usr/share/cdbs/1/rules/patchsys-quilt.mk
を使いますが、DEB_SRCDIR でトップレベルのソースディレクトリを変更し
てはいけません。</li>
</ul><p>その他のパッケージ:</p><ul>
<li>ソースパッケージにおいて quilt の使用をやめるならば、
quilt のファイル群を残さないで下さい。</li>
<li>tarball 中にさらに tarball はを入れるのは避けてください。
オリジナルの tarball を展開してもソースファイル群をそのまま利用できない
場合には、dpkg-source はパッチを適用できません。
することでソースファイル群が直接入手できない場合、dpkg-source
はパッチを適用できません。</li>
</ul><p>(*) 既存のパッチ群全てを刷新するためには、以下のちょっとしたレシピを
使いましょう。この方法でパッチ群は全て修正されて "patch -p1" で
適用できるようになります。</p><pre><code>quilt pop -a; while quilt push; do quilt refresh -p1; done
</code></pre><p>もし、仮の top-level ディレクトリ名を使いたいならば "quilt refresh -pab"
も使用できます。ソースパッケージのディレクトリ名に は一般に上流のバージョン番号が
含まれています. 仮のトップディレクトリ名を使用することには, ソースパッケージの
ディレクトリ名の変更時に, 修正の必要がないという利点があります.</p><p>-- Raphaël Hertzog</p><h5>update-grub はデフォルトで UUID の使用を開始します</h5><p>update-grub の最新版 (GRUB Legacy の 0.97-40 と GRUB 2 の 1.96+20080601-2) は
デフォルトでファイルシステムの UUID の使用を開始します。GRUB 2 のものだけは
それ自身のファイルを取ってくるために内部的に利用しますが、
両方ともルートファイルシステムの UUID を見つけて、「root」パラメータで Linux に渡します。</p><p>GRUB Legacyでは、この変更は、以前の menu.lst が存在しなかったときの「kopt」パラメータの
作成だけに影響します。もし、menu.lst が既に存在していて、GRUB を更新しているだけであるならば、
kopt は変更されないままです。この方法は新規インストールだけに影響するので、
深刻な問題（もしあるならば）が lenny のフリーズ前に見つけられることができるよう、
既存のインストールで、menu.lst の名前を変更し、update-grub を再実行して
明示的にこの機能をテストするなら、望ましいです。</p><p>-- Robert Millan</p><h5>不安定版 (unstable) への wxwidgets2.8 のアップロード</h5><p>最近、Adeodato Simó が <a href="http://packages.debian.org/source/sid/wxwidgets2.8">wxwidgets2.8</a> を不安定版 (unstable) にアップロード
しました。これは長い間求められていた更新ですが、パッケージメンテナはその使用を
控えることが推奨されています。というのは、<a href="http://packages.debian.org/source/sid/wxwidgets2.6">wxwidgets2.6</a> で動作しない
パッケージのみがこの新しいバージョンを使用するべきであり、切替が早過ぎた場合
に安定性と新しいバグに関する問題が発生することが想定されるからです。このこと
は、<a href="http://www.debian.org/releases/lenny">Lenny</a> のデフォルトとして wxwindows2.6 を維持するという決定の背景に
ある理由でもあります。まだ wxwindows2.6 で動作するアプリケーションに対して
新しい widgetset を使用したテストが非公式に、もしくは実験的に実現してから、
様々な判断をするべきです。</p><p>-- Gerfried Fuchs</p><h5>リリースノートの更新を指揮するボランティアを必要としています</h5><p>Frans Pop さんが Sarge と Etch のリリースノートのリードエディターを担当
してきましたが、現在はこの<a href="http://lists.debian.org/debian-doc/2008/05/msg00008.html">職を辞めました</a>。
Lenny のリリースノートのため、新しいエディターチームを必要としていますが、
今までのところ、名乗り出た人はいません。
リリースチームとの緊密な共同作業を支援するのは、開発者ではない人にとって
良い機会です。
気軽に参加し<a href="http://bugs.debian.org/release-notes">オープンの状態になっているバグ</a>に取りかかってください。</p><p>-- Raphael Hertzog</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/07/msg00000.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・中尾隆さん・佐々木洋平さんが行いました。
また、やまねひできさん・武藤健志さん・倉敷悟さんから多数のコメントをいただきました。
ありがとうございます。</p>
