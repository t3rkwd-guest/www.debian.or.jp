開発ニュース寄せ集め (第 4 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Debian 拡張プロポーザル</h5><p>Lars Wirzenius さん、Stefano Zacchiroli さん、Adeodato Simó さんは、
<a href="http://dep.debian.net">Debian 拡張プロポーザル (DEP)</a> という概念を導入しようとしています。
議論の発端となった <a href="http://lists.debian.org/debian-project/2008/01/msg00045.html">dato (Simó さん) のメール</a>を引用すると、
このプロポーザルの目的は以下の 2 つです。</p><ol>
<li><p>現在進行中のプロポーザルの状態を明確に示す。
特に、何らかの設計や実装が合意に達している場合は、
それを明示して議論を決着させる。そして、
実装に費やした時間が無駄にならないことを保証し、
その実装を採用することによる「懸念」をなくす (これは過去における Debian
の「広範な採用対標準のドキュメント」というループを断ち切るのに役立つはず)。</p></li>
<li><p>昔からのやり方やフォーラムで行った決定や達した合意を記録する、
中心となる場所・機構をもつ。「ああ、
メーリングリストの <em>あの</em> スレッドね」ではなく、後から参照できるように。</p></li>
</ol><p>このプロポーザルは、DEP
の文書自体を保存する技術的な方法についての議論が残されてはいますが、
すでに多大な支持を受けています。</p><p>-- Raphael Hertzog</p><h5>wnpp.debian.net</h5><p><a href="http://wnpp.debian.net">http://wnpp.debian.net</a> では、
作業が望まれるパッケージおよびそれらの統計についての、
すてきな概要のページや RSS フィードが提供されています。</p><p>-- Holger Levsen</p><h5>debcheckout</h5><p>よくある要望の中から、ずいぶんお待たせしている debcheckout
についてお知らせします。</p><p>2007 年 9 月から利用できるようになった debcheckout は、
指定したパッケージをバージョン管理システム (VCS) でチェックアウトする、
新しい <a href="http://packages.debian.org/devscripts">devscript</a> です。コマンドラインでパッケージ名を指定すると、
パッケージの Vcs-XXX フィールドを解析し、
必要なバージョン管理システムを使って debian
化されたソースツリーをチェックアウトしようとします。VCS 情報を表示するだけ、
あるいは通常の URL からパッケージをチェックアウトする、といったこともできます。
使い始めは、大抵 <code>debcheckout パッケージ名</code> とするだけで十分です。</p><p>通常、VCS の URL は匿名でのチェックアウトを指すものなので、
チェックアウトしたリポジトリにコミットすることはできません。しかし、debcheckout
は典型的な Debian の URL スキーム (例えば、alioth や launchpad
に設置されている VCS) を理解するので、(認証用に) <code>-a</code> フラグを渡すことで、
チェックアウトで使う URL を改変してコミットできるようにしてくれます。もちろん、
これにはコミットするための権限が必要ですが、Debian 開発者 (DD) は全員、
いくつかのリポジトリ (collab-maint など) については既に条件を満たしています。</p><p>追加情報については、<code>debcheckout (1)</code> マニュアルページを見てください。</p><p>-- Stefano Zacchiroli</p><h5>DEHS から一言</h5><p><a href="http://dehs.alioth.debian.org">DEHS</a> に (おそらく、作成以来となる) 「大きな」変更が若干なされてから、
しばらく経ちました。最近なされた変更のリストをここに示します。</p><p>新しいルックアンドフィール:: DEHS のウェブサイトは、
他の *.d.o ページと似た外見になりました。
さらに将来いくつかスタイルを変更するつもりです :)。</p><p>DEHS の統計:: しばらく前から、DEHS は、Debian
全体の「外部状態」の時間変化を示す<a href="http://dehs.alioth.debian.org/stats.html">統計ページ</a>を提供するようになりました。
いくつかの値が急上昇したように見えますが、
これはバックエンドのちょっとした変化や微調整であって、
アーカイブに実際に大きな変化が起きたわけではありません。</p><p>uscan の更新:: DEHS は、利用できる最新版の uscan を使うようになりました。
これからも最新版を使い続けます。これによって、最新の uscan
における改良点を使って、チェックの品質を向上できるようになります。</p><p>新しい上流バージョンの通知:: DEHS は、新しい上流のバージョンを見つけたときに、
PTS の 'summary' タグ/キーワード宛てに通知を送るようになりました。
このような通知を受け取ってみたいと思う人は、PTS
のドキュメントを読めば<a href="http://www.debian.org/doc/manuals/developers-reference/ch-resources.en.html#s-pkg-tracking-system">購読方法</a>が分かります。
誤った上流のバージョンを報告する watch
ファイルを含むパッケージもありますので注意してください。
そのようなパッケージに気付いたらバグ報告をするつもりです。</p><p>データがより新しく:: DEHS は、
新しいパッケージや更新されたパッケージだけなら毎日、
アーカイブ全体については月に何度か、データを更新するようになりました。つまり、
データがこれまでより新しくなっています。
タイムスタンプを利用してパッケージのチェックの頻度を上げるよう、
現在さらにいくつかの作業を行っています。これによって作業負荷が減り、
同時にさらに新しいデータを提供できるようになるでしょう。</p><p>以上のリストに何か重要な変更を含め忘れていないといいのですが。DEHS
の開発への参加に興味のある方、DEHS についてもっと知りたい方は、
どうぞ私にご連絡ください (DEHS の開発作業が続いてほしいと思う人を Stefano
が拒絶することはないでしょう)。</p><p>-- Raphael Geissert</p><h5>PTS と DEHS の統合</h5><p>PTS と DEHS の統合の最初の実装が利用可能になっています。</p><p>まず、あるパッケージの "summary" キーワードを PTS で購読している場合、
<a href="http://dehs.alioth.debian.org">DEHS</a> によって新しい上流のバージョンが利用可能であるのが検出された時に、
DEHS から通知メールを受け取るようになります (訳注: "summary"
キーワードの購読については、<a href="http://www.debian.org/doc/manuals/developers-reference/">Debian Developer's Reference</a>
の <a href="http://www.debian.org/doc/manuals/developers-reference/ch-resources.en.html#s-pts-commands">4.10.1 The PTS email interface</a> を参照してください)。
DEHS 側での作業を行った Raphael Geissert さんに賛辞を呈します。</p><p>また一方、PTS が更新される度に、
より新しい上流のバージョンを持つパッケージに関する情報と、uscan
の実行に失敗したパッケージに関する情報が集められます。PTS のページで、
より新しい上流のバージョンを持つパッケージは TODO
節に相応の項目を持つようになりますが、uscan
の実行に失敗したパッケージは Problems 節に相応の項目を持ちます。</p><p>-- Stefano Zacchiroli</p><h5>symbols ファイルでのワイルドカードの使用</h5><p>dpkg-dev 1.14.16 以降の dpkg-gensymbols は、debian/*.symbols ファイルにおいて、
ライブラリの任意のバージョン以降のシンボルすべてに同じ依存関係を与えるための、
新しい構文 (例: *@GLIBC_2.0) をサポートしています。これによって、
シンボルにバージョン番号がついている健全なライブラリに、容易に symbols
ファイルを提供できるようになりました。さらに詳しく知りたい場合は dpkg-gensymbols
のマニュアルページをチェックしてください。</p><p>-- Raphael Hertzog</p><h5>dpkg の機能削除のスケジュールと API 情報</h5><p>dpkg 1.14.16 以降では、/usr/share/doc のパッケージディレクトリ内に 2
つのファイル (README.feature-removal-schedule と README.api)
が含まれるようになりました。
それぞれ、削除が予定されている機能の削除スケジュールと、dpkg や dpkg-dev
が提供する API の状態を記述したものです。</p><p>これらは、オンラインでも、
<a href="http://git.debian.org/?p=dpkg/dpkg.git;a=blob;f=README.feature-removal-schedule;hb=master">http://git.debian.org/?p=dpkg/dpkg.git;a=blob;f=README.feature-removal-schedule;hb=master</a>
と <a href="http://git.debian.org/?p=dpkg/dpkg.git;a=blob;f=README.api;hb=master">http://git.debian.org/?p=dpkg/dpkg.git;a=blob;f=README.api;hb=master</a>
で参照できます。</p><p>-- Guillem Jover</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/02/msg00000.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さん・小林儀匡が行いました。
また、かねこせいじさん・倉敷悟さん・武井伸光さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
