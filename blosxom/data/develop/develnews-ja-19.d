開発ニュース寄せ集め (第 19 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>GPG キーサイン coordination は wiki.debian.org に移動しました</h5><p>GPG キーサイン coordination page は、旧来の PHP で稼働するシステムから
 Debian wiki に移動し、より簡単に編集できるようになりました。</p><ul>
<li>http://wiki.debian.org/Keysigning</li>
<li>http://wiki.debian.org/Keysigning/Offers</li>
<li><p>http://wiki.debian.org/Keysigning/Need</p>

<p>-- Christoph Berg</p></li>
</ul><h5>Debian OpenSSH の VCS の変更と支援の要請</h5><p>Debian にとって重要な OpenSSH パッケージのメンテナンスを支援したいと考
え、最終的に、よりよい VCS でパッケージの履歴を管理することにし、実際
に使えるようになりました。詳しくは、
http://lists.debian.org/debian-ssh/2010/01/msg00017.html と
http://wiki.debian.org/Teams/DebianSsh を見てください。Debian 開発者で
支援に興味のある人は、私まで連絡してください。</p><p>-- Colin Watson</p><h5>LXDE のカスタマイズがより簡単になりました。</h5><p>sid にある LXDE の構成要素の幾つかは、最近になって更新もしくは変更され
ました。</p><p>この更新/変更によって、lxsession ではカスタムセッション名がサポートさ
れました。また、lxpanel は XDG<em>CURRENT</em>DESKTOP を読めるようになり、
LXDE の代わりに他のデスクトップ環境のアプリケーションメニューも表示で
きるようになりました。これによって、Debian Pure Blends を用いたカスタ
マイズされた LXDE 作成がより容易に行なえます。</p><p>lxnm と lxpanel-netstat-plugin はともに廃止予定です。lxnm は wicd への
移行が推奨されており、live-helper などの他の Debian のユーティリティ
では既にそのように変更されています。</p><p>-- Andrew Lee (李健秋)</p><h5>WNPP BTS レポートがカテゴリ分けされました</h5><p>ユーザタグとユーザカテゴリを活用して、<a href="http://bugs.debian.org/wnpp">WNPP</a> BTS レポートの見え方が新しくなりました。
これによって、他より多くのバグをもっている (疑似) パッケージの BTS ページでナビゲーション
がしやすくなります。Sandro Tosi さんが、カテゴリをあてはめるための適切な制御メッセージを
思いついてくれたおかげです。</p><p>バグの並び替えに使っているユーザタグはレポートの表題を元にしています。
<a href="http://www.debian.org/devel/wnpp/#l2">WNPP のサイト</a>でフォーマットが説明されています。</p><p>-- Raphael Geissert</p><h5>ソースフォーマット "3.0 (quilt)" の更新</h5><p>dpkg 1.15.5.4 以降の dpkg-source は、quilt が使用する <code>.pc</code> ディレクトリを
(quilt を使わない場合でも) 作成します。また、パッチが適用されているかいないかを
把握するために <code>.pc/applied-patches</code> も使います。このように quilt とは 100%
互換ですので <code>--without-quilt</code> オプションはなくなります。</p><p>同じバージョンで、新しいオプション <code>--single-debian-patch</code> が作られました。
VCS を使っている人は dpkg-source を使って、(debian-changes-&lt;バージョン&gt; の
代わりに) debian-changes という単一のパッチを作成して自動更新できます。
これはフォーマット 1.0 における .diff.gz にやや似ていますが、その他の機能 (bzip2
圧縮や、複数の tar 玉など) の利点があります。
debian/source/patch-header にこの自動パッチのヘッダをセットすることができますので、
そこで変更内容を確認しやすい場所 (大抵の場合は VCS のどこか) を説明することができます。</p><p>パッケージの転換は、安定したペースで行われています：
http://upsilon.cc/~zack/stuff/dpkg-v3/</p><p>自分のパッケージを転換する方法については、http://wiki.debian.org/Projects/DebSrc3.0
で基本的な情報を確認してください。</p><p>-- Raphael Hertzog</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2010/01/msg00001.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・倉敷悟が行いました。
ありがとうございます。</p>
