開発ニュース寄せ集め (第 12 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Bdale Garbee さんが書記を代行</h5><p>Manoj Srivastava さんは、書記を<a href="http://lists.debian.org/debian-vote/2008/12/msg00275.html">辞任しました</a>。
以下の、私たちの<a href="http://www.debian.org/devel/constitution#7">規約第7.2項</a>に従い、DPLが新しい人を任命するまで、
現技術委員会の委員長 Bdale Garbee さんが<a href="http://lists.debian.org/debian-vote/2008/12/msg00292.html">書記代行を務めています</a>。</p><p>-- Raphael Hertzog</p><h5>全パッケージの状態管理追跡に関する新しい提案</h5><p>リリースの大きな遅れを回避するため、不安定版 (unstable) に何千もの RC
バグを残さないよう、それぞれのパッケージにはアクティブなメンテナが必要
です。これを達成するためには、パッケージに新しいメンテナが必要になったと
きに、できるだけ早く正確にそれを見つけなくてはいけません。<a href="http://lists.debian.org/debian-qa/2008/12/msg00046.html">この提案</a>は
この問題を解決し、さらに、メンテナが自身のパッケージに対して実施してい
るメンテナンスなどを自己評価するよう、定期的に尋ねることを目的としてい
ます。このアイデアについて、気軽に意見を出してください。</p><p>-- Raphael Hertzog</p><h5>Packages-arch-specific の管理変更</h5><p>wanna-build管理者達は、Package-arch-specific ファイル(どのパッケージが
どのアーキテクチャから除外されているかが記載されたファイル)に手を入れ
ました. このため, ビルドインフラストラクチャーを調整するこの重要な
ファイルの登録方法が少々変わります:</p><p><a href="http://lists.debian.org/debian-devel/2008/12/msg00340.html">http://lists.debian.org/debian-devel/2008/12/msg00340.html</a></p><p>-- Philipp Kern</p><h5>Githubredir が利用できるようになりました</h5><p>Gunnar Wolf さんは <a href="http://githubredir.debian.net/">GitHub-Redir</a> を<a href="http://gwolf.org/node/1873">発表しました</a>。
このサイトは主に uscan のためのもので、GitHub で任意のコミットを
.zip または .tar.gz でダウンロードできるようにしたものです。
それは突然、バージョン番号で git tag でき、GitHub がそれを
魔法のようにダウンロード可能な状態にするようになったようなものです。
なんてすばらしいことでしょう！</p><p>それらの形式を追跡するのは多少面倒なときもありますが、
GitHub は個々のプロジェクトのタグの一覧を提供し、
それらのタグのどれもが両方のフォーマットでのダウンロードページを持ちます。</p><p>-- Rene Mayorga</p><h5>Lenny 用に更新された Babelbox</h5><p>"Babelbox" デモシステムが Lenny 用に更新されました。D-I Lenny RC1 でテストされ
ましたが、これ以上の変更無しに RC2 でも動作するはずです。</p><p>インストーラ内の様々な変更の結果、Babelbox をインストールして使用するのは Etch の
時と比べて多少簡単になりました。</p><p>Babelbox は、Debian インストーラの自動化機能を実証するために設計されたシステムです。
インストーラにとって利用可能な多数の翻訳及びその品質も示してくれます。Babelbox は、
展示会他、イベントのブースでの実演に適しています。</p><p>さらなる情報については、こちらをご覧ください:</p><p><a href="http://wiki.debian.org/DebianInstaller/BabelBox">http://wiki.debian.org/DebianInstaller/BabelBox</a></p><p>-- Frans Pop</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/12/msg00009.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さんが行いました。
また、今井伸広さん・武井伸光さん・やまねひできさん・victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
