開発ニュース寄せ集め (第 26 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>DebConf ニュース</h5><p>DebConf チームは現在、DebConf12 の<a href="http://meetbot.debian.net/debconf-team/2011/debconf-team.2011-03-01-20.01.html">入札</a>の検討と <a href="http://meetbot.debian.net/debconf-team/2011/debconf-team.2011-03-08-20.01.html">DebConf11</a>
の準備についてミーティングをしています。
DebConf12 をどこで開催するかを決めるミーティングは、協定世界時の 4 月 22 日の
20:00 から開催されます。</p><p>最新の <a href="http://lists.debconf.org/lurker/message/20110224.204950.5f426a36.en.html">DebConf11 の報告</a> によると、特に国と地方の行政からのサポートにより、
Debconf11 の準備は順調とのことです。</p><p>ボスニアを訪れるためのビザを必要とする、またはビザについて何か問題がある可能性の
ある人は、ビザについての<a href="http://debconf11.debconf.org/visas.xhtml">ウェブページ</a>をみて、penta で登録して visa@debconf.org
で準備チームに連絡をとりましょう。DebConf11 準備チームはスルプスカ共和国政府から
援助を受けています。</p><p>DebConf11 は、4 月 1 日までには登録できるようになっているのが目標です。
いつも通り、DebConf に参加するための資金提供が必要なら、最終期限までに
申し込んでください。資金提供が受けられるかについては悲観しないでください。
あなた自身や会社が DebConf を財政的に援助してくれるのであれば、
資金提供についての<a href="http://debconf11.debconf.org/sponsorship.xhtml">ウェブページ</a>をご覧頂き、可能な額の寄付をお願いします。</p><p>カンファレンスの期間、とてもやりがいのある<a href="http://wiki.debconf.org/wiki/DebConf11/Teams#Volunteers">ボランティア</a>を行うことを含め、
別の方法で DebConf11 を援助するには、"どのような援助ができますか？"の
<a href="http://debconf11.debconf.org/helpus.xhtml">ウェブページ</a>を見てください。</p><p>DebConf に参加した、またはどのようなものか興味があれば、3 月 14 日に
でることになっている、DebConf10の最終報告を探し、一読してください。
報告は昔のものも<a href="http://media.debconf.org/">存在し</a>、フィンランドでの DebConf5 までさかのぼることができます。</p><p>-- Paul Wise</p><h5>Multiarch サポート</h5><p><a href="https://wiki.ubuntu.com/MultiarchSpec">multi-arch</a> サポートに向けて dpkg は順調に進んでいます。
しかし、複数のアーキテクチャから同じパッケージを co-install する
ことが可能となることにより様々なパッケージにおいて想定していることが
通用しなくなるでしょう。その問題と、それがどのように
修正されるかについての詳細を知るには、この<a href="http://lists.debian.org/20110302140611.GH20023@rivendell.home.ouaza.com">メッセージ</a>を debian-devel
で確認してください。それらの問題で影響を受けそうなあらゆるパッケージの
報告を返してください。</p><p>-- Raphaël Hertzog</p><h5>win32-loader.exe がミラーネットワークからも利用可能に</h5><p>バージョン 0.6.22 以降、FTP マスターの支援のおかげで、win32-loader.exe
は <a href="http://ftp.ch.debian.org/debian/tools/win32-loader/unstable/win32-loader.exe">Debian の公式ミラー</a>からもダウンロード出来るようになりました。Windows
マシンで実行した場合、この実行ファイル (x86 CD に収録されている setup.exe
のスタンドアローンフレーバ) は Debian-Installer カーネルおよび initrd
をダウンロードして、Debian-Installer からシームレスに再起動出来るように Windows
のブートローダを設定します。すべてのバージョンの Windows で動作すると思われ、GNU/Linux
もしくは GNU/kFreeBSD のどちらのフレーバの Debian-Installer でもダウンロード出来ます。</p><p>-- Didier Raboud</p><h5>チーム特有の NM 質問集</h5><p>チーム特有の P&amp;P、T&amp;S 質問集を私宛に送って欲しいと(呼びかけました)[12]。
返信は整理した後にチーム毎に一つのファイルとして NM テンプレートリポジト
リに追加する予定です。これらは NM にとってだけではなく、人々に、そのチー
ムの動きについて精通する方法を提供する上で有益なものとなるでしょう。</p><p>-- Enrico Zini</p><h5>Debian ゲームチームのミーティング</h5><p>Debian ゲームチームでは、3 月 18 日の午後 9:00 UTC に<a href="http://wiki.debian.org/Games/Meetings/2011-03-18">ミーティング</a>を
予定しています。Debian の内外でゲームをメンテナンスしていたり、ゲームと Debian
に関心がある人に参加するよう呼びかけて、いろいろなトピックについて一緒に議論
したいと考えています。議題はまだ決まっていませんので、どんな項目について話を
するか決めるために、<a href="http://doodle.com/e53i5dad794vxadq">投票</a>に書きこみをお願いします。</p><p>-- Paul Wise</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2011/03/msg00006.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん・倉敷悟が行いました。
また、victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
