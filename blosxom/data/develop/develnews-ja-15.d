開発ニュース寄せ集め (第 15 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>主要なデスクトップ環境のアップデート</h5><p>Debian における三つの主要なデスクトップ環境は更新されたか、近日中に更新
されることになりました。<a href="http://www.xfce.org">Xfce 4.6</a> は既に unstable にアップロードさ
れています。<a href="http://www.kde.org">KDE4.2.2</a> も同様です。<a href="http://www.gnome.org">GNOME 2.26</a> についても今後
数日中にこれらに続くでしょう。</p><p>-- Yves-Alexis Perez</p><h5>Alioth が lenny に更新され、FusionForge 4.7 になりました</h5><p>Alioth.debian.org (別名、VCS/共同開発サーバー) が Lenny に アップグレード されました。
gforge は FusionForge 4.7 に切り替えられ、些細な問題が多数解決しています。
もしこのサービスで何か問題に遭遇したら、そのサイト管理プロジェクトに<a href="https://alioth.debian.org/tracker/?func=add&amp;group_id=1&amp;atid=200001">サポートチケット</a>を開いてください。</p><p>もし Alioth で SSH タイムアウトを経験したら、あなたのSSHクライアントが誤った設定をされている
可能性があります。
Alioth は fail2ban を使用しており、あまりに度重なるログインの失敗が SSH デーモンにより記録されている
IP アドレスからのトラフィックを落とすようファイアーウォールの設定がなされています。
SSH クライアントがあなたのアカウントに登録されていない鍵で
接続を試みることも含まれます。その後で正しい鍵が送られたとしてもです。
あなたの Alioth アカウントにあなたのキーを選択し登録するか、
または (.ssh/config に) 適切な鍵だけを送るように <a href="http://wiki.debian.org/Alioth/SSH">stanza</a> を設定するだけです。
どちらの方法でもログイン時に数ミリ秒の節約になるでしょう。</p><p>-- Raphael Hertzog and Roland Mas</p><h5>wiki.debian.org が MoinMoin 1.7 に移行されました</h5><p>DSA と <a href="http://wiki.debian.org/Teams/DebianWiki">DebianWiki チーム</a> が wiki.debian.org を、多くの新機能を持つ
MoinMoin 1.7 に移行しました。現在 wiki は新しいマシン上で提供されています。
ハードウェアと回線は、<a href="http://www.dg-i.net/">Dembach Goo Informatics</a> より資金援助を受けて
います。また、wiki に安全にログインするための https も今では使用可能になって
います。</p><p>さらなる情報については、<a href="http://wiki.debian.org/DebianWiki/MigratedToMoinmoin17">お知らせのページ</a>を見てください。</p><p>-- Stefano Zacchiroli</p><h5>wanna-build チームからのちょっとしたお知らせ</h5><p>wanna-build チームは、wanna-build へのリクエストや autobuild に関する
 その他の問題についての連絡アドレス debian-wb-team@lists.debian.org を発表しました。
 また、Kurt Roeckx さんが amd64 以外にもその他すべてのアーキテクチャについての
 wanna-build データベースへの変更権限を持ったことも発表しました。</p><pre><code>http://lists.debian.org/debian-project/2009/03/msg00096.html
</code></pre><p>-- Adeodato Simó</p><p>さらに、buildds はアーカイブで現在のバージョンにより近い sbuild 版にアップグレードされています。
 新しい buildds は、powerpc と、hppa、i386に加えられました。詳細は以下を参照してください。</p><pre><code>http://lists.debian.org/debian-devel/2009/04/msg00537.html
</code></pre><p>-- Philipp Kern</p><h5>【急募】メーリングリストのアーカイブからスパムを削除</h5><p>みなさんご存知のとおり、メーリングリストやそのアーカイブは完全にスパム
と無縁という訳ではありません。そのため、アーカイブの各ページには
「Report as Spam」というボタンが用意されています。その結果、不適切もし
くは議論の余地のあるような投稿も報告されるようになってきたので、これら
の指摘をレビューできるシステムをセットアップしました。</p><p>アーカイブからスパムを除去するのを手伝ってもらえるなら、<a href="http://lists.debian.org/archive-spam-removals/review/">このページにあ
る説明</a>にしたがってください。参加するには、@debian.org のメールアド
レスを持っている必要があります。</p><p>メールアドレスがない場合は、<a href="http://lists.debian.org/">メーリングリストのアーカイブ</a>をレビュー
して、「Report as Spam」ボタンを押すことで手伝えます。</p><p>詳細と調整のために <a href="http://wiki.debian.org/Teams/ListMaster/ListArchiveSpam">Wiki ページ</a>も管理しています。</p><p>-- Cord Beermann</p><h5>グループウェアのメーリングリスト</h5><p>Debian の<a href="http://wiki.debian.org/Groupware">グループウェア</a>開発者間での議論や協力をしやすくするため、<a href="mailto:calendarserver-discuss@lists.alioth.debian.org">メーリング
リスト</a>を作成しました。Debian で、グループウェア関連クライアント・サーバをメンテナンス
している (もしくは興味がある) 方は、<a href="http://lists.alioth.debian.org/mailman/listinfo/calendarserver-discuss">購読をお願いします</a>。共通の場を持つことで、例えば
相互接続のテストを行うために新バージョンをアナウンスする、といった作業ができることを意図
しています。</p><p>-- Guido Günther</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/04/msg00012.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん・倉敷悟が行いました。
また、やまねひできさん、victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
