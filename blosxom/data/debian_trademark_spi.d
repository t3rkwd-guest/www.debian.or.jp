商標「ＤＥＢＩＡＮ＼デビアン」がSPI管理の国際商標に統一

<p>
日本国内の商標「ＤＥＢＩＡＮ＼デビアン」につき、かねてより作業を行っておりましたが、
無事 SPI (Software in the Public Interest, Inc.) 管理の<a 
href="http://lists.debian.org/debian-devel-announce/2012/10/msg00001.html">国際商標「ＤＥＢＩＡＮ」(国際登録番号:1084122)
への統一・移行が完了したこと</a>をここに報告させていただきます。</p>
<p>
国内商標については、2002年に<a 
href="http://lists.debian.or.jp/debian-announce/200209/msg00001.html">Debian JP Project 理事(当時)と SPI の連名による取得</a>を行い、
Debian JP Project が管理を行って参りました(*3)。<br>
この移行により、有志という形態から Debian の法的作業を担う団体による統一した形での運用となり、迅速な対応が可能となります。</p>
<p>
これまでの商標の維持、ならびに今回の移行にかかる費用をまかなう寄付を頂いた皆様方に深く感謝致します。</p>

<h5>Debian 商標の使用について</h5>
<p>
Debian に関する商品を販売するなどの際、Debian 商標の表示が必要な場合には、以下のように表現することを推奨します。</p>
<blockquote>
Debian は Software in the Public Interest, Inc. の登録商標です。</blockquote>
<p>
その他、Debian 商標の使用に関する詳細は
 <a href="http://www.spi-inc.org/corporate/trademarks/">http://www.spi-inc.org/corporate/trademarks/</a> 
を参照してください。</p>
