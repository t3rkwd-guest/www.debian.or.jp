Software Design 2013 年 12 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201312"><img src="../image/book/sd201312.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201312">Software Design 2013 年 12 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回はGoogle Summer of CodeでのDebian関連での成果やDebian8「Jessie」の見通しなどを紹介しています。
どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
