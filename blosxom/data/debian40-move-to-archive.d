Debian 4.0 "Etch" の全パッケージが archive.debian.org へ移動します

<p>
これまでのセキュリティ更新を全て含んだ <a
href="http://www.debian.org/News/2010/20100522">Debian 4.0r9 "Etch" が先日 5/22 にリリースされました</a> 
(ちょっと紛らわしいのですが、4.0 "Etch" へのセキュリティサポート自体は 2010 年 2 月に終了しており、
<strong>今回のリリースまでに特に新しい更新は行われていません</strong>)。
そして、サポートが終了したリリースへの作業として、<strong>2010/6/6 に現在リポジトリから取得できる Etch のパッケージ群が <a
href="http://archive.debian.org">archive.debian.org</a> へすべて移動されます</strong>。
これに伴い、各ミラーサーバからも順次パッケージの取得ができなくなります。</p>
<p>
何かしらの理由で Etch のパッケージを使いたい場合は、apt line の設定 (/etc/apt/sources.list) を 
archive.debian.org から取得するように設定を変更しましょう (もちろん、現在の安定版 5.0 "Lenny" 
を利用する方がお勧めです)。</p>
