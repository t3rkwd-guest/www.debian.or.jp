Software Design 2008 年 4 月号「git」記事

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2008/200804"><img src="http://gihyo.jp/assets/images/cover/2008/640804.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2008/200804">Software Design 2008 年 4 月号<strong>「分散バージョン管理システム Git徹底活用ガイド」</strong></a>で、上川純一・岩松信洋・小林儀匡の 3 名が担当しました（あと査読に有志が協力）。</p>
<p>
git は、Linux kernel や X.org の開発などにも使われている (もちろん Debian ではパッケージがある)、Debian 開発者の間でも非常に人気の高い便利なバージョン管理システムです。ぜひ皆様書店でお手に取ってみてください。 3 月 18 日発売です。</p>
<p>
（ちなみに Debian では git は <strong>git-core</strong> パッケージとして提供されています。間違えて aptitude install git しないように注意しましょう）</p>


