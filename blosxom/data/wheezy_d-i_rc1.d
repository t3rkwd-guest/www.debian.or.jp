Debian-Installer 7.0 RC1 リリース

<p>
次期リリースバージョン「Wheezy」用インストーラの最初のリリース候補版 (Release Candidate、RC版) の
<a href="http://lists.debian.org/debian-devel-announce/2013/02/msg00005.html">アナウンス</a>が行われました（注：Debian 7.0 Wheezy そのものではなく、<strong>Wheezy 用インストーラ</strong>の RC1 がリリースされた、ということです）。</p>
<p>
今回のリリースでは、既知の問題点の修正や以下の変更などが行われています (完全なリストは上記アナウンスを参照ください)。</p>
<ul>
<li>アクセシビリティ機能への修正
<li>kFreeBSDでの procfs マウントの修正
<li>ファームウェアの読み込みの改善
<li>network-managerへの設定反映
<li>GUIインストーラーで選択したキーマップをCUIへも反映
<li>VirtualBoxでカーソルが飛ぶのを修正
<li>UEFIモードでの起動時のGRUBメニューの改善
<li>大量のドライバサポートの追加:  8021q, adm8211, at76c50x-usb, b43legacy, bnx2fc, cxgb4, cxgb4vf,fnic, igbvf, int51x1, isci, iwl4965, ixgbevf, libertas_tf_usb, micrel, mlx4_en, mwifiex_pcie, mwl8k, orinoco_usb, pata_piccolo, pch_gbe, pmcraid, prism2_usb, qlge, r8187se, r8192e_pci, r8712u, rtl8192ce, rtl8192cu, rtl8192de, rtl8192se, smsc75xx, smsc9420, smsc95xx, tehuti, ums-eneub6250, ums-realtek, vt6656_stage, vxge</li>
<li>以下のRalink wifi のデバイス IDの追加: 5362, 5392, 539b</li>
<li>Lenovo 10/100 Ethernet USBドライバの追加</li>
</ul>

<p>
既知の問題点として、以下の点が挙げられています。
<ul>
<li>ディスクが 2 つ以上ある場合、GRUB ブートローダーのインストールが失敗することがある (回避策が<a href="http://www.debian.org/devel/debian-installer/errata">正誤表</a>にあります)
<li>i386 アーキテクチャでデスクトップ環境のインストールが CD1 のみではできない (amd64は問題ありません)
<li>UEFI 環境での起動に潜在的な問題がある可能性があります
</ul>
<p>
皆さんも可能な限り、様々な環境でテストして、<a href="http://www.debian.org/devel/debian-installer/errata">正誤表</a>を確認の上、結果をメーリングリストやバグ報告システムなどにて教えて下さい。今ならまだ修正が間に合うでしょう :-)
</p>
