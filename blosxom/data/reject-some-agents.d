ftp2.jp.debian.org にダウンローダ制限をかけています

<p>
Debian JP Project がサービスしているミラーホストの 1 つである、ftp2.jp.debian.org (別名 plat.debian.or.jp, ftp.debian.or.jp。および cdn.debian.or.jp の候補の 1 つ) が複数起動型のダウンローダによって DoS (サービス拒否攻撃) に似た異常なアクセスを受けたため、当該ホストでは wget といくつかの複数起動型ダウンローダを排除するように設定いたしました。これらのダウンローダでパッケージを取得しようとすると、エラー 403 が返されます。
</p><p>
通常の APT コマンドはこの影響を受けません。固有の Debian ミラーを構築するためにダウンローダを使われていた方は、ミラー構築に適した rsync を利用することを推奨します (<a href="http://www.debian.or.jp/community/push-mirror.html">CDN 対応ミラーの設定</a>ガイドの前半の記述を参照してください)。
</p>
