cdimage.debian.or.jp 提供開始

<p>
Debian JP Project は、国内 Debian CD イメージミラーサーバーとして <a 
href="http://cdimage.debian.or.jp">cdimage.debian.or.jp</a> 
の運用を開始しました。</p>
<p>
<a href="http://cdimage.debian.or.jp">cdimage.debian.or.jp</a> は、サーバーを日本国内に配置することで、
CD イメージの配布元である cdimage.debian.org と比較して、ダウンロード速度の大幅な改善（平均4倍以上）
を見込んでいます。</p>
<p>
現在利用可能なイメージは以下のとおりです。</p>
<ul>
<li>デスクトップLiveCD (GNOME)</li>
<li>デスクトップLiveCD (KDE)</li>
<li>デスクトップLiveCD (xfce4)</li>
<li>デスクトップLiveCD (LXDE)</li>
<li>amd64 (DVD / netinst)</li>
<li>i386 (CD / netinst)</li>
</ul>
<p>
また、今後以下のイメージの追加を予定しています。</p>
<ul>
<li>kfreebsd-amd64</li>
<li>kfreebsd-i386</li>
<li>debian-installer beta2 (amd64 / i386)</li>
</ul>
<p>
利用された方は、ぜひフィードバックを<a href="http://www.debian.or.jp/community/ml/openml.html#usersML">メーリングリスト</a>や<a
href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>までお寄せください。</p>
<p>
また、cdimage.debian.or.jp の提供と運用にあたりましては、<a 
href="http://itsherpa.com/">株式会社アイティーシェルパ</a>様にご協力を頂いております。
この場を借りましてお礼申し上げます。</p>

