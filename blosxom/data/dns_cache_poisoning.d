DNS キャッシュ汚染問題への対応について

<p>
各種メディアで既に何度も報じられていますが、セキュリティ技術者 Dan Kaminsky 氏によって DNS プロトコルそのものに起因する深刻な脆弱性が発見されており、先日 8 月 6 日の BlackHat カンファレンスにて正式公開されました (この問題の原理について最も解り易い日本語資料が<a href="http://www.nttv6.net/files/DKA-20080723.pdf">「DNS Cache Poisoningの概要と対処 (PDF)」</a> として公開されています)。現在、DNS キャッシュサーバに対して攻撃可能なツールが出回っている状態となっています。</p>
<p>
ご存知の通り、DNS はインターネットの根幹を成す技術の一つであり、この信頼性が失われることは多大な問題をもたらします。DNS キャッシュサーバを運用されている方は自身のキャッシュサーバが安全かどうかの確認を、インターネット利用者の方は自分が使っている DNS キャッシュサーバが安全かを<strong>ブラウザにて<a href="https://www.dns-oarc.net/oarc/services/dnsentropy">DNS Operations, Analysis, and Research Center (DNS-OARC) のページ</a>にて確認ください (ページの中程に「Test My DNS」の文字があります</strong>ので、そちらをクリックすれば結果が判ります。利用者の方で安全でない結果が出た場合はプロバイダあるいは運用機関に早急に対応が必要な旨の苦情を述べましょう。話が全く通じない、変更を実施しようとしない場合はプロバイダの変更を検討された方が良いかもしれません…)。</p>
<p>
Debian サーバ管理者への情報となりますが、<a href="http://packages.debian.org/bind">bind パッケージ (bind8)</a> については、既に<a href="http://www.debian.org/security/2008/dsa-1604">セキュリティ勧告 (DSA-1604)</a> で報告されているように「BIND 8 の古いコードベースでは、推奨される対抗策 (UDP クエリソースポートのランダム化) を実装することは困難であり、早急に BIND 9 への移行を推奨する」ものとなっています。つまり、<strong>bind パッケージを利用した DNS キャッシュサーバは危険</strong>です。<br>
<a href="http://packages.debian.org/bind9">bind9 パッケージ</a>については同様に<a href="http://www.debian.org/security/2008/dsa-1603">セキュリティ勧告 (DSA-1603)</a> の詳細の通り、対応のためのパッチは適用されていますが、以前の　named.conf のデフォルト設定では危険となる要素である「query-source port 53;」あるいは「query-source-v6 port 53;」文が設定されていたことがありますので、必ず<a href="http://www.debian.org/security/2008/dsa-1603">勧告文を参照</a>して<strong class="attention">通常のパッケージ更新以外の作業が必要であり、完了の確認が必要である</strong>ことを確認ください。<br>
なお、同様にメジャーな DNS サーバソフト djbdns の dnscache には今回の件での対応は現状必要とされていません。他の DNS サーバソフトについて、確認を行いたい場合は、US-CERTの「<a href="https://www.kb.cert.org/vuls/id/800113">Multiple DNS implementations vulnerable to cache poisoning (VU#800113) (7/8)</a>」の Vendor 一覧でチェックするか、ソフトウェアの開発元 (upstream) に直接コンタクトを取るなどすると良いでしょう。</p>
<p>
また、東海地区ではこの問題について協議するため、東海インターネット協議会が
8/9 のオープンソースカンファレンス 2008 において
<a href="https://www.tokai-ic.or.jp/kaminsky.html">緊急DNS勉強会の開催宣言</a>
などをしています。問題をより正確に把握したい方は参加頂くことを検討ください。</p>

<p>
企業などで「対応のためには公的な情報が無いと動けない」という方は
JPCERT/CC (インターネットを介して発生する侵入やサービス妨害等のコンピュータセキュリティインシデントへの日本での対応支援窓口)や JPRS (日本のドメイン名管理団体) などがこの件に対するリリースを出しています (JPRS:「<a href="http://jprs.jp/tech/security/multiple-dns-vuln-cache-poisoning.html">複数のDNSソフトウェアにおけるキャッシュポイズニングの脆弱性について (7/9)</a>」、JPRS:「<a href="http://jprs.jp/tech/security/multiple-dns-vuln-cache-poisoning-update.html">(緊急)複数のDNSソフトウェアにおけるキャッシュポイズニングの脆弱性について(続報) (07/24)</a>」、JPCERT/CC「<a href="https://www.jpcert.or.jp/at/2008/at080013.txt">複数の DNS サーバ製品におけるキャッシュポイズニングの脆弱性 (7/9)</a>」、JPCERT/CC「<a href="https://www.jpcert.or.jp/at/2008/at080014.txt">[続報] 複数の DNS サーバ製品におけるキャッシュポイズニングの脆弱性に関する注意喚起 (7/24)</a>」等) ので、そちらを参照ください。</p>

<p>
なお、今後この問題に起因するソフトウェアの修正とセキュリティ勧告が続くものと思われます (例: 
<a href="http://www.debian.org/security/2008/dsa-1544">pdns-recursor</a> パッケージ、
<a href="http://www.debian.org/security/2008/dsa-1605">glibc</a> パッケージ、
<a href="http://www.debian.org/security/2008/dsa-1619">python-dns</a> パッケージ、
<a href="http://www.debian.org/security/2008/dsa-1623">dnsmasq</a> パッケージなどが該当します。
なお、<a href="http://www.debian.org/security/2008/dsa-1628">powerdns</a> パッケージ (8/11 10:00 時点でリンク先は未掲載) については<a href="http://doc.powerdns.com/changelog.html#CHANGELOG-AUTH-2-9-21-1">今回の件に起因する脆弱性ではなく、別のリスクを避けるもの</a>として報告されています)。
特にサーバ管理者の方はセキュリティ勧告についてご注意ください。</p>
