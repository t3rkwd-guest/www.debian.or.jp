Debian GNU/Linux 4.0 "Etch" のサポート終了まであと約1ヶ月です

<p>Debian GNU/Linux 5.0 コードネーム “Lenny”が <a href="http://www.debian.or.jp/blog/debian50r0.html">2009/02/14 にリリース</a>されてから、そろそろ約1年になろうとしています。
これはつまり、<strong>旧安定版(oldstable) Debian GNU/Linux 4.0 "Etch" のサポートも終了が間近ということ</strong>です。基本的に旧安定版のサポートは安定版がリリースされてから1年ですので、特別なことが無い限り <strong class="attention">4.0 "Etch" のサポートは2010年2月中に終了します</strong>。Etchの利用者の方は移行計画を立てて速やかに更新ください。</p>

<p>バージョンのアップグレードに際してパッケージの大幅な追加／削除／更新を含んでいますが、アップグレード作業の注意点など、詳細については<a href="http://www.debian.org/releases/lenny/releasenotes">Debian GNU/Linux 5.0 リリースノート</a>を参照してください。</p>
<p>
リリースノートには、Debian GNU/Linux 5.0 で追加・変更されたさまざまな機能説明を各アーキテクチャごとに掲載しているほか、前リリース (Debian GNU/Linux 4.0 コードネーム“Etch”) からのアップグレードを行うユーザーのための手順と注意も記述されています。いくつかの重要なパッケージに大幅な変更があるため、前リリースからのアップグレードを行う場合には、必ずこのリリースノートに目を通してください (例: <a href="http://www.debian.org/releases/lenny/i386/release-notes/ch-upgrading.ja.html">i386</a>、<a href="http://www.debian.org/releases/lenny/amd64/release-notes/ch-upgrading.ja.html">amd64</a>)。</p>
<p>
以上、利用者の方への慌てないためのリマインダでした（etch が <a href="http://archive.debian.org/">archive</a> 入りしてから「アップデートができない！」などとならないように気をつけましょう）。</p>
<p>
1/21 追記:<a href="http://lists.debian.org/debian-security-announce/2010/msg00010.html"><strong>2010/2/15 に終了との正式アナウンス</strong>が出ました。</p>

