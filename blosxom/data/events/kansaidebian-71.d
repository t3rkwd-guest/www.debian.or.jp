第71回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130428">第71回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 4 月 28 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター301号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      「<a href="http://www.debian.org/releases/wheezy/releasenotes">Debian 7.0 -- リリースノート</a>」を読んできてください。<br>
      誤植や誤訳などの気づいたこと、わからないなと思うことなどがあれば教えてください。
      </li>

      <li>
      Debian をこれから使ってみようとされている方は Debian もしくは wheezy に対して気になっていることがあれば教えてください。
      </li>

      <li>
      Debian をすでにお使いの方は squeeze から wheezy へのアップグレードではまりそうなところ、気になるところがあれば教えてください。<br>
      もしくは、お使いの squeeze 環境を一つ、wheezy にアップグレードしてみて下さい。そしてその結果を教えてください。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「リリースノートを読んでみよう。」</strong>
  </li>

  <li><strong>「クラウド初心者が AWS に Debian をのっけて翻訳サービスの試行に挑戦してみた」</strong>(担当：倉敷)
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2013年 4 月 27 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=562b3125e6026d4a0dc6383e2b8c620dbc4d6c72">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
