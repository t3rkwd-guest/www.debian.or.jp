第3回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20070602">第3回 関西 Debian 勉強会</a>」が 
6 月に開かれることになりました。関西 Debian 勉強会は、定期的に顔をつき合わせて、
Debian GNU/Linux のさまざまなトピック (新しいパッケージ、Debian 特有の機能の仕組み、Debian 界隈で起こった出来事、などなど) 
について楽しく話し合っていく集まりです。</p>

<p>参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年6月2日(土) 13:00-17:00 (受付は12:30以降、終了後に懇親会も別途予定)</li>
  <li>会場：<a href="http://www.olnr.org/%7Ehatanaka/location.html">大阪電気通信大学 (四條畷) 11-403号室</a></li>
  <li>費用：無償 (懇親会は別途費用、学生は約1000円引きの学割を適用予定)。</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debian 風の iptables な話題</strong> 久保 博 氏<br>
      etch にも sarge にも入っている lokkit と shorewall の紹介。<br>
      パケットフィルタリングの方法について実際の利用例を紹介します。<br></li>

  <li><strong>Debian パッケージの作り方 -- debian/rules を読む --</strong>大浦 真 氏<br>
      Debian のソースパッケージには、debian/ というディレクトリがあり、
      その中に色々なファイルが入っています。今回はその中心となる debian/rules を読んでいきます。<br></li>

  <li><strong>みんなで読む Debian 社会契約</strong> 中本 崇志 氏<br>
	これを読まずしてオープンソース、Debianを語ることなかれ。<br>
	みんなで楽しく「Debian社会契約」を読みましょう。<br>
	予習不要、復習必須！<br></li>

  <li><strong>事前課題発表</strong> 山下 尊也 氏 <br>
      みなさんから提出頂いた事前課題についてスライドで紹介。<br>
      後、テーマの適用などの話などを少しお話します。</li>

  <li><strong>Debian へのバグレポートについて --バグ報告はコミュニケーションだ--</strong> 矢吹 幸治 氏<br>
      Debian GNU/Linux は、だれでも開発に参加することができるディストリビューションです。<br>
      その一端を担っている「バグ報告」ー本セッションでは、バグ報告のツールの使い方、
      問題の切り分け方のガイドライン、BTS (Bug Tracking System) がどのように使われているかの例を示します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前のまとめを作るための情報を送付してください。期限は 5 月 30 日 (水) 23:59 までです。<br>
<strong>事前登録、及び事前送付情報の内容と送付先については
<a href="http://wiki.debian.org/KansaiDebianMeeting20070602">第3回 関西 Debian 勉強会
</a>のページを参照してください。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会主催者：矢吹幸治 (yabuki&#64;{debian.or.jp,netfort.gr.jp} ) までお願いいたします。</p>
