オープンソースカンファレンス 2013 Tokushima ブース出展のお知らせ

<p>徳島県にお住まいのみなさまにお知らせです。3月9日(土)とくぎんトモニプラザにて開催される「<a href="http://www.ospn.jp/osc2013-tokushima/">オープンソースカンファレンス 2013 Tokushima</a>」に関西Debian勉強会が出展します。</p>

<p>関西Debian勉強会は、関西方面のDebianユーザ／ユーザ予備群／開発者らでDebian GNU/LinuxのさまざまなトピックについてFace to Faceで楽しく話し合っていく集まりです。</p>

<p>今回はブース展示のみでセッションはありませんが、徳島の方々と楽しくふれ合いたいと思っています。ぜひ、足をお運びください。<p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
  <li>日程: 2013年3月9日(土) 10:00-18:00</li>
  <li>会場: <a href="http://www.tokuginplaza.com/own/index.asp">とくぎんトモニプラザ(徳島県青少年センター)</a> 3F・4F (JR徳島駅 徒歩約10分)</li>
  <li>参加費: 無料</li>
</ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
<ul>
  <li>Wheezy稼働マシン(Raspberry pi, MK802+)の展示</li>
  <li>東京エリア/関西Debian勉強会の紹介</li>
  <li>Debianのインフォグラフィック日本語版の展示</li>
  <li>Debianステッカーの配布</li>
</ul>
</dd>
</dl>

<p>この件に関するお問い合わせは のがたじゅん (nogajun at {debian.or.jp}) までお願いいたします。</p>
