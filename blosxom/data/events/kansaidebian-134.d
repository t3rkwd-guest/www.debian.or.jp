第 134 回 関西 Debian 勉強会

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20180422">第134回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2018 年 4 月 22 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.osakacommunity.jp/minato/">港区区民センター 楓</a>(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>最近のDebianパッケージ作成環境</strong> - git-buildpackage, sbuild, autopkgtest を例に - (佐々木洋平)<br>
  最近の自身のパッケージ作成の workflow を紹介しつつ、git-buildpackage, sbuild, lintian, piuparts, autopkgtest といった定番ツールについてご紹介します(またはツッコミ待ちです)。
  </li>

</ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.connpass.com/event/85210/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。
</p>
