第35回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20100523">第35回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2010 年 5 月 23 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">大阪福島区民センター 302会議室</a>
(定員：45名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  <li>事前課題1: Squeeze を使っていますか？　使った感じはどうですか？</li>
  <li>事前課題2: Lucid Lynx を使っていますか？　使った感じはどうですか？</ul>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>Debian ユーザのための Ubuntu 入門</strong> (担当：あわしろ いくや)<br>
Debian から派生した Ubuntu は、Debian とは違った進化を遂げています。そんな Ubuntu を、Debian との違いを中心に解説します。開発などのやや突っこんだ話もしますが、気軽に聞きに来てください。</li>

  <li><strong>次期リリースの Squeeze を見てみよう</strong> (担当：のがた じゅん<br>
次期リリース Squeeze のフリーズが近づいていますが、Squeeze のどんなところが変わり、どのような作業が必要とされているのか、みんなで見てみたいと思います。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>5 月 21 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=78b5b9ed2553d360032f46ccaa3fec0aef259bd4">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
