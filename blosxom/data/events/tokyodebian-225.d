2023 年 9 月度 東京エリア・関西合同 Debian 勉強会のお知らせ

<p>
日本にお住まいのDebianにご興味がある皆様こんにちは。
今月の勉強会はオンラインによるweb会議のスタイルで
東京エリア Debian 勉強会と関西 Debian 勉強会が合同で開催することにいたしました。
</p>

<dl>
<dt>東京エリア Debian 勉強会</dt>
<dd>
<p>
東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とする
熱いユーザたちと、実際に Debian Project にてすでに日夜活動している
人らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケージ、
Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味がありご都合の合う方はぜひともご参加下さい。
</p>
<p>
また、勉強会で話をしてみたいという方も随時募集しています。
</p>
</dd>

<dt>関西 Debian 勉強会</dt>
<dd>
<p>
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。
</p>
</dd>
</dl>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
    <li>日時：2023 年 9 月 16 日 ( 土曜日 ) 16:00-17:00</li>
    <li>会場：Jitsi Meet を使った Web ビデオ会議（URLは<a href="https://debianjp.connpass.com/event/295503/">勉強会のページ</a>を参照ください）</li>
    <li>参加費：無料</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
    <li><strong>「DebConf 23 インド 現地レポート」（発表者：henrich さん）</strong></li>
    <ul>
      <li>Debian Projectでは、毎年DebConfというDebian開発者たちが集うイベントを開催しています。</li>
      <li><a href="https://debconf23.debconf.org/">DebConf23</a> はインドのコチ市にて、DebCampが9/3-9/9、DebConfが9/10-9/17の日程で開催されます。</li>
      <li>日本からDebian開発者の henrich さんが現地に赴き参加することになっており、現地の状況をレポートしていただけることになりました。</li>
      <li>当日の現地予定はDebConf23 Day7と終盤です。henrich さんが参加して感じていることをお話ししていただきつつ、勉強会に参加する皆様から聞いてみたいことを質問してみましょう。</li>
    </ul>
  </ol>
</dd>
<dt>事前課題／参加申し込み方法</dt>
<dd>
  <ul>
    <li>事前課題はありません。</li>
    <li><a href="https://debianjp.connpass.com/event/295503/">勉強会のページ</a>を参照して、参加登録をしてください。</li>
  </ul>
</dd>
<dd>
ビデオ会議への参加方法は<a href="https://debianjp.connpass.com/event/295503/">勉強会のページ</a>に記載しています。
</dd>
<dd>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。
</dd>
</dl>

<p>
この勉強会の開催に関するお問い合わせは Debian 勉強会主催者：杉本 (dictoss@{debian.or.jp} ) までお願いいたします。</p>
