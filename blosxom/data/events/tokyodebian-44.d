第44回 東京エリア Debian 勉強会のお知らせ

<p>
9 月も 東京エリア Debian 勉強会が開かれます！
</p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年9月20日(土) 14:00-17:00</li>
  <li>費用：資料代・会場費として 500 円</li>
  <li>会場：<a href=
"http://nyc.niye.go.jp/">国立オリンピック記念青少年総合センター</a> <a href="http://nyc.niye.go.jp/facilities/d6-1.html">センター棟 研修室 セ-503</a>
(小田急線 参宮橋駅下車 徒歩7分)<br>
<iframe width="640" height="480" frameborder="no" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;time=&amp;date=&amp;ttype=&amp;num=10&amp;ie=UTF8&amp;om=1&amp;s=AARTsJqGrqSWVb_axyemF1zwHXjuSM6-eg&amp;msa=0&amp;msid=109811903404931563048.0004391daee4fddf76a7f&amp;ll=35.676193,139.693801&amp;spn=0.033467,0.054932&amp;z=14&amp;output=embed"></iframe><br/><a href="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;time=&amp;date=&amp;ttype=&amp;num=10&amp;ie=UTF8&amp;om=1&amp;msa=0&amp;msid=109811903404931563048.0004391daee4fddf76a7f&amp;ll=35.676193,139.693801&amp;spn=0.033467,0.054932&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left;font-size:small">拡大地図を表示</a>
  </li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
      <li><strong>Po4a でドキュメント翻訳の保守を楽にしよう</strong> （担当：小林儀匡）<br>
          翻訳の保守は新規翻訳よりも手間のかかる作業になりがちです。これを怠ると原文との乖離は大きくなり、
          折角の翻訳も価値が下がっていきます。Debian で開発されている Po4a は、プレーンテキスト、XML、
         HTML、LaTeX、roff (man)など様々な形式のドキュメントを PO という形式に変換して管理し、
          ドキュメント翻訳の保守性を上げるツールです。今回はこの Po4a の使い方と、新たなドキュメント形式
          をサポートするために知っておくべき内部構造について説明します。</li>
      <li><strong>【でびあん】Debian パッケージメンテナというお仕事【現在募集中】</strong> （担当：やまね ひでき）<br>
          「Debian パッケージのメンテナというのはどんな仕事なのか？」「今メンテナを募集している理由は？」
          「どんな人たちがいるの？」「待遇は？」などを現在いくつかのパッケージをメンテしている現場の人間から、
           メンテナに興味がある方へそれまでの経験を踏まえて説明してみようという試みです。
           前提知識はそんなに必要とはしませんのが、不明な点がある場合は発表の場で適宜質問くださればと思います。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 9 月 18 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2008-09.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：岩松 信洋 (iwamatsu&#64;{debian.or.jp,nigauri.org} )
までお願いいたします。</p>
