第74回 関西 Debian 勉強会 @OSC2013 Kansai@Kyoto のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130803">第74回 関西 Debian 勉強会@OSC2013 Kansai@Kyoto </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。今月は、オープンソースカンファレンス2013 Kansai@Kyotoに参加します。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：日時：2013 年 8 月 3 日 (土) 10:00〜18:00 </li>

  <li>会場：<a href="http://www.krp.co.jp/access/index.html">京都リサーチパーク(KRP)</a> OSC総合受付：アトリウム</li>
  <li>費用：無料</li>
  </ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
  <ul>
  <li>Debian 7.0 稼働マシンと勉強会資料の展示</li>
  <li>Debian インストーラDVDの配布</li>
  <li>Debian Tシャツ・ステッカーの販売など</li>
</dd>
<dt> セッション</dt>
<dd>
  <ul>
    <li><a href="https://www.ospn.jp/osc2013-kyoto/modules/eventrsv/?id=2&noform=1">
	    <strong>Debian 7.0 の実情/今後の開発について</strong></a>
		(担当: 河田 鉄太郎（Debian JP Project 事務局長）)</li>
    <li>日時 2013 年 8 月 3 日 (土) 10:00〜10:45 (45 min)</li>
    <li>場所: 会議室B</li>
    <li>内容: 先頃リリースされた Debian 7.0 （wheezy) の紹介と良くあるハマリ所、今後の開発についてご紹介します。</li>
  </ul>
  <ul>
    <li><strong>GPG/PGP キーサインパーティ</strong>(担当: 佐々木 洋平(Debian JP Project)</li>
    <li>日時 2013 年 8 月 3 日 (土) 12:00~12:45 (45 min)</li>
    <li>場所: 会議室C</li>
    <li>内容: キーサインパーティーは、互いの鍵に署名をすべく、PGP(GPG)鍵を持つ人々が集まるものです。キーサインパーティーはWeb of Trustを大規模に拡張するのに寄与します。<br>
        注意: なお、参加される方は<a href="https://sites.google.com/site/kspjapanese/osc2013-kyoto">必要な事前準備の上</a>、
		<a href="http://junkhub.org/ksp/ksp.cgi">事前登録</a>をお願いします。</li>
  </ul>
</dd>

</dd>
<dt>参加方法と注意事項</dt>
<dd>
ブース、セッションともに入場無料です。
セッションについては、<a href="https://www.ospn.jp/osc2013-kyoto/modules/eventrsv/?id=2&noform=1">オープンソースカンファレンスのページ</a>よりお申し込みください。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
