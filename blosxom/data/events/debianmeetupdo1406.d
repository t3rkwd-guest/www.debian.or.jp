Debian meetup Hokkaido 14.06 (6/15) のお知らせ

<p>北海道にお住まいのみなさまにお知らせです。オープンソースカンファレンス 2014 Hokkaidoの翌日、6/15に札幌で<strong>「Debian meetup Hokkaido 14.06」</strong>を開催します。OSC2014doに合わせて他の地域からやってきた Debian 関係者と北海道地区の Debian を中心とした FLOSS ユーザー／コントリビューター／開発者が face to face でマッタリ・じっくり・ゆるーく Debian や Debian ではないことを肴に、コーヒーでも飲みながらコミュニケーションするイベントです。</p>
<p>
Debian や FLOSS の活動に多少でも興味のある方であれば参加資格は OK ですので、ぜひ参加をご検討ください（他の OS/distro ユーザーももちろん welcome です）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
  <li>日程: 2014年6月15日(日) 12:00-18:00</li>
  <li>会場提供: <a href="http://www.infiniteloop.co.jp/map/">株式会社インフィニットループ</a> (札幌市中央区北1条東1丁目6-5 札幌イーストスクエア 6階)</li>
  <li>参加費: 500円〜 （飲食・機材代など。余剰の場合は Debian JP Projectに寄付予定）</li>
  <li>Twitter: <a href="https://twitter.com/debianjp">@debianjp</a>／ ハッシュタグ <a href="https://twitter.com/search?q=%23debian14do">#debian14do</a>
</ul>
</dd>
<dt>参加登録について</dt>
<dd>
<a href="http://debianjp.connpass.com/event/6611/"><strong>Debian meetup Hokkaido 14.06 申し込みページ</strong></a>にて参加登録をお願いします。
</dd>
<dd>
開催会場ビルの入り口は土日は閉まっているので、以下の点をご確認の上でご来訪ください。
<ol>
<li>ビルの左側に回り込み通用口へ
<li>インターホンで「601」を押し呼び出しをする
<li>Debianのイベントに参加される旨とお名前を伝えてください
<li>呼び出しをしても誰も出てこない、などという場合は電話「011-271-1118」までお願い致します
</ol>
（詳しくは<a href="http://www.infiniteloop.co.jp/blog/2012/10/kaigishitsu_hairikata/">こちら</a>も参考にしてください）</dd>
<dt>
内容と参加者側の事前準備について</dt>
<dd>
<ul>
<li>会場には電源と無線LANが用意されています
<li>参加者はBlogに書いたりTweetをすることが推奨されます
<li>内容は、雑談／ハック作業／プレゼン披露、なんでもありです。その場のノリで適当に進めます。
<li>何かのインストール／設定で詰まっているよ、という場合は普段使っている環境があると説明しやすいです。持参を忘れないようにしましょう
<li>自慢のお菓子があれば持ってきてください。みんなでシェアしましょう
<li>楽しいネタを披露してくれた方には、ささやかながら景品を用意しております
<li>特に誰もネタを持ってこない場合は、バグ潰しブートキャンプに変貌する恐れがあります
</ul>
</dd>
</dl>

<p>この件に関するお問い合わせは Debian JP プロジェクト理事会 board at {debian.or.jp} 、あるいは twitter: @debianjp までお願いいたします。</p>

