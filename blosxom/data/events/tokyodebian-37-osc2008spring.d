第37回 Debian 勉強会 in オープンソースカンファレンス 2008 Tokyo/Spring

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この春 3 月は、
<a href="http://www.ospn.jp/osc2008-spring/">オープンソースカンファレンス 2008 Tokyo/Spring</a>
に参加してセミナーやブースでの展示と説明、そして今回の試みとして「Debian パッケージ作成ハンズオン」などを予定しています。
普段は聞けないような様々な情報を交換しあうチャンスです。是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2008年3月1日(土)<br>
  <li>会場：<a href="http://www.jec.ac.jp/college/access.html">日本電子専門学校 7号館</a>　(JR大久保駅徒歩２分・新宿区北新宿1-4-2)
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;s=AARTsJqanBPci4fyHco-uVwHtDeqI3LI4Q&amp;msa=0&amp;msid=109811903404931563048.0004461414af51bab22e4&amp;ll=35.700279,139.696913&amp;spn=0.016728,0.027466&amp;z=15&amp;iwloc=000446142bdc7d09867d3&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;msa=0&amp;msid=109811903404931563048.0004461414af51bab22e4&amp;ll=35.700279,139.696913&amp;spn=0.016728,0.027466&amp;z=15&amp;iwloc=000446142bdc7d09867d3&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small></li>
  <li>費用：無料（セミナー）<br>
      　　　その他有志による物品の販売を予定</ul></dd>

<dt><a href="http://www.ospn.jp/osc2008-spring/modules/eguide/event.php?eid=46"><strong>Debian Overview</strong></a></dt>
<dd>講師：やまねひでき (Debian JP Project)　担当：東京エリア Debian勉強会</dd>
<dd>開始：14時00分</dd>
<dd>会場：7F B会場</dd>
<dd>定員：40人</dd>
<dd>
<p>
皆さんは Debian とその開発団体である Debian Project についてどれほどご存知でしょうか？<br>
今回は、皆さんがより Debian を深く知って<strong>愛して</strong>いただけるよう、Project の簡単な歴史、オープンソースとの関わりと現在の立ち位置や開発体制を眺め、リリースを控えているコードネーム「lenny」の進捗状況について、現在分かっている範囲で気軽にご説明したいと思います。</p>
<p>また、Debian JP Project についても、どんな活動が行われているのか、プロジェクト参加者が皆様にご紹介させていただきます。</p></dd>

<dt><a href="http://www.ospn.jp/osc2008-spring/modules/eguide/event.php?eid=52"><strong>Debian パッケージ作成ハンズオン</strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：東京エリア Debian勉強会</dd>
<dd>開始：15時15分</dd>
<dd>会場：8F 実習室</dd>
<dd>定員：38名</dd>
<dd>
<p>
「どうやってパッケージって作るの?」「試してはみたけど、うまくパッケージが作れない…」と嘆いていたあなたも、これに参加すれば公式 Debian パッケージメンテナへの道が開かれる…かもしれない?<br>
初の試み、基本的な Debian パッケージの作り方を学べる Debian 公式パッケージメンテナによるパッケージ作成ハンズオンです。</p>
<p>
参加者は、出来る限り <strong>Debian GNU/Linux 4.0 (Etch) がインストールされたノートPCをご用意ください</strong>。<br>
このセミナーの詳細は<a href="https://tokyodebian-team.pages.debian.net/2008-02.html">東京エリア Debian 勉強会のページ</a>で案内しますので、定期的にご確認ください。</p></dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料</li>
  <li>フライヤー・Live DVD (OSC2008 春 SP)(配布も予定)</li>
  <li>Debian 稼働マシン (Live DVD デモ機、SuperH 機 予定)</li>
  <li>Debian 掲示板</li>
  <li>有志作成の Debian グッズ (Tシャツ他 予定)</li>
  <li>Debian ステッカー他の販売も予定</li></ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
セミナー／ハンズオン参加希望者は<a href="http://www.ospn.jp/osc2008-spring/">事前登録</a>してください。<br>
ブース展示については特に登録などはありません。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会 担当者：岩松信洋 (iwamatsu&#64;debian.or.jp)
までお願いいたします。</p>
