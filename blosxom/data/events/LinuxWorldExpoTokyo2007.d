LinuxWorld Expo/Tokyo2007「オープン・アップ・OSS！」

<p>
2007年5月30日〜6月1日に東京ビッグサイトにて開催される
<a href="http://www.idg.co.jp/expo/lw/lw2007/">LinuxWorld Expo/Tokyo2007</a>
 内の１コーナー
<a href="http://www.idg.co.jp/expo/lw/lw2007/openuposs.html">「オープン・アップ・OSS！」</a>
にて、各ディストリビュータ代表者を集めたトークイベント
「Distribution/Community が語るLinux Desktop の未来」
が開かれます。このパネラーの一人に Debian JP Project を代表して、会長の上川純一氏が参加します。
お時間のある方はぜひご参加ください。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <li>日時：2007年5月30日（水）13:00-14:00
  <li>会場：<a href="http://www.idg.co.jp/expo/lw/lw2007/access.html">東京ビッグサイト西4ホール LinuxWorldExpo2007</a>会場内
「オープン・アップ・OSS！」コーナー
  <li>席数：20席ほど</dd>
<dt>内容</dt>
<dd>
「意外と知られていない、Linuxデスクトップの魅力・実力」を各ディストリビューションに精通した各氏が、
素朴な疑問に対する回答から技術的な方向性までを語ります。</dd>
</dl>

