第42回 Debian 勉強会のお知らせ

<p>
今月の Debian 勉強会 は Google にお邪魔しての開催となります。
<strong>時間と場所が通常とは違っていますので十分にご注意ください。</strong></p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年7月19日(土) 14:00-18:00</li>
  <li>費用：資料代として 500 円以内を予定</li>
  <li>会場：<a href="http://maps.google.co.jp/maps?f=q&hl=ja&geocode=&q=%E3%82%BB%E3%83%AB%E3%83%AA%E3%82%A2%E3%83%B3%E3%82%BF%E3%83%AF%E3%83%BC&sll=36.5626,136.362305&sspn=24.230398,46.40625&ie=UTF8&z=16">Google 東京オフィス</a> 
（セルリアンタワー<b>オフィス棟</b>内　7F） 。<br>
入館方法について、以下の点を注意してください（勝手に入館して「不審者」とみなされた場合には、警察のお世話になることもあります）。
<ul>
<li>集合場所はセルリアンタワーの <strong>地下１Ｆ</strong> です。</li>
<li><strong>当日 13:55 までに集合</strong>してください。時間厳守です。</li>
<li>到着した場合、担当者まで電話連絡ください。担当者の連絡先は、登録締切り後に各自メールアドレスへ連絡いたします。</li>
<li>当日遅れて参加される方は、<strong>必ず</strong>担当者までその旨をメールで連絡ください。</li></ul>

詳細については
<a href=
"https://tokyodebian-team.pages.debian.net/2008-07.html">勉強会の Web ページを参照ください。
</a><br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;num=10&amp;ie=UTF8&amp;s=AARTsJonWwNsQ3CAiYRdYL237PoPaZUwRA&amp;msa=0&amp;msid=109811903404931563048.000445ad0dcfd0fd0614a&amp;ll=35.660853,139.700003&amp;spn=0.016737,0.027466&amp;z=15&amp;iwloc=000447b1e2b77800bf4b9&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;num=10&amp;ie=UTF8&amp;msa=0&amp;msid=109811903404931563048.000445ad0dcfd0fd0614a&amp;ll=35.660853,139.700003&amp;spn=0.016737,0.027466&amp;z=15&amp;iwloc=000447b1e2b77800bf4b9&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small></li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>kernel patch の Debian パッケージ作成について</strong>（担当：岩松信洋）<br>
      実は Linux kernel 用の各種 patch も パッケージとして配布されている Debian。<br>
      あまり語られることのない Linux kernel patch パッケージの作り方と使い方について紹介します。</li>

  <li><strong>kernel module の Debian パッケージ作成について</strong>（担当：岩松信洋）<br>
      vanilla kernel に取り込まれていない kernel ドライバを 
      Debian 流で扱うにはどのようにすればいいのか。<br>
      Linux kernel module パッケージの作り方と使い方について紹介します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 7 月 17 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2008-07.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会担当者：岩松信洋 (iwamatsu &#64; {debian.or.jp} )
までお願いいたします。</p>
