第5回 関西 Debian 勉強会のお知らせ

<p>
直前になりましたが「<a href="http://wiki.debian.org/KansaiDebianMeeting20070812">第5回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、定期的に顔をつき合わせて、
Debian GNU/Linux のさまざまなトピック (新しいパッケージ、Debian 特有の機能の仕組み、Debian 界隈で起こった出来事、などなど) 
について楽しく話し合っていく集まりです。</p>

<p>参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年8月12日(日) 13:00-15:00 (受付は12:30以降、終了後に懇親会予定)</li>
  <li>会場：<a href="http://www.unity-kobe.jp/information/gakuen.html">神戸研究学園都市 UNITY 大学共同利用施設</a></li>
  <li>費用：有志によるカンパと言う形にしたいと考えています。また、集まった費用で印刷代を引いて余った分は、次回の勉強会に回させて頂きます。 (懇親会は別途 BYOB(Bring Your Own Bottle) 形式です。勉強会終了後、みんなで近くのコンビニやスーパーでお酒やおつまみを買い出しに行きましょう)。</li>
  </ul>
</dd>
<dt>内容（順不同）</dt>
<dd>
  <ol>
  <li><strong>Inkscapeを使ったOSC2007チラシ/DVDジャケットの制作</strong>　のがたじゅん氏<br>
      Open Source Conference 2007 Kansai@Kyoto 
      の関西Debian勉強会ブースで配布したチラシとDVDのジャケットの制作は、
      ベクタードロー系グラフィックツールのInkscapeを使い、
      すべてDebian上で制作しました。その舞台裏などをご紹介します。<br></li>

  <li><strong>Patch基礎編</strong>　矢吹 幸治氏<br>
      フリーソフトウェアおよびオープンソース(以下FLOSS)界隈で、
      他人と共同作業をする際に、よく"patch"(ぱっち)という言葉を聞くと思います。<br>
      FLOSS (Free/Libre and Open Source Software) の醍醐味のひとつに他人との共同作業があります。
      このパッチの概念を知ってもらうことで、どの様にして FLOSS 界隈の人達がネットワーク越しに共同作業を可能にしているのか語ります。<br>
      特に以下のような方にお勧めです。
      <ul>
       <li>GNU/Linuxを使っていて他人とプログラムや文書などの共同作業を行いたい人</li>
       <li>流通しているパッチを使って自分のシステム (Linuxカーネルなど) をカスタマイズしたい人</li></ul>
      <br></li>

  <li><strong>Debian パッケージの作り方 -- dpatch</strong>　大浦 真 氏<br>
      Debian パッケージを作る際には、配布元のソースパッケージに対して、
      そのパッケージ自身のバグや Debian 固有の問題に対処するために patch 
      をあてる必要があることがあります。
      その patch が一つだけの場合はいいのですが、複数ある場合には管理が面倒になります。<br>
      Debian の中には、この patch を管理するためのシステムがいくつかありますが、
      今回はその中の一つ、dpatch の使い方を紹介します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>事前登録については
<a href="http://cotocoto.jp/event/1703">京都.cotocoto</a>の 第5回 関西 Debian 勉強会ページを参照してください。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>
