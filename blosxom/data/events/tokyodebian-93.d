第93回 東京エリアDebian勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今月も Debian 勉強会が開かれます！</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<p>
今回は<a href="http://asahi-net.jp/">株式会社朝日ネット</a>さんの会議室をお借りしての開催となります。

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
   <li>2012年10月20日（土）15:00~19:00<br>
会場：<a href="http://asahi-net.co.jp/jp/location/">株式会社朝日ネット 会議室 東京都中央区銀座6-6-7 朝日ビル9F</a>
   <li>費用：500円（予定）</li>
   <li>その他：地図にある入口は当日閉まっているので、反対側にある裏口から入ってください。
   階段で地下一階に降りて、9Fまでエレベーターで上がってください。エレベーターホールから廊下に向かって右へ廊下を進んで突き当たりの部屋が会場となります。</li>
   </ul>
</dd>

<dt>内容</dt>
<dd>
<ul>
<li><strong>レゴでなめこ収穫機を作ってみた</strong>(担当:本庄 弘典)<br>

レゴブロックで組み立てたロボットを操作する「<a href="http://www.legoeducation.jp/mindstorms/">レゴマインドストーム</a>」で、
iPhoneやAndroid で人気のアプリ『<a href="http://beeworksgames.com/~osawaritantei/saibai/">なめこ栽培キット</a>』
の収穫機をnxt-pythonを使って作ってみましたので、その内容を紹介します。</li>

<li><strong>Haskell の Debian packaging 周辺について語ります</strong>(担当:日比野 啓)<br>

Haskell のパッケージおよび依存関係の仕組みである Cabal, Hackage / その情報を利用して Debian パッケージを作る cabal-debian コマンドおよび debian ライブラリ / Debhelper との連携のためのスクリプトである haskell-devscripts について紹介します。</li>

<li><strong>mtrack</strong>(担当:岩松 信洋)<br>
mtrack とか utouch のネタについて紹介します。</li>

</ul>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2012-10.html">勉強会のページ</a>を参照して、Debian 勉強会予約システムへの登録をしてください。</dd>
<dt>事前課題</dt>
<dd>
<p>
以下のいずれかにご回答ください。</p>
<ol>
<li>Debian での関数型プログラミング言語(*)の利用経験とその時に感じた事柄を挙げてください。（* Lisp, Emacs Lisp, OCaml, Haskell 等）</li>
<li>関数型プログラミング言語初心者に向けたお勧め書籍とどうしてその書籍が良いのか、という理由を挙げてください。</li>
<li>関数型言語で実装されている、使ってみたいソフトをあげてください（Debian パッケージになっているものが望ましいです)</li>
</ol>
</dd>
<dd>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org} )
までお願いいたします。</p>
