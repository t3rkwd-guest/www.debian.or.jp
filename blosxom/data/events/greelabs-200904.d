<a href="http://labs.gree.jp/Top/Study/20090410.html">GREE Labs 第17回 オープンソーステクノロジー勉強会 「でびあんのパブリックイメージとその実際」</a>

<p>
Debian JP Project のメンバーが GREE Labs さんの<a href="http://labs.gree.jp/Top/Study/20090410.html">オープンソーステクノロジー勉強会</a>にお邪魔してお話をすることになりましたので、
お知らせさせていただきます。ご興味とお時間のある方はぜひご参加ください。</p>

<dl>
<dt>開催日時・会場等</dt>
<dd>
<ul>
  <li>日時：2009年4月10日（金）19:30〜20:30 (その後、懇親会を予定)
  <li>会場：グリー株式会社 3Fセミナールーム</a> 東京都港区六本木4-1-4 黒崎ビル
 (最寄り駅：東京メトロ日比谷線・都営地下鉄大江戸線 六本木駅（六本木交差点から六本木一丁目方面へ徒歩4分）　
東京メトロ南北線 六本木一丁目駅（六本木一丁目から六本木交差点方面へ徒歩4分）)<br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?hl=ja&amp;geocode=&amp;ie=UTF8&amp;source=embed&amp;num=10&amp;msa=0&amp;msid=109811903404931563048.00046616122bc15e4e215&amp;ll=35.665556,139.735848&amp;spn=0,0&amp;output=embed"></iframe><br><small><a href="http://maps.google.co.jp/maps/ms?hl=ja&amp;geocode=&amp;ie=UTF8&amp;source=embed&amp;num=10&amp;msa=0&amp;msid=109811903404931563048.00046616122bc15e4e215&amp;ll=35.665556,139.735848&amp;spn=0,0" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>
</ul>
<dt>内容</dt>
<dd>
<p>
「誤解と実際 〜 「パブリックイメージ」と自身が関わってきた「でびあん」について」</p>
<p>
厳格な手続きのもと厳密な検査を経たパッケージと凄腕ハッカーによるその運営… というように語られる Debian 。では実際多少なりとも関わっている人から見るとその見え方はどう変わるのか？変わらないのか？Debian JP Project 関係者が少し話をしてみようかと思います。</p>
<ul>
<li>簡単な開発プロセスの概要</li>
<li>開発支援インフラストラクチャ</li>
<li>人員の参加と離脱〜実際に公式開発者（予定）になるまでの道程</li>
<li>etc...</ul>
</dd>
<dt>参加方法</dt>
<dd>
<a href="http://labs.gree.jp/Top/Study/20090410.html">参加申し込みのページ</a>から申し込みください。<br>
(本件の個人情報取り扱いについて、Debian JP Project では取り扱っておりません)
</dd>
</dl>
<p>
本件については、
GREE Labs オープンソーステクノロジー勉強会内でのイベントとなりますので、
お問い合わせは<a href="http://labs.gree.jp/Top/Study/20090410.html">GREE Labs</a>までお願い致します。
</p>
