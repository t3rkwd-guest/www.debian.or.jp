第64回 東京エリア Debian 勉強会＠筑波大学のお知らせ

<p>
Debian 勉強会とは、Debian の開発者になれることをひそかに夢見るユーザたちと、ある時にはそれを優しく手助けをし、またある時
には厳しく叱咤激励する Debian 開発者らが Face to Face で Debian のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです (Debian をまだ使ったことが無いが興味があるの
で…とか、かなり遠い所から来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。興味と時間があった方、是非御参加下さい。（また、勉強会で話をしてみたい
という方も随時募集しています）。</p>

<p>
今回は都心を離れ、<a href="http://www.tsukuba-linux.org">筑波大学の Linux ユーザグループ「つくらぐ」</a>さんと合同で、
筑波大学さんをお借りしての開催となります。
つくば近郊の方、あるいはつくばエクスプレスに乗ってフラッと来てみたいという方などなど、お待ちしております。</p>

<dl>
<dt>開催日時・参加費用</dt>
<dd>
<ul>
   <li>2010年5月15日（土）15:00~19:00</li>
   <li>費用：無料</li>
</ul>

<dt>会場</dt>
<dd>
筑波大学 第3エリア B棟 302室 [3B302] (つくばエクスプレス「つくば」駅よりバス10分) <br>
(<a href="http://www.tsukuba-linux.org/wiki/%E7%AD%91%E6%B3%A2%E5%A4%A7%E5%AD%A6%E3%81%B8%E3%81%AE%E3%82%A2%E3%82%AF%E3
%82%BB%E3%82%B9">つくらぐさんによる会場までのアクセス案内</a>)</dd>
<dd>
<iframe width="600" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=36.111184,140.100236&amp;ie=UTF8&amp;start=0&amp;ll=36.10522,140.101776&amp;spn=0.033286,0.051413&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=36.111184,140.100236&amp;ie=UTF8&amp;start=0&amp;ll=36.10522,140.101776&amp;spn=0.033286,0.051413&amp;z=14" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>
</dd>


<dt>内容</dt>
<dd>
<ul>
<li>Why GNU/Linux?<br>
なぜ GNU/Linuxなのか、なぜ GNU/Linuxを使うのか。その魅力について説明します。</li>
<li>Lighting Talk ~ Why Ubuntu? / Why Fedora? / Why Gentoo? / Why Debian?<br>
各ディストリビューションの魅力を語ります（それぞれ5分）</li>
<li>DebianでのLinuxカーネルとの付き合い方 (岩松信洋）<br>
DebianでのLinuxカーネルの開発体制などバックグラウンドの話や今時のLinuxカーネル構築方法などについて語ります。</li>
<li>その他</li>
</ul>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2010-05.html">勉強会のページ</a>を参照して、<strong>5/13 までに Debian 
勉強会予約システムおよびつくらぐさんのATNDへの登録をしてください（ご面倒ですが双方への登録をお願いします）。
その際、事前課題がありますのでご回答をお願いします。</strong></dd>

<dt>事前課題</dt>
<dd>
「普段使っているLinuxディストリビューションと使ってみたいLinuxディストリビューションは何ですか？使っている理由、魅力、不
満点、使ってみたい理由を教えてください。」</dd>
<dd>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します (Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、Debianユーザ用公開メーリングリスト (debian-users@debian.or.jp) にてお願いします。</dd>
</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：岩松 (iwamatsu@{debian.org} ) までお願いいたします。</p>

