第 112 回 関西 Debian 勉強会 in OSC2016 Kansai@Kyoto のお知らせ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20160730">第112回 関西 Debian 勉強会 in OSC2016 Kansai@Kyoto</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>

<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2016 年 7 月 30 日 (土) 10:00 - 17:50 (展示 10:00 - 16:00)</li>
    <li>会場：<a href="http://www.krp.co.jp/index.html">京都リサーチパーク(KRP)</a> OSC総合受付：アトリウム</li>
    <li>費用：無料</li>
  </ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
  <ul>
    <li>Debian 8 "Jessie" 稼働マシンと勉強会資料の展示</li>
    <li>Debian インストーラDVD・ステッカー・クリアファイルの配布など</li>
  </ul>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
本イベントは入場無料です。
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
