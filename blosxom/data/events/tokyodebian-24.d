第24回 Debian 勉強会のお知らせ

<p>
みなさんは毎月 Debian の勉強会が開かれているのを御存じでしょうか?<br>
<a href=" https://tokyodebian-team.pages.debian.net/">「Debian 勉強会」</a>
は月に一回、顔をつき合わせて Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について話し合う会です。</p>
<p>
参加者には開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。<br>
興味を持たれた方は是非御参加下さい（また、勉強会で話をしてみたいという方も随時募集しています）。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年1月20日(土) 18:00-21:00
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)
  <li>費用：資料代・会場費として 500 円を予定</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>2007年の Debian 勉強会を計画する！</strong><br>
この勉強会の事前課題の内容や、過去の事例、昨今の世界情勢 :-) などを踏まえ、
年始に相応しく今年の Debian 勉強会の内容を参加者の方々と考えていきます。

  <li><strong>apt-torrent について語ってみる</strong><br>
p2p アプリケーション bittorrent を利用する apt の配布方法、apt-torrent を触ってみた報告です。

  <li><strong>kvm について語ってみる</strong><br>
今旬の技術「仮想化」。ここでは仮想化エンジン「KVM」をちょっとさわってみた、という報告をします。

  <li><strong>Debian Conference 参加について考えてみる</strong><br>
Debian 関係者が年に１度世界中から集まるカンファレンス、Debian Conference。
今年は6月にイギリスのエディンバラで開かれますが、参加にむけての情報交換（と参加を
迷っている方への後押し）を行います。</ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-01.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
