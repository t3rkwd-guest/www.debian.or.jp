第 29 回 関西 Debian 勉強会 in 関西オープンソース2009

<p>
関西Debian勉強会は、関西エリアでDebian GNU/Linuxのさまざまなトピック
（新しいパッケージ、Debian特有の機能や仕組み、Debian 界隈で起こった出来
事などなど）について話し合う勉強会です。
</p>

<p>
今月11月は大阪南港ATCにて開催される、<a href="http://k-of.jp/2009/">関西オープンソース2009に関西</a>
にて、セミナーやブースでの展示と説明などしておりますので、お時間がある方はぜひお越しください。
</p>
<p>
最新情報については <a href="http://wiki.debian.org/KansaiDebianMeeting20091106-07">Debian Wiki のページ</a> を参照して下さい。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年11月6日(金)、11月7日(土)<br>
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="http://k-of.jp/2009/access/">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ステージ企画は無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt><a href="http://www.k-of.jp/2009/list_seminar.html">セミナー<br>
<strong>「第 29 回 関西 Debian 勉強会 in 関西オープンソース2009」</strong></a><br>
（会場:9F-セミナールーム2　11月7日(土) 15:00-15:50  担当：Yukiharu YABUKI(Debian Project/キイレジャパン株式会社))</dt>
<dd>
様々な OSS 開発者に向けて、Debian Project の魅力を語ります。その他、7 月に台湾で開催された mini-Debconf のレポートや、簡単に勉強会の紹介など。
</dd>

<dt><a href="http://www.k-of.jp/2009/list_stage.html">ステージ企画<br>
<strong>「Debian開発者への歩み」</strong><br>
 (展示会場内特設ステージ　11月7日(土) 17:45-18:00　担当：Yukiharu YABUKI(Debian Project/キイレジャパン株式会社))</a></dt>
<dd>
インターネットを拠点として、自由なOSを作るために集まった Debian Projectのメンバーとなるべく出演者が経験したこと、Debian開発者になるのだという動機を語ります。 </dd>

<dt>ブース展示・配布物・販売物</dt>
<dd>
  <ol>
  <li><strong>Debian 稼働マシン</strong></li>
  <li><strong>Debian 同人誌「あんどきゅめんてっどでびあん」</strong></li>
  <li><strong>フライヤー・Debian "Lenny" Live DVD</strong></li>
  <li><strong>有志作成 Debian グッズ（Tシャツ・特製ステッカー）</strong></li>
  </ol>
</dd>

<dt>GPG鍵の交換 (キーサイン) をしませんか</dt>
<dd>
  <p>GPG鍵の生成に使われるSHA-1アルゴリズムについて、衝突耐性に対する脆弱性
  が指摘されています。そのため強度のある新しい GPG 鍵が必要になりました。
  関西オープンソース2009に参加するみんなで、新しく作ったGPG鍵の交換をして、
  信頼の輪 (Web of Trust) を広げませんか。</p>

  <a href="http://wiki.debian.org/KansaiDebianMeeting20091106-07">GPG 鍵を交換するのに必要な準備</a>
  をご確認の上、ブースまでお越しください。

  <dl>
  <dt>事前準備</dt>
  <dd>
<ul>
<li>事前に 4096bit/RSA での GnuPG 鍵の生成（<a href="http://lists.debian.or.jp/debian-users/200909/msg00008.html">debian-users メーリングリストに流れた以前の案内</a>を参考に）
<li>上記で生成したGnuPG 鍵のキーフィンガープリントの印刷物（gpg --fingerprint <あなたの鍵ID> で確認できます。手書きもできますが、多数の人と交換する場合はかなり煩雑です）
<li>身分を証明できる公的証明書の準備（パスポート、運転免許証など。特に日本語が読めない人と交換を行う場合はパスポートが必須）
<li>作成した鍵の公開鍵をキーサーバ (pgp.nic.ad.jp および pgp.mit.edu) にアップロード
</ul>
<p>
当日鍵交換の手順</p>
<ol>
<li>鍵IDとキーフィンガープリントを印刷したものを交換し会う
<li>相手の身分証明書が有効 (本人であるか・有効期限など) なものか、確認する。
<li>互いの身分証明書を見せる
<li>問題なければOKです（証明書や印刷物に不備がある場合は交換を中止してください）
</ol>
  </dd>
</dd>
</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：のがたじゅん (nogajun&#64;{debian.or.jp} ) までお願いいたします。</p>

