第 108 回 関西 Debian 勉強会のお知らせ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20160327">第108回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2016 年 3 月 27 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：20名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      systemdで起動するJessie環境を御用意下さい。幾つか設定を変更することがありますので、VM環境の方が無難かもしれません。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>systemdに浸かってみる</strong>(担当：佐々木洋平)<br>
  Jessieからdefaultとなったinitであるsystemdですが「全てがsystemdになる」と揶揄されるように、
  他にも様々な機能があります。今回はパッケージで提供されているsystemd関連のツールを一通り試してみて、
  どう使うのか/使えるのかについての検証結果を報告します。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.doorkeeper.jp/events/40547">Doorkeeper</a>を参照して、
事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
