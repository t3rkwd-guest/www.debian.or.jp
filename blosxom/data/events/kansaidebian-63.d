第63回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20120826">第63回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 8 月 26 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター301号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
      <li>
      MIT版Kerberosでサービスに必要となるプロセス名と、そのプロセスが使用するポートを 2 つ以上挙げてください。
      </li>
      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>Debian ではじめる Kerberos 認証</strong>(担当：倉敷さん)<br>
   Kerberos について簡単に概観した後で、LDAP と Kerberos を組み合わせたログインアカウント認証システムの構成例を紹介します。
  </li>


  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 8 月 24 日 () 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=3692aaa8674906d19ae4d0605d7a669b8ab51625">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
