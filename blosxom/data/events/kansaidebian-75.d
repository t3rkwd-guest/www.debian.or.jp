第75回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130825">第75回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 8 月 25 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：15名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      構成管理システムをどのように活用(している/したい)かを記述してください
      <blockquote>
      例えば<br>
      もう使っているなら「どう(何に)使っている」「現状のメリットと課題」など<br>
      まだ使ってないなら「どう(何に)使いたい」「こんなことができたらいいのに」など<br>
      </blockquote>
      </li>

      <li>
      お手元の端末に、Wheezy の仮想環境をセットアップしておいてください
      <ul>
      <li>VMware/VirtualBox/KVM など、種類は問いません</li>
      </ul>
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「puppet による構成管理の実践」</strong> (担当：倉敷)
    <ul>
    <li>前半：puppet と reprepro で debian の部分ミラーを作る (実習)</li>
    <li>後半：事前課題を肴にディスカッションもしくは puppetize 実演</li>
    </ul>
    ※前半は、一緒に手を動かしながら仮想環境の Wheezy 上で作業します。会場に通信設備はありませんので、可能な限り、各自で通信環境もご用意ください。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<strong>2013 年 8 月 23 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=e8dae7600c5c07040f64859e230a717f071b3629">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷 悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
