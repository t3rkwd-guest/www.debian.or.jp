東京エリア Debian 勉強会 in オープンソースカンファレンス 2023 Hokkaido

<p>
北海道にいらっしゃる皆様こんにちは。私たちは、<a href="https://event.ospn.jp/osc2023-do/">オープンソースカンファレンス 2023 Hokkaido</a> に参加します。
</p>

<p>
イベントではブース展示を行います。
</p>

<p>
ブース展示ではDebianの紹介やあなたがちょっと知りたいことや疑問に思っていることにお答えします。「他のOSと比べてDebianはどこがいいのか」「Debianを使いたいけど何から始めればいいかわからない」「Debianでここがうまくいかないので教えてほしい」などの疑問がある方はぜひともブースまでいらしてください。
</p>

<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、普段は聞けないような様々な情報を得るチャンスです。
</p>

<p>
興味と時間がある方は是非イベントにご参加下さい。また、勉強会で話をしてみたいという方も随時募集しています。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2023 年 6 月 24 日(土) 10:00-16:00</li>
  <li>会場：<a href="https://www.sapporosansin.jp/access/">札幌市産業振興センター 2F ルームA・ルームB</a>
（地下鉄東西線 東札幌駅 1 番出口から会場まで徒歩 7 分）</li>
  <li>参加費用：無料</li>
  </ul>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」展示</li>
  <li>Debian 稼働マシン 展示</li>
  <li>Debian ステッカー等の配布</li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：杉本 典充 (dictoss &#64; {debian.or.jp} ) までお願いいたします。
</p>
