第45回 Debian 勉強会 in オープンソースカンファレンス 2008 Tokyo/Fall

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この10 月は、
<a href="http://www.ospn.jp/osc2008-fall/">オープンソースカンファレンス 2008 Tokyo/Fall</a>
に参加してセミナーやブースでの展示と説明などを予定しています。
普段は聞けないような様々な情報を交換しあうチャンスです。是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2008年10月4日(土) 10:00-17:00<br>
  <li>会場：<a href=
"http://www.pio.or.jp/plaza/access/index.htm">大田区産業プラザ(PiO)</a>
 (東京都大田区・京急蒲田駅徒歩3分)<br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;om=1&amp;s=AARTsJrQHjq5X2oqsgJF7E3H44KZJxiKEg&amp;msa=0&amp;msid=109811903404931563048.0004393d34d8902867402&amp;ll=35.567003,139.723778&amp;spn=0.033513,0.054932&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;om=1&amp;msa=0&amp;msid=109811903404931563048.0004393d34d8902867402&amp;ll=35.567003,139.723778&amp;spn=0.033513,0.054932&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">拡大地図を表示</a></small></li>
  <li>費用：無料</li></ul></dd>
<dt>セミナー (11:15-12:00＠会議室 D )</dt>
<dd>
  <ul>
  <li><strong><a href="http://www.ospn.jp/osc2008-fall/modules/eguide/event.php?eid=17">「あなた」とオープンソース／フリーソフトウェア、そして「Debian」</a></strong>（担当：やまねひでき）<br>
オープンソース／フリーソフトウェアは、「あなた」にとってどんな価値があるのでしょうか？<br>
どのような利益をもたらすものなのでしょうか？不利益は生じないのでしょうか？<br>
「自由」を求めるディストリビューション Debian のパッケージメンテナの一人として、そして個人／会社等での利用者としてこれらに接する立場から改めて考えた視点を伝えます。</li>
   </ul></dd>

<dt>セミナー (13:00-14:45＠展示会場内特設ステージ)</dt>
<dd>
  <ul>
  <li><strong><a href="http://www.ospn.jp/osc2008-fall/modules/eguide/event.php?eid=46">「勉強会大集合〜東京エリアDebian勉強会の紹介」</a></strong>（担当：岩松信洋）<br>
「勉強会大集合」に東京エリアDebian勉強会として参加します。Debian 勉強会の紹介と運営方法など etc について話をします。</li>
   </ul></dd>



<dt>ミニセミナー (時間／場所未定)</dt>
<dd>
  <ul>
  <li><strong>Debian でぶっこぬき！ FLOSSのFlash環境</strong> (担当：岩松信洋）<br>
自由なソフトウェアで構成される Debian GNU/Linux。プロプラエタリなソフトウェアの代表である Flash-plugin を使わずに GNU/Linux 上で生き延びる方法を話します。</li></ul>
</dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料</li>
  <li>チラシ・LiveDVD（配布も予定）</li>
  <li>Debian 稼働マシン (Armadillo-9 等)</li></ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
セミナー参加希望者は<a href="http://www.ospn.jp/osc2008-fall/">事前登録</a>してください。<br>
ブース展示・ミニセミナーについては特に登録などはありません。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会 担当者：山本浩之 (board&#64;debian.or.jp)
までお願いいたします。</p>
