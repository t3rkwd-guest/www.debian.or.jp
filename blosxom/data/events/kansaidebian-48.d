第48回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20110626">第48回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2011 年 6 月 26 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">大阪 福島区民センター 302号会議室</a>
(定員：45名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
<dl>        
<dt>IPv6 編</dt>
<dd>
<ol>
   <li><a href="http://test-ipv6.jp/">http://test-ipv6.jp/</a> で IPv6 接続性をテストしてください。 (出来れば複数の環境で) 
   <li>World IPv6 Day (以降) で何か影響がありましたか。教えてください (例: 特定のサイトへのアクセスが遅くなった、何度かリロードしないと表示されなくなった) 
</ol>
</dd>
<dt>Debian パッケージ編</dt>
<dd>
Debian パッケージを作成した事の無い人は Debian のソースパッケージの構成を復習しておいて下さい。<br />
過去の Debian 勉強会の資料や Debian JP Project にて公開されている資料がお勧めです。何か分からない点や感想等あれば、教えてください。
<ul>
<li><a href="http://video.google.com/videoplay?docid=2566250264874202701">Debian パッケージ 60 分クッキング</a> (ビデオ)
<li><a href="https://tokyodebian-team.pages.debian.net/pdf2008/debianmeetingresume200802-hanson.pdf">OSC 2008 ハンズオン配布資料</a> (PDF)
</ul>
</dd>
</dl>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「IPv6」</strong> (担当:西山和広)<br>
APNICにおけるIPv4アドレス在庫が枯渇し、IPv6対応が進んできています。 そこでIPv4でしかインターネットに接続できる環境しかなくても、 追加の契約などをしなくても使えるトンネル接続の teredo や 6to4 を 試してみたので、その話をします。
</li>
</dd>
<dd>
  <li><strong>「vcs-buildpackage{svn, git 編}」</strong> (担当: 佐々木洋平)<br>
Debian パッケージの作成にバージョン管理システム(VCS)を使うやり方について、svn-buildpackage と git-buildpackage を例に紹介します。これらは過去の Debian 勉強会でも紹介されてきましたが、Source format 3.0 になって work flow が多少変わっていたりするので、その紹介/How To もできたら良いかと思っています。</li>
  </ol>
</dd>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2011年 6 月 24 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=c979fd898b9b7dad223149cde144724ec1ef6bc2">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
