第9回 関西 Debian 勉強会のお知らせ

<p>
12月の「<a href="http://wiki.debian.org/KansaiDebianMeeting20071209">第9回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年12月9日 (日) 13:45〜17:00 (13:30 より受付。17:30〜19:30 にて懇親会予定)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 
福島区民センター 会議室</a></li>
  <li>費用：500円</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro (関西 Debian 勉強会についての説明)</strong>　山下 尊也 氏<br>
       この勉強会の目的などを説明し、その後、大阪南港ATCで行われた第8回関西
Debian勉強会@関西オープンフォーラムの内容について話をします。
       </li>

  <li><strong>関西オープンフォーラム参加報告</strong>　川江 浩 氏<br>
40歳代の「おっちゃん」が、KOF（関西オープンフォーラム） に参加し
て思った事。オープンソースに関わって来た経緯を、最新のアニメ、Xen、
IPv6、人生観を交えながら報告します。
</li>

  <li><strong>2007年の関西Debian勉強会を振り返る</strong>　山下 尊也 氏<br>
今年から始まった関西 Debian 勉強会も、今回で9回目となりました。この
1年間の思い出と、この勉強会はどのように行われているのかを、説明したいと
思います。
</li>

  <li><strong>WirelessSync</strong>　山下 尊也 氏<br>
時代は、Wireless!!あの機器もDebian で Sync が出来る。
今が、旬の情報を伝授します。
</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<a href="http://cotocoto.jp/event/17074">京都.cotocoto</a>を参照して、
事前登録をしてください
（17:30 からの懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>
