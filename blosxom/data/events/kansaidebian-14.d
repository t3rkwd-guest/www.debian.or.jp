第14回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080629">第14回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p></p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 6 月 29 日 (日) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a herf="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 会議室</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第14回関西Debian勉強会の内容について話をします。</li>

  <li><strong>pbuilder でパッケージをビルドしてみる</strong>　大浦 真 氏<br>
   pbuilder とは、Debian のソースパッケージを chroot 環境でビルドするた
めのシステムです。自分の手元にあるパッケージを自分でカスタマイズした環境
の中だけでビルドしていても、それが他の環境でビルドできる保証はありませ
ん。それを簡単にチェックするために pbuilder を利用することができます。今
回はこの pbuilder の使い方を実例を示しながらお話します。</li>

<li><strong>HotPlug の現状</strong>　川島 氏<br>
現在の Linux では，USB ストレージデバイスを差し込むだけで自動的に認識，
マウントが行われます．このような HotPlugの仕組みについて，カーネルから
udev デーモン，HAL デーモンまでのイベントの伝搬を，ソースコードを元に述
べたいと思います．</li>

<li><strong>SSL のサーバー証明書が失効する時</strong>　久保 博 氏<br>
[http://www.debian.org/security/2008/dsa-1571 DSA-1571] の OpenSSL の脆
弱性の問題は、日本べサイン社がプレスリリースを発表するまで社会的影響が広
がりました。これを機会に、OpenSSL とサーバー証明書について、ユーザーの視
点で仕組みを解説します。この問題の初心者向けです。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>6月 26日(木)24:00までに</strong>
<a href="http://cotocoto.jp/event/27654">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


