第65回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20121028">第65回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 10 月 28 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区民センター 楓</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      poEdit など、使用した事がある翻訳支援ツールの概要・長所・短所を教えて下さい。<br>
      po4a や 自作のスクリプトなどでも結構です。<br>
      翻訳に参加したことが無い方やツールを使用しない方は、何か1つ翻訳支援ツールについて調べ、概要・長所・短所を教えて下さい。
      </li>

      <li>
      今年八月以降のDSAを一つ選び、英語の原文と日本語訳を読み比べて、翻訳に関して忌憚なく批評して下さい。<br>
      DSA は、ML debian-security-announce AT lists DOT debian DOT org と debian-users AT debian DOT or DOT jp のバックナンバーを読むか、
      <a href="http://www.debian.org/security/">セキュリティ情報</a>あるいは <a href="http://www.debian.org/security/2012/">2012 年に報告されたセキュリティ勧告</a> の web ページで探すことができます。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>本気で "使える" 翻訳環境を構築してみた（い）</strong>(担当：八津尾さん)<br>
  OSS 界隈で活躍しているトランスレータの皆さんやこれから翻訳に参加してみようかとお考えの皆さんと一緒に、私が実践してみた翻訳ソリューションを元に "本当に使える" 翻訳環境について議論してみたいと思います。
  </li>

  <li><strong>DSA翻訳の舞台裏</strong>(担当：久保さん)<br>
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 10 月 26 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=8facb9d54456484c174eea39493f1a3fd6d6263e">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
