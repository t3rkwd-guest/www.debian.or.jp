第 123 回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。2 月も 東京エリア Debian 勉強会が開かれます！
</p>

<p>
 東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とす
る熱いユー ザたちと、実際に Debian　Project にてすでに日夜活動している人
らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケー
ジ、Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2015 年 2 月 21 日 ( 土曜日 ) 14:00-19:00</li>
   <li>費用：500 円</li>
   <li>会場：<a href="http://www.jp.square-enix.com/company/ja/access/index2.html">株式会社 スクウェア・エニックス セミナールーム</a></li>
　 <li>当日について：一旦14:00にビル内で集合して中に入ります。また、Debianに関する作業時間 ( hack time ) を勉強会中に用意しています。詳しくは<a href="http://debianjp.connpass.com/event/11680/">勉強会のページ</a>を参照してください。( 前回に引き続き connpass にて案内、参加登録を行っています ) </li>
   </ul>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong> セミナ </strong><br>
「Debian GNU/kFreeBSDにおけるJail構築を試してみた」 dictossさん <br>
<small>※補足:<a href="http://ct-study.connpass.com/event/10566/">「第6回 コンテナ型仮想化の情報交換会＠東京」</a>の発表とほぼ同等の内容となります。</small></li>
　<li><strong> [!!NEW 2/8] LT </strong><br>
       「Gnukと私」 alohaugさん </li>
　<li><strong>hack time (全員)</strong><br>
	Debian に関する作業を各自で行います</li>
  <li><strong>成果発表 (全員)</strong><br>
	行った作業について簡単に発表いただきます</li>
  </ol>
</dd>

<dt>事前課題／参加申し込み方法</dt>
<dd>
<a href="http://debianjp.connpass.com/event/11680/">勉強会のページ</a>を参照して、参加登録をしてください。</dd>
<dd>
<br>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：野島 貴英 (nozzy@{debian.or.jp} ) までお願いいたします。</p>
