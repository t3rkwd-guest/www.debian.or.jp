Debian/Ubuntuユーザミートアップ in 札幌 2017.07を開催します

<p>
2017年7月14日と15日に開催される<a href="https://www.ospn.jp/osc2017-do/">OSC北海道2017</a>に
<a href="http://www.debian.or.jp">Debian JP Project</a>、
<a href="https://tokyodebian.alioth.debian.org/">東京エリアDebian勉強会</a>/
<a href="https://wiki.debian.org/KansaiDebianMeeting">関西Debian勉強会</a>)、
<a href="http://ubuntulinux.jp/">Ubuntu Japanese Team</a>
が出展とセミナーを行います。OSCには日本在住のDebian・Ubuntu関係者が参加するのですが、
ぜひこの機会を利用して北海道在住のDebian/Ubuntu
ユーザと交流をしたいと思い、「Debian / Ubuntu ユーザーミートアップ in
札幌 2017.07」と題したイベントを行うことにしました。Debian/Ubuntuを主題とした15分ほどのセミナー形式を数本行った後、参加者のみなさんと意見交換するミートアップタイムを行うというスケジュールとなっています。
北海道在住のDebian/Ubuntuユーザーの皆さん、Debian/Ubuntu に興味のある方々、参加お待ちしております。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2017年7月14日(金) 19:00-20:50<br>
  <li>会場：株式会社インフィニットループ 会議室 (<a
		  href="http://www.infiniteloop.co.jp/blog/2015/07/keiro/">サッポロファクトリー1条館　3階</a>)</li>
  <li>参加費用：無料
</dd>

<dt>参加方法と内容</dt>
<dd>参加人数を把握したいので、
<a href="https://debianjp.connpass.com/event/60850/">Debian / Ubuntu ユーザーミートアップ in 札幌 2017.07</a>
から登録お願いします。
内容も同サイトからお願いいたします。
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：岩松 信洋 (iwamatsu &#64; {debian.or.jp} )
までお願いいたします。
</p>
