第1回Debianパッケージング道場 のお知らせ

<p>
Debian パッケージの作り方を教える道場です。以下のような方を想定しています。</p>
<ol>
<li>あのソフトウェアがDebianパッケージになっていないので、パッケージにしてみたい。
<li>普段使っているDebianパッケージに修正を加えて、利用したい。
<li>メンテナンスしているDebianパッケージを更新してみる。
<li>モダンなDebianパッケージの作成方法とか知りたい。
<li>etc...
</ol>
<p>
丸一日、Debian漬けでDebianパッケージを作ってみましょう。<br>
Debian Developer と Debian Maintainer が、パッケージの作成をサポートします。</p>

<dl>
<dt>開催日時・会場・事前登録等</dt>
<dd>
  <ul>
  <li>日程：2013年2月9日(土) 10:00〜17:00<br>
  <li>会場：<a href="http://www.kyoto-u.ac.jp/ja/access/campus/map6r_n.htm">京都大学 理学研究科3号館 数学教室 109 号室</a></li>
  <li>定員: 20 名
  <li>参加費用：無料
  <li>事前登録: <a href="http://www.zusaar.com/event/506003">Zusaarにページが用意してあります</a>ので適宜登録ください
  </ul>
</dd>

<dt><strong>道場へ参加する前に必要な事前準備について</strong></dt>
<dd>
<ol>
<li>パソコンを使うので、各自パソコンを持ってきてください。
<li>使うパソコンで無線LAN、有線LAN が動作する環境を整えてください。
<li>使うパソコンで Debian / unstable が動作する環境を整えてください（VirtualBox 等の仮想環境で大丈夫です）。
<li>Debian / unstable に 最低限のビルド環境を構築してください。<br>
    sudo apt-get install build-essential debhelper devscripts cowdancer pbuilder
<li>パッケージにしたいソフトウェアを見つけておいてください。<br>
    <strong>こちらではパッケージング方法について教えることはできますが、ネタは提供できません。</strong>
<li><a href="http://www.debian.org/doc/manuals/maint-guide/">Debian 新メンテナーガイド</a> や
  <a href="http://www.debian.org/doc/manuals/developers-reference/">Debian 開発者リファレンス</a>に目を通しておいてください。
</ol>
</dd>

<dt>会場について</dt>
<dd>
<ul>
<li>建物内には喫煙所はなく、喫煙所は建物外の離れた場所になります。喫煙される方はなるべく受付前や昼食時にお済ませください。
</dd>

<dt>当日の流れ</dt>
<dd>
<ol>
<li>簡単な挨拶など。
<li>基本的なパッケージの作成方法を説明。
<li>最新のパッケージング事情の説明と使い方を説明。
<li>パッケージ作成作業開始
  <ul>
  <li>参加者は何かパッケージにしたいものを持ってきて、それをパッケージ化する<strong>（こちらから提供はしません)</strong>。
  <li>参加者は弄りたいまたは修正したい既存のパッケージを見つけてくる。そしてそれをいじる。
  <li><a href="http://wnpp.debian.net">保守担当者のいないパッケージ、パッケージ化して欲しいソフトウェア一覧</a>から気になるソフトウェアを探してみるのもいいでしょう（要するにパッケージするテーマを持って参加してみましょうということです）。
  <li>わからないところがあったら、遠慮無くその辺にいるDebian Developerを捕まえて聞いてください。
  <li>Debian Developer はメンターになってパッケージをDebianにインストールするまで面倒をみる（このへんが道場っぽいはず）。
  <li>できれば参加者はGPGキーサインをする。<br>
      GPG 鍵、公的な身分証明書（パスポート、運転免許証等。可能であれば複数）を持ってきてください。
 </ul>
<li>作業内容発表<br>
    できたパッケージ、作業内容を発表
</ol>
<p>
その他Debianのバグ修正をしたい人やもくもくと開発したい人も歓迎します。こんなことやりたい！、こんなこと教えて！
とかいろいろ<a href="http://www.zusaar.com/event/506003">登録ページ</a>のコメントに書いてください。</p>
</dd>

</dl>

<p>
本件に関するお問い合わせは主催者: 佐々木 洋平（uwabami at {debian.or.jp}）、倉敷 悟（lurdan at {debian.or.jp}）、岩松信洋（iwamatsu at {debian.or.jp}）
までお願い致します。
</p>

