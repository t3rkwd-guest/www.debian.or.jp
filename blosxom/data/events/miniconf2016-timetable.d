Mini Debian Conference Japan 2016 タイムテーブル公開のお知らせ

  <p>2016年12月10日(土曜日)に<a href="http://cybozu.co.jp/company/info/">サイボウズ株式会社</a>において、
  「Mini Debian Conference Japan 2016」を開催します。
  先日(11/29)より<a href="http://miniconf.debian.or.jp/index.html#timetable">タイムテーブル</a>を公開しました。
  Debianパッケージング、OSSバーチャルシンガー、組込み、他ビルド
  システムの適用方法など、幅広い発表内容となっています。
  参加を予定されている方、是非ご登録をお願いします。また、会社や学校、SNS等でMini Debian Conference Japan 2016に
  興味がありそうな方がいれば、紹介してみて下さい。

  <dl>
    <dt>Mini Debian Conference Japan 2016 登録サイト</dt>
    <dd>
      <ul>
	<li><a href="http://debianjp.connpass.com/event/44481/">http://debianjp.connpass.com/event/44481/</a></li>
      </ul>
    </dd>

    <dt>2016 LibreOfficeKaigi/MiniDebianConfJapan Party 登録サイト</dt>
    <dd>
      <ul>
	<li><a href="http://connpass.com/event/44587/">http://connpass.com/event/44587/</a></li>
      </ul>
    </dd>
    <dd>
  </dl>

  それぞれ別の登録となっています。
  <font color="#FF0000">会場の出入りが自由ではありません。
  入場方法については、参加者登録者のみに参加登録サイト(connpass)
  のメッセージ機能で事前にご案内します。そのため事前に登録をお願いします。</font><br>
  また、Debianおよびフリーソフトウェアの開発に重要なPGP/GPGキーサインが行える
  <a href="http://miniconf.debian.or.jp/ksp.html">PGP/GPGキーサインパーティ</a>についても登録が開始されています。
  こちらもよろしくお願いします。</br>
  懇親会については有料（一般2000円・学生1000円）となっています。
  今回の懇親会は併催する<a href="http://libojapan.connpass.com/event/42685/">LibreOffice Kaigi 2016.12</a>との合同で開催いたします。
  懇親会では発表者の方および参加者の方と交流ができますので、是非<a
  href="http://connpass.com/event/44587/">ご参加</a>ください。
  </p>

  <h4>Mini Debian Conference Japan 2016 開催概要</h4>

  <dl>
    <dt>開催日時</dt>
    <dd>
      <ul>
	<li>2016年12月10日(土曜日) 10時00分〜18時00分まで(予定)</li>
      </ul>
    </dd>

    <dt>開催場所</dt>
    <dd>
      <ul>
	<li><a href="http://cybozu.co.jp/company/info/">サイボウズ株式会社</a> 東京都中央区日本橋2-7-1 東京日本橋タワー</li>
	<li><a href="http://cybozu.co.jp/company/info/map_tokyo.html">地図: http://cybozu.co.jp/company/info/map_tokyo.html</a></li>
      </ul>
    </dd>

    <dt>参加費</dt>
    <dd>無料</dd>

    <dt>公式タグ</dt>
    <dd>Twitter: <a href="https://twitter.com/search?q=%23debianjp&src=hash">#debianjp</a></dd>
    <dd>ブログ: debianjp</dd>
  </dl>

  <h4>お問い合わせ</h4>
  <p>
  Mini Debian Conference Japan 2016に関するお問い合わせは、miniconf2016 _at_ debian.or.jp までメールにてご連絡ください。
  </p>
