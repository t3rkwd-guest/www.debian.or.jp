第 124 回 関西 Debian 勉強会 - Debian 9 "Stretch" リリースパーティ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20170618">第124回 関西 Debian 勉強会 Debian 9 "Stretch" リリースパーティ</a>」
のお知らせです。
</p>

<p>
Debian 9 "Stretch" のリリースを祝って世界各地で行なわれるリリースパーティを関西でも行ないます。
アップグレード人柱話やデスクトップ環境などあれこれわいわいやりながら Debian 9 "Stretch" のリリースを祝いませんか。
</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2017 年 6 月 18 日 (日) 13:00 - 19:00</li>
    <li>会場：<a href="https://www.sakura.ad.jp/corporate/">さくらインターネット大阪本社 イベントスペース</a>
(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong></li>

  <li><strong>リリースパーティ</strong></li>

  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.connpass.com/event/59443/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。
</p>
