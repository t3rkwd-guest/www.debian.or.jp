第65回 Debian 勉強会 in オープンソースカンファレンス 2010 Tokyo/fall

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この 9 月は、
<a href="http://www.ospn.jp/osc2010-fall/">オープンソースカンファレンス 2010 Tokyo/fall</a>
に参加してセミナーの時間を使ったトークやブースでの展示と説明、そしてGPGキーサインパーティなどを予定しています。
普段は聞けないような様々な情報を交換しあうチャンスです。是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2010年9月11日(土)<br>
  <li>会場：<a href="http://www.openstreetmap.org/?lat=35.6427195668221&lon=139.410289227962&zoom=15">明星大学 日野キャンパス 26号館</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul></dd>

<dt><a href="http://www.ospn.jp/osc2010-fall/modules/eguide/event.php?eid=47"><strong>GPGキーサイン説明およびGPGキーサインパーティ</strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：GPGキーサイン運営チーム</dd>
<dd>対象：オープンソースソフトウェア開発者、GPGを使ったパッケージリポジトリを利用しているディストリビューションユーザ（例: Debian, Ubuntu, CentOS)
<dd>開始：10時15分〜</dd>
<dd>会場：504</dd>
<dd>
<p>
メールへの署名やファイルの暗号化、Debian パッケージへのサイン等などに使われる GnuPG 鍵について、
Debian 関係者が集まるイベントでは信頼の輪 (Web of Trust) を広げるために<a 
href="http://www.debian.org/events/keysigning">キーサインの交換がよく行われます</a>。
今回のオープンソースカンファレンス 2010 Tokyo/Fall も Debian 公式開発者をはじめとする関係者が集まる良い機会ですので、
希望する方と適宜キーサインの交換を行う予定です。</p>
<p>
キーサインを希望される方は以下を確認の上、ブースへご来場ください。</p>
</dd>
<dd>
	<ul>
	<li>事前に 4096bit/RSA での GnuPG 鍵の生成（<a href="http://lists.debian.or.jp/debian-users/201009/msg00008.html">debian-users 
            メーリングリストに流れた以前の案内</a>を参考にどうぞ）
	<li>上記で生成したGnuPG 鍵のキーフィンガープリントの印刷物（gpg --fingerprint <あなたの鍵ID> で確認できます。
            手書きもできますが、多数の人と交換する場合はかなり煩雑です）
	<li>身分を証明できる公的証明書の準備（パスポート、運転免許証など。特に日本語が読めない人と交換を行う場合はパスポートが必須）
	<li>作成した鍵の公開鍵をキーサーバ (pgp.nic.ad.jp および pgp.mit.edu) にアップロード
	</ul>
<p>
当日鍵交換の手順</p>
	<ol>
	<li>鍵IDとキーフィンガープリントを印刷したものを交換し会う
	<li>相手の身分証明書が有効 (本人であるか・有効期限など) なものか、確認する。
	<li>互いの身分証明書を見せる
	<li>問題なければOKです（証明書や印刷物に不備がある場合は交換を中止してください）
	</ol>
</dd>
<dt><a href="http://www.ospn.jp/osc2010-fall/modules/eguide/event.php?eid=9"><strong>でびあん　らんだむとぴっくす</strong></a></dt>
<dd>講師：やまねひでき (Debian JP Project)　担当：東京エリアDebian勉強会</dd>
<dd>対象：Debian の動向に興味のある方
<dd>開始：14時00分〜</dd>
<dd>会場：507</dd>
<dd>内容：
<p>ゆるーい感じで、ここ最近のDebian界隈の話題を取り上げ簡単に説明をします。</p>
	<ul>
	<li>Squeeze frozen --- リリース時期はいつ？
	<li>Debconf、そして miniconf
	<li>教育、GIS、科学系向けDebian --- Debian Pure Blendsの紹介
	<li>celebrates Debian's 17th birthday!　など
	</ul></dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」</li>
  <li>Debian 稼働マシン (予定)</li>
  <li>有志作成の Debian グッズ (Tシャツ／でびあんぶれら他 予定)</li></ol>
</dd>

<p>
この件に関するお問い合わせは Debian 勉強会 担当者：やまねひでき (henrich&#64;debian.or.jp)
までお願いいたします。</p>
