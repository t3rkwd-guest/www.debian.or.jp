第77回 東京エリア Debian 勉強会のお知らせ

<p>
Debian 勉強会とは、Debian の開発者になれることをひそかに夢見るユーザたちと、ある時にはそれを優しく手助けをし、またある時
には厳しく叱咤激励する Debian 開発者らが Face to Face で Debian のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです (Debian をまだ使ったことが無いが興味があるの
で…とか、かなり遠い所から来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。興味と時間があった方、是非御参加下さい。（また、勉強会で話をしてみたい
という方も随時募集しています）。</p>


<dl>
<dt>開催日時・参加費用</dt>
<dd>
<ul>
   <li>2011年06月18日（土）14:00 〜 17:00</li>
   <li>費用：500円（資料印刷代）</li>
</ul>

<dt>会場</dt>
<dd>
 <a href="http://nyc.niye.go.jp/facilities/d7.html">国立オリンピック記念青少年総合センター</a><br>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>DPN trivia quiz</strong><br>
Debian 関連のニュースにおいついていますか? 簡単なクイズでDebian常識テストしちゃいます。</li>

  <li><strong>Debian JP 定例会議処理系にXSLTを使ってみた</strong> (担当: 上川純一)<br>
Debian JP 定例会議で使われているツールにXSLTを使ってみました。その内容を紹介します。
</li>

  <li><strong>Debian で sphinx と doxygen を使う</strong> (担当: 前田耕平)<br>
Debian で sphinx と doxygen を扱う方法を紹介します。</li>

  <li><strong>OSC仙台報告</strong> (担当: 吉田＠板橋)<br>
OSC仙台に参加したのでその報告を行います。</li>

  </ol>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2011-06.html">勉強会のページ</a>を参照して、<strong>06/16 までに Debian 
勉強会予約システムへの登録をしてください。
その際、事前課題がありますのでご回答をお願いします。</strong></dd>


<dt>事前課題</dt>
<dd>
最近自分がDebianでやったことや興味のあることを教えてください。</dd>
<dd>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します (Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、Debianユーザ用公開メーリングリスト (debian-users@debian.or.jp) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川 (dancer@{debian.org} ) までお願いいたします。</p>

