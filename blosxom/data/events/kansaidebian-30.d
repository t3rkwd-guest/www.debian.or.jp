第30回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20091227">第30回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2009 年 12 月 27 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 302会議室</a>
(定員：45名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  <li>事前準備について： <a href="http://www.openstreetmap.org/">OpenStreetMap のサイト</a>にて、
お住まい近辺の地図がどの様な形かを見ておいてください。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「あなたに 5 分あげます」</strong> (担当：参加者全員)<br>
5 分間で自己紹介をしてください。使い方は自由です。
普通にしゃべるもよし、ネタをしゃべるもよし（プロジェクタも使えます）。</li>

  <li><strong>「Debian を使って愉しむ Open Street Map 入門」 </strong> (担当：たなかとしひさ)<br>
Open Street Mapは道路地図などの地理情報データを誰でも利用できるよう、
フリーの地理情報データを作成することを目的としたプロジェクトです。<br>
今回の勉強会では、Debian System の応用として、Debian を使って Open Street Map 
への参加の方法、地図の作図、閲覧について簡単に入門と言う形でお話させて頂きます。</li>

  <li><strong>「ハンドメイド GPS ロガー」</strong><br>
Debian システムの応用例として、Debian lenny をインストールしたネットブックと 
GPS レシーバを使った GPS ロガーの紹介をします。<br>

SSD 搭載のネットブックを使えば車の振動を気にせずに、車に載せてログを取ることができます。<br>

そして、GPS レシーバから出力されるデータをファイルとして保存、
ログデータを使ってリアルタイムに緯度経度を表示、
地図上に位置を示すカーソルを表示などが可能です。
</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>12 月 25 日 (金) 24:00 までに</strong>
<a href="http://cotocoto.jp/event/37105">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (<a href="http://r.gnavi.co.jp/k029418/map1.htm">志な乃亭 野田阪神店</a>にて、
会費 4,000 円程度を予定) に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷 悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
