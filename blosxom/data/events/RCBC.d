RC バグを潰して GuruPlug 他商品をゲットしよう！

<p>
7/25から8/7までに Debian の RC バグをつぶした人に <a href="http://www.globalscaletechnologies.com/t-guruplugdetails.aspx">GuruPlug</a> などの商品が当たるというイベントが開かれています。在宅からの参戦でもwelcomeです。参加方法など、詳しくは<a href="http://upsilon.cc/~zack/blog/posts/2010/07/RCBC_-_release_critical_bugs_contest/">Debian Project Leaderのblog</a>を参照してください。</p>


