第 148 回 関西 Debian 勉強会 - Debian 10 Buster Release Party in 関西

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20190707">第148回 関西 Debian 勉強会 Debian 10 Buster Release Party in 関西</a>」
のお知らせです。
関西Debian勉強会は、関西エリアでDebianのさまざまなトピック（新しいパッケージ、Debian特有の機能や仕組み、
Debian界隈で起こった出来事などなど）について話し合う勉強会です。
</p>

<p>
約二年に渡り開発が続けれられてきた、Debian 10 Buster のリリースが 2019 年 7 月 6 日と発表されました。 一緒にリリースをお祝いしましょう。
</p>

<p>
パーティー形式で勧めます。料理や飲み物の持ち込みを歓迎いたします。
</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2019 年 7 月 7 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.enokojima-art.jp/">江之子島文化芸術創造センター</a>
(定員：35名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong></li>

  <li><strong>リリースノート読み</strong></li>

  <li><strong>live update</strong></li>

  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.connpass.com/event/135926/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。
</p>
