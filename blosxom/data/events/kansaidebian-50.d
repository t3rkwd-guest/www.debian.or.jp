第50回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20110828">第50回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2011 年 8 月 28 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.openstreetmap.org/?mlat=34.986855&mlon=135.753908&zoom=18&layers=M">オムロン京都センタービル啓真館</a>
(定員：30名)</li>
    <li>会場は施錠されています。参加者は 13:20 に<a href="http://www.openstreetmap.org/?mlat=34.986747&mlon=135.753801&zoom=18&layers=M">会場西側の入口</a>にお集り下さい。</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <dl>
      <dt>Debian GNU/Linux unstable (sid) が使える環境を用意しておいて下さい。 普段お使いの環境が sid の場合には特に事前課題はありません。普段使いの環境が sid 以外の場合には、以下の方法が考えられます。</dt>
      <dd>
        <ol>
          <li>VirtualBox などの仮想マシン上に sid を install する</li>
          <li>(c)debootstrap + schroot で sid 環境へ切り替えられるようにしておく。過去の勉強会資料、例えば「<a href="https://tokyodebian-team.pages.debian.net/html2008/debianmeetingresume200804-kansaise5.html">chrootから始めるsid</a>」や「<a href="https://tokyodebian-team.pages.debian.net/html2009/debianmeetingresume200904-kansaise3.html">Debianを新規にinstallしてから常用環境にするまで</a>」などが参考になるでしょう</li>
          <li>squeeze/wheezy を sid に upgrade する</li>
        </ol>
      </dd>
      </dl>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「モダンな Debian パッケージ作成入門」</strong> (担当:佐々木洋平)<br>
Debian パッケージに関して, debhelper(>= 7.0.50~), soruce format 3.0(quilt), DEP5(Machine-readable debian/copyright), といった最近の話題に触れつつ, 「モダン」なパッケージ作成方法についてハンズオン形式で紹介します. 既存のパッケージの rebuild から始めて, 幾つかのパッケージを最初から作成するところまでを実習する予定です.<br>
※会場にはネットワーク環境がありません. 運営側で幾つか WiMAX 回線を提供しますが, 自前のネットワーク環境をお持ちの方は, 当日ご協力下さいますと幸いです.</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2011年 8 月 26 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=9dc708b53f616d45f6ee403d125f99fabc9ba0ba">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
