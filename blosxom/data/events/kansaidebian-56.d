第56回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20120226">第56回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 2 月 26 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター302号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
      <li>chroot について知っていますか。
        <ol>
        <li>知らない人、chroot について調べたことを記入してください。<br>
        今なら、Software Design 2012年2月号 pp72 をお勧めします。</li> 
        <li>知っている人、PAM とオートマウンタの設定を触ったことはありますか?<br>
        設定を触ったことがあるかたはその内容も記入してください。</li>
        </ol>
      </li>
      <li>DebianPolicy の<a href="http://www.debian.org/doc/debian-policy/ch-relationships.html">7章</a>に目を通しておいてください。</li>
      </ol>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「Autofs と pam_chroot で作るマルチユーザー環境」</strong>(担当：久保)<br>
PC を持参される方は次のパッケージをインストールしてきて下さい。<br>
  libpam-chroot, autofs5, openssh-server
  </li>

  <li><strong>「emacs24で問題なく使える t-code.deb を作った話」</strong>(担当：西田)</li>
  <li><strong>月刊 Debian Policy</strong>(担当：倉敷)<br>
  「パッケージの依存関係についてのルール」
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 2 月 26 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=3ce8ab98fc505fc3758be903ee37b2a1c3d4f92c">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
