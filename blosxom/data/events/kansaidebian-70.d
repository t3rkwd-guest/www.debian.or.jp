第70回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130324">第70回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 3 月 24 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区民センター 梅</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      どんな拡張機能があれば便利か、考えてみて、教えてください。
      </li>

      <li>
      wheezy (sid) で GNOME3 環境を用意してきてください。
      </li>

      <li>
      「<a href="http://fr2012.mini.debconf.org/slides/LargeGnomeDeployment.pdf">Large deployment of GNOME from the administrator perspective</a>」を読んで気になる点、わからない点を教えてください。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>


  <li><strong>UbuntuとGNOME Shellと私</strong>(担当：あわしろいくや)<br>
  GNOME Shellを愛用している演者が、GNOME Shellの魅力とUbuntuとGNOMEの微妙な関係についてまとめます。<br>
  もちろんDebianで使う場合にも、きっと役に立つはずです。
  </li>

  <li><strong>管理者視点からのGNOMEの大規模な配置</strong><br>
  Mini Debconf Paris 2012 で行なわれた Josselin Mouette さんの「Large deployment of GNOME from the administrator perspective」プレゼン資料を元に GNOME を読みといてみます。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2013年 3 月 23 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=dbd3460eff310544a4c3b125971c0d7c4dac7c24">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
