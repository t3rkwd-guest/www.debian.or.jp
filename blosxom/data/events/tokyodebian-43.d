第43回 Debian 勉強会のお知らせ

<p>
今月の Debian 勉強会 は IRC 越しに <a href="http://debconf8.debconf.org">Debconf8</a> (Debian Conference) 会場（アルゼンチン マルデルプラタ） とやりとりしつつの開催となります。
<strong>時間と場所が通常とは違っていますので十分にご注意ください。</strong></p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は基本的に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年8月11日(月) 22:00-24:00 (予定)</li>
  <li>会場：マルデルプラタ Debconf8 会場、もしくは IRC irc.debian.or.jp #debconf8 チャンネル(文字コード: iso-2022-jp)</li></ul>
詳細については
<a href=
"https://tokyodebian-team.pages.debian.net/2008-08.html">勉強会の Web ページ</a>を参照ください。
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>22:00〜 Debconf 参加者の今回の意気込み</strong>（担当：上川純一）</li>
  <li><strong>22:30〜 Debconf 参加者への Debconf 質問コーナー</strong></li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は <a href="http://www.debian.or.jp/community/irc.html">IRC のページ</a>を参考にして、<strong>#debconf8 チャンネル</strong>へ対象時間に接続ください。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会担当者：岩松信洋 (iwamatsu &#64; {debian.or.jp} )
までお願いいたします。</p>
