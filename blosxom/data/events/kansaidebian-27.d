第27回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20090927">第27回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2009 年 9 月 27 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.krp.co.jp/access/room.html">京都リサーチパーク 1 号館 4F A 会議室</a> (<a herf="http://www.krp.co.jp/access/index.html">交通・アクセス</a>)
(定員：56名)</li>
  <li>費用：500円 (印刷代などの諸費用のため)</li>
  <li>注意：行き方などのお問い合わせは、迷惑がかからないようにリサーチパーク様へは行わず、このページの下にある本イベント連絡先担当までお願いします。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> (担当：のがた)<br>
勉強会の簡単な説明をするほか、今回の会場をご提供頂いた京都リサーチパーク様よりご挨拶を頂きます。</li>

  <li><strong>自己紹介</strong> (担当：全員)<br></li>

  <li><strong>GUI がついてかっこよくなった reportbug を使ってみよう</strong> (担当：のがた)<br>
reportbug を使ったことがありますか?「Debian のバグ報告はメールベースで、よくわかんないから放置!」という人もいるかもしれません。<br>
ちょっとまってください。<br>
reportbug を使うとウィザード形式で簡単にバグ報告ができるのです。以前の reportbug が文字ばかりで挫折した人も、GTK の GUI がついて格好良くなった reportbug を使ってみませんか?</li>

  <li><strong>debian mentors ってご存知ですか？</strong> (担当：ささき)<br>
mentors.debian.net ってご存知ですか? いわゆる Debian Developer(=DD) でない人が、DD に師(=mentor)になってもらって、作成したパッケージを審査してもらい、公式パッケージとして upload してもらう制度です。Debian Developer の第一歩として、今回はパッケージを作成したその後の第一歩についてお話します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>9 月 25 日 (金) 24:00 までに</strong>
<a href="http://cotocoto.jp/event/31676">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
京都駅周辺で予定している懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：のがたじゅん (nogajun&#64;{debian.or.jp} ) までお願いいたします。</p>
