第60回 東京エリア Debian 勉強会「Bug Squashing Party」のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今年も Debian 勉強会が開かれます！</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<p>
2010年3月に Debian 次期安定版である "Squeeze" がリリースフリーズに入る予定です。
今回は、その Squeeze リリースの障害となるバグを修正するための
<strong>「Bug Squashing Party」</strong>を開催します。<br>
バグを潰しまくるのはもちろん、パッケージメンテナンス、ドキュメント/po 翻訳などをやりたい方も参加できます。
Debian / オープンソースの開発者が集まりますので、他のディストリビューション開発者で情報交換されたい方や、
GPGサインをやりたい方もどうぞご参加ください。</p>
<p>
参考情報: <a href="http://people.debian.org/~vorlon/rc-bugsquashing.html">Bug Squashing とは</a>／
<a href="http://wiki.debian.org/BSP/BeginnersHOWTO">バグの潰し方</a></p>


<dl>
<dt>開催日時・会場</dt>
<dd>
   <li>日時：2010年1月23日(土曜日) 9:00-21:00前後</li>
   <li>会場：<a href="http://www.u-tokyo.ac.jp/campusmap/cam02_03_02_j.html">東京大学先端科学技術研究センター3号館</a> M2階 （<a href="http://www.u-tokyo.ac.jp/campusmap/map02_02_j.html">東京大学駒場第二キャンパス　東京都目黒区駒場4-6-1</a>)<br>
なお、会場は<strong>飲食可能</strong>です。お菓子や飲み物の持参／差し入れを歓迎します。<br>
会場は無線メインになる予定ですが、場合によっては e-mobile を所有している方に回線共有のご協力をお願いするかもしれません。</li>
   <li>費用：なし</li>
   </ul>
</dd>

<dt>作業予定対象</dt>
<dd>
  <ul>
  <li><a href="http://bugs.debian.org/release-critical/">RC バグリスト</a> (<a href="http://bts.turmzimmer.net/details.php?bydist=both&sortby=packages&ignhinted=on&ignclaimed=on&ignnew=on&ignpending=on&ignbritney=on&ignotherfixed=on&new=7&refresh=1800">バグリスト詳細版</a>)</li>
  <li><a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?dist=unstable;tag=ipv6">IPv6 バグ一覧</a></li>
  <li><a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=piuparts;users=debian-qa@lists.debian.org&amp;archive=both">piuparts バグ一覧</a></li>
  <li>メンテナンスしているパッケージの squeeze に向けての修正</li>
  <li>po / ドキュメントの翻訳</li>
  </ul>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
<ul>
<li><a href="http://atnd.org/events/2481">事前登録</a>を行ってください。</li>
<li><a href="https://tokyodebian-team.pages.debian.net/2010-01.html">Debian 勉強会の Web ページ</a> を参照して、1月21日(木)までに事前課題の回答を勉強会用メーリングリストに送付してください。詳細については<a href="https://tokyodebian-team.pages.debian.net/prework-update.html">課題提出方法</a>を参考にしてください。</li>
<li>作業用のマシン環境、デバッグ用の資料、機材は自身で持参ください。マシン環境は、RC バグ修正のために unstable 環境であることが望ましいです（難しい場合は仮想マシン環境などを用意ください）。</li>
<li>ネットワークケーブル、ハブ</li>
<li>電源タップ、延長ケーブル</li>
<li>お菓子や飲み物</li>
<li>GPGキーサインしたい人：フィンガープリントを印刷したもの（名刺などが望ましい）と、公的な証明書（パスポート、運転免許証）</li>
<li>直接参加できない方は、IRC チャンネルでご参加ください (サーバ: osdn.debian.or.jp、チャンネル名: #debianjp )</li>
</ul></dd>

</dl>
<p>
この件に関するお問い合わせは コーディネイター：岩松 信洋 (iwamatsu&#64;debian.org) 及び 荒木 靖宏 (ar&#64;debian.org) 
までお願いいたします。</p>
