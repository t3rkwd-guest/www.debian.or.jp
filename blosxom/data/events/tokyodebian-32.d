第32回 Debian 勉強会のお知らせ

<p>
Debian 勉強会は、メールや web だけでは語り切れない Debian 
に関わる話題について浅いところから深いところまであらゆることを 
Face to Face で 情報交換するイベントです。</p>
<p>
参加者は関東近郊を中心とした国籍・性別不問の Debian ユーザ／ユーザ予備群で、
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ます。<br>
普段は聞けないような様々な情報を得るチャンスです。是非御参加下さい。<br>
（また、勉強会で話をしてみたいという方も随時募集しています）</p>
<p>
今月はいつもの会場とは場所を移して新宿近郊の国立オリンピック記念青少年総合センターでの開催です。<br>
内容は Debian のデフォルトメールサーバであるのにも関わらず不遇な扱いを受けている Exim について、
再度見直しをしてみようというお題を中心に apt-get 以外にもある「apt-xxx」の紹介などを行います。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年9月15日(土) 18:00-21:00</li>
  <li>会場：<a href=
"http://nyc.niye.go.jp/facilities/d7.html">国立オリンピック記念青少年総合センター センター棟 研修室 セ-108</a>
(小田急線 参宮橋駅下車 徒歩7分)<br>
<iframe width="640" height="480" frameborder="no" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?f=q&hl=ja&geocode=&time=&date=&ttype=&num=10&ie=UTF8&om=1&s=AARTsJqGrqSWVb_axyemF1zwHXjuSM6-eg&msa=0&msid=109811903404931563048.0004391daee4fddf76a7f&ll=35.676193,139.693801&spn=0.033467,0.054932&z=14&output=embed"></iframe><br/><a href="http://maps.google.co.jp/maps/ms?f=q&hl=ja&geocode=&time=&date=&ttype=&num=10&ie=UTF8&om=1&msa=0&msid=109811903404931563048.0004391daee4fddf76a7f&ll=35.676193,139.693801&spn=0.033467,0.054932&z=14&source=embed" style="color:#0000FF;text-align:left;font-size:small">拡大地図を表示</a></li>
  <li>費用：資料代・会場費として 500 円を予定</li></ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>あまり知られていないかもしれない apt-xxx</strong>（担当：岩松 信洋）<br>
	
	apt は Debian ユーザーにとってなくてはならない存在。<br>
        しかし、あなたは apt-get だけで満足していませんか？普段は日の目を見ないapt-xxxを紹介します。

  <li><strong>Exim 再発見</strong>（担当：小室 文）<br>

	MTA の一つ、Exim は Debian の Default MTA と呼ばれながらも、今や Postfix
        の人気にすっかり影を潜めてしまいました。しかし Default MTA と呼ばれたのには何か理由があるはず……。<br>
        Exim の歴史、利点・不利点、導入方法等についてご紹介します。

  <li><strong>ディスカッション</strong>（担当：岩松 信洋）<br>

	事前課題でいただいた資料をベースに参加者でディスカッションします。</li></ol></dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。
期限は 9 月 13 日 (木) 中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-09.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者代理：岩松 信洋 (iwamatsu&#64;debian.or.jp )
までお願いいたします。</p>

