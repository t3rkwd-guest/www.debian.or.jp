第 171 回 東京エリア Debian 勉強会 in オープンソースカンファレンス 2019 Tokyo/Spring

<p>
東京近辺にいらっしゃる皆様こんにちは。私たちは、<a href="https://www.ospn.jp/osc2019-spring/">オープンソースカンファレンス 2019 Tokyo/Spring</a> に参加します。
</p>

<p>
イベントではセミナー発表のほかブース展示も行います。
</p>

<p>
ブース展示ではDebianの紹介やあなたがちょっと知りたいことや疑問に思っていることにお答えします。「他のOSと比べてDebianはどこがいいのか」「Debianを使いたいけど何から始めればいいかわからない」「Debianでここがうまくいかないので教えてほしい」などの疑問がある方はぜひともブースまでいらしてください。
</p>

<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、普段は聞けないような様々な情報を得るチャンスです。
</p>

<p>
興味と時間がある方は是非イベントにご参加下さい。また、勉強会で話をしてみたいという方も随時募集しています。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2019年2月23日(土)<br>
  <li>会場：<a href="https://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 26号館 2F</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul>
</dd>

<dt>
<a href="https://www.ospn.jp/osc2019-spring/modules/eguide/event.php?eid=21"><strong>Debian Updates</strong></a>
</dt>
<dd>講師：杉本 典充（東京エリアDebian勉強会）担当：Debian JP Project / 東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：14時00分 〜</dd>
<dd>会場：26号館 1F 103教室</dd>
<dd>
<p>
2019年1月からフリーズを開始するDebian 10 (Buster)の話題を中心に最近のDebian Project 界隈のトピックを紹介します。
</p>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」展示</li>
  <li>Debian 稼働マシン 展示</li>
  <li>Debian インストールCD、ステッカー等の配布</li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：杉本 典充 (dictoss &#64; {debian.or.jp} ) までお願いいたします。
</p>
