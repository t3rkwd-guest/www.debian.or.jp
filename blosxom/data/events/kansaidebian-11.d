第11回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080323">第11回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 3 月 23 日 (日) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/minato.html">大阪 港区民センター</a>(<a href="http://www.osakacommunity.jp/minato/index.html">大阪・港区コミュニティ協会</a>)</li>
  <li>費用：500円</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro (関西 Debian 勉強会についての説明)</strong>　山下 尊也 氏<br>
       この勉強会の目的などを説明し、その後、前回の勉強会、
そして日本電子専門学校で行われた OSC Spring の話を行います。</li>

  <li><strong>バグレポートから参加するDebianパッケージ開発</strong>　木下 達也 氏<br>
Debianパッケージについての問題・要望をDebian開発者へ伝えることは、Debianの成長へと繋がります。<br>
Debianパッケージを使っていて問題を見つけた場合にどのようにすればよいのか、
パッケージ情報の調べ方、バグレポートの書き方、Debian開発者との協同などについてお話しします。</li>

  <li><strong>Debian 開発者のコーナーの歩き方</strong>　大浦 真 氏<br>
Debian プロジェクトの Web ページ内の<a href="http://www.debian.org/devel/">「Debian 開発者のコーナー」</a>には、
Debian の開発や改良を行っていくために必要な情報が集まっています。
ここには、Debian 開発者のみならず、Debian を深く使っていくために必要な情報がそろっています。
今回は、この「Debian 開発者のコーナー」にどのような情報が集まっているのか、紹介していきます。</li>

  <li><strong>GPG 最初の一歩</strong>　倉敷 悟 氏<br>
GPG というと、「ああ、なんか apt-get の時にゴチャゴチャ文句言ってくるアレか」
といった印象をお持ちの方もおられるのではないかと思います。
実は Debian には様々な形で GPG が組みこまれていて、ssh 等にならぶマストアイテムといっても過言ではありません。
このセッションでは、GPG の用途と役割について簡単にご紹介した後、典型的な使用方法についてデモを行います。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>3月 20日(木) 正午までに</strong>
<a href="http://cotocoto.jp/event/22253">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>
