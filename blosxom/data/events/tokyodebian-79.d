第79回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今年も Debian 勉強会が開かれます！
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2011年8月20日(土曜日) 18:00-21:00</li>
   <li>費用：500円</li>
   <li>会場：<a href="http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪 第一教室</a></li>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>DPN trivia quiz</strong> (担当:岩松 信洋)<br>
Debian 関連のニュースにおいついていますか? 簡単なクイズでDebian常識テストしちゃいます。</li>
  <li><strong>はじめてのapt</strong> (担当: 山田 泰資)<br>
日常運用からソースビルドまで説明します。</li>
  <li><strong>パッケージ作成AtoZ〜作りながらのQ&amp;A</strong> (担当: 山本 浩之、山田 泰資、他)<br>
Debianでocamlを使う方法について一人勉強会をやってみました。</li>
  <li><strong>Sponsor Upload</strong> (担当: 岩松 信洋)<br>
Debian パッケージのアップロード方法の一つである Sponsor Upload について説明します。</li>
  <li><strong>Debconf 11 レポート</strong> (担当: 岩松 信洋)<br>
先月末に行われた Debconf 11 のレポートを行います。</li>
  </ol>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2011-08.html">勉強会のページ</a>を参照して、<strong>08/18 までに Debian 
勉強会予約システムへの登録をしてください。
その際、事前課題がありますのでご回答をお願いします。</strong></dd>


<dt>事前課題</dt>
<dd>
Debianの各種ツール（aptとかdpkgなど）について、
<ul>
<li>このツールの解説が欲しい！
<li>こんなツールありませんか？（あったらいいのに）</ul>
などと思っていることを挙げて下さい。</dd>
<dd>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します (Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、Debianユーザ用公開メーリングリスト (debian-users@debian.or.jp) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川 (dancer@{debian.org} ) までお願いいたします。</p>
