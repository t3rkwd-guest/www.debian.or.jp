第38回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20100822">第38回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2010 年 8 月 22 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">大阪福島区民センター 303会議室</a>
(定員：22名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  <li>事前課題1: Debian が動作する CPU、ターゲット機器について予習をしておいて下さい。 </li>
  <li>事前課題2: それらのうち、これまでに使用したことがあるアーキテクチャを教えてください。(例：i386、amd64、arm、powerpcなど) </li>
  <li>事前課題3: 組込み機器の OS として Debian を使う場合のメリット／デメリットについて、思う所を書いてください。</li> 
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>emdebian について　−関西 Debian 勉強会参加者中間報告−</strong> (担当：たなかとしひさ)<br>
私は２０１０年年初の関西 Debian 勉強会で、今年の抱負として emdebian に ついて勉強する事を目標としました。 ２０１０年も半ばを過ぎましたので、関西 Debian 勉強会参加者の中間報告として、 emdebian について勉強した事をお話出来ればと考えています。 また、組込み機器と Debian、組込み機器と Linux の現状と今後について、議論 させて頂けたらと考えています。 </li>

  <li><strong>Debian GNU/kFreeBSDで暮らせる環境を構築してみる</strong> (担当：杉本典充)<br>
現在、FreeBSDカーネルを使ってDebianシステムを構築した 「Debian GNU/kFreeBSD」の開発が行われています。 squeezeでリリース予定になっているDebian GNU/kFreeBSDで 暮らせる環境を構築する過程とDebian GNU/Linuxとの違いで つまずいた部分等をお話します。 </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>8 月 20 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=7ca5351f68dc1866bd973757a5e7ff4e2ad58dc6">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷 悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
