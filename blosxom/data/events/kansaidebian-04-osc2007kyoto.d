第4回 関西 Debian 勉強会 in オープンソースカンファレンス 2007 Kyoto

<p>
関西 Debian 勉強会はこれから定期的に顔をつき合わせて、Debian GNU/Linux
のさまざまなトピック(新しいパッケージ、Debian 特有の機能の仕組、Debian
界隈で起こった出来事、などなど）について話し合う会です。今月は 
<a href="http://www.ospn.jp/osc2007-kansai/">オープンソースカンファレンス 2007 Kyoto</a> にお邪魔して、
セミナーやブースでの展示と説明などを予定しています。
</p>

<p>
参加者には Debian ユーザのみならず、開発の最前線に立つ Debian の公式開発
者や開発者予備軍の方もおりますので、普段は聞けないようなさまざまな情報を
得るチャンスです。興味を持たれた方は気軽にご参加ください (また、勉強会で
話をしてみたい、協力したいという方も随時募集しています)。
</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年7月21日(土)<br>
        セミナーは 13:00-13:45<br>
        ミニセミナーは随時開催予定 (ブースは10:00-17:00を予定しています)
  </li>
  <li>会場：京都コンピュータ学院 京都駅前校 (JR京都駅八条口より徒歩7分)</li>
  <li>主催：オープンソースカンファレンス実行委員会</li>
  <li>共催：京都コンピュータ学院<br>
        京都情報大学院大学</li>
  <li>費用：セミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>
<dt>セミナー (教室E)</dt>
<dd>
  <ol>
  <li><strong>関西 Debian 勉強会とは</strong></li>
  <li><strong>Debian.org / Debian JP / 関西 Debian 勉強会の関係</strong></li>
  <li><strong>Debconf 7 ミニ報告 + Debconf 日本開催について。</strong></li>
  </ol>
(時間の関係上セミナー内容が変わる可能性があります)
</dd>
<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>有志作成 Debian グッズ（Tシャツ・ステッカー）</strong></li>
  <li><strong>チラシ・インストールDVD</strong></li>
  <li><strong>Debian 稼働マシン</strong></li>
  <li><strong>Debian 掲示板</strong> − みなさまが抱えている Debian に対する意見などを書いてください</li>
  </ol>
</dd>
<dt>ミニセミナー (ブースにて開催)</dt>
<dd>
  <ol>
  <li><strong>Debian 質問所</strong> − セミナー内容や日頃の Debian に関するあれこれに回答します</li>
  <li><strong>Say goodbye to Microsoft. Now.</strong></li>
  <li><strong>その他未定</strong></li>
  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
セミナー参加希望者は <a href="http://www.ospn.jp/osc2007-kansai/">http://www.ospn.jp/osc2007-kansai/</a> から事前登録を行って下さい。
ブース展示・ミニセミナーその他については特に登録などはありません。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会主催者：矢吹幸治 (yabuki&#64;{debian.or.jp,netfort.gr.jp} ) までお願いいたします。</p>
