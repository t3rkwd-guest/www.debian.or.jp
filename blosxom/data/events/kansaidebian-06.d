第6回 関西 Debian 勉強会のお知らせ

<p>
9月の「<a href="http://wiki.debian.org/KansaiDebianMeeting20070915">第6回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年9月15日<strike>(日)</strike>(土) 13:15〜17:00 (13:00 より受付。18:00〜21:00 にて懇親会予定)</li>
  <li>会場：<a href="http://www.himeji-du.ac.jp/access/index.html">姫路独協大学 本部棟西館</a><br>
（会場へのアクセスについて、<a href="http://lilo.linux.or.jp/wiki/lms/20070616">LILO Monthly Seminar
の資料</a>を参考にしてください）</li>
  <li>費用：無料 (姫路独協大学さんの好意により部屋代・印刷代など不要のため)</li>
  </ul>
</dd>
<dt>内容（順不同）</dt>
<dd>
  <ol>
  <li><strong>Debian パッケージの作り方 -- ライブラリ編</strong>　大浦 真 氏<br>
      Debian のライブラリのパッケージは他のパッケージとは異なり、
      ライブラリの本体と開発用のファイルが別のパッケージになっています。
      今回は、ライブラリを作る手助けをする libtool の説明も含めて、
      ライブラリパッケージの作り方を紹介します。</li>

  <li><strong>Debian で翻訳をはじめてみよう</strong>　倉敷 悟氏<br>
      Debian で翻訳の対象となる領域、Debian 上で翻訳する時の環境、実際に作業を進める様子などをご紹介します。<br>
      参加申込でご希望があれば、その部分の説明やデモを多めにしようかと考えてます。
      ご紹介する内容は個人的な経験ベースなので、猛者のツッコミ歓迎です。</li>

  <li><strong>DebTorrent に触ってみた</strong>　山下 尊也氏<br>
      P2P である BitTorrent を使ったパッケージ取得システムである DebTorrent が、
      公式パッケージに追加されたので DebTorrent を触った感想などについてお話したいと思います。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<a href="http://wiki.debian.org/KansaiDebianMeeting20070915">第6回 関西 Debian 勉強会</a>ページの<strong>事前課題を確認の上</strong>、
<a href="http://cotocoto.jp/event/1756">京都.cotocoto</a>を参照して、
<strong>9月12日まで</strong>に事前登録をしてください
（懇親会に参加する場合は、コメントに「宴会参加」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>
