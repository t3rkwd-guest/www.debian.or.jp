第 93 回 関西 Debian 勉強会 Debian Bug Squashing Party のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20150117">第93回 関西 Debian 勉強会 Debian Bug Squashing Party</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2015 年 1 月 17 日 (土) 9:00 - 21:00</li>
    <li>会場：<a href="http://kyoto123.jimdo.com/%E9%81%8A%E5%AD%90%E5%BA%B5%E4%BC%9A%E5%A0%B4/">京町家　遊子庵</a>
(定員：20名)</li>
    <li>費用：500円 (部屋代のため)</li>
    <li>事前準備: <br>

      <ul>

      <li>
      unstable が動作しているマシンおよび開発環境<br>
        <ul>
          <li>pbuilder/cowbuilderの整備</li>
          <li>修正するソフトウェアのソースコード等</li>
        </ul>
      </li>

      <li>
      デバッグ用の資料、機材
      </li>

      <li>
      ネットワークケーブル、電源タップ、など
      </li>

      <li>
      パッケージ作成、バグ報告の仕方<br>
      過去の資料などを参照して予習しておいてください。
      </li>

      </ul>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ul>

  <li><strong>Bug Squashing Party</strong><br>
    <ul>
    <li>RC バグ潰し</li>
    <li>パッケージ、ソフトウェアメンテナンス、ドキュメント/po翻訳などのBug Squashing</li>
    <li>GPGサイン</li>
    <li>などなど</li>
    </ul>
  </li>

  </ul>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<a href="http://debianjp.doorkeeper.jp/events/19568">Doorkeeper</a>を参照して、
事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
