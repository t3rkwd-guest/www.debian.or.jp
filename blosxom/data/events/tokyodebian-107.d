第 107 回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。12月も 東京エリア Debian 勉強会が開かれます！
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、 etc ）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2013 年 12 月 21 日 ( 土曜日 ) 15:00-18:00</li>
   <li>費用：500 円</li>
   <li>会場：<a href="http://www.jp.square-enix.com/company/ja/access/index2.html">株式会社 スクウェア・エニックス セミナールーム</a></li>
　 <li>当日について：一旦15:00にビル内で集合して中に入ります。詳しくは<a href="https://tokyodebian-team.pages.debian.net/2013-12.html">勉強会のページ</a>を参照してください。</li>
   </ul>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>2013年の振り返り (野島)</strong><br>
	勉強会のアンケート結果について語ります</li>
  <li><strong>Debian勉強会2014年度計画 (野島)</strong><br>
	Debian勉強会2014年度計画を皆さんとたてます</li>
  <li><strong>debian GNU/Hurd (野島)</strong><br>
	debian GNU/Hurd 2013について語ります</li>
  </ol>
</dd>

<dt>事前課題／参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2013-12.html">勉強会のページ</a>を参照して、Debian 勉強会予約システムへの登録をしてください。</dd>
<dd>
<br>
事前課題は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：野島 貴英 (nozzy@{debian.or.jp} ) までお願いいたします。</p>
