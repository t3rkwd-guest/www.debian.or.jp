第46回 Debian 勉強会 (事前準備＋ハンズオン形式) のお知らせ 

<p>
今月も Debian 勉強会が開かれます！久々に荻窪での開催、そして内容はちょっとコアに 
LaTeX を取り上げます。そして、<strong>今回は<a 
href="http://lists.debian.or.jp/debian-users/200810/msg00010.html">事前準備</a>を実施した上でのハンズオン</strong>を予定しています。</p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年11月15日(土) 18:00-21:00</li>
  <li>費用：資料代・会場費として 500 円</li>
  <li>会場：<a 
href="http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)</li></ul>

</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debian unstable (sid) + LaTeX 関連パッケージのインストール</strong> (担当: 各自 :-)<br>
  今回の<a href="http://lists.debian.or.jp/debian-users/200810/msg00010.html">事前課題</a>です。直前に慌てないためにも早めの準備がお勧めです。
事前課題に関する質問がある場合は <a href="http://www.debian.or.jp/community/ml/openml.html#usersML">debian-users 
メーリングリスト</a>にてお願いします。</li>
  <li><strong>「その場で勉強会資料を作成しちゃえ」 Debian を使った LaTeX 原稿作成</strong>（担当：上川 純一）<br>
Debian GNU/Linux で文章を作成しているときはどうしていますか? 今回は Debian 勉強会で使っている LaTeX というツールを紹介します。みんなで Debian 勉強会資料を作成できるレベルまで到達しましょう。<br>
今セッションは emacs を利用した実践的な実習形式で行います。スムースに進行するため、あらかじめ事前課題を完了した状態でのノートパソコンを持参ください。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 11 月 13 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href="https://tokyodebian-team.pages.debian.net/2008-11.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
<dt>勉強会中継について</dt>
<dd>
今回は <a href="http://www.ustream.tv/">ustream</a> を使って勉強会の中継および録画を行う予定です。参加者の方で顔出し等ができない方は、主催者まで連絡をください。また、細かい情報等はこのページを参照してください。
  <ul>
  <li>チャンネル名: <a href="http://www.ustream.tv/channel/tskyo-debian-meeting-200811">tokyo-debian-meeting-200811</a></li>
  <li>中継時間: 18:10 - 20:59
  <li>注意: ネットワークの状態により、中継ができない場合があります
  </ul>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
