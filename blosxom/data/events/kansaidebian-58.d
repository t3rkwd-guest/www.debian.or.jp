第58回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20120422">第58回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 4 月 22 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
        <li>Debian Policy の第2章を読んで、アーカイブエリアのmain、contrib、non-freeの違いについて説明してください。</li>
        <li>次のうち、著作権の対象になりそうなものを選んでみよう。<br>
            パスワード、Linux のカーネルイメージ、Linux カーネルのソースコード、Emacs Lisp の言語仕様、動画の圧縮方式
        </li>
      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>月刊 Debian Policy 「Debian アーカイブ」</strong>(担当：河田)</li>

  <li><strong>「フリーソフトウェアと戯れるための著作権入門」</strong>(担当：久保)</li>

  <li><strong>スクリプト言語 Konoha の Debianパッケージ化</strong>(担当：酒井)<br>
  upstream との調整経緯、debian/rules 修正とパッケージ分割について
  </li>

  <li><strong>月刊 t-code パッケージ修正</strong>(担当：西田)</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 4 月 20 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=2184bcd94cd81eaf9e5ff4e35ee705ee3aa69ac5">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
