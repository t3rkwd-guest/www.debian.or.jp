第34回 Debian 勉強会のお知らせ

<p>
今月も Debian 勉強会が開かれます！ Debian 勉強会とは、Face to Face で
Debian GNU/Linux のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について話し合うイベ
ントです。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザで
す (Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所か
ら来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者
予備軍の方も居ますので、普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい
（また、勉強会で話をしてみたいという方も随時募集しています）。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年11月17日(土) 18:00-21:00</li>
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)</li>
  <li>費用：資料代・会場費として 500 円を予定</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debian での Bluetooth 活用方法紹介</strong>（担当：野首 貴嗣）<br>
	
	最近気づいたら普及してしまっている Bluetooth、もはや使い方が分
	からないなんていってられません。Debian GNU/Linux で Bluetooth
	を活用する方法をご紹介します。</li>

  <li><strong>live-helper ネタ</strong>（担当：岩松 信洋）<br>

	Debian GNU/Linux の Live CD をつくるための仕組み、live-helper,
	実は使い方を知っていれば簡単に使えます。live-helper の使い方を紹
	介します。</li>

  <li><strong>イベント報告</strong>（担当：やまねひでき、上川 純一）<br>

  	OSC Tokyo/Fall の報告として、会場で ML350 に Debian をインストー
  	ルしてみたのでそれにかこつけてお話します。また、関西で開催され
  	た Kansai Open Forum の報告をします。</li>

  <li><strong>Tomoyo Linux Debian Package</strong>（担当：やまねひでき）<br>

	Tomoyo Linux の Debian Package を最近作成してみたので、その体験
	について紹介します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 11 月 15 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-11.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
