第41回 Debian 勉強会のお知らせ

<p>
6 月も Debian 勉強会が開かれます！
</p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年6月21日(土) 18:00-21:00</li>
  <li>費用：資料代・会場費として 500 円</li>
  <li>会場：<a href=
"http://nyc.niye.go.jp/">国立オリンピック記念青少年総合センター</a> <a href="http://nyc.niye.go.jp/facilities/d6-1.html">センター棟 研修室 セ-307</a>
(小田急線 参宮橋駅下車 徒歩7分)<br>
<iframe width="640" height="480" frameborder="no" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?f=q&hl=ja&geocode=&time=&date=&ttype=&num=10&ie=UTF8&om=1&s=AARTsJqGrqSWVb_axyemF1zwHXjuSM6-eg&msa=0&msid=109811903404931563048.0004391daee4fddf76a7f&ll=35.676193,139.693801&spn=0.033467,0.054932&z=14&output=embed"></iframe><br/><a href="http://maps.google.co.jp/maps/ms?f=q&hl=ja&geocode=&time=&date=&ttype=&num=10&ie=UTF8&om=1&msa=0&msid=109811903404931563048.0004391daee4fddf76a7f&ll=35.676193,139.693801&spn=0.033467,0.054932&z=14&source=embed" style="color:#0000FF;text-align:left;font-size:small">拡大地図を表示</a>
  </li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
      <li><strong>dpatch/debhelperで作成するパッケージについて</strong> （担当：小林儀匡）<br>
        Debian の Patch システムである dpatch と Debian Package 作成をサポートするプログラム群、 debhelper を
        使った Debian Package の作り方について紹介します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 6 月 19 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2008-06.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
