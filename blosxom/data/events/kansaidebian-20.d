第20回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20081214">第20回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 12 月 14 日 (日) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a href="http://www.lockon.co.jp/aboutus/map.html">株式会社ロックオン 大阪本社</a>
(定員：20名前後)</li>
  <li>集合場所: ブリーゼブリーゼ 1階 <a href="http://news.walkerplus.com/kansai/machi/200810/00000877/">ブリCH(ブリチャン)前</a></li>
  <li>費用：500円 (印刷代などの諸費用のため)</li>
  <li>注意：会場のあるビルのセキュリティの都合上、自由に出入りする事ができませんので、<strong>必ず13:30には集合してください</strong>。担当者の連絡先については登録締切り後お知らせいたします。事前に遅れることが分かっている場合は、cotocotoのコメント欄に遅れる旨と参加する時間を書いてください。</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第20回関西 Debian 勉強会の内容について話をします。</li>

  <li><strong>LaTexBeamer</strong> 大浦 真 氏<br>
Debian の入ったコンピュータでプレゼンテーションを行うためには、OpenOffice.org Impress などいくつかの方法があります。今回はLaTeX でプレゼンテーションを作るための LaTeX Beamer を紹介します。実際にプレゼンテーションのファイルを作りながら、使い方を説明していきます。</li>

  <li><strong>関西Debian勉強会資料作成について </strong><br>
K-OF 配布 DVD で予告していた「Debian 勉強会の資料を LaTeX で作成」を実施します。LaTeX は難しいというかた、みんなでやるから大丈夫!! 当日は、自分のノートPCと「関西Debian勉強会 in 関西オープンソース2008」で配布していた Live DVD を持参する、もしくは <a href="http://wiki.debian.org/KansaiDebianMeetingKOF2008#head-679832e5c7b08fac06df881fcfc3be15887978ae">こちらから ISOファイル（debian_live-binary-20081103220704.iso）をダウンロード</a>してDVDに保存して持って来て下さい。</li>

  <li><strong>K-OF まとめ</strong>　関西オープンソース参加者各位<br>
11月7〜8日に開かれた関西オープンソースの内容について、今後のイベント参加のためにまとめたいと思います。</li>
 
  <li><strong>来年の関西 Debian 勉強会について</strong><br>
来年度の関西 Debian 勉強会の内容を考えます。勉強会でして欲しいこと／したい事などの募集とディスカッションを行います。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>12 月 12 日 (金) 12:00 までに</strong>
<a href="http://cotocoto.jp/event/29005">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


