第 119 回 東京エリア Debian 勉強会 x 関東LibreOfficeオフ x Jessieインストーラテスト会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。OSC 2014 Tokyo/Fallの後にも 東京エリア Debian 勉強会は開かれます！
</p>

<p>
 東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とす
る熱いユー ザたちと、実際に Debian　Project にてすでに日夜活動している人
らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケー
ジ、Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>

<p>
 今回は、関東LibreOfficeさんとのコラボで勉強会を開きます。また、Debian開発初心者の方向け企画としてJessieインストーラテスト会（希望者のみ）をします。
</p>


<p>
今回Debian側として参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2014 年 10 月 25 日 ( 土曜日 ) 14:00-19:00</li>
   <li>費用：今回 0 円（ 会誌が欲しい方のみ：500 円とします )</li>
   <li>会場：<a href="http://www.jp.square-enix.com/company/ja/access/index2.html">株式会社 スクウェア・エニックス セミナールーム</a></li>
　 <li>当日について：一旦 14:00 にビル内で集合して中に入ります。また、Debian に関する作業時間を勉強会中に用意しています。詳しくは<a href="https://tokyodebian-team.pages.debian.net/2014-10-25.html">勉強会のページ</a>を参照してください。</li>
   </ul>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong> Debian x LibreOffice ( 野島 ) </strong><br>
	Debian ユーザから見た LibreOffice 利用について語ってみます </li>
  <li><strong>もくもく会 (全員)</strong><br>
	Debian / LibreOffice に関する作業を各自で行います</li>
  <li><strong> Jessie インストーラテスト会( 開発初心者の希望者のみ )</strong><br>
	別室で次期 Debian の安定版である Jessie インストーラのテストをします </li>
  <li><strong>成果発表 (全員)</strong><br>
	行った作業について簡単に発表いただきます</li>
  </ol>
</dd>

<dt>事前課題／参加申し込み方法</dt>
<dd>
Debian 側から参加を申し込むという方は <a href="https://tokyodebian-team.pages.debian.net/2014-10-25.html">勉強会のページ</a>を参照して、Debian 勉強会予約システムへの登録をしてください。</dd>
<dd>
<br>
事前課題は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：野島 貴英 (nozzy@{debian.or.jp} ) までお願いいたします。</p>
