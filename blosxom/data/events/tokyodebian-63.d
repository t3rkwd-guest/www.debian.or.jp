第63回 東京エリアDebian勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今月も Debian 勉強会が開かれます！
今回の開催場所は西東京にある、<a href="http://www.city.inagi.tokyo.jp/">稲城市</a>での開催になります。
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
   <li>2010年4月17日（土）13:00~17:00<br>
会場：<a href="http://www.iplaza.inagi.tokyo.jp/map/index.html">稲城市立iプラザ 中会議室 </a><br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=%E7%A8%B2%E5%9F%8E%E5%B8%82%E7%AB%8Bi%E3%83%97%E3%83%A9%E3%82%B6&amp;oe=utf-8&amp;client=iceweasel-a&amp;ie=UTF8&amp;hl=ja&amp;hq=%E7%A8%B2%E5%9F%8E%E5%B8%82%E7%AB%8Bi%E3%83%97%E3%83%A9%E3%82%B6&amp;hnear=&amp;ll=35.621094,139.472165&amp;spn=0.008372,0.013733&amp;z=16&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?q=%E7%A8%B2%E5%9F%8E%E5%B8%82%E7%AB%8Bi%E3%83%97%E3%83%A9%E3%82%B6&amp;oe=utf-8&amp;client=iceweasel-a&amp;ie=UTF8&amp;hl=ja&amp;hq=%E7%A8%B2%E5%9F%8E%E5%B8%82%E7%AB%8Bi%E3%83%97%E3%83%A9%E3%82%B6&amp;hnear=&amp;ll=35.621094,139.472165&amp;spn=0.008372,0.013733&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>
   </li>
   <li>費用：500円 </li>
</ul>
</dd>

<dt>内容</dt>
<dd>
<ul>
<li>Upstart 再入門 (担当: 前田 耕平)<br>
次期リリースでinitがUpstartに変わります。予習のために再入門してみましょう。
</li>

<li>
piuparts ってなによ (担当: 岩松 信洋)<br>
次期リリースゴールの一つである piuparts-clean。これの中心にある piuparts について説明します。
</li>

<li>
もっとパッケージが探しやすくなる~ debtags 入門 (担当: やまねひでき)<br>
様々なソフトが手軽に導入できることが Debian の魅力の一つです。しかし、現在 Debian には 20000 個を越えるパッケージが存在しています。さて、どうやって大量のパッケージから目当てのソフトを探せばいいのでしょう？そんな悩みを改善してくれる debtags の紹介と簡単な使い方を案内します。
</li>

</ul>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
<ul>
<li>勉強会に参加する方は、4/15 日 24 時までに <a href="http://debianmeeting.appspot.com/event?eventid=0013e9df2ac265e769263ef259c1e0c9db79bd42">Debian 勉強会受付システム</a>にて事前登録を行ってください。</li>
<li>GPGキーサインしたい人：フィンガープリントを印刷したもの（名刺などが望ましい）と、公的な証明書（パスポート、運転免許証）をご用意ください。</li>
</ul>
</dd>

</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：岩松 (iwamatsu&#64;{debian.org} )
までお願いいたします。</p>
