第17回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080927">第17回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 9 月 27 日 (土) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 会議室</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第17回関西 Debian 勉強会の内容について話をします。</li>

  <li><strong>Debian Live に Ubiquity を移植できるか？</strong>　山下 尊也 氏<br>
Ubuntu Desktop CDには、Live CD でブートしてからインストールを行う事が出来る Ubiquityと言うパッケージが含まれています． 現在のところ、Debianには Ubiquity のようなパッケージはありません。そのため Debian Liveに移植できないかを調べてみましたので、Ubiquityが何を行っているのかを踏まえて説明します。</li>

 
  <li><strong>Debian Mentors の紹介</strong>　倉敷 悟 氏<br>
Debian Project が提供している、開発者の育成を目的とした一連のシステムについて簡単にご紹介します。 これらを活用することで、新米開発者はドキュメントに記述されていないパッケージ作成の機微について議論したり、不明な点について質疑応答したり、最低限の自動テストをクリアした自作パッケージをあずかってもらったり、することができます。 </li>

  <li><strong>10分でわかるDebianフリーソフトウェアガイドライン(DFSG)</strong>　木下 達也 氏<br>
ある著作物が「フリー」かどうか、フリー(自由な)OSであるDebianの構成要素として適しているかどうかを判定する際の基準、それが「Debianフリーソフト ウェアガイドライン(DFSG)」です。その内容について簡潔に説明します。 </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>9 月 25 日 (木) 24:00 までに</strong>
<a href="http://cotocoto.jp/event/28510">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


