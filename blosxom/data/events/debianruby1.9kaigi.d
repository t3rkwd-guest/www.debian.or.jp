Debian Ruby1.9会議のお知らせ

<p>
Debianにおける主要なRubyパッケージ開発者の一人であるやまだあきらさんの Blog にて、
<a href="http://arika.org/diary/2009/06/04/DebianRuby19Kaigi">Debian Ruby1.9会議のお知らせ</a>
が告知されています。今後の Debian での Ruby 関連パッケージをどのように改善していくか、
強い興味や改善案をお持ちの方はぜひご参加ください。</p>
<dl>
<dt>内容<dt>
<dd>
<p>
Debian(sid)におけるRuby 1.9系パッケージについて以下のことを話し合います。</p>
<ul>
  <li>現状の確認
  <li>課題の整理
  <li>解決方法の検討
  <li>スケジュールの検討
</ul></dd>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2009/07/05 13:00</li>
  <li>場所：川崎〜横浜　の間のどこかを予定</li>
　<li>定員：10名（予定）</li>
  </ul>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加には<a href="http://atnd.org/events/797">ATEND</a>を参照して事前登録をしてください
</dd>
</dl>


