第81回 東京エリア Debian 勉強会＠筑波大学のお知らせ

<p>
Debian 勉強会とは、Debian の開発者になれることをひそかに夢見るユーザたちと、ある時にはそれを優しく手助けをし、またある時
には厳しく叱咤激励する Debian 開発者らが Face to Face で Debian のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです (Debian をまだ使ったことが無いが興味があるの
で…とか、かなり遠い所から来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。興味と時間があった方、是非御参加下さい。（また、勉強会で話をしてみたい
という方も随時募集しています）。</p>

<p>
今回は都心を離れ、<a href="http://www.tsukuba-linux.org">筑波大学の Linux ユーザグループ「つくらぐ」</a>さんと合同で、
筑波大学さんをお借りしての開催となります。まだ詳細が固まっておりませんが、先に日程の告知をさせていただきます。
つくば近郊の方、あるいはつくばエクスプレスに乗ってフラッと来てみたいという方などなど、お待ちしております。</p>

<dl>
<dt>開催日時・参加費用</dt>
<dd>
<ul>
   <li>2010年10月22日（土）13:00~18:00</li>
   <li>費用：無料</li>
</ul>

<dt>会場</dt>
<dd>
筑波大学 第3エリア B棟 302室 [3B302] (つくばエクスプレス「つくば」駅よりバス10分) <br>
(<a href="http://www.tsukuba-linux.org/wiki/%E7%AD%91%E6%B3%A2%E5%A4%A7%E5%AD%A6%E3%81%B8%E3%81%AE%E3%82%A2%E3%82%AF%E3
%82%BB%E3%82%B9">つくらぐさんによる会場までのアクセス案内</a>)</dd>
<dd>
<iframe width="600" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=36.111184,140.100236&amp;ie=UTF8&amp;start=0&amp;ll=36.10522,140.101776&amp;spn=0.033286,0.051413&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=36.111184,140.100236&amp;ie=UTF8&amp;start=0&amp;ll=36.10522,140.101776&amp;spn=0.033286,0.051413&amp;z=14" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>
</dd>


<dt>内容</dt>
<dd>
現在、鋭意調整中です。
</dd>

<p>
この件に関するお問い合わせは Debian 勉強会：前田 (mkouhei@{debian.or.jp} ) までお願いいたします。</p>

