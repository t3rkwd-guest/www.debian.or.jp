第 2 回 Debian JP Project Bug Squash Party のおしらせ

<p>
第 2 回 Debian JP Project Bug Squash Party（以下、BSP) を開催します。<br>

<a href="http://wiki.debian.org/BSP">BSP</a> とはバグつぶしパーティのことです。
Debian の場合ですと、安定版 (stable) のリリース前や定期的に行われており、リリースクリティカルバグ (RC) 
を減らしたり問題のあるパッケージを集中的に直します。
また、各々がメンテナンスしているパッケージのメンテナンスや抱えている問題を参加者で議論し、
解決する事も行います。
</p>
<p>
前回行われた Debian JP Project による成果は <a
href="http://wiki.debian.org/BSP/DebianJP">http://wiki.debian.org/BSP/DebianJP<a>、
他国で開催される／された BSP に関しては、<a
href="http://debian.wiki.org/BSP">http://wiki.debian.org/BSP</a>
からリンクを参考にしてください。</p>

<h5>開催内容</h5>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
 <li>日時：2012年11月24日(土) 9:30-18:00</li>
 <li>場所 : <a href="http://www.plathome.co.jp/">ぷらっとホーム株式会社</a>　会議室<br>
最寄り駅：市ヶ谷／九段下<br>
東京都千代田区九段北4-1-3 日本ビルディング九段別館</li>
 <li>IRC : #debianjp at irc.debian.or.jp</li>
 <li>Wiki : <a href="http://wiki.debian.org/BSP/2012/11/jp/Tokyo">http://wiki.debian.org/BSP/2012/11/jp/Tokyo</a><br>
           前回の BSP : <a href=" http://wiki.debian.org/BSP/DebianJP_01">http://wiki.debian.org/BSP/DebianJP_01</a>
</ul>
</dd>
<dt>参加について</dt>
<dd>
<ul>
 <li>参加費 :  無料
 <li><strong>参加希望者は <a href="http://wiki.debian.org/BSP/2012/11/jp/Tokyo">Debian Wiki のページ</a> へ記入ください</strong><br>
 <li>(<a href="http://www.debian.or.jp/community/irc.html">IRC による参加</a>も可能です)。</li>
</dd>
<dt>内容</dt>
<dd>
<ol>
 <li>参加者による交流</li>
 <li>Debian Package のバグを修正（特に <a href="http://udd.debian.org/bugs.cgi?release=wheezy_and_sid&patch=ign&pending=ign&wontfix=ign&unreproducible=ign&claimed=ign&deferred=ign&notwheezy=ign&merged=ign&done=ign&fnewerval=7&rc=1&sortby=id&sorto=asc">RC バグ</a>)</li>
 <li>Debian Package メンテナンス</li>
 <li>po-debconf 等の翻訳など</li>
 <li>ぷらっとホーム <a href="http://openblocks.plathome.co.jp/products/">OpenBlocks AX3/A6</a>(armel/armhfアーキテクチャ）の実機ハックなど</li>
</ol>
</dd>
<dt>その他</dt>
<dd>
<ul>
 <li>BSP 用のパソコン、周辺機器等は各々で用意してください。</li>
 <li>OpenBlocks AX3/A6は、当日会場に実機を準備しますので、興味のある方は自由に触っていただけます。</li>
 <li>ネットワークについては調整中です。</li>
<!-- 
 <li>ネットワークは使用可能です。</li>
 <li>会場のビデオ録画とストリーミング中継を計画しております。ご協力をお願いします。<br>
 （参加者の方で不都合がある方は対応いたします。ご連絡ください。）</li>
-->
</ul>
</dd>
</dl>

<p>
末尾になりますが、会場提供の<a href="http://www.plathome.co.jp/">ぷらっとホーム株式会社</a>樣に御礼申し上げます。<br>
本イベントにつきまして、何かございましたら担当：やまねひでき <a href="mailto:henrich@debain.or.jp">&lt;henrich@debian.or.jp&gt;</a> までご連絡ください（また、今後の開催についてご協力いただける方も募集しております）。<br>
多数の参加をお待ちしております。
</p>
