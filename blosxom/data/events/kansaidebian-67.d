第67回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20121223">第67回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 12 月 23 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      関西Debian勉強会で今年印象に残った話と来年聞きたい話を教えてください。
      </li>

      <li>
      DebianPolicy の<a href="http://www.debian.org/doc/debian-policy/ch-maintainerscripts.html">6章</a>に目を通しておいてください。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「Android 端末（Asus Transformer TF201）への Debian インストール奮闘記」</strong>(担当：cuzic)<br>
     いままでまったく Debian を使ったことがない発表者が Android と Debian の両方の環境を同時稼働させるためにした試行錯誤の作業をそのまま紹介します。
  </li>

  <li><strong>月刊 Debian Policy</strong>(担当：かわだ)<br>
  「パッケージ管理スクリプトとインストールの手順」
  </li>


  <li><strong>「2012年の振り返りと2013年の企画」</strong>(担当：Debian JP)<br>
  年初に表明頂いた目標の達成状況を簡単に確認した後、来年の勉強会についてディスカッションします。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 12 月 21 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=4864ebac1a513d3996c92cfc6d5f7e0dc6abaf41">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
