第 41 回 関西 Debian 勉強会 in 関西オープンソース2010

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20101105-1106">第41回 関西 Debian 勉強会@KOF2010
 </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまな
トピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：11月6日(土) 10:00〜18:00
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="http://k-of.jp/2010/access/">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ミニセミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt><a href="http://www.k-of.jp/2010/list_seminar.html#21">セミナー<br>
<strong>「でびあん らんだむとぴっくす 〜 Squeezeマダァー?（・∀・ ）っ/凵⌒☆チンチン」</strong><br>
（会場:9F-セミナールーム2　11月6日(土) 13:00-13:50)</a></dt>
<dd>
<ul>
<li>
Debian 次期安定版 Squeeze についての開発状況や、ここ最近の Debian 界隈の話題などをゆるーくお話します。
</li>
<li>
担当：佐々木洋平(Debian JP Project)</li>
</ul>
</dd>

<dt><a href="http://www.k-of.jp/2010/list_stage.html#46">セミナー<br>
<strong>「GPG キーサインパーティ」</strong><br>
 (会場:9F-セミナールーム3　11月6日(土) 17:00-17:50)</a></dt>
<dd>
OSS開発者、ユーザ共に必要なGPGの説明と、GPGキーサインパーティを行ないます。キーサインに参加する人は、事前に下記URLを参照し、登録をお願いします (グループキーサイン形式で行ないますので事前登録をよろしくお願いします)。
<br>
http://www.math.kyoto-u.ac.jp/~uwabami/ksp-kof2010/
<br>
 キーサインパーティに出席できない人も、関西Debian勉強会ブースにてハッシュ値の照合を行なえますので, 是非ご登録ください。
</dd>

<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 稼働マシン展示と Debian 体験コーナー</strong>
  <ul>
    <li>ノートPCをお持ちください。ネットワークブートであなたのPCでDebianが起動します!</li></ul>
  </li>
  <li><strong>有志作成 Debian グッズ（Tシャツ）</strong></li>
  <li><strong>Debian 同人誌「あんどきゅめんてっどでびあん」</strong></li>
  <li><strong>Debian Live DVD 配布</strong></li>
  </ol>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
