第30回 Debian 勉強会のお知らせ

<p>
今月も Debian 勉強会が開かれます！ Debian 勉強会とは、Face to Face で
Debian GNU/Linux のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について話し合うイベ
ントです。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザで
す。(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所か
ら来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者
予備軍の方も居ますので、普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい♪
（また、勉強会で話をしてみたいという方も随時募集しています）。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年7月21日(土) 18:00-21:00
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)
  <li>費用：資料代・会場費として 500 円を予定</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debconf7 報告会</strong>（担当：岩松 信洋）<br>
	
	スコットランド・エジンバラで開催された世界中から
	Debian 開発者が集まるカンファレンス、 Debconf7 に
	参加・発表をしてきた岩松さんと上川さんが現地の様子や
	Debconfの模様、発表の感想などを熱く語ります。

  <li><strong>Debian Conference 日本開催実現！についてディスカッション</strong>（担当：上川 純一）<br>
	
	来年はアルゼンチンでDebconfが行われます。南米に負けてられないと
	日本でDebconfを行う為にクリアしないといけない課題や進捗など
	広くグループディスカッションします。
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。
期限は 7 月 19 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-07.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
