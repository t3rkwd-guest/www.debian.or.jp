第2回 関西 Debian 勉強会のお知らせ

<p>
前回に引き続き、関西方面でも勉強会を！という声に応えるのと、Debian GNU/Linux 4.0 ("Etch")のリリースを記念して、
「<a href="http://wiki.debian.org/KansaiDebianMeeting20070421">第2回 関西 Debian 勉強会</a>」
が開かれることになりました。関西 Debian 勉強会は、定期的に顔をつき合わせて、
Debian GNU/Linux のさまざまなトピック (新しいパッケージ、Debian 特有の機能の仕組み、Debian 界隈で起こった出来事、などなど) について話し合っていく会です。</p>

<p>参加者には開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないようなさまざまな情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年4月21日(土) 13:00-17:00 (受付は12:30以降、終了後に懇親会も別途予定)</li>
  <li>会場：<a href="http://www.olnr.org/%7Ehatanaka/location.html">大阪電気通信大学 (四條畷) 11-403号室</a></li>
  <li>費用：無償</li>
  <li>申し込みシステムは、今回 cotocoto.jp に変更になっています。宴会の申し込み方法は、<a href="http://wiki.debian.org/KansaiDebianMeeting20070421">第2回 関西 Debian 勉強会</a>のページにあります。</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>etchリリース記念インストールとか</strong> たかや氏 <br>
    <ul>
      <li>http://goodbye-microsoft.com/ を使ってWindowsからインストール。</li>
      <li>laptop向けの事　(cpufreqとかsuspend、sleep関係の事) </li>
    </ul>
  <li><strong>Debian GNU/LinuxからのYouTube活用ノウハウ　(マルチメディア関係の事)</strong>のがたじゅん氏<br>
  <li><strong>20分で作る Debian パッケージ</strong> 大浦氏 <br>
一般的な Debian パッケージの作り方について</li>
  <li><strong>プロジェクトトラッカーの話</strong>　矢吹氏 <br>
    <ul>
      <li>あることに費している時間を計測するには、どうしたらいいか。</li>
      <li>自分の作業時間を見積もるにはどうしたらいいか?</li>
      <li>記録を取って、基準を作ろう</li>
    </ul>
  </li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前のまとめを作るための情報を送付してください。期限は 4 月 15 日 (日) です。<br>
<strong>事前登録、及び事前送付情報の内容と送付先については
<a href="http://wiki.debian.org/KansaiDebianMeeting20070421">第2回 関西 Debian 勉強会
</a>のページを参照してください。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会主催者：矢吹幸治 (yabuki&#64;{debian.or.jp,netfort.gr.jp} ) までお願いいたします。</p>
