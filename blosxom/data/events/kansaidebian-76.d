第76回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130922">第76回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 9 月 22 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区区民センター 梅</a>
(定員：15名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      サウンドデバイスを持つPCあるいはARMボードにDebian GNU/Linux Wheezyをインストールし、パッケージ「alsa-base」、「libasound2」、「libasound2-data」、「libasound2-plugins」、「alsa-utils」をインストールしておいてください。
      </li>

      <li>
      「dgit」を動かせる環境を用意しておいてください。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「Linuxとサウンドシステム」</strong> (担当：坂本)<br>
    DebianなどのLinuxディストリビューション及びAndroidを対象として、音声を扱うハードウェアをシステムがどのように駆動してアプリケーションが入出力を行うかを概説します。
  </li>

  <li><strong>「dgit でソースパッケージを触ってみる」</strong> (担当：倉敷)<br>
    最近、新しいパッケージングツールとして dgit というものが登場しました。 日々更新が続いていて、まだ安定しているわけではないですが、どんなものかちょっと触ってみたので、簡単にご紹介します。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<strong>2013 年 9 月 21 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=0aa7cc9cb437bb7c4ca403d08789ceebafb96850">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください。)
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。
</p>
