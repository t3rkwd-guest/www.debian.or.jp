第49回 Debian 勉強会 in オープンソースカンファレンス 2009 Tokyo/Spring

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この 2 月は、
<a href="http://www.ospn.jp/osc2009-spring/">オープンソースカンファレンス 2009 Tokyo/Spring</a>
に参加してセミナーやブースでの展示と説明、そして「Debian パッケージ作成ハンズオン」などを予定しています。
普段は聞けないような様々な情報を交換しあうチャンスです。是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2009年2月21日(土)<br>
  <li>会場：<a href="http://www.jec.ac.jp/college/access.html">日本電子専門学校 7号館</a>　(JR大久保駅徒歩２分・新宿区北新宿1-4-2)
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;s=AARTsJqanBPci4fyHco-uVwHtDeqI3LI4Q&amp;msa=0&amp;msid=109811903404931563048.0004461414af51bab22e4&amp;ll=35.700279,139.696913&amp;spn=0.016728,0.027466&amp;z=15&amp;iwloc=000446142bdc7d09867d3&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;msa=0&amp;msid=109811903404931563048.0004461414af51bab22e4&amp;ll=35.700279,139.696913&amp;spn=0.016728,0.027466&amp;z=15&amp;iwloc=000446142bdc7d09867d3&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small></li>
  <li>費用：無料（セミナー）<br>
      　　　その他有志による物品の販売を予定</ul></dd>

<dt><a href="http://www.ospn.jp/osc2009-spring/modules/eguide/event.php?eid=13"><strong>Debian パッケージ作成ハンズオン トライ アゲイン（2コマ連続</strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：東京エリア Debian勉強会</dd>
<dd>対象：Debian パッケージを作ってみたい人。将来、Debian Developerになりたい人、Debian/Ubuntu に貢献したい人
<dd>開始：13:00〜</dd>
<dd>会場：8階ハンズオン教室</dd>
<dd>
<p>
「どうやってパッケージって作るの?」「試してはみたけど、うまくパッケージが作れない…」と嘆いていたあなたも、これに参加すれば公式 Debian パッケージメンテナへの道が開かれる…かもしれない?<br>
基本的な Debian パッケージの作り方を学べる Debian 公式パッケージメンテナによるパッケージ作成ハンズオンです。</p>
<p>
</p></dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」</li>
  <li>フライヤー・Live DVD (OSC2009 春 SP)(配布も予定)</li>
  <li>Debian 稼働マシン (Live DVD デモ機、Chumby予定)</li>
  <li>有志作成の Debian グッズ (Tシャツ他 予定)</li></ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
セミナー／ハンズオン参加希望者は<a href="http://www.ospn.jp/osc2009-spring/">事前登録</a>してください。<br>
ブース展示については特に登録などはありません。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会 担当者：山本 浩之 (yamamoto&#64;debian.or.jp)
までお願いいたします。</p>
