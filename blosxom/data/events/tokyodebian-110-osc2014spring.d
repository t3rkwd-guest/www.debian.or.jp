第110回 東京エリア Debian 勉強会 in オープンソースカンファレンス 2014 Tokyo/Spring

<p>
<a href="https://tokyodebian-team.pages.debian.net/2014-03.html">「第 110 回 東京エリア Debian 勉強会 in オープンソースカンファレンス 2014 Tokyo/Spring」</a>
のお知らせです。</p>
<p> 東京エリア Debian 勉強会とは、Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまなトピックについて 東京方面にて集まって語り合うイベントです。</p>
<p> 3 月は、
<a href="http://www.ospn.jp/osc2014-spring/">オープンソースカンファレンス 2014 Tokyo/Spring</a>
に参加を予定しています。 毎回の事ですが様々な Debian ユーザーが集まります。普段は聞けないような様々な情報を交換しあうチャンスです。この機会にぜひお越しください。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2014 年 3 月 1 日 ( 土 ) <br>
  <li>会場：<a href="http://www.ospn.jp/osc2014-spring/">オープンソースカンファレンス 2014 Tokyo/Spring 会場内</a></li>
  <li>参加費用：無料<br></ul></dd>
<dt>セミナー発表</dt>
<dd>タイトル：<a href="https://www.ospn.jp/osc2014-spring/modules/eguide/event.php?eid=40"><strong>Debian Update &amp; Debianの EFI/UEFI 対応について</strong></a></dd>
<dd>内容：Debian 界隈で起こった最新情報の紹介と、最近の PC にデフォルトで搭載されつつある EFI &amp; UEFI の Debian 対応状況、インストール方法を紹介します。
<dd>講師：岩松 信洋 ( 東京エリア Debian 勉強会 )</dd>
<dd>対象：Debian ユーザ、 Debian 開発者、Debian の情報収集を目的としている人</dd>
<dd>開始：12 時 00 分〜12 時 45 分</dd>
</dd>

<p>また、オープンソースカンファレンス 2014 Tokyo/Spring は 2 月 28 日 ( 金 )、3 月 1 日( 土 ）
の ２ 日間行われますが、東京エリア Debian 勉強会は 3 月 1 日 ( 土 ) のみの展示を行います。お越しいただく際は日程にご注意ください。</p>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li> Wheezy PC の展示 </li>
  <li> あんどきゅめんてっどでびあんの展示/頒布</li>
  <li> 東京エリア / 関西 Debian 勉強会の紹介</li>
　<li> その他 ( ...計画中... ) </li>
</dd>

<p>この件に関するお問い合わせは 講師担当：岩松 ( iwamatsu at {debian.org})まで。</p>



