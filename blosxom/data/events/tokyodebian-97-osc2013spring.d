第97回 東京エリアDebian 勉強会 in オープンソースカンファレンス 2013 Tokyo/spring

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
 2 月は、
<a href="http://www.ospn.jp/osc2013-spring/">オープンソースカンファレンス 2013 Tokyo/spring</a>
に参加を予定しています。 毎回の事ですが様々なDebianユーザーが集まります。普段は聞けないような様々な情報を交換しあうチャンスです。この機会にぜひお越し下さい。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2013年2月23日(土)<br>
  <li>会場：<a href="http://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 28号館302教室</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul></dd>
<dt>セミナー発表</dt>
<dd>タイトル：<a href="https://www.ospn.jp/osc2013-spring/modules/eguide/event.php?eid=57"><strong>Debian Update</strong></a></dd>
<dd>内容：Debianの最新動向について語ります
<dd>講師：野島 貴英 (Debian JP Project)　担当：東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：10時00分〜10時45分</dd>
</dd>

<p>また、オープンソースカンファレンス 2013 Tokyo/springは22日(金)、23日(土）
の２日間行われますが、Debian JP Projectは23日(土)のみの展示を行います。
お越し頂く際は日程にご注意ください。</p>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li> Wheezy PCの展示 </li>
  <li> あんどきゅめんてっどでびあんの展示/頒布</li>
  <li> Debian のインフォグラフィック日本語版の配布</li>
  <li> Debianステッカーの配布</li>
  <li> Debian インストール CDの配布</li>
  <li> 東京エリア/関西Debian勉強会の紹介</li>
</dd>

<p>この件に関するお問い合わせは ブース担当：吉田(koedoyoshida at
{gmail.com})、もしくは、講師担当：野島(nozzy at {debian.or.jp})まで。</p>



