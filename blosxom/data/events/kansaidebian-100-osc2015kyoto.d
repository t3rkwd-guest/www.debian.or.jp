第 100 回 関西 Debian 勉強会 in OSC2015 Kansai@Kyoto のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20150808">第100回 関西 Debian 勉強会 in OSC2015 Kansai@Kyoto</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>

<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2015 年 8 月 8 日 (土) 10:00 - 17:50 (展示 10:00 - 16:00)</li>
    <li>会場：<a href="http://www.krp.co.jp/index.html">京都リサーチパーク(KRP)</a> OSC総合受付：アトリウム</li>
    <li>費用：無料</li>
  </ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
  <ul>
    <li>Debian 8 "Jessie" 稼働マシンと勉強会資料の展示</li>
    <li>Debian インストーラDVD・ステッカー・クリアファイルの配布など</li>
  </ul>
</dd>

<dt> セッション</dt>
<dd>
  <ul>
    <li><a href="https://www.ospn.jp/osc2015-kyoto/modules/eguide/event.php?eid=26">
	    <strong>Debian Updates (Jessie, Stretch, Buster)</strong></a>
		(担当: 佐々木 洋平)</li>
    <li>日時 2015 年 8 月 8 日 (土) 11:00〜11:45 (45 min)</li>
    <li>場所: 1号館4F 会議室E</li>
    <li>内容: リリースされた Debian8 "Jessie" の特徴や今後のリリース（Debian9 "Stretch", Debian10 "Buster"）の話題を中心に、最近のDebian Projectのトピックを紹介します。</li>
  </ul>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
ブース、セッションともに入場無料です。
セッションについては、<a href="https://www.ospn.jp/osc2015-kyoto/modules/eventrsv/register2.php">オープンソースカンファレンスのページ</a>よりお申し込みください。
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
