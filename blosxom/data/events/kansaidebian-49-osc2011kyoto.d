第49回 関西 Debian 勉強会 @OSC2011 Kyoto のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20110716">第49回 関西 Debian 勉強会@OSC2011 Kyoto </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。今月は、オープンソースカンファレンス2011 Kansai@Kyotoに参加します。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：日時：2011 年 7 月 16 日 (土) 10:00〜18:00 </li>

  <li>会場：<a href="http://www.krp.co.jp/access/index.html">京都リサーチパーク(KRP) 東地区1号館 4F</a></li>
  <li>費用：無料</li>
  </ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
  <ul>
  <li>Debian 稼働マシンと勉強会資料の展示</li>
  <li>Debian 同人誌の販売</li>
  <li>Debian Tシャツの販売(予定)</li>
</dd>
<dt> セッション</dt>
<dd>
  <ul>
    <li><strong>Debian: Squeeze, Wheezy, and Sid</strong>(担当: 佐々木 洋平(Debian JP Project)</li>
    <li>日時 2011 年 7 月 16 日 (土) 10:00〜10:45 (45 min)</li>
    <li>場所: 小会議室E</li>
    <li>内容: 今年 2 月にリリースされた Debian 6.0 "Squeeze" の概要， 次期リリース Wheezy に向けた開発状況， そして Sid での生活方法についてお話します</li>
    <li><strong></strong>(担当: 佐々木 洋平(Debian JP Project)</li>
    <li>日時 2011 年 7 月 16 日 (土) 12:00~12:15 (15 min)</li>
    <li>場所: 小会議室E</li>
    <li>内容: キーサインパーティーは、互いの鍵に署名をすべく、PGP(GPG)鍵を持つ人々が集まるものです。キーサインパーティーはWeb of Trustを大規模に拡張するのに寄与します。時間は10分ほどです。<br>
        注意: なお、参加される方は<strong><a href="https://sites.google.com/site/kspjapanese/osc2011-kyoto/">必要な事前準備の上</a></strong>でご参加ください。</li>
  </ul>
</dd>

</dd>
<dt>参加方法と注意事項</dt>
<dd>
ブース、セッションともに入場無料です。
セッションについては、<a href="https://www.ospn.jp/osc2011-kyoto/modules/eventrsv/?id=2&noform=1">オープンソースカンファレンスのページ</a>よりお申し込みください。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
