Debian JP Project の公式 Web ページをリニューアル

<p>
Debian JP Project の公式 Web ページ (<a href="http://www.debian.or.jp/">http://www.debian.or.jp/</a>) のデザインを一新し、情報の検索容易性と、管理の向上を図りました。Debian JP Project は、今後とも、日本国内における Debian に関する情報の交換と発表の場を提供し続けます。
</p><p>
Debian JP Project の Web ページに関するご感想・ご質問は webadmin&#64;debian.or.jp  までお送りください。
</p>
