#!/usr/bin/perl -w
#  Make head/foot file from www.debian.or.jp
use strict;

my($dest) = "";
my($ref) = "/blog/dummy.html";
my($flag) = 0;

open(F, "../../../ttreerc") || die "Can't open ttreerc: $!\n";
while (<F>) {
    if (/^dest = (.*)/) {
	$dest = $1 . $ref;
	last;
    }
}
close(F);
open(HEAD, ">head.html") || die "Can't create head.html: $!\n";
open(FOOT, ">foot.html") || die "Can't create foot.html: $!\n";

open(F, "$dest") || die "Can't open $dest: $!\n";
while (<F>) {
    if (/^\-\- BEGIN ARTICLE \-\-/) {
	$flag = 1;
	next;
    }
    if (/^\-\- END ARTICLE \-\-/) {
	$flag = 2;
	next;
    }

    if ($flag == 0) {
	s/\"\.\./\"\//g;
	s/=\"\/\//=\"\//g;
	print HEAD;
    } elsif ($flag == 2) {
	s/\"\.\./\"\//g;
	s/=\"\/\//=\"\//g;
	print FOOT;
    }
}
close(F);
print HEAD "<span>\$back_and_forth::prev \$back_and_forth::main \$back_and_forth::next</span>\n";
close(HEAD);
close(FOOT);
