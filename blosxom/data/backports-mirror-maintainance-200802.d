Backports ミラーサーバメンテナンスのご連絡 (2/10)

<p>
Debian JP Project メンバーが提供する <a href="http://www.jp.backports.org/">Debian Backports 公式ミラーサイト</a>は、収容先ビルの電源法定点検のため、下記の日程でサービスを一時停止します (別サーバ名として利用できる http://backports.mithril-linux.org も同じサーバを指していますので同様となります)。
</p>
<ul>
<li>日時：2008/02/10　06:00〜18:00 （時間は前後することがあります）
<li>対象：<a href="http://www.jp.backports.org/">Debian Backports 公式ミラーサイト www.jp.backports.org</a> (及びその別名 backports.mithril-linux.org）
<li>内容：停電のため、サーバ停止
</ul>
<p>
上記日程の前後で利用できません（apt line に指定している場合、サーバが反応しないなどのエラーが表示されます）ので、ご留意下さい。</p>
