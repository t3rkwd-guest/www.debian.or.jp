ミラー遅延について（2/18 復旧済み）

<p>
Debian JP Project が提供しているミラーサーバ (ftp.jp.debian.org/cdn.debian.or.jp) について、
2/14 より上流での遅延が発生しており更新パッケージが伝播されておりません。</p>
<p>
現状問い合わせを行っていますが、上流での回復まで今しばらくお待ち下さい。</p>
<p>
（追記：2/18 朝6時現在、復旧した事を確認しました。
原因は上流機器でのカーネルアップデート時における設定の漏れであり、再発は無いとのことです）</p>
